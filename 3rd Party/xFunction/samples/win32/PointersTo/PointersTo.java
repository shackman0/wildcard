import com.excelsior.xFunction.*;


public class PointersTo {

    public static void main(String[] args) throws Exception{

        xFunction  foo = new xFunction("PointersTo","int* _foo@8(int*,Point*)");
        Pointer   arg1 = Pointer.create("int*  ");
        Point        p = new Point(); p.x=10; p.y=20;
        Pointer   arg2 = Pointer.createPointerTo(p);
        Pointer result = (Pointer)(foo.invoke(arg1,arg2));
        if(((Integer)arg1.deref()).intValue()!=2) throw new Error("*a!=2");
        p = (Point)arg2.deref();
    
        if (p.x!=1) throw new Error("x!=1");
        if (p.y!=20) throw new Error("y!=20");

        foo = new xFunction("PointersTo","CSTRING _foo2@16  (int*,int**,int*,int*)");
        int[] arr1 = new int[20];
        arr1[10] = 10; arr1[11] = 11;

        arg1 = (Pointer)Argument.create("int*",arr1);
        arg2 = Pointer.create("int**");
        Argument val = new Argument(13);
        Pointer arg3 = val.createPointer();

        int[][][] arr4 = new int[][][]{{{1,2},{3,4}}};
        Pointer   arg4 = (Pointer)Argument.create("int*",arr4);

        
        String str = (String)foo.invoke(arg1,arg2,arg3,arg4);

        arr1 = (int[])arg1.createArray(20);



        if ((arr1[10]!=10)||(arr1[12]!=21))
            throw new Error();

        int dim = ((Integer)arg3.deref()).intValue();

        int[] arr2 = (int[])((Pointer)arg2.deref()).createArray(dim);
    
        if(arr2.length!=4) throw new Error();

        for(int i=0;i<dim;i++)
            if(arr2[i]!=i)
                throw new Error();

        if(!str.equals("Some string"))
            throw new Error();
    
        int[][][] arr5=(int[][][])arg4.createArray(new int[]{1,2,2});

        if((arr5[0][0][0]!=1)||(arr5[0][0][1]!=2)||(arr5[0][1][0]!=3)||
                (arr5[0][1][1]!=4))
            throw new Error();
    
    }
}



class Point extends Structure{

    int y,z,x;

    public String defineLayout(){
        return "int x, int y";
    }
}