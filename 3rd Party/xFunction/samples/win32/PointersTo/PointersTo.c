#include <stdio.h>
#include <malloc.h>


struct Point{int x,y;};


__declspec(dllexport) int* __stdcall foo(int* a,struct Point * b){
    int* arr=(int*)malloc(100);
    printf("Function foo get struct argument = %d,%d\n",b->x,b->y);
    b->x=1;
    *a=2;
    return arr;
} 
    
__declspec(dllexport) char* __stdcall foo2(int* a,int** b,int *dim, int* ma ){

    int* arr=(int*)malloc(4*sizeof(int));

    printf("Multiarray: ={{{%d,%d},{%d,%d}}}\n", *ma,*(ma+1), *(ma+1*2+0), *(ma+1*2+1));

    a[12]=a[11]+a[10];
    a[13]=*dim;

    *dim=4;
    arr[0]=0; arr[1]=1; arr[2]=2; arr[3]=3;

    *b=arr;

    return "Some string";
}