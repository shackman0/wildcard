#pragma pack(1)
typedef struct{
    char a;
    double b;
#pragma pack(2)
    int c;
    char d;

} PACKED_STRUCT;


__declspec(dllexport) int __stdcall foo(PACKED_STRUCT my){
    int result = 1;

    result &= my.a == 'e';
    result &= my.b == 134.153;
    result &= my.c == 98761;
    result &= my.d == 'z';

    return result;
} 
