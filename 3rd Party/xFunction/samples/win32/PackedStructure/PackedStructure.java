import com.excelsior.xFunction.*;

public class PackedStructure {

    public static void main(String[] str) throws xFunctionException {

        xFunction foo = new xFunction("PackedStructure","int _foo@16(PACKED_STRUCT)");
    
        PACKED_STRUCT my_struct = new PACKED_STRUCT();

        my_struct.a = 'e';
        my_struct.b = 134.153;
        my_struct.c = 98761;
        my_struct.d = 'z';

        Integer foo_result = (Integer)foo.invoke(new Argument(my_struct));

        System.out.println(foo_result);

        if(foo_result.intValue()==0) {
            throw new Error();
        }

        System.out.println("OK");
    }
}



class PACKED_STRUCT extends Structure {

    char a;
    double b; 
    int c;
    char d;

    public String defineLayout() {
        return "#pack(1) char a, double b, #pack(2) int c, char d";
    }

    public PACKED_STRUCT() {}
}
