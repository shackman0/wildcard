
typedef struct{
    int arr1[3][7];
    int arr2[4][5];
} MY_STRUCT;


__declspec(dllexport) void* __stdcall foo(MY_STRUCT *my){

    int i, j;

    printf("sample10.dll: Printing arr1\n");

    for(i=0;i<3;i++){
        for(j=0;j<7;j++){
            printf("%d  ", my->arr1[i][j]);
            my->arr1[i][j] = 100+i*7+j;
        }
        printf("\n");
    }
    printf("\n");


    for(i=0;i<4;i++)
        for(j=0;j<5;j++)
            my->arr2[i][j] = 1000+i*5+j;

    return my->arr2;
} 
