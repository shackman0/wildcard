import com.excelsior.xFunction.*;


public class Arrays{

    public static void main(String[] str) throws xFunctionException{

        // creating xFunction object
        xFunction f=new xFunction("Arrays","int* _foo@4(MY_STRUCT*)");

        MY_STRUCT my = new MY_STRUCT();
    int i,j;

    for(i=0;i<3;i++)
        for(j=0;j<7;j++)
                my.arr1[i][j] = i+j;

        // invoking function
        // return value is a pointer to 2D array
        Pointer arg = Pointer.createPointerTo(my);
    Pointer retarg = (Pointer)f.invoke(arg);

    int dimensions[] = new int[2];
    dimensions[0] = 4;
    dimensions[1] = 5;

    my = (MY_STRUCT)arg.deref();

    // creating 2D array from pointer
    int retarr[][] = (int[][])retarg.createArray(dimensions);


    System.out.println("Printing 'retarr[][]' array");
    printArray(retarr);


    System.out.println("\nPrinting 'arr1[][]' array");
    printArray(my.arr1);

    System.out.println("\nOK");
    }


    public static void printArray(int[][] array){
        int i,j;

    for(i=0;i<array.length;i++){
        for(j=0;j<array[i].length;j++){
            System.out.print(array[i][j]+"  ");
        }
        System.out.println();
    }
    }
}



class MY_STRUCT extends Structure{

    int[][] arr1;
    int[][] arr2;

    public String defineLayout(){
        return "int[3][7] arr1, int[4][5] arr2";
    }

    public MY_STRUCT(){
        arr1 = new int[3][7];
        arr2 = new int[4][5];
    }
}


