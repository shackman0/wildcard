#include <malloc.h>


typedef struct _node {
    int value;
    struct _node* left;
    struct _node* right;
} node;


__declspec(dllexport) struct _node* __stdcall insert(struct _node* t, int a) {
    if (!t) {
        t = malloc(sizeof(node));
        t->value = a;
        t->left  = 0;
        t->right = 0;
    } else {
        if (t->value > a)
            t->left = insert(t->left, a);
        else
            t->right = insert(t->right, a);
    }
    return t;
}