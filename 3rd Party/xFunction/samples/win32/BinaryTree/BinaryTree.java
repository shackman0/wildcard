import com.excelsior.xFunction.*;

public class BinaryTree {
    public static void main(String[] args) throws Exception {
        xFunction f = new xFunction("BinaryTree", "Node* _insert@8(Node*,int)");
        Pointer arg = Pointer.NULL;

        Pointer res = (Pointer)f.invoke(arg, new Argument(1));
        res = (Pointer)f.invoke(res, new Argument(2));
        res = (Pointer)f.invoke(res, new Argument(-1));
        res = (Pointer)f.invoke(res, new Argument(0));
        res = (Pointer)f.invoke(res, new Argument(4));
        res = (Pointer)f.invoke(res, new Argument(-5));
        res = (Pointer)f.invoke(res, new Argument(7));

        Node t = (Node)res.deref();
        System.out.println(t.toStr());
    }
}

class Node extends Structure {
    int value;
    Pointer left;
    Pointer right;
    
    public String defineLayout() {
        return "int value, Node* left, Node* right";
    }

    public String toStr() throws NullDereferenceException {
        String res = (new Integer(value)).toString();
        if (!left.isNull()) {
            res = ((Node)left.deref()).toStr() + " " + res;
        }
        if (!right.isNull()) {
            res = res + " " + ((Node)right.deref()).toStr();
        }
        return res;
    }
}