import com.excelsior.xFunction.*;


public class CStrings{

    public static void main(String[] args) throws xFunctionException{

        xFunction foo = new xFunction("CStrings","void _func@8 (CSTRING*,CSTRING)");

        Pointer  arg1 = Pointer.create("CSTRING*");
        Argument arg2 = new Argument("A string", Argument.CSTRING);

        foo.invoke(arg1,arg2);

        String str1 = (String)(arg1.deref());
        String str2 = (String)(arg2.getValue());
        if(!str1.equals("Some string")) throw new Error();
        if(!str2.equals("a string")) throw new Error();

    }
}
