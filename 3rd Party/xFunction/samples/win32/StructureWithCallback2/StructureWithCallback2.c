
typedef void (__stdcall *PROC)();

typedef struct{
    char *str1;
    char *str2;
    PROC proc;
} MY_STRUCT;


__declspec(dllexport) void __stdcall call_callback(MY_STRUCT *my){

    MY_STRUCT my_struct;

    my->str1[2] = 'x';
    my->str2[4] = 'z';

    my_struct.str1 = "String 1";
    my_struct.str2 = "String 2";

    my->proc(&my_struct, "call_callback string");
} 
