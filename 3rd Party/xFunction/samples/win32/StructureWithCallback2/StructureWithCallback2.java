import com.excelsior.xFunction.*;


public class StructureWithCallback2{

    public static void main(String[] str) throws xFunctionException{

    xFunction f=new xFunction("StructureWithCallback2","void _call_callback@4(MY_STRUCT*)");

        MY_STRUCT my = new MY_STRUCT();
        PROC proc = new PROC();


        my.str1 = "A string";
        my.str2 = "The string";
        my.proc = proc;

        Pointer arg = Pointer.createPointerTo(my);

    f.invoke(arg);

        my = (MY_STRUCT)arg.deref();


        System.out.println(my.str1);
        System.out.println(my.str2);

    System.out.println("OK");
    }
}



class MY_STRUCT extends Structure{

    String str1;
    String str2;
    PROC proc;

    public String defineLayout(){
        return "CSTRING str1, CSTRING str2, PROC proc";
    }

    public MY_STRUCT(){}
}



class PROC extends Callback{

    public String defineSignature(){
        return "void foo(MY_STRUCT*, CSTRING)";
    }

    void foo(Pointer p, String s) throws xFunctionException{

        MY_STRUCT my = (MY_STRUCT)p.deref();

        System.out.println("Callback called successfully!");
        System.out.println(my.str1);
        System.out.println(my.str2);
        System.out.println(s);
    }
}

