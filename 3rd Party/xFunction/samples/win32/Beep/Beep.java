import com.excelsior.xFunction.*;


public class Beep {

    public static void main(String[] args) throws xFunctionException{

        xFunction b = new xFunction("kernel32","int Beep(int,int)");
        b.invoke(new Argument(500),new Argument(1000));
    }
}   
