import com.excelsior.xFunction.*;

public class BStrings {

    public static void main(String[] args) throws xFunctionException {
        //passing a BSTR
        xFunction foo=new xFunction("BStrings","void _foo1@4(BSTR)");
        foo.invoke(new Argument("abcd", Argument.BSTR));

        //receiving BSTR through the pointer
        xFunction foo2=new xFunction("BStrings","void _foo2@4(BSTR*)");
        Pointer p = Pointer.create("BSTR*");
        foo2.invoke(p);

        String a = (String)p.deref();
        System.out.println("Java function main(): \nString received from foo2 = \"" + 
                a + "\"\nlength = "+ a.length() + "\n");

        //passing and receiving BSTR in a structure
        xFunction foo3=new xFunction("BStrings","void _foo3@8(STRUC)");
        STRUC s = new STRUC();
        s.in = "input";
        Argument arg = new Argument(s);
        foo3.invoke(arg);
        s = (STRUC)arg.getValue();

        System.out.println("Java function main:\n s.in = \"" + s.in + 
                "\", s.out points to \"" + (String)s.out.deref() + "\"");
    }
}

class STRUC extends Structure {
    String in;
    Pointer out;

    public STRUC() {
        try {
           out = Pointer.create("BSTR*");
        } catch(Exception e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    public String defineLayout() {
        return "BSTR in, BSTR* out";
    }
}

