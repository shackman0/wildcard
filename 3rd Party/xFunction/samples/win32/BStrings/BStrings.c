#include <stdio.h>

typedef wchar_t* BSTR;

__declspec(dllexport) void __stdcall foo1(BSTR s) {
    printf("Native function foo1() :\nprefix length in bytes = %d\n", 
                *(int*)((int)(s) - 4));
    wprintf(L"passed string = \"%s\"\n\n", s);
}

wchar_t* str = L"\xA\x0" L"gha" L"\x0" L"aa";


__declspec(dllexport) void __stdcall foo2(BSTR* s) {
   *s = &str[2];
}

typedef struct {
    BSTR in;
    BSTR* out;
} STRUC;

__declspec(dllexport) void __stdcall foo3(STRUC s) {
    printf("Native function foo3() :\n in BSTR is (length = %d, string = \"%ls\")\n\n", 
                *(int*)((int)(s.in) - 4), s.in);
    *(s.out) = &str[2];
}

