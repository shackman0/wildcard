import com.excelsior.xFunction.*;


public class ManyArguments{

    public static void main(String[] str) throws xFunctionException{

        xFunction foo=new xFunction("ManyArguments","int _foo@32  (char,short,int,long,float,double)");
        Argument[] args=new Argument[6];
        args[0]=new Argument((char)1);
        args[1]=new Argument((short)2);
        args[2]=new Argument(3);
        args[3]=new Argument((long)4);
        args[4]=new Argument(5.0F);
        args[5]=new Argument(6.0);

        if(((Integer)foo.invoke(args)).intValue()!=0)
            throw new Error("error passing prim types");

        args=new Argument[6];
        args[0]=Argument.create("char",new Character((char)1));
        args[1]=Argument.create("short",new Short((short)2));
        args[2]=Argument.create("int", new Integer(3));
        args[3]=Argument.create("long",new Long(4));
        args[4]=Argument.create("float",new Float(5.0));
        args[5]=Argument.create("double",new Double(6.0));

        if(((Integer)foo.invoke(args)).intValue()!=0)
            throw new Error("error passing prim types");

        foo=new xFunction("ManyArguments","int _foo2@8(CSTRING, UNICODESTRING)");

        if(((Integer)foo.invoke(new Argument("cs",Argument.CSTRING),
                new Argument("us", Argument.UNICODESTRING))).intValue()!=0)
            throw new Error("error passing strings");

        args=new Argument[2];
        args[0]=Argument.create("CSTRING","cs");
        args[1]=Argument.create("UNICODESTRING","us");

        if(((Integer)foo.invoke(args)).intValue()!=0)
            throw new Error("error passing strings");
        
    }
}
