#include <stdio.h>

//testing: getting values of different primitive types

__declspec(dllexport) int __stdcall foo(char a, short b, int c, __int64 d, float e, double g){
     if((a!=1)||(b!=2)||(c!=3)||(d!=4)||(e!=5.0)||(g!=6.0))
	return -1;
     else 
	return 0;
} 
	
__declspec(dllexport) int __stdcall foo2(char* cstr, wchar_t* ustr){
    printf("Get cstr=%s, ustr=%d %d  %d\n",cstr,ustr[0],ustr[1],ustr[2]);
    if((cstr[0]!='c')||(cstr[1]!='s')||(cstr[2]!=0))
	return -1;
    if((ustr[0]!='u')||(ustr[1]!='s')||(ustr[2]!=0))
	return -1;
    return 0;
} 
