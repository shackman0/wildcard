#include <stdio.h>

//testing: returning values of different primitive types

__declspec(dllexport) void __stdcall retVoid(){
} 
__declspec(dllexport) char __stdcall retChar(){
    return 1;
} 
__declspec(dllexport) short __stdcall retShort(){
    return 2;
} 
__declspec(dllexport) int __stdcall retInt(){
    return 3;
} 
__declspec(dllexport) _int64 __stdcall retLong(){
    return 4;
} 
__declspec(dllexport) float __stdcall retFloat(){
    return 5.0;
} 
__declspec(dllexport) double __stdcall retDouble(){
    return 6.0;
} 

__declspec(dllexport) char* __stdcall retCStr(){
    return "cs";
} 

wchar_t ustr[3]={'u','s',0};

__declspec(dllexport) wchar_t* __stdcall retUStr(){
    return ustr;
} 


typedef struct str_ {
    int a;
    int b;
    float c;
    double d;
} STRUC;


STRUC __declspec(dllexport) __stdcall retSTRUC(){
    STRUC s;
    s.a=1;
    s.b=2;
    s.c=3.02;
    s.d=4.00000001;
    return s;
}

int arr2D[2][2] = {{1,2},{3,4}};

__declspec(dllexport) void* __stdcall ret2DArray(){
    return arr2D;
}
