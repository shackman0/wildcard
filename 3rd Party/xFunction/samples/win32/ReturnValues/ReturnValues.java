import com.excelsior.xFunction.*;


public class ReturnValues{

    public static void main(String[] str) throws xFunctionException{

    xFunction foo=new xFunction("ReturnValues","void _retVoid@0  (             )");
    if(foo.invoke()!=null)
        throw new Error();

    foo=new xFunction("ReturnValues","char _retChar@0()");
    if(((Character)foo.invoke()).charValue()!=(char)1)
        throw new Error();

    foo=new xFunction("ReturnValues","short _retShort@0()");
    if(((Short)foo.invoke()).shortValue()!=2)
        throw new Error();

    foo=new xFunction("ReturnValues","int _retInt@0()");
    if(((Integer)foo.invoke(new Argument[0])).intValue()!=3)
        throw new Error();

    foo=new xFunction("ReturnValues","long _retLong@0()");
    if(((Long)foo.invoke()).longValue()!=4){
        throw new Error();
    }

    foo=new xFunction("ReturnValues","float _retFloat@0()");
    if(((Float)foo.invoke()).floatValue()!=5.0)
        throw new Error();

    foo=new xFunction("ReturnValues","double _retDouble@0()");
    if(((Double)foo.invoke()).doubleValue()!=6.0)
        throw new Error();

    foo=new xFunction("ReturnValues","CSTRING _retCStr@0()");
    if(!((String)foo.invoke()).equals("cs"))
        throw new Error();

    foo=new xFunction("ReturnValues","UNICODESTRING _retUStr@0()");
    if(!((String)foo.invoke()).equals("us"))
        throw new Error();

    foo=new xFunction("ReturnValues","STRUC _retSTRUC@0()");
    STRUC s = (STRUC)foo.invoke();
    if ((s.a!=1)||(s.b!=2)||(s.c!=3.02f)||(s.d!=4.00000001))
        throw new Error();

    foo=new xFunction("ReturnValues","int* _ret2DArray@0()");
    Pointer pret = (Pointer)foo.invoke();
    int[][] ret = (int[][])pret.createArray(new int[]{2,2} );
    if ((ret[0][0]!=1)||(ret[0][1]!=2)||(ret[1][0]!=3)||(ret[1][1]!=4))
        throw new Error();

    System.out.println("OK");
    }
}

class STRUC extends Structure {
    int a,b;
    float c;
    double d;

    public String defineLayout() {
        return "int a, int b, float c, double d";
    }         
}
