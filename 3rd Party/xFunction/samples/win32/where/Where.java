import com.excelsior.xFunction.*;



class Where{

    public static void main(String[] str) throws xFunctionException{

        if(str.length == 0){
            System.out.println("Usage: where FileName");
            return;
        }

        Finder w = new Finder();

        String res = w.find(str[0]);

        if(res != null){
            System.out.println("File location is");
            System.out.println(res);
        }else
            System.out.println("File '"+str[0]+"' not found");
    }
}





class Finder{

    static final int MAX_PATH_LENGTH = 1024;
    static final int MAX_DIRS_NUMBER = 50;
    static final int INVALID_HANDLE_VALUE = -1;

    String[] dirs;


    public Finder(){
        dirs = new String[MAX_DIRS_NUMBER];
    }


    void init_dirs() throws xFunctionException{

        xFunction GetEnvVar=new xFunction("kernel32","int GetEnvironmentVariableA(CSTRING, CSTRING, int)");

        Argument arg = new Argument(new String(), Argument.CSTRING, MAX_PATH_LENGTH);

        GetEnvVar.invoke(new Argument("path", Argument.CSTRING),
                         arg,
                         new Argument(MAX_PATH_LENGTH));

        String path = (String)arg.getValue();

        String tmp;
        int i=0, len, pos=0;

        do{
            len=0;

            while( (pos+len+1) < path.length() &&
                   path.charAt(pos+len) != ';' ) len++;

            if(pos==path.length()) break;

            tmp = new String(path.getBytes(), pos, len);
            pos += len+1;

            if( tmp.length() != 0 ){
                if(tmp.charAt(tmp.length()-1) != '\\' )
                    tmp += '\\';
            }else
                continue;

            dirs[i++] = new String(tmp);
        }while( i<MAX_DIRS_NUMBER );
    }





    public String find(String file_to_find) throws xFunctionException{

        xFunction findFirst=new xFunction("kernel32","int FindFirstFileA(CSTRING, WIN32_FIND_DATA*)");

        init_dirs();


        WIN32_FIND_DATA finfo=new WIN32_FIND_DATA();

        int handle, i;
        Argument arg1 = new Argument(file_to_find, Argument.CSTRING);
        Pointer arg2 = Pointer.createPointerTo(finfo);


        handle=((Integer)findFirst.invoke(arg1,arg2)).intValue();


        if(handle != INVALID_HANDLE_VALUE){

            finfo = (WIN32_FIND_DATA)arg2.deref();
            return (new String(finfo.cFileName)).toLowerCase();
        }


        for(i=0; dirs[i] != null; i++){

            arg1 = new Argument(dirs[i]+file_to_find, Argument.CSTRING);
            arg2 = Pointer.createPointerTo(finfo);

            handle=((Integer)findFirst.invoke(arg1,arg2)).intValue();

            if(handle != INVALID_HANDLE_VALUE) break;
        }


        if(handle == INVALID_HANDLE_VALUE) return null;


        finfo = (WIN32_FIND_DATA)arg2.deref();

        int index=0;
        while(finfo.cFileName[index]!='\0') {
            index++;
        }
        return dirs[i] + new String(finfo.cFileName, 0, index);
    }
}





class WIN32_FIND_DATA extends Structure{

    int dwFileAttributes;
    FILETIME ftCreationTime;
    FILETIME ftLastAccessTime;
    FILETIME ftLastWriteTime;
    int    nFileSizeHigh;
    int    nFileSizeLow;
    int    dwReserved0;
    int    dwReserved1;
    char[]    cFileName;
    char[]    cAlternateFileName;

    public String defineLayout(){
        return "int dwFileAttributes, FILETIME ftCreationTime, "+
               "FILETIME ftLastAccessTime, FILETIME ftLastWriteTime, "+
               "int nFileSizeHigh, int nFileSizeLow, int dwReserved0, "+
               "int dwReserved1, char[260] cFileName, "+
               "char[14] cAlternateFileName";
    }

    public WIN32_FIND_DATA(){}
}




class FILETIME extends Structure{

    int dwLowDateTime;
    int dwHighDateTime;

    public String defineLayout(){
         return "int dwLowDateTime, int dwHighDateTime";
    }
}


