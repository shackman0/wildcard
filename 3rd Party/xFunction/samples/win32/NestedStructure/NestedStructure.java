import com.excelsior.xFunction.*;


class NestedStructure {

    public static void main(String[] args) throws xFunctionException {
        xFunction foo = new xFunction("NestedStructure", "void _foo@4(Struct2)");

        Struct1 s1 = new Struct1();
        s1.a = '1';

        Struct2 s2 = new Struct2('5', s1, '3');

        Argument arg = new Argument(s2);

        foo.invoke(arg);
        System.out.println("OK");
    }

}





class Struct2 extends Structure {

    public char a;
    public Struct1 s1;
    public char b;

    public Struct2(char a, Struct1 s1, char b) {
        this.a = a;
        this.s1 = s1;
        this.b = b;
    }

    public Struct2() {}

    public String defineLayout() {
        return "char a, Struct1 s1, char b";
    }
}


class Struct1 extends Structure {

    public char a;

    public Struct1() {
    }

    public String defineLayout() {
        return "char a";
    }
}


