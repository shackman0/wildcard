#include <stdio.h>


typedef struct {
    char a;
} Struct1;

typedef struct {
    char a;
    Struct1 s1;
    char b;
} Struct2;

__declspec(dllexport) void __stdcall foo(Struct2 s2) {
    printf("NestedStructure.dll. foo called: %c%c%c\n", s2.s1.a, s2.a, s2.b);
}
