import com.excelsior.xFunction.*;


public class SimpleFunction{

    public static void main(String[] args) throws xFunctionException{
    xFunction foo=new xFunction("SimpleFunction","int _foo@16  (int,int,int,int)");

    if(((Integer)foo.invoke(new Argument(100),
                            new Argument(200),
                            new Argument(100000),
                            new Argument(2000000000))).intValue()!=100+200+100000+2000000000)
            throw new Error();
    }
}
