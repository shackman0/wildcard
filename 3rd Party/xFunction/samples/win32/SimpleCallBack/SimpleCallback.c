
//testing: simplest callback
typedef  float (__stdcall *PROC)();


__declspec(dllexport) float __stdcall call_callback(PROC func){
    return func(1000,(__int64)1,(double)0.77);
} 
