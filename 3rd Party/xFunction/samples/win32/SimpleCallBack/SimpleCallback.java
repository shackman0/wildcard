import com.excelsior.xFunction.*;


public class SimpleCallback {

    public static void main(String[] str) throws xFunctionException{
    xFunction foo=new xFunction("SimpleCallback","float _call_callback@4(Func)");
    
    Func x=new Func();

    if(((Float)foo.invoke(new Argument(x))).doubleValue()!=(float)0.77)
        throw new Error();

    System.out.println("OK");
        
    }
}


class Func extends Callback{

    public String defineSignature(){
    return "float foo(int,long,double)";
    }

    float foo(int a,long b,double c){
    System.out.println("Callback calles successfully: getting "+a+","+b+","+c);
    if(a!=1000)
        throw new Error();
    if(b!=1)
        throw new Error();
    return (float)c;
    }
}