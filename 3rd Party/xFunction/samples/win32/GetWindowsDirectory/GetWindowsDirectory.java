import com.excelsior.xFunction.*;


class GetWindowsDirectory{

     public static void main(String[] str) throws xFunctionException{

        String win_path = new String();

        xFunction f=new xFunction("kernel32", "int GetWindowsDirectoryA(CSTRING,int)");

        Argument arg = new Argument(win_path, Argument.CSTRING);
        f.invoke(arg, new Argument(256));

        win_path = (String)arg.getValue();

        System.out.println("Windows Directory is '"+win_path+"'");
     }
}