
typedef  int (__stdcall *PROC)();


typedef struct{
    PROC func;
    int  i;
    double d;
} MY_STRUCT;


__declspec(dllexport) int __stdcall call_callback(MY_STRUCT* my){
    return my->func(my->i, my->d);
} 
