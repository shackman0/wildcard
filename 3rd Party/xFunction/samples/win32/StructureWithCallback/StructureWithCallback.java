import com.excelsior.xFunction.*;

public class StructureWithCallback{

    public static void main(String[] str) throws xFunctionException{

    xFunction foo=new xFunction("StructureWithCallback","int _call_callback@4(MY_STRUCT*)");
    
    MY_STRUCT my_struct=new MY_STRUCT();
        PROC func=new PROC();

        my_struct.func = func;
        my_struct.a = 10;
        my_struct.c = 20.0d;

    int foo_result = ((Integer)foo.invoke(Pointer.createPointerTo(my_struct))).intValue();

        System.out.println(foo_result);

    if( foo_result != my_struct.a)
        throw new Error();

    System.out.println("OK");
    }
}




class PROC extends Callback{

    public String defineSignature(){
    return "int foo(int,double)";
    }

    int foo(int a,double c){
    System.out.println("Callback calles successfully: getting "+a+", "+c);
    return a;
    }
}




class MY_STRUCT extends Structure{

    PROC   func;
    int    a;
    double c; 

    public String defineLayout(){
        return "PROC func, int a, double c";
    }

    public MY_STRUCT(){}
}
