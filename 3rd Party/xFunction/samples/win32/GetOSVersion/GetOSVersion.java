import com.excelsior.xFunction.*;


public class GetOSVersion {

    public static void main(String[] str) throws xFunctionException{

        xFunction f=new xFunction("kernel32", "int GetVersionExA(OSVERSIONINFO*)");

        OSVERSIONINFO info = new OSVERSIONINFO();

        info.dwOSVersionInfoSize = 148;//Size of OSVERSIONINFO

        Pointer arg = Pointer.createPointerTo(info);

        f.invoke(arg);

        info = (OSVERSIONINFO)arg.deref();

        System.out.println("Major Version: "+info.dwMajorVersion);
        System.out.println("Minor Version: "+info.dwMinorVersion);
        System.out.println("Build Number: "+info.dwBuildNumber);
        System.out.println("Platform Id: "+info.dwPlatformId);

        System.out.println("CSD Version: "+new String(info.szCSDVersion) );

    System.out.println("OK");
    }
}



class OSVERSIONINFO extends Structure{

    int dwOSVersionInfoSize;
    int dwMajorVersion;
    int dwMinorVersion;
    int dwBuildNumber;
    int dwPlatformId;

    char[] szCSDVersion;

    public String defineLayout(){
        return "int dwOSVersionInfoSize,int dwMajorVersion,int dwMinorVersion,"+
               "int dwBuildNumber,int dwPlatformId, char[128] szCSDVersion";
    }

    public OSVERSIONINFO(){
        szCSDVersion = new char[128];
    }
}


