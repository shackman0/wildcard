import com.excelsior.xFunction.*;

public class ScanC {
   public static void main(String[] str)throws xFunctionException{
        xFunction findFirst=new xFunction("kernel32","int FindFirstFileA(CSTRING, WIN32_FIND_DATA*)"); 
        WIN32_FIND_DATA a=new WIN32_FIND_DATA();
        Argument arg1=new Argument("C:\\*.*",Argument.CSTRING);
        Pointer arg2=Pointer.createPointerTo(a);
        int handle=((Integer)findFirst.invoke(arg1,arg2)).intValue();

        xFunction findNext=new xFunction("kernel32","int FindNextFileA(int, WIN32_FIND_DATA*)"); 
        Argument arg3=new Argument(handle);

        int res;
        do{
            a=(WIN32_FIND_DATA)arg2.deref();
            char c;
            int i=0;
            do{
                c=a.cFileName[i];
                i++;
                System.out.print(c);
            }while(c!=0);
            System.out.println("");     
        
            res=((Integer)findNext.invoke(arg3,arg2)).intValue();
        }while(res==1);
        xFunction findClose=new xFunction("kernel32","int FindClose(int)"); 
        findClose.invoke(arg3);
   }
}

class WIN32_FIND_DATA extends Structure{


    int dwFileAttributes; 
    FILETIME ftCreationTime; 
    FILETIME ftLastAccessTime; 
    FILETIME ftLastWriteTime; 
    int    nFileSizeHigh; 
    int    nFileSizeLow; 
    int    dwReserved0; 
    int    dwReserved1; 

    char[]    cFileName; 
    char[]    cAlternateFileName; 
    public String defineLayout(){
        return "int dwFileAttributes,FILETIME ftCreationTime,FILETIME ftLastAccessTime,"+
        "FILETIME ftLastWriteTime, int nFileSizeHigh, int nFileSizeLow, int dwReserved0,int dwReserved1,"+
        "char[260] cFileName,char[14] cAlternateFileName"; 
    }
    public WIN32_FIND_DATA(){}
}

class FILETIME extends Structure{
    int dwLowDateTime; 
    int dwHighDateTime; 
    public String defineLayout(){
         return "int dwLowDateTime, int dwHighDateTime";
    }
}


