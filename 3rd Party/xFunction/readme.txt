README.TXT      Copyright (c) 1999-2006 Excelsior. All Rights Reserved.
-----------------------------------------------------------------------

                      xFunction 2.17 for Windows
                                  
                            Read Me First!

INSTALLATION

Thank you for purchasing xFunction. To begin its usage, unpack the 
downloaded archive into an empty directory. The following files and
subdirectories shall be created:

license.pdf   - Excelsior Software License Agreement
readme.txt    - this file
xFunction.jar - JAR containing the com.excelsior.xFunction package
xFunction.dll - DLL with native methods
xFunction.pdf - xFunction documentation in PDF format
doc\          - xFunction documentation in HTML format
help\         - xFunction documentation in Windows Help format
samples\      - sample programs illustrating xFunction usage 

When using the xFunction library in your programs, ensure that the
java.library.path property contains a path to the directory where
xFunction.dll resides.


SAMPLE PROGRAMS

For your convenience, each sample resides in a separate subdirectory
containing a text file with sample description and scripts for building
and executing the sample. Those scripts assume that you have Microsoft
Visual C++ and Sun JDK 1.2 or above installed; if that is true, no
additional setup is required to build and execute samples.


SUPPORT

In case of a technical problem, email us at support@excelsior-usa.com.
Our support facilities may also be reached as described at:

http://www.excelsior-usa.com/support.html 


CONTACT

Excelsior, LLC     | Tel    : +7 3832 39 78 24
6 Lavrenteva Ave.  | Fax    : +1 509 271 5205
Novosibirsk        | E-mail : info@excelsior-usa.com
630090 Russia      | Web    : http://www.excelsior-usa.com
