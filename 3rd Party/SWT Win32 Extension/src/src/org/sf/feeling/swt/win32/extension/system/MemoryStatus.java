/*******************************************************************************
 * Copyright (c) 2007 cnfree.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  cnfree  - initial API and implementation
 *******************************************************************************/
package org.sf.feeling.swt.win32.extension.system;

import org.sf.feeling.swt.win32.internal.extension.Extension;
import org.sf.feeling.swt.win32.internal.extension.MEMORYSTATUS;

/**
 * This class represents MEMORYSTATUS structure.
 * 
 * @author <a href="mailto:cnfree2000@hotmail.com">cnfree</a>
 */
public class MemoryStatus
{

	private int totalPhys;

	private int availPhys;

	private int totalPageFile;

	private int availPageFile;

	private int totalVirtual;

	private int availVirtual;

	private static MemoryStatus memoryStatus = null;

	private MemoryStatus()
	{
	}

	/**
	 * Get a global MemoryStatus instance.
	 * 
	 * @return a global MemoryStatus instance.
	 */
	public static MemoryStatus getInstance()
	{
		if (memoryStatus == null)
		{
			memoryStatus = new MemoryStatus();
			memoryStatus.refreshStatus();
		}
		return memoryStatus;
	}

	/**
	 * Get MEMORYSTATUS information and refresh MemoryStatus internal data.
	 */
	public void refreshStatus()
	{
		MEMORYSTATUS status = Extension.GlobalMemoryStatus();
		memoryStatus.totalPhys = status.dwTotalPhys;
		memoryStatus.totalVirtual = status.dwTotalVirtual;
		memoryStatus.totalVirtual = status.dwTotalPageFile;
		memoryStatus.availPhys = status.dwAvailPhys;
		memoryStatus.availPageFile = status.dwAvailPageFile;
		memoryStatus.availVirtual = status.dwAvailVirtual;
	}

	/**
	 * Get system total physical memory size.
	 * 
	 * @return system total physical memory size.
	 */
	public int getTotalPhys()
	{
		return totalPhys;
	}

	/**
	 * Get system available physical memory size.
	 * 
	 * @return system available physical memory size.
	 */
	public int getAvailPhys()
	{
		return availPhys;
	}

	/**
	 * Get system total physical memory page file.
	 * 
	 * @return system total physical memory page file.
	 */
	public int getTotalPageFile()
	{
		return totalPageFile;
	}

	/**
	 * Get system available physical memory page file.
	 * 
	 * @return system available physical memory page file.
	 */
	public int getAvailPageFile()
	{
		return availPageFile;
	}

	/**
	 * Get system total virtual memory size.
	 * 
	 * @return system total virtual memory size.
	 */
	public int getTotalVirtual()
	{
		return totalVirtual;
	}

	/**
	 * Get system available virtual memory size.
	 * 
	 * @return system available virtual memory size.
	 */
	public int getAvailVirtual()
	{
		return availVirtual;
	}
}
