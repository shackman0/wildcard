package org.sf.feeling.swt.win32.extension.io.listener;

import org.eclipse.swt.widgets.Event;

public interface CDDriveChangeListener
{
	public void driveEjected(Event event);

	public void driveLoaded(Event event);
}
