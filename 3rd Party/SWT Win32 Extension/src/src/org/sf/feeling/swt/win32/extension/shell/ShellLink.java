/*******************************************************************************
 * Copyright (c) 2007 cnfree.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  cnfree  - initial API and implementation
 *******************************************************************************/
package org.sf.feeling.swt.win32.extension.shell;

import java.io.File;

import org.sf.feeling.swt.win32.internal.extension.Extension;

/**
 * Shell link Utility class.
 * 
 * @author <a href="mailto:cnfree2000@hotmail.com">cnfree</a>
 * 
 */
public class ShellLink {
	/**
	 * Get the shortcut target position of specified linked file.
	 * 
	 * @param linkFilePath
	 *            a linked file(*.lnk)'s path.
	 * @return shortcut target position
	 */
	public static String getShortCutTarget(String linkFilePath) {
		return Extension.GetShortCutTarget(linkFilePath);
	}

	/**
	 * Get the shortcut arguments of specified linked file.
	 * 
	 * @param linkFilePath
	 *            a linked file(*.lnk)'s path.
	 * @return shortcut target arguments.
	 */
	public static String getShortCutArguments(String linkFilePath) {
		return Extension.GetShortCutArguments(linkFilePath);
	}

	/**
	 * Get the shortcut target position of specified linked file.
	 * 
	 * @param linkFile
	 *            is a linked file(*.lnk).
	 * @return shortcut target position
	 */
	public static String getShortCutTarget(File linkFile) {
		return getShortCutTarget(linkFile.getAbsolutePath());
	}

	/**
	 * Get the shortcut arguments of specified linked file.
	 * 
	 * @param linkFile
	 *            a linked file(*.lnk).
	 * @return shortcut target arguments.
	 */
	public static String getShortCutArguments(File linkFile) {
		return getShortCutArguments(linkFile.getAbsolutePath());
	}

	/**
	 * Create a shortcut file by a specified file.
	 * 
	 * @param sourceFilePath
	 *            path of the specified file for creating shortcut
	 * @param linkFilePath
	 *            path of the shortcut file are created shortcut for specified
	 *            file
	 * @param linkDescriptor
	 *            the descriptor of shortcut
	 * @return return 0 if creating shortcut failed.
	 * 
	 */
	public static int createShortCut(String sourceFilePath,
			String linkFilePath, String linkDescriptor) {
		return Extension.CreateShortCut(sourceFilePath, linkFilePath,
				linkDescriptor);
	}

	/**
	 * Create a shortcut file by a specified file.
	 * 
	 * @param sourceFile
	 *            the specified file for creating shortcut
	 * @param linkFile
	 *            the shortcut file are created shortcut for specified file
	 * @param linkDescriptor
	 *            the descriptor of shortcut
	 * @return return 0 if creating shortcut failed.
	 * 
	 */
	public static int createShortCut(File sourceFile, File linkFile,
			String linkDescriptor) {
		return createShortCut(sourceFile.getAbsolutePath(), linkFile
				.getAbsolutePath(), linkDescriptor);
	}
}
