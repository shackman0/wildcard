/*******************************************************************************
 * Copyright (c) 2007 cnfree.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  cnfree  - initial API and implementation
 *******************************************************************************/
package org.sf.feeling.swt.win32.extension.shell;

import java.io.File;

import org.eclipse.swt.internal.win32.OS;
import org.eclipse.swt.internal.win32.TCHAR;
import org.sf.feeling.swt.win32.extension.Win32;
import org.sf.feeling.swt.win32.extension.io.Keyboard;
import org.sf.feeling.swt.win32.internal.extension.Extension;
import org.sf.feeling.swt.win32.internal.extension.SHFILEOPSTRUCT;
import org.sf.feeling.swt.win32.internal.extension.SHFILEOPSTRUCTA;
import org.sf.feeling.swt.win32.internal.extension.SHFILEOPSTRUCTW;

/**
 * Shell System Utility class.
 * 
 * @author <a href="mailto:cnfree2000@hotmail.com">cnfree</a>
 * 
 */
public class ShellSystem
{
	public final static int WALLPAPER_CENTER = Win32.WPSTYLE_CENTER;

	public final static int WALLPAPER_TILE = Win32.WPSTYLE_TILE;

	public final static int WALLPAPER_STRETCH = Win32.WPSTYLE_STRETCH;

	public final static int WALLPAPER_MAX = Win32.WPSTYLE_MAX;

	/**
	 * Set specified file as wallpaper.
	 * 
	 * @param picture
	 *            is a file to set as wallpaper.
	 * @param style
	 *            display style, one of {@link #WALLPAPER_CENTER},
	 *            {@link #WALLPAPER_TILE}, {@link #WALLPAPER_STRETCH},
	 *            {@link #WALLPAPER_MAX}.
	 * @return If set wallpaper successfully, return true, else return false.
	 */
	public static boolean setWallPaper(File picture, int style)
	{
		return Extension.SetWallPaper(picture.getAbsolutePath().toCharArray(), style);
	}

	/**
	 * Set specified file as wallpaper.
	 * 
	 * @param picturePath
	 *            is a file path to set as wallpaper.
	 * @param style
	 *            display style, one of {@link #WALLPAPER_CENTER},
	 *            {@link #WALLPAPER_TILE}, {@link #WALLPAPER_STRETCH},
	 *            {@link #WALLPAPER_MAX}.
	 * @return If set wallpaper successfully, return true, else return false.
	 */
	public static boolean setWallPaper(String picturePath, int style)
	{
		if (picturePath == null) return false;
		return Extension.SetWallPaper(picturePath.toCharArray(), style);
	}

	/**
	 * Move specified file to recycle
	 * 
	 * @param hWnd
	 *            system handle.
	 * @param file
	 *            specified file will be moved to recycle
	 * @param confirm
	 *            whether pop up confirm dialog.
	 * @return return 0 if the operation failed.
	 */
	public static int removeToRecycle(int hWnd, File file, boolean confirm)
	{
		int result;
		SHFILEOPSTRUCT lpFileOp;
		if (Extension.IsUnicode) lpFileOp = new SHFILEOPSTRUCTW();
		else
			lpFileOp = new SHFILEOPSTRUCTA();
		lpFileOp.wFunc = 3;
		String fileName = file.getAbsolutePath();
		int hHeap = Extension.GetProcessHeap();
		TCHAR buffer = new TCHAR(0, fileName, true);
		int byteCount = buffer.length() * TCHAR.sizeof;
		int lpFileName = Extension.HeapAlloc(hHeap, Extension.HEAP_ZERO_MEMORY, byteCount);
		Extension.MoveMemory(lpFileName, buffer, byteCount);
		lpFileOp.pFrom = lpFileName;
		if (confirm) lpFileOp.fFlags = Win32.FOF_ALLOWUNDO;
		else
			lpFileOp.fFlags = Win32.FOF_NOCONFIRMATION | Win32.FOF_SILENT;
		if (Extension.IsUnicode)
		{
			result = Extension.SHFileOperationW(lpFileOp);
		}
		else
			result = Extension.SHFileOperationA(lpFileOp);
		if (lpFileName != 0) OS.HeapFree(hHeap, 0, lpFileName);
		return result;
	}

	/**
	 * Show desktop
	 */
	public static void showDesktop()
	{
		Keyboard.keyDown(Win32.VK_LWIN, 0, false);
		Keyboard.keyDown('M', 0, false);
		Keyboard.keyUp('M', 0, false);
		Keyboard.keyUp(Win32.VK_LWIN, 0, false);
	}
}
