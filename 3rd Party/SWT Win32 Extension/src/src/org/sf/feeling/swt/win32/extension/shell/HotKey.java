/*******************************************************************************
 * Copyright (c) 2007 cnfree.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  cnfree  - initial API and implementation
 *******************************************************************************/
package org.sf.feeling.swt.win32.extension.shell;

import org.eclipse.swt.widgets.Shell;
import org.sf.feeling.swt.win32.internal.extension.util.FlagSet;

/**
 * An utility class that provide system hot key function.
 * 
 * @author <a href="mailto:cnfree2000@hotmail.com">cnfree</a>
 * 
 */
public class HotKey extends FlagSet {
	private static final int MOD_ALT = 0x0001;
	private static final int MOD_CONTROL = 0x0002;
	private static final int MOD_SHIFT = 0x0004;

	private int _keyCode;

	public HotKey(int keyCode) {
		super();

		_keyCode = keyCode;
	}

	public void setAlt(boolean set) {
		setupFlag(MOD_ALT, set);
	}

	public boolean isAlt() {
		return contains(MOD_ALT);
	}

	public void setControl(boolean set) {
		setupFlag(MOD_CONTROL, set);
	}

	public boolean isControl() {
		return contains(MOD_CONTROL);
	}

	public void setShift(boolean set) {
		setupFlag(MOD_SHIFT, set);
	}

	public boolean isShift() {
		return contains(MOD_SHIFT);
	}

	public int getKeyCode() {
		return _keyCode;
	}

	/**
	 * Register a system hot key for specified shell.
	 * 
	 * @param shell
	 *            the shell will be registered hot key.
	 * @param hotKey
	 *            specified hot key.
	 */
	public static void setHotKey(Shell shell, HotKey hotKey) {
		Windows.setHotKey(shell.handle, hotKey);
	}

	/**
	 * Get the system hot key of specified shell.
	 * 
	 * @param shell
	 *            specified shell
	 * @return the system hot key of specified shell.
	 */
	public static HotKey getHotKey(Shell shell) {
		return Windows.getHotKey(shell.handle);
	}
}
