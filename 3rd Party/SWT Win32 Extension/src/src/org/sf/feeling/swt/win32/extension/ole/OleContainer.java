/*******************************************************************************
 * Copyright (c) 2007 cnfree.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  cnfree  - initial API and implementation
 *******************************************************************************/
package org.sf.feeling.swt.win32.extension.ole;

import org.eclipse.swt.widgets.Composite;

/**
 * An abstract class that allow intercept ole control event.
 * 
 * @author <a href="mailto:cnfree2000@hotmail.com">cnfree</a>
 * 
 */
public abstract class OleContainer extends Composite {

	/**
	 * @see org.eclipse.swt.widgets.Composite#Composite(Composite, int)
	 * @param parent
	 *            a widget which will be the parent of the new instance (cannot
	 *            be null)
	 * @param style
	 *            the style of widget to construct
	 */
	public OleContainer(Composite parent, int style) {
		super(parent, style);
	}

	/**
	 * Add a hook interceptor to listen ole control event.
	 * 
	 * @param interceptor
	 *            the interceptor used to listen ole control event.
	 */
	public abstract void addHookInterceptor(OleHookInterceptor interceptor);

	/**
	 * the interceptor used to listen ole control event.
	 * 
	 * @return the interceptor used to listen ole control event.
	 */
	public abstract OleHookInterceptor getHookInterceptor();
}
