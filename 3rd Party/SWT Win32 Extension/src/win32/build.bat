@echo off
REM /*****************************************************************************
REM * Copyright (c) 2007 cnfree.
REM * All rights reserved. This program and the accompanying materials
REM * are made available under the terms of the Eclipse Public License v1.0
REM * which accompanies this distribution, and is available at
REM * http://www.eclipse.org/legal/epl-v10.html
REM *
REM * Contributors:
REM * cnfree - initial API and implementation
REM *****************************************************************************/
@echo off

IF NOT "%JAVA_HOME%"=="" GOTO MAKE

rem *****
rem Javah
rem *****
set JAVA_HOME=C:\jdk1.4
set path=%JAVA_HOME%;%path%

rem ********
rem MSVC 6.0
rem ********
call "D:\Program Files\Microsoft Visual Studio\VC98\Bin\VCVARS32.BAT"

rem ****** 
rem MS-SDK
rem ******
set Mssdk "D:\Program Files\Microsoft Platform SDK"
call "D:\Program Files\Microsoft Platform SDK\SetEnv.Cmd"

:MAKE
cd C:\SWTANDJFACE\org.sf.feeling.swt.win32.extension\win32
nmake -f make_win32.mak %1 %2 %3 %4


del *.obj,*.lib,*.exp,*.res
del "./../lib/swt-extension-win32.dll"
move swt-extension-win32.dll "./../lib"
pause
