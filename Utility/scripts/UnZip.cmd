@echo off

rem This script runs UnZip on the specified archive file.
rem You MUST modify the values in the User Modifiable Section below when you install the
rem application before this script will work.

rem invocation parms:
rem %1: the full path and name of the Jar file in which UnZip may be found, including the ".jar" suffix
rem %2: the path and name of the archive file
rem %3: the destination directory into which to unzip the archive file 

if (%1. == .) (
  echo Missing Jar file full path name
  goto end
)

if (%2. == .) (
  echo Missing archive file name
  goto end
)

if (%3. == .) (
  echo Missing destination directory
  goto end
)


rem clear all the variables we use in this script so we don't "inherit" any from the windows environment.
rem do not modify this list.

set JAVA2SE=
set JVM_PARMS=
set RUN_JAVA=
set CLASSPATH=


rem ********** BEGIN USER MODIFIABLE SECTION **********
rem ********** BEGIN USER MODIFIABLE SECTION **********
rem ********** BEGIN USER MODIFIABLE SECTION **********

rem **********
rem If you do not want to rely on the PATH environment variable to find the JVM, then set the JAVA2SE variable to 
rem point to the bin directory of where the Java 2 SE runtime jar files are located. Use an absolute path.

rem set JAVA2SE=C:\Program Files\Java\jre1.6.0_07\bin\


rem **********
rem Uncomment the RUN_JAVA you want to use to launch the application
rem javaw hides the dos window. java will display a dos window.
rem this is a required variable

rem set RUN_JAVA=start "Update Server" /i /b "%JAVA2SE%javaw.exe"
set RUN_JAVA="%JAVA2SE%java.exe"


rem set the CLASSPATH based upon whether we are running in development (./bin) or production (.jar)

rem set CLASSPATH=./bin;../Utility/bin;
set CLASSPATH=%1;


rem ********** END USER MODIFIABLE SECTION **********
rem ********** END USER MODIFIABLE SECTION **********
rem ********** END USER MODIFIABLE SECTION **********


rem ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********
rem ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********
rem ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********

echo UnZip %2 to %3 now ...
rem invoke UnZip
%RUN_JAVA% -classpath %CLASSPATH% %JVM_PARMS% com.World2One.Utility.UnZip %2 %3
echo UnZip done!

:end

rem pause if this command file doesn't work to have the DOS window remain open so you can read the error message 
rem pause


