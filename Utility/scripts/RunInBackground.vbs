
' launch a program or .cmd/.bat file invisibly
' usage: wscript.exe RunInBackground.vbs <your program/.cmd/.bat file name> [parameters to your program]

Count = WScript.Arguments.Count

if (Count < 1) then
  WScript.Echo "Usage: wscript.exe RunInBackground.vbs <target program (.exe, .bat, .cmd file) to run> [parameters to target program]"
  WScript.Quit
end if

CommandLine = ""

For I = 0 to Count - 1
  CommandLine = CommandLine & """" & WScript.Arguments(I) & """" & " "
next

'WScript.Echo CommandLine
WScript.CreateObject("WScript.Shell").Run CommandLine, 0, False
