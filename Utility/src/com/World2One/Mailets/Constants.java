package com.World2One.Mailets;


/**
 * <p>Title: Constants</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One, Inc.</p>
 */
public class Constants
{

  public static final String DEBUG                                 = "Debug";

  public static final String SOAP_PROTOCOL                         = "SOAPProtocol";
  public static final String SOAP_HOST                             = "SOAPHost";
  public static final String SOAP_PORT                             = "SOAPPort";
  
  public static final String INJECT_SIGNATURE_ON_REPLY_AND_FORWARD  = "InjectSignatureOnReplyOrForward";
  public static final String GUI_SUPPORTED                         = "GUISupported";
  public static final String DISPLAY_MESSAGE_SENT_POPUP            = "DisplayMessageSentPopup";
  public static final String LOG_AUTHORIZED_RECIPIENTS             = "LogAuthorizedRecipients";
  public static final String W2O_SUPPORT_SEND_EMAIL_ON_ERROR       = "W2OSupportSendEmailOnError";
  public static final String W2O_SUPPORT_TO_EMAIL_ADDRESS          = "W2OSupportToEmailAddress";
  public static final String W2O_SUPPORT_FROM_EMAIL_ADDRESS        = "W2OSupportFromEmailAddress";
  public static final String W2O_SUPPORT_SMTP_HOST                 = "W2OSupportSMTPHost";
  public static final String W2O_SUPPORT_PORT                      = "W2OSupportPort";
  public static final String W2O_SUPPORT_SMTP_PROTOCOL             = "W2OSupportSMTPProtocol";
  public static final String W2O_SUPPORT_AUTH_USER                 = "W2OSupportAuthUser";
  public static final String W2O_SUPPORT_AUTH_PASSWORD             = "W2OSupportAuthPassword";
  public static final String W2O_SUPPORT_SUBJECT                   = "W2OSupportSubject";

  public static final String DATABASE_SOFTWARE  = "DatabaseSoftware";
  public static final String DATABASE_HOST      = "DatabaseHost";
  public static final String DATABASE_PORT      = "DatabasePort";
  public static final String DATABASE_NAME      = "DatabaseName";
  public static final String DATABASE_USER      = "DatabaseUser";
  public static final String DATABASE_PASSWORD  = "DatabasePassword";
  
  public static final String RMI_BIND_ADDR = "RMIBindAddr";
  public static final String RMI_BIND_PORT = "RMIBindPort";

  
  
  private Constants()
  {
    super();
  }
  
  
}
