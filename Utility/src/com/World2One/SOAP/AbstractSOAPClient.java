package com.World2One.SOAP;

import java.io.ByteArrayInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.AccessController;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.text.MessageFormat;
import java.util.HashMap;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.stream.StreamSource;

import com.World2One.Utility.Constants;
import com.World2One.Utility.ToolBox;



/**
 * <p>Title: AbstractSOAPClient</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One, Inc.</p>
 */
public class AbstractSOAPClient
{


  public static final String API_KEY = "65d65f65-5398-4d03-b913-1a19056906a7";
  public static final String API_URL = "{0}://{1}:{2}/DesktopModules/W2OServices/{3}";

  protected static final String SOAP_ACTION = "SOAPAction"; 

  
  

  protected AbstractSOAPClient()
  {
    super();
  }

  protected URL GetAPIURL(
    String soapProtocol,
    String soapHost,
    String soapPort,
    String service ) 
    throws MalformedURLException
  {
    String urlString = MessageFormat.format(API_URL, soapProtocol, soapHost, soapPort, service);
    URL url = new URL(urlString);
    
    return url;
  }

  protected static HashMap<String,SOAPBodyElement> GetElementsByTagName(SOAPBody soapBody)
  {
    HashMap<String,SOAPBodyElement> elementsByTagName = (HashMap)ToolBox.GetElementsByTagName(soapBody);
    
    return elementsByTagName;
  }
  
  protected SOAPMessage SendMessage(
    URL    soapServiceURL,
    String messageString,
    String soapAction ) 
    throws SOAPException, PrivilegedActionException 
  {
    //Create the message
    MessageFactory messageFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
    SOAPMessage soapMessage = messageFactory.createMessage();
    soapMessage.getSOAPHeader().detachNode();
    
    MimeHeaders messageMimeHeaders = soapMessage.getMimeHeaders();
    if (soapAction != null)
    {
      messageMimeHeaders.addHeader(SOAP_ACTION, soapAction);
    }

    //Create objects for the message parts
    SOAPPart soapPart = soapMessage.getSOAPPart();
    
    //Populate the message
    StreamSource soapMessageSource = new StreamSource(new ByteArrayInputStream(messageString.getBytes()));
    soapPart.setContent(soapMessageSource);
    soapPart.normalize();

    //Create the connection
    SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
    SOAPConnection soapConnection = soapConnectionFactory.createConnection();

    //Send the message
    PrivilegedSendMessage privilegedSendMessage = new PrivilegedSendMessage(soapConnection,soapMessage,soapServiceURL);
    SOAPMessage response = (SOAPMessage)AccessController.doPrivileged(privilegedSendMessage);

    //Close the connection
    soapConnection.close();
    
    return response;
  }

  public String GetFaultMessage(SOAPBody soapBody)
  {
    SOAPFault fault = soapBody.getFault();
    Name code = fault.getFaultCodeAsName();
    String string = fault.getFaultString();
    
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(Constants.CR_LF)
                 .append("  FAULT")
                 .append(Constants.CR_LF)
                 .append("  name=\"")
                 .append(code.getQualifiedName())
                 .append("\"")
                 .append(Constants.CR_LF)
                 .append("  string=\"")
                 .append(string)
                 .append("\"");
    
    return ToolBox.GetStackTrace(new Exception(stringBuilder.toString()));
  }
  
  protected class PrivilegedSendMessage
    implements PrivilegedExceptionAction
  {
    protected SOAPConnection _soapConnection;
    protected SOAPMessage    _soapMessage;
    protected URL            _soapServiceURL;
    
    public PrivilegedSendMessage(
      SOAPConnection soapConnection,
      SOAPMessage    soapMessage,
      URL            soapServiceURL )
    {
      super();

      _soapConnection = soapConnection;
      _soapMessage = soapMessage;
      _soapServiceURL = soapServiceURL;
    }

    @Override
    public Object run()
      throws Exception
    {
      SOAPMessage response = _soapConnection.call(_soapMessage, _soapServiceURL);
      
      return response;
    }
    
  }

}
