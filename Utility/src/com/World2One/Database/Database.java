package com.World2One.Database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * <p>Title: Database</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One, Inc.</p>
 */
public class Database
{

  protected String _software;
  protected String _host;
  protected String _port;
  protected String _name;
  protected String _user;
  protected String _password;

  
  public Database(
    String software,
    String host,
    String port,
    String name,
    String user,
    String password )
  {
    super();
    
    _software = software;
    _host = host;
    _port = port;
    _name = name;
    _user = user;
    _password = password;
  }

  
  protected Connection _connection = null;

  public Connection GetConnection()
    throws SQLException
  {
    if (_connection == null)
    {
      _connection = DriverManager.getConnection(GetConnectURL(), _user, _password);
    }
    
    return _connection;
  }

  protected String _connectURL = null;
  
  public String GetConnectURL()
  {
    if (_connectURL == null)
    {
      _connectURL = "jdbc:" + _software + "://" + _host + ":" + _port + "/" + _name;
    }
    
    return _connectURL;
  }

  public void Disconnect()
    throws SQLException
  {
    if (_connection != null)
    {
      SQLRollbackTransaction();
      _connection.close();
      _connection = null;
    }
  }
  
  public Statement CreateStatement()
    throws SQLException
  {
    Statement statement = GetConnection().createStatement();
    
    return statement;
  }

  public static final String BEGIN_TRANSACTION = "begin transaction";

  public boolean SQLBeginTransaction()
    throws SQLException
  {
    Statement sqlStmt = CreateStatement();
    boolean rc = sqlStmt.execute(BEGIN_TRANSACTION);
    sqlStmt.close();

    return rc;
  }

  public static final String COMMIT_TRANSACTION = "commit transaction";

  public boolean SQLCommitTransaction()
    throws SQLException
  {
    Statement sqlStmt = CreateStatement();
    boolean rc = sqlStmt.execute(COMMIT_TRANSACTION);
    sqlStmt.close();

    return rc;
  }
  
  public static final String ROLLBACK_TRANSACTION = "rollback transaction";
  
  public boolean SQLRollbackTransaction()
    throws SQLException
  {
    Statement sqlStmt = CreateStatement();
    boolean rc = sqlStmt.execute(ROLLBACK_TRANSACTION);
    sqlStmt.close();

    return new Boolean(rc);
  }

  public PreparedStatement PrepareSQLStatement(String sqlString)
    throws SQLException
  {
    Connection connection = GetConnection();
    PreparedStatement preparedStatement = connection.prepareStatement(sqlString,ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
    
    return preparedStatement;
  }

}
