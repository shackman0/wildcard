package com.World2One.Utility;

/**
 * <p>Title: MutableString</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One, Inc.</p>
 */
public class MutableString
{
  
  public String _string = null;
  
  public MutableString()
  {
    super();
  }

  @Override
  public String toString()
  {
    return String.valueOf(_string);
  }
}
