package com.World2One.Utility;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

/**
 * <p>Title: ToolBox</p>
 * <p>Description:</p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One, Inc.</p>
 */
public class ToolBox
{

  private ToolBox()
  {
    super();
  }

  public static final String[] NODE_TYPES = new String[11];
  {
    NODE_TYPES[Node.ELEMENT_NODE - 1] = "Element Node";
    NODE_TYPES[Node.ATTRIBUTE_NODE - 1] = "Attribute Node";
    NODE_TYPES[Node.TEXT_NODE - 1] = "Text Node";
    NODE_TYPES[Node.CDATA_SECTION_NODE - 1] = "CData Section Node";
    NODE_TYPES[Node.ENTITY_REFERENCE_NODE - 1] = "Entity Reference Node";
    NODE_TYPES[Node.ENTITY_NODE - 1] = "Entity Node";
    NODE_TYPES[Node.PROCESSING_INSTRUCTION_NODE - 1] = "Processing Instruction Node";
    NODE_TYPES[Node.COMMENT_NODE - 1] = "Coment Node";
    NODE_TYPES[Node.DOCUMENT_NODE - 1] = "Document Node";
    NODE_TYPES[Node.DOCUMENT_TYPE_NODE - 1] = "Document Type Node";
    NODE_TYPES[Node.DOCUMENT_FRAGMENT_NODE - 1] = "Document Fragment Node";
    NODE_TYPES[Node.NOTATION_NODE - 1] = "Notation Node";
  }

  public static String PrintTree(Element element)
  {
    StringBuilder stringBuilder = new StringBuilder();
    PrintTree(element, stringBuilder, 0);

    return stringBuilder.toString();
  }

  public static String PrintTree(Element element, int indent)
  {
    StringBuilder stringBuilder = new StringBuilder();
    PrintTree(element, stringBuilder, indent);

    return stringBuilder.toString();
  }

  public static void PrintTree(Element element, StringBuilder stringBuilder, int indent)
  {
    stringBuilder.append(Constants.SPACES.substring(0, indent));
    stringBuilder.append(element.getLocalName());

    NamedNodeMap attributes = element.getAttributes();
    if (attributes != null)
    {
      int numberOfAttributes = attributes.getLength();
      if (numberOfAttributes > 0)
      {
        stringBuilder.append(": ");
        for (int indx = 0; indx < numberOfAttributes; indx++)
        {
          Node attributeNode = attributes.item(indx);
          String name = attributeNode.getLocalName();
          String value = attributeNode.getNodeValue().trim();
          stringBuilder.append(" ");
          stringBuilder.append(name);
          stringBuilder.append("=\"");
          stringBuilder.append(value);
          stringBuilder.append("\"");
        }
      }
    }
    stringBuilder.append(Constants.CR_LF);

    NodeList childNodes = element.getChildNodes();
    int numberOfChildNodes = childNodes.getLength();
    for (int indx = 0; indx < numberOfChildNodes; indx++)
    {
      Node childNode = childNodes.item(indx);
      if (childNode instanceof Element)
      {
        PrintTree((Element) childNode, stringBuilder, indent + 2);
      }
      else
      if (childNode instanceof Text)
      {
        String value = childNode.getNodeValue().trim();
        if (value.length() > 0)
        {
          stringBuilder.append(Constants.SPACES.substring(0, indent + 2));
          stringBuilder.append("\"");
          stringBuilder.append(value);
          stringBuilder.append("\"");
          stringBuilder.append(Constants.CR_LF);
        }
      }
    }
  }

  public static String WrapText(String text, int maximumLineLength)
  {
    String[] splitLines = SplitText(text,maximumLineLength);
    
    StringBuilder stringBuilder = new StringBuilder();
    for (String splitLine: splitLines)
    {
      stringBuilder.append(splitLine);
      stringBuilder.append(Constants.CR_LF);
    }
    stringBuilder.setLength(stringBuilder.length() - Constants.CR_LF.length());
    
    return stringBuilder.toString();
  }
  
  public static String[] SplitText(String text, int maximumLineLength)
  {
    // return empty array for null text
    if (text == null)
    {
      return new String[] {};
    }

    // return text if maximumLineLength is zero or less
    if (maximumLineLength <= 0)
    {
      return new String[] { text };
    }
    
    // return text if less than maximumLineLength
    if (text.length() <= maximumLineLength)
    {
      return new String[] { text };
    }
    
    char[] chars = text.toCharArray();
    Vector<String> lines = new Vector();
    StringBuilder line = new StringBuilder();
    StringBuilder word = new StringBuilder();

    for (int indx = 0; indx < chars.length; indx++)
    {
      word.append(chars[indx]);

      if (chars[indx] == ' ')
      {
        if ((line.length() + word.length()) > maximumLineLength)
        {
          lines.add(line.toString());
          line.delete(0, line.length());
        }

        line.append(word);
        word.delete(0, word.length());
      }
    }

    // handle any extra chars in current word
    if (word.length() > 0)
    {
      if ((line.length() + word.length()) > maximumLineLength)
      {
        lines.add(line.toString());
        line.delete(0, line.length());
      }
      line.append(word);
    }

    // handle extra line
    if (line.length() > 0)
    {
      lines.add(line.toString());
    }

    String[] splitLines = new String[lines.size()];
    int counter = 0;
    for (Enumeration enumeration = lines.elements(); enumeration.hasMoreElements(); counter++)
    {
      splitLines[counter] = (String) enumeration.nextElement();
    }

    return splitLines;
  }

  public static String GetStackTrace(Exception ex)
  {
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    PrintStream printStream = new PrintStream(outputStream);
    ex.printStackTrace(printStream);
    printStream.flush();
    String stackTrace = outputStream.toString();
    printStream.close();
    
    return stackTrace;
  }

  /**
   *  
   * @param xmlValue
   * @return
   */
  public static String Convert2SafeXMLValue(String xmlValue)
  {
    String newXMLValue = xmlValue.replaceAll("&", "&amp;")   // gotta do & first
                                 .replaceAll("\"", "&quot;")
                                 .replaceAll("'", "&apos;")
                                 .replaceAll("<", "&lt;")
                                 .replaceAll(">", "&gt;");
      
    return newXMLValue;
  }

  /**
   * 
   * @param dim window size
   * @return
   */
  public static Point CalculateWindowLocation(Dimension dim)
  {
    //Get the screen size
    Toolkit toolkit = Toolkit.getDefaultToolkit();
    Dimension screenSize = toolkit.getScreenSize();

    //Calculate the frame location
    int x = (screenSize.width - dim.width) / 2;
    int y = (screenSize.height - dim.height) / 2;

    //Set the new frame location
    Point location = new Point(x, y);
    
    return location;
  }

  public static HashMap<String,Element> GetElementsByTagName(Element element)
  {
    HashMap<String,Element> elementsByTagName = new HashMap();
    
    NodeList elementsList = element.getElementsByTagName("*");
    int numberOfElements = elementsList.getLength();
    for (int indx = 0; indx < numberOfElements; indx++)
    {
      org.w3c.dom.Node node = elementsList.item(indx);
      Element nodeAsElement = (Element)node;
      String elementName = nodeAsElement.getNodeName();
      elementsByTagName.put(elementName, nodeAsElement);
    }
    
    return elementsByTagName;
  }

  protected static Templates __templates = null;

  public static Templates GetTemplates() 
    throws TransformerConfigurationException
  {
    if (__templates == null)
    {
      // describe the style sheet to format the output
      String xslStr = "<?xml version=\"1.0\" encoding=\"UTF-16\"?>" +
                        "<xsl:stylesheet version=\"1.0\" xmlns:xalan=\"http://xml.apache.org/xslt\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">" +
                        "<xsl:output method=\"xml\" indent=\"yes\" xalan:indent-amount=\"2\"/>" +
                        "<xsl:template match=\"/\">" +
                        "<xsl:copy-of select=\".\"/>" +
                        "</xsl:template>" +
                      "</xsl:stylesheet>";

      // Use a Transformer for output
      StreamSource streamSource = new StreamSource(new java.io.StringReader(xslStr));
      TransformerFactory transformerFactory = TransformerFactory.newInstance();
      transformerFactory.setAttribute("indent-number", new Integer(2));
      
      __templates = transformerFactory.newTemplates(streamSource);
    }

    return __templates;
  }

  public static void WriteXML(
    Document     document,
    StreamResult streamResult ) 
    throws TransformerException
  {
    Transformer transformer = GetTemplates().newTransformer();
    transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
    transformer.setOutputProperty(OutputKeys.METHOD, "xml");
    transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
    
    DOMSource domSource = new DOMSource(document);
    transformer.transform(domSource, streamResult);

    // debugging
//    StringWriter stringWriter = new StringWriter();
//    StreamResult debuggingStreamResult = new StreamResult(stringWriter);
//    transformer.transform(domSource, debuggingStreamResult);
//    String xmlString = stringWriter.toString();
//    System.out.println(xmlString);
    
  }
  
  /**
   * determines if the queryString matches at least one of the regexPatterns
   * 
   * @param queryString
   * @param patterns
   * @return
   */
  public static Pattern Matches(String queryString, HashSet<Pattern> patterns)
  {
    for (Pattern pattern: patterns)
    {
      Matcher matcher = pattern.matcher(queryString);
      boolean matches = matcher.matches();
      if (matches == true)
      {
        return pattern;
      }
    }
    
    return null;
  }
  public static String URL (
    String  ipaddress,
    Integer port )
  {
    StringBuilder url = new StringBuilder();

    url.append("//");

    if (ipaddress.contains(":") == true)   // IPv6
    {
      url.append("[");
    }

    url.append(ipaddress.trim());

    if (ipaddress.contains(":") == true)
    {
      url.append("]");
    }

    if (port != null)
    {
      url.append(":");
      url.append(port);
    }

    url.append("/");

    return url.toString();
  }

  public static String GetDTS()
  {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MMM-dd hh:mm:ss");
    String dts = dateFormat.format(new GregorianCalendar().getTime());
    
    return dts;
  }


  /**
   * @param parameterKeyWordPrefix the prefix that identifies a parm keyword, usually "-"
   * @param args
   * @return
   */
  public static HashMap<String,String> ProcessParms(
    String    parameterKeyWordPrefix,
    boolean   verbose,
    String... args)
  {
    HashMap<String,String> parms = new HashMap();
    
    for (int indx = 0; indx < args.length; indx++)
    {
      String key = args[indx];
      if (key.startsWith(parameterKeyWordPrefix) == false)
      {
        throw new RuntimeException("key word \"" + key + "\" must start with \"" + parameterKeyWordPrefix + "\".");
      }
      String value = null;
      if (indx + 1 < args.length)
      {
        String candidateValue = args[indx + 1];
        if (candidateValue.startsWith(parameterKeyWordPrefix) == false)
        {
          value = candidateValue;
          indx++;
        }
      }
      String previousValue = parms.put(key, value);
      if (verbose == true)
      {
        System.out.print(key);
        if (value != null)
        {
          System.out.print("=" + value);
        }
        System.out.println();
      }
      if (previousValue != null)
      {
        throw new RuntimeException("key word \"" + key + "\" specified more than once.");
      }
    }
    
    return parms;
  }

  public static final int         EXIT_PARM_MULTIPLY_DEFINED_ERROR        = -10;
  public static final int         EXIT_INCORRECT_PARM_COMBINATION_ERROR   = -11;
  public static final int         EXIT_MISSING_PARM_VALUE_ERROR           = -12;
  public static final int         EXIT_INCORRECT_PARM_VALUE_ERROR         = -15;
  public static final int         EXIT_STARTUP_ERROR                      = -16;

  public static void ReportExitError(String errorMessage)
  {
    if (errorMessage != null)
    {
      System.err.println(errorMessage);
    }

    // invoke the garbage collector, which will cause the remote garbage collector to be
    // run too
    System.gc();
    // ask the system to finalize all objects that are no longer referenced,
    // which will cause finalization to occur on all remote objects as well
    System.runFinalization();
  }


  public static void Exit(String errorMessage, int exitCode)
  {
    ReportExitError(errorMessage);
    // now it's safe for us to exit to the system.
    System.exit(exitCode);
  }

  public static String Prepend(
    String prependString,
    int    minLength, 
    String string )
  {
    while(string.length() < minLength)
    {
      string = prependString + string;
    }  

    return string;
  }

  public enum TrimPosition
  {
    Leading,
    Trailing,
    Both;
  }
  
  public static String Trim(
    String         trimString,
    String         string,
    TrimPosition   trimPosition ) 
  {
    int trimLength = trimString.length();
    if ( (trimPosition == TrimPosition.Leading) ||
         (trimPosition == TrimPosition.Both) )
    {
      while (string.startsWith(trimString) == true)
      {
        string = string.substring(trimLength);
      }
    }
    if ( (trimPosition == TrimPosition.Trailing) ||
         (trimPosition == TrimPosition.Both) )
   {
      while (string.endsWith(trimString) == true)
      {
        string = string.substring(0,string.length() - trimLength);
      }
   }
    
    return string;
  }
  
  /**
   * unit test
   * @param args
   */
//  public static void main(String... args)
//  {
//    System.out.println(Trim("0","0000007B00",TrimPosition.Leading));
//  }
  
}
