package com.World2One.Utility;


import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * <p>Title: Base64InputStream</p>
 * <p>Description: Base64InputStream</p>
 * BASE64 encoding encodes 3 bytes into 4 characters.
 *   |11111122|22223333|33444444|
 * Each set of 6 bits is encoded according to the toBase64 map. If the number of input bytes is not a multiple of 3,
 * then the last group of 4 characters is padded with one or two = signs. Each output line is at most 76 characters.
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: TruckMaster Logistics Systems, Inc.</p>
 * @author Floyd Shackelford, adapted from Core Java
 */


public class Base64InputStream
  extends FilterInputStream
{

   private static final int[]     __fromBase64 = {  -1, -1, -1, -1, -1, -1, -1, -1,
                                                    -1, -1, -1, -1, -1, -1, -1, -1,
                                                    -1, -1, -1, -1, -1, -1, -1, -1,
                                                    -1, -1, -1, -1, -1, -1, -1, -1,
                                                    -1, -1, -1, -1, -1, -1, -1, -1,
                                                    -1, -1, -1, 62, -1, -1, -1, 63,
                                                    52, 53, 54, 55, 56, 57, 58, 59,
                                                    60, 61, -1, -1, -1, -1, -1, -1,
                                                    -1,  0,  1,  2,  3,  4,  5,  6,
                                                     7,  8,  9, 10, 11, 12, 13, 14,
                                                    15, 16, 17, 18, 19, 20, 21, 22,
                                                    23, 24, 25, -1, -1, -1, -1, -1,
                                                    -1, 26, 27, 28, 29, 30, 31, 32,
                                                    33, 34, 35, 36, 37, 38, 39, 40,
                                                    41, 42, 43, 44, 45, 46, 47, 48,
                                                    49, 50, 51, -1, -1, -1, -1, -1  };

   private int                    _indx = 0;
   private final int[]            _chars = new int[4];


  public Base64InputStream(InputStream inputStream)
  {
    super(inputStream);
  }

  @Override
  public int read (
    byte[]  b,
    int     off,
    int     len )
    throws IOException
  {
    if (len > b.length - off)
    {
      len = b.length - off;
    }
    for (int i = 0; i < len; i++)
    {
      int ch = read();
      if (ch == -1)
      {
        return i;
      }
      b[i + off] = (byte)ch;
    }
    return len;
  }

  @Override
  public int read (byte[] b)
    throws IOException
  {
    return read(b, 0, b.length);
  }

  @Override
  public int read()
    throws IOException
  {
    int charRead;
    if (_indx == 0)
    {
      // skip whitespace
      do
      {
        _chars[0] = super.read();
        if (_chars[0] == -1)
       {
         return -1;
       }
      } while (Character.isWhitespace((char)_chars[0]));
      _chars[1] = super.read();
      if (_chars[1] == -1)
      {
        return -1;
      }
      _indx++;
      charRead = (__fromBase64[_chars[0]] << 2) | (__fromBase64[_chars[1]] >> 4);
    }
    else
    if (_indx == 1)
    {
      _chars[2] = super.read();
      if ( (_chars[2] == '=') ||
           (_chars[2] == -1) )
     {
       return -1;
     }
      _indx++;
      charRead = ((__fromBase64[_chars[1]] & 0x0F) << 4) | (__fromBase64[_chars[2]] >> 2);
    }
    else
    {
      _chars[3] = super.read();
      if ( (_chars[3] == '=') ||
           (_chars[3] == -1) )
      {
        return -1;
      }
      _indx = 0;
      charRead = ((__fromBase64[_chars[2]] & 0x03) << 6) | __fromBase64[_chars[3]];
    }
    return charRead;
  }


}
