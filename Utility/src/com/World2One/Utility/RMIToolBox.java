package com.World2One.Utility;

import java.net.MalformedURLException;
import java.net.SocketException;
import java.rmi.AccessException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RMISecurityManager;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import com.World2One.OS.NetworkInfo;



/**
 * <p>Title: RMIToolBox</p>
 * <p>Description: 
 * 
 * to use as a Server:
 * 
 *   RMIToolBox<Foo> rmiToolBox = new RMIToolBox(Foo.class);
 *   rmiToolBox.LaunchServer(args);
 * 
 * to use as a Client:
 * 
 *   RMIToolBox<Foo> rmiToolBox = new RMIToolBox(Foo.class);
 *   Foo boundClient = rmiToolBox.BindToServer(args);    
 * 
 * </p>
 * see: 
 *   http://java.sun.com/developer/onlineTraining/rmi/RMI.html
 *   http://archives.java.sun.com/cgi-bin/wa?A2=ind0302&L=rmi-users&P=3663
 *   http://archives.java.sun.com/cgi-bin/wa?A2=ind0503&L=rmi-users&P=293
 *   http://www.pramati.com/docstore/1230033/sa_settingup_firewalls.htm
 *   http://www.cs.swan.ac.uk/~csneal/InternetComputing/Tunnelling.html
 *   
 *   an interesting article on hole-punching. might be worth implementing: http://www.ddj.com/java/217400127
 *   
 *   we can buy this. it claims to solve all our rmi vs firewall problems: http://www.rmiproxy.com/
 * 
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public class RMIToolBox<REMOTE extends Remote>
{

  
  // return codes when this server exits to the OS
  public static final int         EXIT_OK                                 = 0;
  public static final int         EXIT_RMI_BIND_PORT_UNSPECIFIED_ERROR    = -1;
  public static final int         EXIT_CANNOT_BIND_TO_REGISTRY_ERROR      = -13;
  public static final int         EXIT_REGISTRY_ERROR                     = -14;

  /**
   * the parm indicating which IP address to bind to for advertising RMI services.
   *
   * usage is: -rmiBindAddr <rmi bind addr>
   */
  public static final String   RMI_BIND_ADDR = "-rmiBindAddr";

  /**
   * the parm indicating which Port to bind to for advertising RMI services.
   *
   * usage is: -rmiBindPort <rmi bind port>
   */
  public static final String   RMI_BIND_PORT = "-rmiBindPort";

  
  /**
   * the "master" instance of the impl
   */
  public REMOTE  _masterRMIServiceImpl = null;

  public Registry       _registry = null;

  
  public String    _rmiBindAddr     = null;
  public Integer   _rmiBindPort     = null;

  protected Class<REMOTE> _rmiImplClass;

  protected Integer _defaultRMIBindPort = null;
  
  
  /**
   * 
   * @param rmiImplClass the server class that implements REMOTE
   */
  public RMIToolBox(Class<REMOTE> rmiImplClass)
  {
    super();
    
    _rmiImplClass = rmiImplClass;
    
    System.setProperty("java.rmi.server.disableHttp", "false");
  }

  public void SetDefaultRMIBindPort(int defaultRMIBindPort)
  {
    _defaultRMIBindPort = defaultRMIBindPort;
  }
  
  public int GetDefaultRMIBindPort()
  {
    return _defaultRMIBindPort;
  }
  
  /**
   * clients: use this to bind to this server
   * 
   * @param rmiBindAddr
   * @param rmiBindPort
   * @return bound client
   * @throws RemoteException
   * @throws MalformedURLException
   * @throws NotBoundException
   */
  public REMOTE BindToServer (
    String       rmiBindAddr,
    int          rmiBindPort ) 
    throws RemoteException, MalformedURLException, NotBoundException, java.rmi.ConnectException
  {
    System.setSecurityManager(new RMISecurityManager());

    String bindingURL = "//" + rmiBindAddr + ":" + rmiBindPort + "/" + _rmiImplClass.getSimpleName();
    String message = "Servers currently bound at " + bindingURL;
    String[] bindings = Naming.list(bindingURL);
    for (int i = 0; i < bindings.length; i++)
    {
      message += ("  \"" + bindings[i] + "\"");
    }
    System.out.println(message);
    
    REMOTE boundClient = (REMOTE)Naming.lookup(bindingURL);

    return boundClient;
  }
  
  /**
   * clients can also use this to capture rmi parms
   * @param args
   * @throws SocketException
   */
  public void ProcessRMIParms(String[] args) 
    throws SocketException
  {
    try
    {
      for (int indx = 0; indx < args.length; indx++)
      {
        if (args[indx].equalsIgnoreCase(RMI_BIND_ADDR) == true)
        {
          if (_rmiBindAddr == null)
          {
            indx++;
            _rmiBindAddr = args[indx].trim();
          }
          else
          {
            ToolBox.Exit(RMI_BIND_ADDR + " was specifed more than once", ToolBox.EXIT_PARM_MULTIPLY_DEFINED_ERROR);
          }
        }
        else
        if (args[indx].equalsIgnoreCase(RMI_BIND_PORT) == true)
        {
          if (_rmiBindPort == null)
          {
            indx++;
            _rmiBindPort = new Integer(args[indx].trim());
          }
          else
          {
            ToolBox.Exit(RMI_BIND_PORT + " was specifed more than once", ToolBox.EXIT_PARM_MULTIPLY_DEFINED_ERROR);
          }
        }
      }
    }
    catch (Exception ex)
    {
      ToolBox.Exit("A Parameter value is missing.", ToolBox.EXIT_MISSING_PARM_VALUE_ERROR);
    }
  
    // initialize missing parms
    if (_rmiBindAddr == null)
    {
      _rmiBindAddr = NetworkInfo.GetLocalIPAddress();
    }
  
    if (_rmiBindPort == null)
    {
      _rmiBindPort = _defaultRMIBindPort;
    }

    // check for missing parms
    if (_rmiBindPort == null)
    {
      ToolBox.Exit(RMI_BIND_PORT + " was not specifed", EXIT_RMI_BIND_PORT_UNSPECIFIED_ERROR);
    }
  }

  public void Quiesce() 
    throws AccessException, RemoteException, NotBoundException
  {
    UnbindServerFromRegistry();
    ToolBox.Exit(null,RMIToolBox.EXIT_OK);
  }


  public void BindServerToRegistry() 
    throws InstantiationException, IllegalAccessException, RemoteException, MalformedURLException
  {
    // bind server to registry
    _masterRMIServiceImpl = _rmiImplClass.newInstance();
    
    /**
     * don't let rmi choose an random anonymous port.
     * set the specific port at which the service calls. see http://archives.java.sun.com/cgi-bin/wa?A2=ind0302&L=rmi-users&P=3663
     */
    UnicastRemoteObject.unexportObject(_masterRMIServiceImpl,true);   // shouldn't i first check that the object is exported?  
    UnicastRemoteObject.exportObject(_masterRMIServiceImpl, _rmiBindPort);

    // Bootstrap registry service
    _registry = LocateRegistry.createRegistry(_rmiBindPort);
    _registry.rebind(_rmiImplClass.getSimpleName(),_masterRMIServiceImpl);

    String bindingURL = ToolBox.URL(_rmiBindAddr,_rmiBindPort);
    String[] bindings = Naming.list(bindingURL);
    String message = "Servers currently bound at " + bindingURL;
    for (int i = 0; i < bindings.length; i++)
    {
      message += ("  \"" + bindings[i] + "\"");
    }
  }

  protected void UnbindServerFromRegistry() 
    throws AccessException, RemoteException, NotBoundException
  {
    _registry.unbind(_rmiImplClass.getSimpleName());
    _registry = null;
  }

  /**
   * convenience method to launch the server
   * @param args
   */
  public void LaunchServer(String... args)
  {
    try
    {
      ProcessRMIParms(args);
      BindServerToRegistry();

      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(_rmiImplClass.getSimpleName());
      stringBuilder.append(" is ready to receive connections at ");
      stringBuilder.append(_rmiBindAddr);
      stringBuilder.append(":");
      stringBuilder.append(_rmiBindPort);
      String javaRMIServerHostname = System.getProperty("java.rmi.server.hostname");
      if (javaRMIServerHostname != null)
      {
        stringBuilder.append(" via firewall at ");
        stringBuilder.append(javaRMIServerHostname);
      }

      System.out.println(stringBuilder.toString());
    }
    catch (SocketException ex)
    {
      ex.printStackTrace();
      ToolBox.Exit(ex.getLocalizedMessage(),EXIT_CANNOT_BIND_TO_REGISTRY_ERROR);
    }
    catch (RemoteException ex)
    {
      ex.printStackTrace();
      ToolBox.Exit(ex.getLocalizedMessage(),ToolBox.EXIT_STARTUP_ERROR);
    }
    catch (MalformedURLException ex)
    {
      ex.printStackTrace();
      ToolBox.Exit(ex.getLocalizedMessage(),ToolBox.EXIT_STARTUP_ERROR);
    }
    catch (InstantiationException ex)
    {
      ex.printStackTrace();
      ToolBox.Exit(ex.getLocalizedMessage(),ToolBox.EXIT_STARTUP_ERROR);
    }
    catch (IllegalAccessException ex)
    {
      ex.printStackTrace();
      ToolBox.Exit(ex.getLocalizedMessage(),ToolBox.EXIT_STARTUP_ERROR);
    }
  }

  public REMOTE BindToServer() 
    throws RemoteException, MalformedURLException, NotBoundException, java.rmi.ConnectException
  {
    REMOTE boundClient = BindToServer(_rmiBindAddr,_rmiBindPort);

    return boundClient;
  }

  public String GetRmiBindAddr()
  {
    return _rmiBindAddr;
  }

  public void SetRmiBindAddr(String rmiBindAddr)
  {
    _rmiBindAddr = rmiBindAddr;
  }

  public Integer GetRmiBindPort()
  {
    return _rmiBindPort;
  }

  public void SetRmiBindPort(Integer rmiBindPort)
  {
    _rmiBindPort = rmiBindPort;
  }

}
