package com.World2One.Utility;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * <p>Title: Base64OutputStream</p>
 * <p>Description: Base64OutputStream</p>
 * BASE64 encoding encodes 3 bytes into 4 characters.
 *   |11111122|22223333|33444444|
 * Each set of 6 bits is encoded according to the toBase64 map. If the number of input bytes is not a multiple of 3,
 * then the last group of 4 characters is padded with one or two = signs. Each output line is at most 76 characters.
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: TruckMaster Logistics Systems, Inc.</p>
 * @author Floyd Shackelford, adapted from Core Java
 */

public class Base64OutputStream
  extends FilterOutputStream
{

  private static final char[]     __toBase64 = {  'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                                  'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                                  'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                                  'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                                  'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                                  'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                                  'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                                  '4', '5', '6', '7', '8', '9', '+', '/' };

  private int                     _column = 0;
  private int                     _indx = 0;
  private final int[]             _inbuf = new int[3];

  
  
  public Base64OutputStream(OutputStream outputStream)
  {
    super(outputStream);
  }

  @Override
  public void write(int charToWrite)
    throws IOException
  {
    _inbuf[_indx] = charToWrite;
    _indx++;
    if (_indx == 3)
    {
      super.write(__toBase64[(_inbuf[0] & 0xFC) >> 2]);
      super.write(__toBase64[((_inbuf[0] & 0x03) << 4) | ((_inbuf[1] & 0xF0) >> 4)]);
      super.write(__toBase64[((_inbuf[1] & 0x0F) << 2) | ((_inbuf[2] & 0xC0) >> 6)]);
      super.write(__toBase64[_inbuf[2] & 0x3F]);
      _column += 4;
      _indx = 0;
      if (_column >= 76)
      {
        super.write('\n');
        _column = 0;
      }
    }
  }

  @Override
  public void flush()
    throws IOException
  {
    if (_indx == 1)
    {
      super.write(__toBase64[(_inbuf[0] & 0xFC) >> 2]);
      super.write(__toBase64[(_inbuf[0] & 0x03) << 4]);
      super.write('=');
      super.write('=');
    }
    else
    if (_indx == 2)
    {
      super.write(__toBase64[(_inbuf[0] & 0xFC) >> 2]);
      super.write(__toBase64[((_inbuf[0] & 0x03) << 4) | ((_inbuf[1] & 0xF0) >> 4)]);
      super.write(__toBase64[(_inbuf[1] & 0x0F) << 2]);
      super.write('=');
    }
    _indx = 0;
  }



}
