package com.World2One.Utility;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 * <p>Title: LogHandler</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public class LogHandler
  extends java.util.logging.StreamHandler
{

  protected OutputStream          _infoOutputStream;
  protected OutputStream          _errorOutputStream;
  protected Level                 _errorThreshold;
  protected ByteArrayOutputStream _handlerOutputStream;
  
  
  public LogHandler(
    OutputStream infoOutputStream,
    OutputStream errorOutputStream,
    Level        errorThreshold )
  {
    super();
    
    _infoOutputStream = infoOutputStream;
    _errorOutputStream = errorOutputStream;
    _errorThreshold = errorThreshold;
    
    _handlerOutputStream = new ByteArrayOutputStream();
    setOutputStream(_handlerOutputStream);
  }
  
  @Override
  public void publish(LogRecord logRecord)
  {
    super.publish(logRecord);
  
    try
    {
      Level level = logRecord.getLevel();
      if (level.intValue() >= _errorThreshold.intValue())
      {
        _errorOutputStream.write(_handlerOutputStream.toByteArray());
        _errorOutputStream.flush();
      }
      else
      {
        _infoOutputStream.write(_handlerOutputStream.toByteArray());
        _infoOutputStream.flush();
      }
      _handlerOutputStream.reset();
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }

}
