package com.World2One.Utility;

/**
 * <p>Title: MutableBoolean</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One, Inc.</p>
 */
public class MutableBoolean
{

  
  public Boolean _boolean = null;
  
  
  public MutableBoolean()
  {
    super();
  }
  

  @Override
  public String toString()
  {
    return String.valueOf(_boolean);
  }

}
