package com.World2One.Utility;

/**
 * <p>Title: MutableObject</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public class MutableObject<TYPE>
{
  
  public TYPE _object;
  
  public MutableObject()
  {
    super();
  }
  
  @Override
  public String toString()
  {
    return String.valueOf(_object);
  }

}
