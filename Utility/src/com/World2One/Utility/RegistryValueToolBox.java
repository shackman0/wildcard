package com.World2One.Utility;

import org.sf.feeling.swt.win32.extension.registry.RegistryException;
import org.sf.feeling.swt.win32.extension.registry.RegistryValue;
import org.sf.feeling.swt.win32.extension.registry.ValueType;

import com.World2One.Utility.ToolBox.TrimPosition;

/**
 * <p>Title: RegistryToolBox</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public class RegistryValueToolBox
{

  private RegistryValueToolBox()
  {
    super();
  }


  public static Integer GetDataAsInteger(RegistryValue registryValue)
  {
    Integer value = null;
    
    ValueType valueType = registryValue.getType();
    if ( (valueType == ValueType.REG_DWORD) || 
         (valueType == ValueType.REG_DWORD_LITTLE_ENDIAN) ||
         (valueType == ValueType.REG_DWORD_BIG_ENDIAN) ) 
    {
      value = (Integer)registryValue.getData();
    }
    else
    if ( (valueType == ValueType.REG_SZ) ||
         (valueType == ValueType.REG_EXPAND_SZ) ||
         (valueType == ValueType.REG_MULTI_SZ) )
    {
      String valueAsString = registryValue.getData().toString().trim();
      if (valueAsString.length() > 0)
      {
        valueAsString = ToolBox.Trim("0", valueAsString, TrimPosition.Leading);
        if (valueAsString.length() == 0)
        {
          value = 0;
        }
        else
        {
          value = Integer.valueOf(valueAsString);
        }
      }
    }
    else 
    {
      throw new RegistryException("Invalid type " + valueType.toString());
    }
      
    return value;
  }

  public static Boolean GetDataAsBoolean(RegistryValue registryValue)
  {
    Boolean value = null;
    
    ValueType valueType = registryValue.getType();
    if ( (valueType == ValueType.REG_DWORD) || 
         (valueType == ValueType.REG_DWORD_LITTLE_ENDIAN) ||
         (valueType == ValueType.REG_DWORD_BIG_ENDIAN) ) 
    {
      value = ((Integer)registryValue.getData() != 0);
    }
    else
    if ( (valueType == ValueType.REG_SZ) ||
         (valueType == ValueType.REG_EXPAND_SZ) ||
         (valueType == ValueType.REG_MULTI_SZ) )
    {
      String valueAsString = registryValue.getData().toString().trim();
      value = Boolean.valueOf(valueAsString);
    }
    else
    if ( (valueType == ValueType.REG_NONE) || 
         (valueType == ValueType.REG_BINARY) ) 
    {
      byte[] dataBytes = (byte[]) registryValue.getData();  

      for (int index = 0; index < dataBytes.length; index++) 
      {
        if (dataBytes[index] != 0)
        {
          value = true;
          break;
        }
      }
    }
    else 
    {
      throw new RegistryException("Invalid type " + valueType.toString());
    }
      
    return value;
  }

  public static String GetDataAsString(RegistryValue registryValue)
  {
    String value = null;
    
    ValueType valueType = registryValue.getType();
    if ( (valueType == ValueType.REG_SZ) ||
         (valueType == ValueType.REG_EXPAND_SZ) ||
         (valueType == ValueType.REG_MULTI_SZ) )
    {
      value = registryValue.getData().toString();
    }
    else 
    if ( (valueType == ValueType.REG_DWORD) || 
         (valueType == ValueType.REG_DWORD_LITTLE_ENDIAN) ||
         (valueType == ValueType.REG_DWORD_BIG_ENDIAN) ) 
    {
      value = ((Integer) registryValue.getData()).toString();
    }
    else 
    if ( (valueType == ValueType.REG_NONE) || 
         (valueType == ValueType.REG_BINARY) ) 
    {
      StringBuilder stringBuilder = new StringBuilder();
      byte[] dataBytes = (byte[]) registryValue.getData();

      for (int index = 0; index < dataBytes.length; index++) 
      {
        char character = (char)dataBytes[index];
        if (character != 0)
        {
          stringBuilder.append(character);
        }
      }
      value = stringBuilder.toString();
    }
    else
    {
      throw new RegistryException("Invalid type " + valueType.toString());
    }
      
    return value;
  }

  public static byte[] ConvertString2Binary(String string) 
  {
    byte[] stringBytes = string.getBytes();
    int stringLength = string.length();
    byte[] binaryBytes = new byte[stringLength * 2];
    int jndx = 0;
    for (int indx = 0; indx < stringLength; indx++)
    {
      binaryBytes[jndx++] = stringBytes[indx];
      binaryBytes[jndx++] = 0;
    }
    
    return binaryBytes;
  }
  
}
