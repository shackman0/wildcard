package com.World2One.Utility;

/**
 * <p>Title: MutableInteger</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One, Inc.</p>
 */
public class MutableInteger
{

  public Integer _integer;
  
  public MutableInteger()
  {
    super();
  }

  @Override
  public String toString()
  {
    return String.valueOf(_integer);
  }

}
