package com.World2One.Utility;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * <p>Title: UnZip</p>
 * <p>Description: see: http://piotrga.wordpress.com/2008/05/07/how-to-unzip-archive-in-java/</p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public class UnZip
{

  protected Logger _logger;

  public UnZip(Logger logger)
  {
    super();
    
    _logger = logger;
  }

  public void UnzipArchive(
    File archive, 
    File outputDirectory )
  {
    try
    {
      ZipFile zipfile = new ZipFile(archive);
      for (Enumeration enumeration = zipfile.entries(); enumeration.hasMoreElements();)
      {
        ZipEntry entry = (ZipEntry) enumeration.nextElement();
        UnzipEntry(zipfile, entry, outputDirectory);
      }
    }
    catch (Exception ex)
    {
      _logger.log(Level.SEVERE, "Error while extracting file " + archive, ex);
    }
  }

  protected void UnzipEntry(ZipFile zipfile, ZipEntry entry, File outputDir)
    throws IOException
  {

    if (entry.isDirectory())
    {
      CreateDirectory(new File(outputDir, entry.getName()));
      return;
    }

    File outputFile = new File(outputDir, entry.getName());
    if (!outputFile.getParentFile().exists())
    {
      CreateDirectory(outputFile.getParentFile());
    }

    _logger.log(Level.INFO, "Extracting: " + entry);
    BufferedInputStream inputStream = new BufferedInputStream(zipfile.getInputStream(entry));
    BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(outputFile));

    try
    {
      Copy(inputStream, outputStream);
    }
    finally
    {
      outputStream.close();
      inputStream.close();
    }
  }

  protected static final int BUFFER_SIZE = 4096;

  /**
   * transfer bytes from inputStream to outputStream
   * @param inputStream
   * @param outputStream
   * @throws IOException
   */
  protected void Copy(
    InputStream  inputStream, 
    OutputStream outputStream )
    throws IOException
  {
    byte[] buffer = new byte[BUFFER_SIZE];
    int length;
    try
    {
      while ((length = inputStream.read(buffer)) > 0)
      {
        outputStream.write(buffer, 0, length);
      }
    }
    finally
    {
      inputStream.close();
      outputStream.close();
    }
  }

  protected void CreateDirectory(File directory)
  {
    _logger.log(Level.INFO, "Creating dir " + directory.getName());
    if ( (directory.exists() == false) &&
         (directory.mkdirs() == false) )
    {
      throw new RuntimeException("Can not create directory " + directory);
    }
  }

  /**
   * 
   * @param args 
   *   [0] = archive name, required
   *   [1] = destination directory, optional, defaults to "."
   */
  public static void main(String... args)
  {
    String archiveName = args[0];
    File archive = new File(archiveName);
    
    String destinationDirectoryName = args.length > 1 ? args[1] : ".";
    File destinationDirectory = new File(destinationDirectoryName);
    
    LogHandler consoleHandler = new LogHandler(System.out,System.err,Level.WARNING);
    Logger logger = Logger.getLogger(UnZip.class.getName());
    logger.setLevel(Level.ALL);
    logger.setUseParentHandlers(false);
    logger.addHandler(consoleHandler);
    
    UnZip unZip = new UnZip(logger);
    unZip.UnzipArchive(archive, destinationDirectory);
  }
  
  
}
