package com.World2One.Utility;

/**
 * <p>Title: Constants</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One, Inc.</p>
 */
public interface Constants
{
  
  public static final String WILD_CARD_UPDATE_ZIP_FILE_NAME = "WildCardUpdate.zip";
  public static final int WILDCARD_VERSION = 1;

  public static final String CONFIGURATOR_UPDATE_ZIP_FILE_NAME = "ConfiguratorUpdate.zip";
  public static final int CONFIGURATOR_VERSION = 1;

  public static final String KEEP_ALIVE_UPDATE_ZIP_FILE_NAME = "KeepAliveUpdate.zip";
  public static final int KEEP_ALIVE_VERSION = 1;
  
  public static final String SPACES = "                                                                                                                      ";

  public static final String CR_LF = "\r\n";

  public static final String PARM_KEYWORD_PREFIX = "-";

  public static final String PID_FILE_NAME = "pid.txt";

  // keep alive
  public static final String KEEP_ALIVE_TOP_FORMAT = "{0}/Keep Alive/";                              // 0 = wildcard top

  public static final String KEEP_ALIVE_PID_FILE_PATH_FORMAT = KEEP_ALIVE_TOP_FORMAT + "/Runtime/";  
  public static final String KEEP_ALIVE_PID_FILE_PATH_NAME_FORMAT = KEEP_ALIVE_PID_FILE_PATH_FORMAT + PID_FILE_NAME;

  // JAMES
  public static final String JAMES_TOP_FORMAT = "{0}/JAMES/";                                        // 0 = wildcard top

  public static final String JAMES_RUNTIME_PATH_FORMAT = JAMES_TOP_FORMAT + "/apps/james/Runtime/";  

  public static final String W2O_REMOTE_DELIVERY_FILE_NAME = "W2ORemoteDelivery.xml";
  public static final String W2O_REMOTE_DELIVERY_FILE_PATH_FORMAT = JAMES_RUNTIME_PATH_FORMAT + W2O_REMOTE_DELIVERY_FILE_NAME;  

  public static final String JAMES_CONFIG_FILE_NAME = "config.xml";
  public static final String JAMES_CONFIG_FILE_PATH_FORMAT = JAMES_TOP_FORMAT + "/apps/james/SAR-INF/" + JAMES_CONFIG_FILE_NAME;

  public static final String JAMES_PID_FILE_PATH_FORMAT = JAMES_TOP_FORMAT + "/apps/james/Runtime/";  
  public static final String JAMES_PID_FILE_PATH_NAME_FORMAT = JAMES_PID_FILE_PATH_FORMAT + PID_FILE_NAME;

  public static final String JAMES_BIN_PATH_FORMAT       = JAMES_TOP_FORMAT + "/bin/";
  public static final String JAMES_RUN_FILE_NAME = "run.bat";
  public static final String JAMES_RUN_FILE_PATH_FORMAT = JAMES_BIN_PATH_FORMAT + JAMES_RUN_FILE_NAME;
  
}
