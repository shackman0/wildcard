package com.World2One.SMTP;

/**
 * <p>Title: MissingParameterException</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public class MissingParameterException
  extends Exception
{

  public MissingParameterException()
  {
    super();
  }

  public MissingParameterException(String message)
  {
    super(message);
  }

  public MissingParameterException(String message, Throwable cause)
  {
    super(message, cause);
  }

  public MissingParameterException(Throwable cause)
  {
    super(cause);
  }

}
