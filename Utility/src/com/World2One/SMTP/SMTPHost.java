package com.World2One.SMTP;


import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.World2One.Utility.Constants;
import com.World2One.Utility.ToolBox;


/**
 * <p>Title: SMTPHost</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One, Inc.</p>
 */
public class SMTPHost
{

  protected static final String ELEMENT_SMTP_HOST = "SMTPHost";
  
  protected static final String ATTRIBUTE_APPLICATION_NAME          = "ApplicationName";            
  protected static final String ATTRIBUTE_SENDER_EMAIL              = "SenderEmail";            
  protected static final String ATTRIBUTE_HOST_ADDRESS              = "HostAddress";           
  protected static final String ATTRIBUTE_PORT                      = "Port";                   
  protected static final String ATTRIBUTE_USER                      = "User";                   
  protected static final String ATTRIBUTE_PASSWORD                  = "Password";               
  protected static final String ATTRIBUTE_SECURE_CONNECTION         = "SecureConnection";       

  
  
  public enum SecureConnection
  {
    None
    {
      @Override
      public String GetSMTPProtocol()
      {
        return "smtp";
      }

      @Override
      public boolean UseAuth()
      {
        return false;
      }

      @Override
      public int GetDefaultPort()
      {
        return 25;
      }
    },
    SSL
    {
      @Override
      public String GetSMTPProtocol()
      {
        return "smtps";
      }

      @Override
      public boolean UseAuth()
      {
        return true;
      }

      @Override
      public int GetDefaultPort()
      {
        return 465;
      }
    },
    TLS
    {
      @Override
      public String GetSMTPProtocol()
      {
        return "smtp";
      }

      @Override
      public boolean UseAuth()
      {
        return true;
      }

      @Override
      public int GetDefaultPort()
      {
        return 587;
      }
    };
    public abstract String GetSMTPProtocol();
    public abstract boolean UseAuth();
    public abstract int GetDefaultPort();
  }
  
  protected Element _smtpHostNode;
  
  
  
  public SMTPHost()
  {
    super();
  }
  
  public SMTPHost(Element smtpHostNode) 
  {
    super();

    _smtpHostNode = smtpHostNode;
  }
  
  public SMTPHost(
    String           applicationName,
    String           senderEmail,
    String           hostAddress,
    String           hostPort,
    String           userAccount,
    String           password,
    SecureConnection secureConnection )
  {
    super();

    _smtpHostNode = __document.createElement(ELEMENT_SMTP_HOST);
    
    _smtpHostNode.setAttribute(ATTRIBUTE_APPLICATION_NAME,applicationName);
    _smtpHostNode.setAttribute(ATTRIBUTE_SENDER_EMAIL,senderEmail);
    _smtpHostNode.setAttribute(ATTRIBUTE_HOST_ADDRESS,hostAddress);
    _smtpHostNode.setAttribute(ATTRIBUTE_PORT,hostPort);
    _smtpHostNode.setAttribute(ATTRIBUTE_USER,userAccount);
    _smtpHostNode.setAttribute(ATTRIBUTE_PASSWORD,password);
    _smtpHostNode.setAttribute(ATTRIBUTE_SECURE_CONNECTION,secureConnection.name());
  }

  public String GetApplicationName()
    throws MissingParameterException 
  {
    NamedNodeMap attributes = _smtpHostNode.getAttributes();
    Node applicationNameNode = attributes.getNamedItem(ATTRIBUTE_APPLICATION_NAME);
    String applicationNameNodeValue;
    if ( (applicationNameNode == null) ||
         ( (applicationNameNodeValue = applicationNameNode.getNodeValue().trim()).length() == 0) )
    {
      throw new MissingParameterException(_smtpHostNode.getLocalName() + " has no " + ATTRIBUTE_APPLICATION_NAME + " attribute.");
    }
    
    return applicationNameNodeValue;
  }
  
  public String GetSenderEmail() 
    throws MissingParameterException 
  {
    NamedNodeMap attributes = _smtpHostNode.getAttributes();
    Node senderEmailNode = attributes.getNamedItem(ATTRIBUTE_SENDER_EMAIL);
    String senderEmailNodeValue;
    if ( (senderEmailNode == null) ||
         ( (senderEmailNodeValue = senderEmailNode.getNodeValue().trim()).length() == 0) )
    {
      throw new MissingParameterException(_smtpHostNode.getLocalName() + " has no " + ATTRIBUTE_SENDER_EMAIL + "attribute.");
    }
    
    return senderEmailNodeValue;
  }

  public void SetSenderEmail(String senderEmail)
  {
    NamedNodeMap attributes = _smtpHostNode.getAttributes();
    Node senderEmailNode = attributes.getNamedItem(ATTRIBUTE_SENDER_EMAIL);
    senderEmailNode.setNodeValue(senderEmail);
  }
  
  public String GetHostAddress() 
    throws MissingParameterException
  {
    NamedNodeMap attributes = _smtpHostNode.getAttributes();
    Node hostAddressNode = attributes.getNamedItem(ATTRIBUTE_HOST_ADDRESS);
    String hostAddressNodeValue;
    if ( (hostAddressNode == null) ||
         ( (hostAddressNodeValue = hostAddressNode.getNodeValue().trim()).length() == 0) )
    {
      throw new MissingParameterException(_smtpHostNode.getLocalName() + " has no HostAddress attribute.");
    }
    
    return hostAddressNodeValue;
  }

  public void SetHostAddress(String hostAddress)
  {
    NamedNodeMap attributes = _smtpHostNode.getAttributes();
    Node hostAddressNode = attributes.getNamedItem(ATTRIBUTE_HOST_ADDRESS);
    hostAddressNode.setNodeValue(hostAddress);
  }
  
  public Integer GetPort() 
    throws MissingParameterException
  {
    NamedNodeMap attributes = _smtpHostNode.getAttributes();
    Node portNode = attributes.getNamedItem(ATTRIBUTE_PORT);
    String portNodeValue;
    if ( (portNode == null) ||
         ( (portNodeValue = portNode.getNodeValue().trim()).length() == 0) )
    {
      throw new MissingParameterException(_smtpHostNode.getLocalName() + " has no Port attribute.");
    }

    return Integer.valueOf(portNodeValue);
  }

  public void SetPort(String port)
  {
    NamedNodeMap attributes = _smtpHostNode.getAttributes();
    Node portNode = attributes.getNamedItem(ATTRIBUTE_PORT);
    portNode.setNodeValue(port);
  }
  
  public String GetUser()
  {
    NamedNodeMap attributes = _smtpHostNode.getAttributes();
    Node userNode = attributes.getNamedItem(ATTRIBUTE_USER);
    String userNodeValue = null;
    if ( (userNode != null) &&
         (userNode.getNodeValue().trim().length() > 0) )
    {
      userNodeValue = userNode.getNodeValue().trim();
    }
    
    return userNodeValue;
  }

  public void SetUser(String user)
  {
    NamedNodeMap attributes = _smtpHostNode.getAttributes();
    Node userNode = attributes.getNamedItem(ATTRIBUTE_USER);
    userNode.setNodeValue(user == null ? "" : user);
  }
  
  public String GetPassword()
    throws MissingParameterException
  {
    NamedNodeMap attributes = _smtpHostNode.getAttributes();
    Node passwordNode = attributes.getNamedItem(ATTRIBUTE_PASSWORD);
    String passwordNodeValue = null;
    if ( (passwordNode != null) &&
         (passwordNode.getNodeValue().trim().length() > 0) )
    {
      if (GetUser() == null)
      {
        throw new MissingParameterException(_smtpHostNode.getLocalName() + " has a Password attribute but no User attribute.");
      }

      passwordNodeValue = passwordNode.getNodeValue().trim();
    }
      
    return passwordNodeValue;
  }

  public void SetPassword(String password)
  {
    NamedNodeMap attributes = _smtpHostNode.getAttributes();
    Node passwordNode = attributes.getNamedItem(ATTRIBUTE_PASSWORD);
    passwordNode.setNodeValue(password == null ? "" : password);
  }
  
  public SecureConnection GetSecureConnection() 
    throws MissingParameterException
  {
    NamedNodeMap attributes = _smtpHostNode.getAttributes();
    Node secureConnectionNode = attributes.getNamedItem(ATTRIBUTE_SECURE_CONNECTION);
    String secureConnectionNodeValue;
    if ( (secureConnectionNode == null) ||
         ( (secureConnectionNodeValue = secureConnectionNode.getNodeValue().trim()).length() == 0) )
    {
      throw new MissingParameterException(_smtpHostNode.getLocalName() + " has no SecureConnection attribute.");
    }
    
    return SecureConnection.valueOf(secureConnectionNodeValue);
  }

  public void SetSecureConnection(SecureConnection secureConnection)
  {
    NamedNodeMap attributes = _smtpHostNode.getAttributes();
    Node secureConnectionNode = attributes.getNamedItem(ATTRIBUTE_SECURE_CONNECTION);
    secureConnectionNode.setNodeValue(secureConnection == null ? null : secureConnection.name());
  }

  public void AddToSMTPHostsOverrideDocument()
  {
    Element w2oRemoteDelivery = (Element) __document.getElementsByTagName("W2ORemoteDelivery").item(0);
    w2oRemoteDelivery.appendChild(_smtpHostNode);
  }
  
  /**
   * 
   * @return true = removed. false = not in document.
   */
  public boolean RemoveFromSMTPHostsOverrideDocument()
  {
    Element w2oRemoteDelivery = (Element) __document.getElementsByTagName("W2ORemoteDelivery").item(0);
    try
    {
      w2oRemoteDelivery.removeChild(_smtpHostNode);
    }
    catch (DOMException ex)
    {
      String message = ex.getMessage();
      if (message.startsWith("NOT_FOUND_ERR") == true)
      {
        return false;
      }
      throw ex;
    }
    
    return true;
  }
  
  public static class SMTPHostsOverridesKey
  {
    public String _applicationName = null;
    public String _senderEmail     = null;
    
    public SMTPHostsOverridesKey (
      String applicationName,
      String senderEmail )
    {
      super();
    
      if (applicationName != null)
      {
        _applicationName = applicationName.trim().toLowerCase();
      }
      
      if (senderEmail != null)
      {
        _senderEmail = senderEmail.trim().toLowerCase();
      }
    }
    
    @Override
    public int hashCode()
    {
      String string = toString();
      int hashCode = string.hashCode();
      
      return hashCode;
    }
    
    /**
     * a null field means "match any". e.g.
     *   [null,"foo@bar.com"] matches ["anything (including null)","foo@bar.com"]
     *   ["foo",null] matches ["foo","anything (including null)"]
     */
    @Override
    public boolean equals(Object other)
    {
      if (other == null)
      {
        return false;
      }
      
      if ( (other instanceof SMTPHostsOverridesKey) == false)
      {
        return false;
      }
      
      SMTPHostsOverridesKey otherSMTPHostsOverridesKey = (SMTPHostsOverridesKey)other;
      
      boolean equals = 
        ( ( (_applicationName == null) || 
            (otherSMTPHostsOverridesKey._applicationName == null) ||
            (_applicationName.trim().equals(otherSMTPHostsOverridesKey._applicationName.trim()) == true) ) &&
          ( (_senderEmail == null) ||
            (otherSMTPHostsOverridesKey._senderEmail == null) ||
            (_senderEmail.trim().equals(otherSMTPHostsOverridesKey._senderEmail.trim()) == true) ) );
      
      return equals;
    }
    
    @Override
    public String toString()
    {
      return _applicationName + "," + _senderEmail;
    }
  }
  
  public SMTPHostsOverridesKey CreateSMTPHostsOverridesKey() 
    throws MissingParameterException
  {
    return new SMTPHostsOverridesKey(GetApplicationName(),GetSenderEmail());
  }
  
  @Override
  public String toString()
  {
    StringBuilder stringBuilder = new StringBuilder();

    try
    {
      stringBuilder.append("SMTPHost")
      .append(Constants.CR_LF)
      .append("  senderEmail=\"")
      .append(GetSenderEmail())
      .append("\"")
      .append(Constants.CR_LF)
      .append("  hostAddress=\"")
      .append(GetHostAddress())
      .append("\"")
      .append(Constants.CR_LF)
      .append("  port=\"")
      .append(GetPort())
      .append("\"")
      .append(Constants.CR_LF)
      .append("  user=\"")
      .append(GetUser())
      .append("\"")
      .append(Constants.CR_LF)
      .append("  password=\"")
      .append(GetPassword())
      .append("\"")
      .append(Constants.CR_LF)
      .append("  secureConnection=\"")
      .append(GetSecureConnection())
      .append("\"")
      .append(Constants.CR_LF);
    }
    catch (MissingParameterException ex)
    {
      if (stringBuilder.length() > 0)
      {
        stringBuilder.append(" ");
      }
      stringBuilder.append(ex.getLocalizedMessage());
    }
    
    return stringBuilder.toString();
  }

  protected static Document __document = null;
  
  public static Document GetW2ORemoteDeliveryAsDocument(String w2oRemoteDeliveryFilePath) 
    throws ParserConfigurationException, SAXException, IOException
  {
    if (__document == null)
    {
      DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
      File propertiesFile = new File(w2oRemoteDeliveryFilePath);
      __document = docBuilder.parse(propertiesFile);
      __document.getDocumentElement().normalize();
    }
    
    return __document;
  }
  
  public static void WriteW2ORemoteDelivery(String w2oRemoteDeliveryFilePath) 
    throws TransformerException
  {
    File file = new File(w2oRemoteDeliveryFilePath);
    StreamResult streamResult = new StreamResult(file);
    ToolBox.WriteXML(__document, streamResult);
  }
  
  public static void ClearSMTPHostsOverrides(
    String        w2oRemoteDeliveryFilePath,
    StringBuilder debug) 
    throws ParserConfigurationException, SAXException, IOException, MissingParameterException, TransformerException
  {
    SMTPHostsOverridesTable smtpHosts = GetSMTPHostsOverrides(w2oRemoteDeliveryFilePath,debug);
    for (SMTPHost smtpHost: smtpHosts.Values())
    {
      smtpHost.RemoveFromSMTPHostsOverrideDocument();
    }
    WriteW2ORemoteDelivery(w2oRemoteDeliveryFilePath);
  }
  
  /**
   * 
   * @param debug
   * @return key = SMTPHostsOverridesKey, value = smtp account information
   * @throws IOException 
   * @throws SAXException 
   * @throws ParserConfigurationException 
   * @throws MissingParameterException 
   */
  public static SMTPHostsOverridesTable GetSMTPHostsOverrides(
    String        w2oRemoteDeliveryFilePath,
    StringBuilder debugStringBuilder) 
    throws ParserConfigurationException, SAXException, IOException, MissingParameterException
  {
    SMTPHostsOverridesTable smtpHostsOverride = new SMTPHostsOverridesTable();

    Document doc = GetW2ORemoteDeliveryAsDocument(w2oRemoteDeliveryFilePath);

    NodeList smtpHosts = doc.getElementsByTagName(ELEMENT_SMTP_HOST);
    int numberOfSMTPHosts = smtpHosts.getLength();

    if (numberOfSMTPHosts > 0)
    {
      for (int indx = 0; indx < numberOfSMTPHosts; indx++)
      {
        Element smtpHostNode = (Element)smtpHosts.item(indx);
        SMTPHost smtpHost = new SMTPHost(smtpHostNode);
        smtpHostsOverride.Put(smtpHost.CreateSMTPHostsOverridesKey(), smtpHost);      

        if (debugStringBuilder != null)
        {
          debugStringBuilder.append(Constants.CR_LF);
          debugStringBuilder.append(smtpHost.toString());
        }
      }
    }
    
    return smtpHostsOverride;
  }

  /**
   * <p>Title: SMTPHostsOverridesTable</p>
   * <p>Description: </p>
   * wrapper for a hashtable because the hashcodes between equal SMTPHostsOverridesKeys are not necessarily the same. 
   * hashtable.get() requires that the hashcodes be equal between keys.
   */
  public static class SMTPHostsOverridesTable
  {

    protected Hashtable<SMTPHostsOverridesKey,SMTPHost> _hashtable = new Hashtable();

    public SMTPHostsOverridesTable()
    {
      super();
    }
    
    public Enumeration<SMTPHostsOverridesKey> Keys()
    {
      return _hashtable.keys();
    }
    
    public Collection<SMTPHost> Values()
    {
      return _hashtable.values();
    }
    
    public boolean ContainsKey(SMTPHostsOverridesKey key)
    {
      Enumeration<SMTPHostsOverridesKey> keys = Keys();
      while (keys.hasMoreElements() == true)
      {
        SMTPHostsOverridesKey smtpHostsOverridesKey = keys.nextElement();
        if (smtpHostsOverridesKey.equals(key) == true)
        {
          return true;
        }
      }
      
      return false;
    }
    
    public SMTPHost Put(SMTPHostsOverridesKey key, SMTPHost value)
    {
      return _hashtable.put(key,value);
    }
    
    public SMTPHost Get(SMTPHostsOverridesKey parameterKey)
    {
      Enumeration<SMTPHostsOverridesKey> keys = Keys();
      while (keys.hasMoreElements() == true)
      {
        SMTPHostsOverridesKey hashtableKey = keys.nextElement();
        if (hashtableKey.equals(parameterKey) == true)
        {
          return _hashtable.get(hashtableKey); // use the key from the hashtable, not the parameter key
        }
      }
      
      return null;
    }
    
    public SMTPHost Remove(SMTPHostsOverridesKey parameterKey)
    {
      Enumeration<SMTPHostsOverridesKey> keys = Keys();
      while (keys.hasMoreElements() == true)
      {
        SMTPHostsOverridesKey hashtableKey = keys.nextElement();
        if (hashtableKey.equals(parameterKey) == true)
        {
          return _hashtable.remove(hashtableKey);   // use the key from the hashtable, not the parameter key
        }
      }
      
      return null;
    }
    
  }
   
}
