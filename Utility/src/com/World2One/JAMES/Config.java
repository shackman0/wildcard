package com.World2One.JAMES;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.World2One.Utility.ToolBox;

/**
 * <p>Title: Config</p>
 * <p>Description: manipulation routines for the JAMES config.xml file</p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One, Inc.</p>
 */
public class Config
{

  
  protected static final String ELEMENT_MAILET = "mailet";
  protected static final String ATTRIBUTE_CLASS = "class";
  
  protected static final String MAILET_CLASS_SIGNATURE_INJECTION_SERVICE = "W2OSignatureInjectionService";
  protected static final String MAILET_CLASS_JDBC_ARCHIVER               = "W2OJDBCArchiverService";
  protected static final String MAILET_CLASS_SOAP_ARCHIVER               = "W2OSOAPArchiverService";
  

  protected Document _jamesConfigDocument = null;
  
  
  public Config(String jamesConfigFilePath)
    throws ParserConfigurationException, SAXException, IOException
  {
    super();
    
    DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
    File jamesConfigFile = new File(jamesConfigFilePath);
    _jamesConfigDocument = docBuilder.parse(jamesConfigFile);
    _jamesConfigDocument.getDocumentElement().normalize();
  }


  public Document GetJamesConfigAsDocument() 
  {
    return _jamesConfigDocument;
  }

  /**
   * 
   * @return key = String mailet class. value = <mailet/> Element
   */
  public HashMap<String,Element> GetMailetElements()
  {
    HashMap<String,Element> mailetElements = null;
    
    NodeList mailetElementsList = _jamesConfigDocument.getElementsByTagName(ELEMENT_MAILET);
    int numberOfMailetNodes = mailetElementsList.getLength();

    if (numberOfMailetNodes > 0)
    {
      mailetElements = new HashMap();

      for (int indx = 0; indx < numberOfMailetNodes; indx++)
      {
        Element mailetElement = (Element)mailetElementsList.item(indx);
        NamedNodeMap mailetAttributes = mailetElement.getAttributes();
        String mailetClass = mailetAttributes.getNamedItem(ATTRIBUTE_CLASS).getNodeValue();
        mailetElements.put(mailetClass, mailetElement);      
      }
    }

    return mailetElements;
  }
  
  public void Write(String jamesConfigFilePath) 
    throws TransformerException
  {
    File file = new File(jamesConfigFilePath);
    StreamResult streamResult = new StreamResult(file);
    ToolBox.WriteXML(_jamesConfigDocument, streamResult);
  }
  
}
