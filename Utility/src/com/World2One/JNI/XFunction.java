package com.World2One.JNI;

import com.excelsior.xFunction.Argument;
import com.excelsior.xFunction.FunctionNotFoundException;
import com.excelsior.xFunction.IllegalSignatureException;
import com.excelsior.xFunction.IncompatibleArgumentTypeException;
import com.excelsior.xFunction.LibraryNotFoundException;
import com.excelsior.xFunction.WrongArgumentNumberException;
import com.excelsior.xFunction.xFunction;

/**
 * <p>Title: XFunction</p>
 * <p>Description:</p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public class XFunction<RETURN_TYPE>
  extends xFunction
{

  
  public static XFunction Create (
    String      dll,
    String      method)
  {
    try
    {
      return new XFunction(dll, method);
    }
    catch (com.excelsior.xFunction.FunctionNotFoundException ex)
    {
      throw new RuntimeException(ex);
    }
    catch (com.excelsior.xFunction.LibraryNotFoundException ex)
    {
      throw new RuntimeException(ex);
    }
    catch (com.excelsior.xFunction.IllegalSignatureException ex)
    {
      throw new RuntimeException(ex);
    }
  }

  
  
  public XFunction (
    String      dll,
    String      method ) 
    throws LibraryNotFoundException, FunctionNotFoundException, IllegalSignatureException
  {
    super(dll,method);
  }

  
  public RETURN_TYPE Invoke(Argument... arguments) 
  {
    try
    {
      return (RETURN_TYPE)invoke(arguments);
    }
    catch (WrongArgumentNumberException ex)
    {
      throw new RuntimeException(ex);
    }
    catch (IncompatibleArgumentTypeException ex)
    {
      throw new RuntimeException(ex);
    }
  }
  
  public RETURN_TYPE Invoke()
  {
    try
    {
      return (RETURN_TYPE)invoke();
    }
    catch (WrongArgumentNumberException ex)
    {
      throw new RuntimeException(ex);
    }
    catch (IncompatibleArgumentTypeException ex)
    {
      throw new RuntimeException(ex);
    }
  }


}
