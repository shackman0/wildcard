package com.World2One.JNI.Win32;

import com.World2One.JNI.XFunction;
import com.World2One.Utility.MutableInteger;
import com.excelsior.xFunction.Argument;
import com.excelsior.xFunction.IllegalSignatureException;
import com.excelsior.xFunction.IllegalStringConversionException;
import com.excelsior.xFunction.NullDereferenceException;
import com.excelsior.xFunction.Pointer;


/**
 * <p>Title: Advapi32</p>
 * <p>Description: see: http://msdn.microsoft.com/en-us/library/ms724875(VS.85).aspx</p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public class Advapi32
{

  protected static final String ADVAPI32 = "Advapi32";
//  static
//  {
//    ExternalLibrariesLoader.LoadSystemLibraries(ADVAPI32);
//  }
  


  /**
   * see: http://www.serverwatch.com/tutorials/article.php/1476831
   */
  public enum PredefinedKey
  {
    HKEY_CLASSES_ROOT
    {
      @Override
      public int GetValue()
      {
        return 0x80000000;
      }
    },
    HKEY_CURRENT_USER
    {
      @Override
      public int GetValue()
      {
        return 0x80000001;
      }
    },
    HKEY_LOCAL_MACHINE
    {
      @Override
      public int GetValue()
      {
        return 0x80000002;
      }
    },
    HKEY_USERS
    {
      @Override
      public int GetValue()
      {
        return 0x80000003;
      }
    },
    HKEY_CURRENT_CONFIG
    {
      @Override
      public int GetValue()
      {
        return 0x80000005;
      }
    };
    
//    HKEY_PERFORMANCE_DATA,
//    HKEY_PERFORMANCE_NLSTEXT,
//    HKEY_PERFORMANCE_TEXT,
    
    public abstract int GetValue();
  }
  
  private Advapi32()
  {
    super();
  }
  
  /**
   * see: http://msdn.microsoft.com/en-us/library/ms724837(VS.85).aspx
   * 
   * C++
   * LONG WINAPI RegCloseKey( __in  HKEY hKey );
   * 
   * @param key
   * @return
   */
  protected static final String REG_CLOSE_KEY_SIGNATURE = "long RegCloseKey(int)";
  public static long RegCloseKey(int registryKeyHandle) 
  {
    XFunction<Long> regCloseKey = CreateFunction(REG_CLOSE_KEY_SIGNATURE);
    
    Argument hKey = new Argument(registryKeyHandle);
    
    long rc = regCloseKey.Invoke(hKey);
    
    return rc;
  }
  
  /**
   * see: http://msdn.microsoft.com/en-us/library/ms724840(VS.85).aspx
   * 
   * C++
   * LONG WINAPI RegConnectRegistry (
   *   __in_opt  LPCTSTR lpMachineName,
   *   __in      HKEY hKey,
   *   __out     PHKEY phkResult );
   *   
   * @param machineName
   * @param key may only be: HKEY_LOCAL_MACHINE, HKEY_PERFORMANCE_DATA, HKEY_USERS 
   * @return
   */
  protected static final String REG_CONNEXT_REGISTRY_SIGNATURE = "long RegConnectRegistry(CSTRING,int,int*)";
  public static long RegConnectRegistry(
    String                     machineName,
    PredefinedKey              predefinedKey,
    MutableInteger             registryKeyHandle ) 
    throws IllegalStringConversionException
  {
    try
    {
      XFunction<Long> regConnectRegistry = CreateFunction(REG_CONNEXT_REGISTRY_SIGNATURE);
      
      Argument lpMachineName = new Argument(null,Argument.CSTRING);
      Argument hKey = new Argument(predefinedKey.name(),Argument.CSTRING);
      Pointer phkResult = Pointer.create("int*");
      
      long rc = regConnectRegistry.Invoke(lpMachineName,hKey,phkResult);
      
      if (phkResult.isNull() == true)
      {
        registryKeyHandle._integer = null;
      }
      else
      {
        registryKeyHandle._integer = (Integer) phkResult.deref();
      }
      
      return rc;
    }
    catch (IllegalSignatureException ex)
    {
      throw new RuntimeException(ex);
    }
    catch (NullDereferenceException ex)
    {
      throw new RuntimeException(ex);
    }
  }
  
  /**
   * see: http://msdn.microsoft.com/en-us/library/ms724844(VS.85).aspx
   * 
   * C++
   * LONG WINAPI RegCreateKeyEx(
   *   __in        HKEY hKey,
   *   __in        LPCTSTR lpSubKey,
   *   __reserved  DWORD Reserved,
   *   __in_opt    LPTSTR lpClass,
   *   __in        DWORD dwOptions,
   *   __in        REGSAM samDesired,
   *   __in_opt    LPSECURITY_ATTRIBUTES lpSecurityAttributes,
   *   __out       PHKEY phkResult,
   *   __out_opt   LPDWORD lpdwDisposition
   * );
   * 
   * @param key may only be: HKEY_CLASSES_ROOT, HKEY_CURRENT_CONFIG, HKEY_CURRENT_USER, HKEY_LOCAL_MACHINE, HKEY_USERS
   *  
   */
  public static long RegCreateKeyEx(
    int         registryKey,
    String      subKey,
    String      keyClassType 
    /* todo */ )
  {
    // todo
    return -1;
  }
  
  /**
   * see: http://msdn.microsoft.com/en-us/library/ms724845(VS.85).aspx
   * 
   * C++
   * LONG WINAPI RegDeleteKey(
   *   __in  HKEY hKey,
   *   __in  LPCTSTR lpSubKey
   * );
   * 
   */
  public static void RegDeleteKey(/* todo */)
  {
    // todo
  }
  
  /**
   * see: http://msdn.microsoft.com/en-us/library/ms724847(VS.85).aspx
   * 
   * C++
   * LONG WINAPI RegDeleteKeyEx(
   *   __in        HKEY hKey,
   *   __in        LPCTSTR lpSubKey,
   *   __in        REGSAM samDesired,
   *   __reserved  DWORD Reserved
   * );
   * 
   */
  public static void RegDeleteKeyEx(/* todo */)
  {
    // todo
  }
  
  /**
   * see: http://msdn.microsoft.com/en-us/library/ms724848(VS.85).aspx
   * 
   * C++
   * LONG WINAPI RegDeleteKeyValue(
   *   __in      HKEY hKey,
   *   __in_opt  LPCTSTR lpSubKey,
   *   __in_opt  LPCTSTR lpValueName
   * );
   * 
   */
  public static void RegDeleteKeyValue(/* todo */)
  {
    // todo
  }
  
  /**
   * see: http://msdn.microsoft.com/en-us/library/ms724851(VS.85).aspx
   * 
   * C++
   * LONG WINAPI RegDeleteValue(
   *   __in      HKEY hKey,
   *   __in_opt  LPCTSTR lpValueName
   * );
   * 
   */
  public static void RegDeleteValue(/* todo */)
  {
    // todo
  }
  
  /**
   * see: http://msdn.microsoft.com/en-us/library/ms724862(VS.85).aspx
   * 
   * C++
   * LONG WINAPI RegEnumKeyEx(
   *   __in         HKEY hKey,
   *   __in         DWORD dwIndex,
   *   __out        LPTSTR lpName,
   *   __inout      LPDWORD lpcName,
   *   __reserved   LPDWORD lpReserved,
   *   __inout      LPTSTR lpClass,
   *   __inout_opt  LPDWORD lpcClass,
   *   __out_opt    PFILETIME lpftLastWriteTime
   * );
   * 
   */
  public static void RegEnumKeyEx(/* todo */)
  {
    // todo
  }
  
  /**
   * see: http://msdn.microsoft.com/en-us/library/ms724865(VS.85).aspx
   * 
   * C++
   * LONG WINAPI RegEnumValue(
   *   __in         HKEY hKey,
   *   __in         DWORD dwIndex,
   *   __out        LPTSTR lpValueName,
   *   __inout      LPDWORD lpcchValueName,
   *   __reserved   LPDWORD lpReserved,
   *   __out_opt    LPDWORD lpType,
   *   __out_opt    LPBYTE lpData,
   *   __inout_opt  LPDWORD lpcbData
   * );
   * 
   */
  public static void RegEnumValue(/* todo */)
  {
    // todo
  }
  
  /**
   * see: http://msdn.microsoft.com/en-us/library/ms724867(VS.85).aspx
   * 
   * C++
   * LONG WINAPI RegFlushKey(
   *   __in  HKEY hKey
   * );
   * 
   */
  public static void RegFlushKey(/* todo */)
  {
    // todo
  }
  
  /**
   * see: http://msdn.microsoft.com/en-us/library/ms724868(VS.85).aspx
   * 
   * C++
   * LONG WINAPI RegGetValue(
   *   __in         HKEY hkey,
   *   __in_opt     LPCTSTR lpSubKey,
   *   __in_opt     LPCTSTR lpValue,
   *   __in_opt     DWORD dwFlags,
   *   __out_opt    LPDWORD pdwType,
   *   __out_opt    PVOID pvData,
   *   __inout_opt  LPDWORD pcbData
   * );
   * 
   */
  public static void RegGetValue(/* todo */)
  {
    // todo
  }
  
  /**
   * see: http://msdn.microsoft.com/en-us/library/ms724889(VS.85).aspx
   * 
   * C++
   * LONG WINAPI RegLoadKey(
   *   __in      HKEY hKey,
   *   __in_opt  LPCTSTR lpSubKey,
   *   __in      LPCTSTR lpFile
   * );
   * 
   */
  public static void RegLoadKey(/* todo */)
  {
    // todo
  }
  
  /**
   * see: http://msdn.microsoft.com/en-us/library/ms724894(VS.85).aspx
   * 
   * C++
   * LONG WINAPI RegOpenCurrentUser(
   *   __in   REGSAM samDesired,
   *   __out  PHKEY phkResult
   * );
   * 
   */
  public static void RegOpenCurrentUser(/* todo */)
  {
    // todo
  }
  
  /**
   * see: http://msdn.microsoft.com/en-us/library/ms724897(VS.85).aspx
   * 
   * C++
   * LONG WINAPI RegOpenKeyEx(
   *   __in        HKEY hKey,
   *   __in_opt    LPCTSTR lpSubKey,
   *   __reserved  DWORD ulOptions,
   *   __in        REGSAM samDesired,
   *   __out       PHKEY phkResult
   * );
   * 
   */
  public static void RegOpenKeyEx(/* todo */)
  {
    // todo
  }
  
  /**
   * see: http://msdn.microsoft.com/en-us/library/ms724899(VS.85).aspx
   * 
   * C++
   * LONG WINAPI RegOpenUserClassesRoot(
   *   __in        HANDLE hToken,
   *   __reserved  DWORD dwOptions,
   *   __in        REGSAM samDesired,
   *   __out       PHKEY phkResult
   * );
   * 
   */
  public static void RegOpenUserClassesRoot(/* todo */)
  {
    // todo
  }
  
  /**
   * see: http://msdn.microsoft.com/en-us/library/ms724902(VS.85).aspx
   * 
   * C++
   * LONG WINAPI RegQueryInfoKey(
   *   __in         HKEY hKey,
   *   __out        LPTSTR lpClass,
   *   __inout_opt  LPDWORD lpcClass,
   *   __reserved   LPDWORD lpReserved,
   *   __out_opt    LPDWORD lpcSubKeys,
   *   __out_opt    LPDWORD lpcMaxSubKeyLen,
   *   __out_opt    LPDWORD lpcMaxClassLen,
   *   __out_opt    LPDWORD lpcValues,
   *   __out_opt    LPDWORD lpcMaxValueNameLen,
   *   __out_opt    LPDWORD lpcMaxValueLen,
   *   __out_opt    LPDWORD lpcbSecurityDescriptor,
   *   __out_opt    PFILETIME lpftLastWriteTime
   * );
   * 
   */
  public static void RegQueryInfoKey(/* todo */)
  {
    // todo
  }
  
  /**
   * see: http://msdn.microsoft.com/en-us/library/ms724911(VS.85).aspx
   * 
   * C++
   * LONG WINAPI RegQueryValueEx(
   *   __in         HKEY hKey,
   *   __in_opt     LPCTSTR lpValueName,
   *   __reserved   LPDWORD lpReserved,
   *   __out_opt    LPDWORD lpType,
   *   __out_opt    LPBYTE lpData,
   *   __inout_opt  LPDWORD lpcbData
   * );
   * 
   */
  public static void RegQueryValueEx(/* todo */)
  {
    // todo
  }
  
  /**
   * see: http://msdn.microsoft.com/en-us/library/ms724913(VS.85).aspx
   * 
   * C++
   * LONG WINAPI RegReplaceKey(
   *   __in      HKEY hKey,
   *   __in_opt  LPCTSTR lpSubKey,
   *   __in      LPCTSTR lpNewFile,
   *   __in      LPCTSTR lpOldFile
   * );
   * 
   */
  public static void RegReplaceKey(/* todo */)
  {
    // todo
  }
  
  /**
   * see: http://msdn.microsoft.com/en-us/library/ms724915(VS.85).aspx
   * 
   * C++
   * LONG WINAPI RegRestoreKey(
   *   __in  HKEY hKey,
   *   __in  LPCTSTR lpFile,
   *   __in  DWORD dwFlags
   * );
   * 
   */
  public static void RegRestoreKey(/* todo */)
  {
    // todo
  }
  
  /**
   * see: http://msdn.microsoft.com/en-us/library/ms724917(VS.85).aspx
   * 
   * C++
   * LONG WINAPI RegSaveKey(
   *   __in      HKEY hKey,
   *   __in      LPCTSTR lpFile,
   *   __in_opt  LPSECURITY_ATTRIBUTES lpSecurityAttributes
   * );
   * 
   */
  public static void RegSaveKey(/* todo */)
  {
    // todo
  }
  
  /**
   * see: http://msdn.microsoft.com/en-us/library/ms724919(VS.85).aspx
   * 
   * C++
   * LONG WINAPI RegSaveKeyEx(
   *   __in      HKEY hKey,
   *   __in      LPCTSTR lpFile,
   *   __in_opt  LPSECURITY_ATTRIBUTES lpSecurityAttributes,
   *   __in      DWORD Flags
   * );
   * 
   */
  public static void RegSaveKeyEx(/* todo */)
  {
    // todo
  }
  
  /**
   * see: http://msdn.microsoft.com/en-us/library/aa379314(VS.85).aspx
   * 
   * C++
   * LONG WINAPI RegSetKeySecurity(
   *   __in  HKEY hKey,
   *   __in  SECURITY_INFORMATION SecurityInformation,
   *   __in  PSECURITY_DESCRIPTOR pSecurityDescriptor
   * );
   * 
   */
  public static void RegSetKeySecurity(/* todo */)
  {
    // todo
  }
  
  /**
   * see: http://msdn.microsoft.com/en-us/library/ms724921(VS.85).aspx
   * 
   * C++
   * LONG WINAPI RegSetKeyValue(
   *   __in      HKEY hKey,
   *   __in_opt  LPCTSTR lpSubKey,
   *   __in_opt  LPCTSTR lpValueName,
   *   __in      DWORD dwType,
   *   __in_opt  LPCVOID lpData,
   *   __in      DWORD cbData
   * );
   * 
   */
  public static void RegSetKeyValue(/* todo */)
  {
    // todo
  }
  
  /**
   * see: http://msdn.microsoft.com/en-us/library/ms724923(VS.85).aspx
   * 
   * C++
   * LONG WINAPI RegSetValueEx(
   *   __in        HKEY hKey,
   *   __in_opt    LPCTSTR lpValueName,
   *   __reserved  DWORD Reserved,
   *   __in        DWORD dwType,
   *   __in_opt    const BYTE *lpData,
   *   __in        DWORD cbData
   * );
   * 
   */
  public static void RegSetValueEx(/* todo */)
  {
    // todo
  }
  
  /**
   * see: http://msdn.microsoft.com/en-us/library/ms724924(VS.85).aspx
   * 
   * C++
   * LONG WINAPI RegUnLoadKey(
   *   __in      HKEY hKey,
   *   __in_opt  LPCTSTR lpSubKey
   * );
   * 
   */
  public static void RegUnLoadKey(/* todo */)
  {
    // todo
  }

  protected static XFunction CreateFunction(String functionSignature)
  {
    return XFunction.Create(ADVAPI32, functionSignature);
  }
  

}
