package com.World2One.OS;

import java.io.IOException;
import java.text.MessageFormat;

/**
 * <p>Title: WindowsProcess</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public class WindowsProcess
  extends AbstractProcess
{
  
  public WindowsProcess(
    String     processName,
    Integer    processID )
  {
    super(processName,processID);
  }
  
  protected static final String TASKKILL_COMMAND = "taskkill /PID {0} /T /F";
  
  @Override
  public boolean Kill()
  {
    boolean success = false;
    
    String commandLine = MessageFormat.format(TASKKILL_COMMAND, String.valueOf(_processID));
    try
    {
      Runtime.getRuntime().exec(commandLine);
      success = true;
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
    
    return success;
  }
  
  protected static final String TASKLIST_PID_EQ_COMMAND = "tasklist /FI \"PID eq {0}\" /NH";
  
  @Override
  public boolean IsRunning()
  {
    return IsRunning(_processID);
  }
  
  public static boolean IsRunning(int pid)
  {
    boolean isRunning = false;

    try
    {
      String commandLine = MessageFormat.format(TASKLIST_PID_EQ_COMMAND, String.valueOf(pid));
      String taskListResults = NetworkInfo.RunCommand(commandLine);
      System.out.println("INFO: WindowsProcess.IsRunning(" + String.valueOf(pid) + "): " + TASKLIST_PID_EQ_COMMAND + " returned \"" + taskListResults + "\"");
      isRunning = (taskListResults.toLowerCase().contains("no tasks") == false);
    }
    catch (IOException ex)
    {
      // do nothing
    }

    return isRunning;
  }

  public static void main(String... args)
  {
    System.out.println("IsRunning " + IsRunning(5416));
  }
}
