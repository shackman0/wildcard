package com.World2One.OS;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.HashSet;

/**
 * <p>Title: WindowsProcessManager</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public class WindowsProcessManager
  extends AbstractProcessManager
{

  protected WindowsProcessManager()
  {
    super();
  }

  protected static final String FIND_PROCESSES_NAME_TASKLIST_COMMAND = "tasklist /NH /FI \"IMAGENAME eq {0}\" /FO csv";

  @Override
  public HashSet<AbstractProcess> FindProcesses(String wildcardName)
  {
    HashSet<AbstractProcess> processes = new HashSet();

    String commandLine = MessageFormat.format(FIND_PROCESSES_NAME_TASKLIST_COMMAND, wildcardName);
    try
    {
      String output = NetworkInfo.RunCommand(commandLine).trim();
      String[] lines = output.split("\n\r");
      if (lines[0].equalsIgnoreCase("INFO: No tasks are running which match the specified criteria.") == false)
      {
        for (String line: lines)
        {
          String[] csvValues = line.split(",");
          if (csvValues.length > 0)
          {
            String processName = csvValues[0].split("\"")[1];
            String pid = csvValues[1].split("\"")[1];
            WindowsProcess windowsProcess = new WindowsProcess(processName,Integer.valueOf(pid));
            processes.add(windowsProcess);
          }
        }
      }
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
    
    return processes;
  }

  @Override
  public WindowsProcess CreateProcess(String processName, Integer processID)
  {
    return new WindowsProcess(processName,processID);
  }

  protected static final String GET_MY_PROCESS_NAME_TASKLIST_COMMAND = "tasklist /NH /FI \"PID eq {0}\" /FO csv";
  
  @Override
  protected String GetMyProcessName()
  {
    String myProcessName = null;
    
    try
    {
      int myProcessID = GetMyProcessID();
      String commandLine = MessageFormat.format(GET_MY_PROCESS_NAME_TASKLIST_COMMAND, String.valueOf(myProcessID));
      String output = NetworkInfo.RunCommand(commandLine).trim();
      if (output.length() > 0)
      {
        String[] csvValues = output.split(",");
        myProcessName = csvValues[0].split("\"")[1];
      }
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
    
    return myProcessName;
  }

  /**
   * unit test
   * @param args
   */
//  public static void main(String... args)
//  {
//    WindowsProcessManager windowsProcessManager = new WindowsProcessManager();
//    System.out.println(windowsProcessManager.FindProcesses("winmail*"));
//  }
  
}
