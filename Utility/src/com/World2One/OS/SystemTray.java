package com.World2One.OS;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.Menu;
import java.awt.PopupMenu;
import java.awt.Toolkit;
import java.awt.TrayIcon;

/**
 * <p>Title: SystemTray</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One, Inc.</p>
 */
public class SystemTray
{
  
  protected static final String W2O_TRAY_ICON_PATH = "../apps/james/Runtime/W2O_20x23.png";
  
  protected static SystemTray __systemTray = new SystemTray();
  
  public static SystemTray GetSingleton()
  {
    return __systemTray;
  }
  
  
  protected boolean     _installed = false;
  protected PopupMenu   _popupMenu = null;
  protected TrayIcon    _trayIcon = null;
  
  
  private SystemTray()
  {
    super();
  }

  public void InstallToSystemTray() 
    throws AWTException
  {
    if (java.awt.SystemTray.isSupported() == true)
    {
      if (_installed == false)
      {
        if (_popupMenu == null)
        {
          _popupMenu = new PopupMenu();
        }
        if (_trayIcon == null)
        {
          Image image = Toolkit.getDefaultToolkit().getImage(W2O_TRAY_ICON_PATH);
          _trayIcon = new TrayIcon(image, "W2O", _popupMenu);
          _trayIcon.setToolTip("World2One Local Email Server");
          _trayIcon.setImageAutoSize(true);
        }

        java.awt.SystemTray javaSystemTray = java.awt.SystemTray.getSystemTray();
        javaSystemTray.add(_trayIcon);
        
        _installed = true;
      }
    }
  }
  
  public void RemoveFromSystemTray()
  {
    if (java.awt.SystemTray.isSupported() == true)
    {
      if (_installed == true)
      {
        _installed = false;  
        java.awt.SystemTray javaSystemTray = java.awt.SystemTray.getSystemTray();
        javaSystemTray.remove(_trayIcon);
      }
    }
  }
  
  public void AddMenu(Menu menu) 
    throws AWTException
  {
    if (java.awt.SystemTray.isSupported() == true)
    {
      InstallToSystemTray();
      _popupMenu.add(menu);
    }
  }
  
}
