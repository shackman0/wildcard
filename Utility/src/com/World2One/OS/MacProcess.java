package com.World2One.OS;

/**
 * TODO
 * 
 * <p>Title: MacProcess</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public abstract class MacProcess
  extends AbstractProcess
{
  
  public MacProcess(
    String processName,
    int    processID )
  {
    super(processName,processID);
  }

}
