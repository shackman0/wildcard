package com.World2One.OS;

import java.io.File;

/**
 * <p>Title: AbstractExternalLibraries</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One, Inc.</p>
 */
public class ExternalLibrariesLoader
{

  private ExternalLibrariesLoader()
  {
    super();
  }
  
  /**
   * it is up to the child class to determine when to call this. 
   *
   * @See http://groups-beta.google.com/group/borland.public.jbuilder.compiler/browse_thread/thread/457b522f351326c2/cd82825fb562d843?lnk=st&q=java.library.path+modify&rnum=7&hl=en#cd82825fb562d843
   *
   * When Windows is asked to locate a library and the LoadLibrary call
   * does not specify a path, it searches in the following directories:
   *
   *     1. The directory from which the application loaded.
   *     2. The current directory.
   *     3. The System directory, as returned by GetSystemDirectory().
   *        This is normally set to "C:/Windows/System32" for Windows NT,
   *        and "C:/Windows/System" for Windows 95/98.
   *     4. For Windows NT only, /Windows/System.
   *     5. The Windows Directory, as returned by GetWindowsDirectory().
   *        This is normally set to "C:/Windows".
   *     6. The PATH environment variable.
   *
   * But this really does not matter, since the Sun Java runtime always
   * locates the library file itself, and specifies the full path to the
   * Windows LoadLibrary() function.
   *
   * When System.loadLibrary() is called, it determines a file name based on
   * the supplied library name.  This file name is system dependent: for
   * Windows it appends ".dll" to the library name, and for Solaris it
   * prepends "lib" and appends ".so".  For debug versions it appends "_g"
   * first.
   *
   * System.loadLibrary() forwards the call to the RunTime class, which in
   * turn forwards the call to the ClassLoader for the class that the
   * loadLibrary call is made from.  ClassLoader has a method named
   * findLibrary() which may be overridden to return the path to the library
   * file.  Most ClassLoaders do not override _proxy method, but the extension
   * ClassLoader uses _proxy mechanism to load libraries for java extensions, by
   * looking in the path specified by the system property "java.ext.dirs".
   *
   * If findLibrary does not find the library, the ClassLoader looks in the
   * path specified by the system property "sun.boot.library.path", which is
   * initialized to point to the JRE's /bin directory.
   *
   * If the library is not found there, it then looks in the path specified by
   * the system property "java.library.path".
   *
   * On Windows, "java.library.path" is initialized as follows.  Note that
   * this is almost identical to the Windows library lookup, but it excludes
   * the "/Windows/System" directory under Windows NT.
   *
   *     1. The directory from which application is loaded.
   *     2. The current directory
   *     3. The System directory (GetSystemDirectory)
   *     4. The Windows directory (GetWindowsDirectory)
   *     5. The PATH environment variable
   *
   * For Solaris "java.library.path" is initialized to:
   *
   *     1. The value of the environment variable "LD_LIBRARY_PATH".
   *     2. "/usr/lib"
   *
   */
  
  public static void LoadLibraries(
    String    path2Libraries,
    String... libraryNames )
  {
    for (String libraryName : libraryNames)
    {
      try
      {
//        System.out.print("attempting System.loadLibrary(" + libraryName + ") ...");
        System.loadLibrary(libraryName);
//        System.out.println("success!");
      }
      catch (UnsatisfiedLinkError ex)
      {
        if (path2Libraries == null)
        {
          throw ex;
        }
//        System.out.println("FAILED!");
        File file = new File(path2Libraries);
        String absolutePath2Libraries = file.getAbsolutePath() + File.separator;
        String theDLLpath = absolutePath2Libraries + libraryName + ".dll";
//        System.out.print("attempting System.load(" + theDLLpath + ")");
        try
        {
          System.load(theDLLpath);
//          System.out.println("success!");
        }
        catch (UnsatisfiedLinkError ex1)
        {
//          System.out.println("FAILED!");
          throw ex1;
        }
      }
    }
  }

  public static void LoadSystemLibraries(String... libraryNames)
  {
    LoadLibraries(null,libraryNames);
  }
}
