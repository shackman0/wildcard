package com.World2One.OS;

import java.io.IOException;

/**
 * <p>Title: PIDFileNotFoundException</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public class PIDFileNotFoundException
  extends IOException
{
  
  public PIDFileNotFoundException(String message)
  {
    super(message);
  }

}
