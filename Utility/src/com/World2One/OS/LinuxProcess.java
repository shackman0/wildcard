package com.World2One.OS;

/**
 * TODO
 * 
 * <p>Title: LinuxProcess</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public abstract class LinuxProcess
  extends AbstractProcess
{

  public LinuxProcess(
    String processName,
    int    processID )
  {
    super(processName,processID);
  }
  
}
