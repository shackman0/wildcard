package com.World2One.OS;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.HashSet;

import com.World2One.Utility.MutableInteger;

/**
 * <p>Title: ProcessManager</p>
 * <p>Description: used to query and manipulate processes running under this Operating System</p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public abstract class AbstractProcessManager
{
  
  protected static AbstractProcessManager __singleton;
  static
  {
    // create the ProcessManager appropriate for this system
    if (OperatingSystemInfo.IsWindows() == true)
    {
      __singleton = new WindowsProcessManager();
    }
//    else
//    if (OperatingSystemInfo.IsLinux() == true)
//    {
//      __singleton = new LinuxProcessManager();
//    }
//    else
//    if (OperatingSystemInfo.IsMacOSX() == true)
//    {
//      __singleton = new MacProcessManager();
//    }
    else
    {
      throw new RuntimeException("Unsupported Operating System");
    }
  }
  
  public static AbstractProcessManager GetSingleton()
  {
    return __singleton;
  }
  
  
  /**
   * 
   * @param pidFilePathName
   * @throws IOException 
   */
  public static void WriteMyPIDToPIDFile(String pidFilePathName) 
    throws IOException
  {
    AbstractProcess myProcess = __singleton.GetMyProcess();
    myProcess.WritePIDFile(pidFilePathName);
  }

  public static void DeleteMyPIDFile(String pidFilePathName) 
  {
    AbstractProcess myProcess = __singleton.GetMyProcess();
    myProcess.DeletePIDFile(pidFilePathName);
  }

  /**
   * creates a process having the specified processName and the pid found in pidFilePathName
   * @param pidFilePathName
   * @param processName
   * @return
   * @throws IOException
   */
  public static AbstractProcess CreateProcess(
    String pidFilePathName,
    String processName ) 
    throws IOException, PIDFileNotFoundException
  {
    AbstractProcess process = __singleton.CreateProcess(processName,(Integer)null);
    Integer pid = process.ReadPIDFile(pidFilePathName);
    if (pid == null)
    {
      throw new PIDFileNotFoundException(pidFilePathName);
    }
    
    return process;
  }

  public static boolean IsRunning(
    String         pidFilePathName,
    MutableInteger processID ) 
    throws PIDFileNotFoundException, IOException
  {
    AbstractProcess process = CreateProcess(pidFilePathName,"is running?");
    boolean processIsRunning = process.IsRunning();
    if (processIsRunning == true)
    {
      processID._integer = process.GetProcessID();
    }
    else
    {
      processID._integer = null;
    }

    return processIsRunning;
  }
  

  
  protected AbstractProcessManager()
  {
    super();
  }

  public AbstractProcess GetMyProcess()
  {
    String myProcessName = GetMyProcessName();
    Integer myProcessID = GetMyProcessID();

    AbstractProcess myProcess = CreateProcess(myProcessName,myProcessID);
    
    return myProcess;
  }
  
  protected Integer GetMyProcessID()
  {
    RuntimeMXBean runtimeMXBean = ManagementFactory.getRuntimeMXBean();
    String processName = runtimeMXBean.getName(); // returns pid@hostname
    int index = processName.indexOf("@");
    String pidString = processName.substring(0,index);
    int myProcessID = Integer.valueOf(pidString);
    
    return myProcessID;
  }
  
  protected abstract String GetMyProcessName();
  
  public abstract AbstractProcess CreateProcess(String processName, Integer processID);
  
  public abstract HashSet<AbstractProcess> FindProcesses(String wildcardName);
  
  public void FindProcesses(String wildcardName, HashSet<AbstractProcess> processes)
  {
    processes.addAll(FindProcesses(wildcardName));
  }
  
}
