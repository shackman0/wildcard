package com.World2One.OS;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * <p>Title: AbstractProcess</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public abstract class AbstractProcess
{

  protected String    _processName = null;
  protected Integer   _processID = null;
  
  
  public AbstractProcess(
    String     processName,
    Integer    processID )
  {
    super();
    
    _processName = processName;
    _processID = processID;
  }
  
  public String GetProcessName()
  {
    return _processName;
  }
  
  public int GetProcessID()
  {
    return _processID;
  }

  
  /**
   * 
   * @param pidFilePathName
   * @return null means the pidFilePathName file does not exist or an error occurred. otherwise returns the contents of pidFilePathName.
   * @throws IOException 
   */
  public Integer ReadPIDFile(String pidFilePathName) 
    throws IOException
  {
    _processID = null;
    
    File pidFile = new File(pidFilePathName);
    if (pidFile.exists() == true)
    {
      FileReader fileReader = new FileReader(pidFile);
      BufferedReader bufferedReader = new BufferedReader(fileReader);
      StringBuilder stringBuilder = new StringBuilder();
      int charRead;
      while ((charRead = bufferedReader.read()) != -1)
      {
        stringBuilder.append((char)charRead);
      }
      bufferedReader.close();
      String pidString = stringBuilder.toString().trim();
      _processID = Integer.valueOf(pidString);
    }
    
    return _processID;
  }

  public void WritePIDFile(String pidFilePathName)
    throws IOException
  {
    File pidFile = new File(pidFilePathName);
    if (pidFile.exists() == true)
    {
      pidFile.delete();
    }
    FileWriter fileWriter = new FileWriter(pidFile);
    BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
    bufferedWriter.write(String.valueOf(_processID));
    bufferedWriter.flush();
    bufferedWriter.close();
  }

  public void DeletePIDFile(String pidFilePathName)
  {
    File pidFile = new File(pidFilePathName);
    pidFile.delete();
  }
  
  /**
   * @return true = process killed. false = error occurred, process not killed.
   */
  public abstract boolean Kill();
  
  /**
   * @return true = this process is running. false = this process is not running.
   */
  public abstract boolean IsRunning();


}
