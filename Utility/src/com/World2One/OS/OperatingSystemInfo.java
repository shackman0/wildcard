package com.World2One.OS;

import java.awt.Toolkit;

import javax.swing.UIManager;

/**
 * <p>Title: OperatingSystem</p>
 * <p>Description: see https://swingx.dev.java.net/source/browse/swingx/src/java/org/jdesktop/swingx/util/OS.java?view=markup</p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public class OperatingSystemInfo
{
  public static final String SYSTEM_PROPERTY_OS_NAME             = System.getProperty("os.name");
  public static final String SYSTEM_PROPERTY_OS_VERSION          = System.getProperty("os.version");
  public static final String SYSTEM_PROPERTY_SYSTEM_ARCHITECTURE = System.getProperty("os.arch");

  
  private static final boolean __osIsMacOsX;
  private static final boolean __osIsWindows;
  private static final boolean __osIsWindowsXP;
  private static final boolean __osIsWindows2003;
  private static final boolean __osIsWindowsVista;
  private static final boolean __osIsLinux;
  

  
  static
  {
    String os = SYSTEM_PROPERTY_OS_NAME.toLowerCase();

    __osIsMacOsX = "mac os x".equals(os);
    __osIsWindows = (os != null) && (os.indexOf("windows") != -1);
    __osIsWindowsXP = "windows xp".equals(os);
    __osIsWindows2003 = "windows 2003".equals(os);
    __osIsWindowsVista = "windows vista".equals(os);
    __osIsLinux = (os != null) && (os.indexOf("linux") != -1);
  }

  
  public static Integer GetMajorVersion()
  {
    return Integer.parseInt(SYSTEM_PROPERTY_OS_VERSION.substring(0, 1));
  }
  
  /**
   * @return true if this VM is running on Mac OS X
   */
  public static boolean IsMacOSX()
  {
    return __osIsMacOsX;
  }

  /**
   * @return true if this VM is running on Windows
   */
  public static boolean IsWindows()
  {
    return __osIsWindows;
  }

  /**
   * @return true if this VM is running on Windows XP
   */
  public static boolean IsWindowsXP()
  {
    return __osIsWindowsXP;
  }

  /**
   * @return true if this VM is running on Windows 2003
   */
  public static boolean IsWindows2003()
  {
    return __osIsWindows2003;
  }

  /**
   * @return true if this VM is running on Windows Vista
   */
  public static boolean IsWindowsVista()
  {
    return __osIsWindowsVista;
  }

  /**
   * @return true if this VM is running on a Linux distribution
   */
  public static boolean IsLinux()
  {
    return __osIsLinux;
  }

  /**
   * @return true if the VM is running Windows and the Java application is rendered using XP Visual Styles.
   */
  public static boolean IsUsingWindowsVisualStyles()
  {
    if (IsWindows() == false)
    {
      return false;
    }

    boolean xpthemeActive = Boolean.TRUE.equals(Toolkit.getDefaultToolkit().getDesktopProperty("win.xpstyle.themeActive"));
    if (!xpthemeActive)
    {
      return false;
    }
    else
    {
      try
      {
        return System.getProperty("swing.noxp") == null;
      }
      catch (RuntimeException e)
      {
        return true;
      }
    }
  }

  /**
   * Returns the name of the current Windows visual style.
   * <ul>
   * <li>it looks for a property name "win.xpstyle.name" in UIManager and if not found
   * <li>it queries the win.xpstyle.colorName desktop property ({@link Toolkit#getDesktopProperty(java.lang.String)})
   * </ul>
   * 
   * @return the name of the current Windows visual style if any.
   */
  public static String GetWindowsVisualStyle()
  {
    String style = UIManager.getString("win.xpstyle.name");
    if (style == null)
    {
      // guess the name of the current XPStyle
      // (win.xpstyle.colorName property found in awt_DesktopProperties.cpp in JDK source)
      style = (String) Toolkit.getDefaultToolkit().getDesktopProperty("win.xpstyle.colorName");
    }
    return style;
  }

}
