package com.World2One.OS;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.ParseException;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



/**
 * <p>Title: NetworkInfo</p>
 * <p>Description: NetworkInfo</p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com, Inc.</p>
 * @author Floyd Shackelford
 *
 * copied from: http://forum.java.sun.com/thread.jsp?forum=4&thread=245711
 *
 * try to determine MAC address of local network card; this is done
 * using a shell to run ifconfig (linux) or ipconfig (windows). The
 * output of the processes will be parsed.
 * <p>
 * To run the whole thing, just type java NetworkInfo
 * <p>
 * Current restrictions:
 * <ul>
 * <li>Will probably not run in applets
 * <li>Tested Windows / Linux only
 * <li>Tested J2SDK 1.4 only
 * <li>If a computer has more than one network adapters, only one MAC address will be returned
 * <li>will not run if user does not have permissions to run ifconfig/ipconfig (e.g. under linux this is typically
 *     only permitted for root)
 * </ul>
 */


public class NetworkInfo
{
  public static final String EXCEPTION_UNKNOWN_OPERATING_SYSTEM = "EXCEPTION_UNKNOWN_OPERATING_SYSTEM";
  public static final String EXCEPTION_CANNOT_READ_MAC_ADDRESS_FOR = "EXCEPTION_CANNOT_READ_MAC_ADDRESS_FOR";
  public static final String EXCEPTION_CANNOT_READ_MAC_ADDRESS_FROM = "EXCEPTION_CANNOT_READ_MAC_ADDRESS_FROM";

  
  private NetworkInfo()
  {
    super();
  }
  
  /**
   * return the non-local host ip address
   * @return
   * @throws SocketException 
   */
  public static String GetLocalIPAddress() 
    throws SocketException
  {
    Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();

    while (networkInterfaces.hasMoreElements())
    {
      NetworkInterface networkInterface = networkInterfaces.nextElement();
      Enumeration<InetAddress> inetAddresses = networkInterface.getInetAddresses();
      while (inetAddresses.hasMoreElements())
      {
        InetAddress inetAddress = inetAddresses.nextElement();
        String hostAddress = inetAddress.getHostAddress();
        if (hostAddress.equals("127.0.0.1") == false)
        {
          return hostAddress;
        }
      }
    }

    return null;
  }

  public static String TraceRoute(String targetHost) throws IOException
  {
    if(OperatingSystemInfo.IsWindows() == true)
    {
      return RunCommand("tracert " + targetHost);
    }
    else
    if(OperatingSystemInfo.IsLinux() == true)
    {
      return RunCommand("traceroute " + targetHost);
    }
    else
    {
      throw new RuntimeException("does not support this OS");
    }
  }

  public static String GetMacAddress() 
    throws ParseException, IOException
  {
    if(OperatingSystemInfo.IsWindows() == true)
    {
      return WindowsParseMacAddress(RunCommand("ipconfig /all"));
    }
    else
      if(OperatingSystemInfo.IsLinux() == true)
    {
      return LinuxParseMacAddress(RunCommand("/sbin/ifconfig"));
    }
    else
    {
      throw new RuntimeException("does not support this OS");
    }
  }

  public static String RunCommand(String commandString)
    throws IOException
  {
    Process process = Runtime.getRuntime().exec(commandString);
    InputStream stdoutStream = new BufferedInputStream(process.getInputStream());
    InputStream stderrStream = new BufferedInputStream(process.getErrorStream());
    StringBuilder stringBuilder = new StringBuilder();
    boolean processExited = false;
    while (processExited == false)
    {
      int characterRead = stdoutStream.read();
      if (characterRead == -1)
      {
        characterRead = stderrStream.read();
      }
      if (characterRead == -1)
      {
        try
        {
          process.exitValue();    // throws IllegalThreadStateException if the process has not exited.
          processExited = true;
        }
        catch (IllegalThreadStateException ex)
        {
          // the process has not yet terminated. continue to loop looking for output.
          Thread.yield();
        }
      }
      else
      {
        stringBuilder.append((char)characterRead);
      }
    }

    stdoutStream.close();
    stderrStream.close();

    return stringBuilder.toString();
  }

  /* Linux stuff */
  
  protected static final String LINUX_MAC_ADDRESS_LABEL = "HWaddr";

  protected static String LinuxParseMacAddress(String ifConfigResponse)
    throws ParseException, java.net.UnknownHostException
  {
    String localHost = InetAddress.getLocalHost().getHostAddress();
    StringTokenizer tokenizer = new StringTokenizer(ifConfigResponse, "\n");
    String lastMacAddress = null;
    while(tokenizer.hasMoreTokens())
    {
      String line = tokenizer.nextToken().trim();
      boolean containsLocalHost = line.indexOf(localHost) >= 0;
      // see if line contains IP address
      if ( (containsLocalHost == true) &&
           (lastMacAddress != null) )
      {
        return lastMacAddress;
      }

      // see if line contains MAC address
      int macAddressPosition = line.indexOf(LINUX_MAC_ADDRESS_LABEL);
      if (macAddressPosition < 0)
      {
        continue;
      }

      String macAddressCandidate = line.substring(macAddressPosition + LINUX_MAC_ADDRESS_LABEL.length()).trim();
      if (IsLinuxMacAddress(macAddressCandidate) == true)
      {
        lastMacAddress = macAddressCandidate;
        continue;
      }
    }
    String resourceMessage = "Cannot read MAC address for " + localHost + " " + ifConfigResponse;
    ParseException ex = new ParseException(resourceMessage, 0);
    throw ex;
  }

  /**
   * on linux, the MAC address looks something like: 00:50:8D:4D:CE:62
   * @param macAddressCandidate
   * @return
   */
  protected static final Pattern __linuxMACAddressPattern = Pattern.compile("^([0-9a-fA-F]{2}?:){5}?[0-9a-fA-F]{2}?$");

  protected static boolean IsLinuxMacAddress(String macAddressCandidate)
  {
    Matcher matcher = __linuxMACAddressPattern.matcher(macAddressCandidate);
    return matcher.matches();
  }

  /* Windows stuff */

  protected static final String WINDOWS_MAC_ADDRESS_LABEL = "Physical Address. . . . . . . . . :";

  protected static String WindowsParseMacAddress(String ipConfigResponse)
    throws ParseException
  {
    StringTokenizer tokenizer = new StringTokenizer(ipConfigResponse, "\n");
    while(tokenizer.hasMoreTokens() == true)
    {
      String line = tokenizer.nextToken().trim();
      int macAddressPosition = line.indexOf(WINDOWS_MAC_ADDRESS_LABEL);
      if (macAddressPosition >= 0)
      {
        String macAddressCandidate = line.substring(macAddressPosition + WINDOWS_MAC_ADDRESS_LABEL.length()).trim();
        if (IsWindowsMacAddress(macAddressCandidate) == true)
        {
          return macAddressCandidate;
        }
      }
    }

    String resourceMessage = "Cannot read MAC address from " + ipConfigResponse;
    ParseException ex = new ParseException(resourceMessage, 0);
    throw ex;
  }

 /**
 * on windows, the MAC address looks something like: 00-13-02-8D-8F-26
 * @param macAddressCandidate
 * @return
 */

  protected static final Pattern __windowsMACAddressPattern = Pattern.compile("^([0-9a-fA-F]{2}?-){5}?[0-9a-fA-F]{2}?$");

  protected static boolean IsWindowsMacAddress(String macAddressCandidate)
  {
    Matcher matcher = __windowsMACAddressPattern.matcher(macAddressCandidate);
    return matcher.matches();
  }

}
