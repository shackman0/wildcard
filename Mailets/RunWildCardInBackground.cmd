@echo off

if not "%1" == "" goto run_in_background

rem determine directory in which this script resides
rem %~dp0 is drive and path of this script (under NT)

if not "%~dp0" == "" goto run_in_background

echo.
echo Error: parameter 1 is not set.
echo   This needs to be set manually for Win9x as its command
echo   prompt scripting does not allow it to be set automatically.
echo   Pass in the absolute path to this script as the first
echo   parameter to this script. 
echo.

goto end

:run_in_background

cd %~d0%
cd %~p0%
cd %1

wscript.exe ".\RunInBackground.vbs" ".\run.bat" "%1"

exit

:end
