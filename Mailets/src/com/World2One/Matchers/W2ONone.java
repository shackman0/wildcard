package com.World2One.Matchers;

import java.util.Collection;
import java.util.HashSet;

import javax.mail.MessagingException;

import org.apache.mailet.GenericMatcher;
import org.apache.mailet.Mail;

/**
 * <p>Title: W2ONone</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public class W2ONone
  extends GenericMatcher 
{
  
  public W2ONone()
  {
    super();
  }

  @Override
  public Collection match(Mail mail)
    throws MessagingException
  {
    return new HashSet();
  }
  

}
