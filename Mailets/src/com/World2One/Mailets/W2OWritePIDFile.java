package com.World2One.Mailets;

import java.io.IOException;
import java.text.MessageFormat;

import javax.mail.MessagingException;

import org.apache.mailet.Mail;

import com.World2One.OS.AbstractProcessManager;
import com.World2One.Utility.MutableInteger;


/**
 * <p>Title: W2OKeepAlive</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public class W2OWritePIDFile
  extends AbstractMailet
{

  protected static final String WILDCARD_TOP = "../../";  // wildcard top relative to JAMES/bin/
  protected static final String JAMES_PID_FILE_PATH_NAME = MessageFormat.format(com.World2One.Utility.Constants.JAMES_PID_FILE_PATH_NAME_FORMAT, WILDCARD_TOP);  

  
  public W2OWritePIDFile()
  {
    super();
  }
  
  
  @Override
  public void init() 
    throws MessagingException 
  {
    super.init();  

    try
    {
      MutableInteger processID = new MutableInteger();
      if (AbstractProcessManager.IsRunning(JAMES_PID_FILE_PATH_NAME,processID) == true)
      {
        throw new MessagingException("WildCard is already running with PID " + processID._integer);
      }
    }
    catch (IOException ex)
    {
      // do nothing. probably couldn't find the pid file.
    }

    try
    {
      AbstractProcessManager.WriteMyPIDToPIDFile(JAMES_PID_FILE_PATH_NAME);
    }
    catch (IOException ex)
    {
      throw new MessagingException(ex.getLocalizedMessage());
    }
  }


  @Override
  public void service(Mail mail)
    throws MessagingException
  {
    // do nothing
  }
  
  @Override
  public void destroy() 
  {
    AbstractProcessManager.DeleteMyPIDFile(JAMES_PID_FILE_PATH_NAME);
  }

}
