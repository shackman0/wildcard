package com.World2One.Mailets.SOAP;

import java.net.URL;
import java.text.MessageFormat;
import java.util.HashMap;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPMessage;

import com.World2One.Utility.Constants;
import com.World2One.Utility.MutableString;

/**
 * <p>Title: Cards</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One, Inc.</p>
 */
public class Cards
  extends com.World2One.Mailets.SOAP.AbstractSOAPClient
{
  
  public static final String API_CARDS  = "Cards.asmx";
  
  public static final String SOAP_ACTION_GET_CARD_RESOURCES_BY_EMAIL_ADDRESS    = "http://world2one.com/GetCardResourcesByEmailAddress"; 

  protected static final String GET_CARD_RESOURCES_SOAP_1_1_REQUEST_BODY = 
    "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + Constants.CR_LF +
    "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">" + Constants.CR_LF + 
    "  <soap:Body>" + Constants.CR_LF +
    "    <GetCardResourcesByEmailAddress xmlns=\"http://world2one.com/\">" + Constants.CR_LF + 
    "      <emailAddress>{0}</emailAddress>" + Constants.CR_LF +     // email address
    "      <apikey>" + API_KEY + "</apikey>" + Constants.CR_LF + 
    "    </GetCardResourcesByEmailAddress>" + Constants.CR_LF +
    "  </soap:Body>" + Constants.CR_LF + 
    "</soap:Envelope>";  
  
  
 
  
  public Cards()
  {
    super();
  }
  
  public enum PlainTextHandling
  {
    LeaveAsPlainText,
    ConvertToHTML;
  }
  
  public static class MutablePlainTextHandling
  {
    public PlainTextHandling _plainTextHandling;
    
    @Override
    public String toString()
    {
      return _plainTextHandling.toString();
    }
  }
  
  /**
   * applicable only to HTML formatted e-mails
   */
  public enum HTMLImageSource
  {
    Inline,
    URL;
  }
  
  public static class MutableHTMLImageSource
  {
    public HTMLImageSource _htmlImageSource;
    
    @Override
    public String toString()
    {
      return _htmlImageSource.toString();
    }
  }
  
  /**
   * 
   * @param soapHost  in
   * @param soapPort  in
   * @param emailAddress  in
   * @param iCardID  out
   * @param htmlText  out
   * @param plainText  out
   * @param imageData  out
   * @param plainTextHandling  out
   * @param htmlImageSource  out
   * @return null = iCard resources were found for the specified email address. not null = error message.
   * @throws Exception 
   */
  public String GetCardResourcesByEmailAddress(
    String                   soapProtocol,
    String                   soapHost,
    String                   soapPort,
    String                   emailAddress,
    MutableString            iCardID,
    MutableString            htmlText,
    MutableString            plainText,
    MutableString            imageData,
    MutablePlainTextHandling plainTextHandling,
    MutableHTMLImageSource   htmlImageSource ) 
    throws Exception
  {
    soapProtocol = soapProtocol.trim();
    soapHost = soapHost.trim();
    soapPort = soapPort.trim();
    emailAddress = emailAddress.trim();
    
    if (_debug == true)
    {
      StringBuilder stringBuilder = new StringBuilder();
      
      stringBuilder.append(Constants.CR_LF)
                   .append("  soapProtocol=\"")
                   .append(soapProtocol)
                   .append("\"")
                   .append(Constants.CR_LF)
                   .append("  soapHost=\"")
                   .append(soapHost)
                   .append("\"")
                   .append(Constants.CR_LF)
                   .append("  soapPort=\"")
                   .append(soapPort)
                   .append("\"")
                   .append(Constants.CR_LF)
                   .append("  emailAddress=\"")
                   .append(emailAddress)
                   .append("\"")
                   .append(Constants.CR_LF);
      
      _abstractMailet.log(stringBuilder.toString()); 
    }
    
    URL soapServiceURL = GetAPIURL(soapProtocol,soapHost,soapPort,API_CARDS);
    String formattedBody = MessageFormat.format(GET_CARD_RESOURCES_SOAP_1_1_REQUEST_BODY, emailAddress);
    SOAPMessage soapMessage = SendMessage(soapServiceURL,formattedBody,SOAP_ACTION_GET_CARD_RESOURCES_BY_EMAIL_ADDRESS);
    SOAPBody soapBody = soapMessage.getSOAPBody();
    
    if (soapBody.hasFault() == true)
    {
      String faultMessage = GetFaultMessage(soapBody);
      
      return faultMessage;
    }
    
    HashMap<String,SOAPBodyElement> elementsByTagName = GetElementsByTagName(soapBody);    
    
    SOAPBodyElement w2oErrorBodyElement = elementsByTagName.get("W2OError");
    if (w2oErrorBodyElement != null)
    {
      SOAPBodyElement faultStringBodyElement = elementsByTagName.get("faultstring");
      String faultString = faultStringBodyElement.getTextContent();
      
      return (faultString);
    }
    
    SOAPBodyElement iCardIDBodyElement = elementsByTagName.get("CardId");
    if (iCardIDBodyElement == null)
    {
      return ("iCard for " + emailAddress + " is missing the iCardID element");
    }
    String iCardIDValue = iCardIDBodyElement.getTextContent();
    if (iCardIDValue.trim().length() == 0)
    {
      return ("iCard for " + emailAddress + " has a blank iCardID value");
    }
    iCardID._string = iCardIDValue;

    
    SOAPBodyElement plainTextHandlingBodyElement = elementsByTagName.get("PlainTextHandling");
    if (plainTextHandlingBodyElement == null)
    {
      return ("iCard for " + emailAddress + " is missing the PlainTextHandling element");
    }
    String plainTextHandlingValue = plainTextHandlingBodyElement.getTextContent();
    if (plainTextHandlingValue.trim().length() == 0)
    {
      return ("iCard for " + emailAddress + " has a blank PlainTextHandling value");
    }
    plainTextHandling._plainTextHandling = PlainTextHandling.valueOf(plainTextHandlingValue);

    
    SOAPBodyElement htmlImageSourceBodyElement = elementsByTagName.get("HTMLImageSource");
    if (htmlImageSourceBodyElement == null)
    {
      return ("iCard for " + emailAddress + " is missing the HTMLImageSource element");
    }
    String htmlImageSourceValue = htmlImageSourceBodyElement.getTextContent();
    if (htmlImageSourceValue.trim().length() == 0)
    {
      return ("iCard for " + emailAddress + " has a blank HTMLImageSource value");
    }
    htmlImageSource._htmlImageSource = HTMLImageSource.valueOf(htmlImageSourceValue);
    
    SOAPBodyElement htmlBodyElement = elementsByTagName.get("Data_Html");
    if (htmlBodyElement == null)
    {
      return ("iCard for " + emailAddress + " is missing the Data_Html element");
    }
    String htmlValue = htmlBodyElement.getTextContent();
    if (htmlValue.trim().length() == 0)
    {
      return ("iCard for " + emailAddress + " has a blank Data_Html table value");
    }
    htmlText._string = htmlValue;
    
    SOAPBodyElement plainTextBodyElement = elementsByTagName.get("Data_Text");
    if (plainTextBodyElement == null)
    {
      return ("iCard for " + emailAddress + " is missing the Data_Text element");
    }
    String plainTextValue = plainTextBodyElement.getTextContent();
    if (plainTextValue.trim().length() == 0)
    {
      plainTextValue = "tbd";
//      return ("iCard for " + emailAddress + " has a blank Data_Text value");
    }
    plainText._string = plainTextValue;
    
    SOAPBodyElement imageDataBodyElement = elementsByTagName.get("Data_Image");
    if (imageDataBodyElement == null)
    {
      return ("iCard for " + emailAddress + " is missing the Data_Image element");
    }
    String imageDataValue = imageDataBodyElement.getTextContent();
    if (imageDataValue.trim().length() == 0)
    {
      return ("iCard for " + emailAddress + " has a blank Data_Image value");
    }
    imageData._string = imageDataValue;

    if (_debug == true)
    {
      StringBuilder stringBuilder = new StringBuilder();
      
      stringBuilder.append(Constants.CR_LF);
      stringBuilder.append("  htmlText=\"");
      stringBuilder.append(htmlText);
      stringBuilder.append("\"");
      stringBuilder.append(Constants.CR_LF);
      stringBuilder.append("  plainText=\"");
      stringBuilder.append(plainText);
      stringBuilder.append("\"");
      stringBuilder.append(Constants.CR_LF);
      stringBuilder.append("  imageData=\"");
      stringBuilder.append(imageData._string.substring(0, Math.min(20, imageData._string.length())));
      if (imageData._string.length() > 20)
      {
        stringBuilder.append("...");
      }
      stringBuilder.append("\"");
      stringBuilder.append(Constants.CR_LF);
      stringBuilder.append("  plainTextHandling=\"");
      stringBuilder.append(plainTextHandling);
      stringBuilder.append("\"");
      stringBuilder.append(Constants.CR_LF);
      stringBuilder.append("  htmlImageSource=\"");
      stringBuilder.append(htmlImageSource);
      stringBuilder.append("\"");
      stringBuilder.append(Constants.CR_LF);
      
      _abstractMailet.log(stringBuilder.toString()); 
    }

    return null;
  }
  
}
