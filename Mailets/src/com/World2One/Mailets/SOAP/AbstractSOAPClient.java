package com.World2One.Mailets.SOAP;

import java.net.URL;
import java.security.PrivilegedActionException;

import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;

import com.World2One.Mailets.AbstractMailet;
import com.World2One.Utility.Constants;
import com.World2One.Utility.ToolBox;



/**
 * <p>Title: AbstractSOAPClient</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One, Inc.</p>
 */
public class AbstractSOAPClient
  extends com.World2One.SOAP.AbstractSOAPClient
{


  protected boolean _debug = false;
  protected AbstractMailet _abstractMailet = null;
  

  protected AbstractSOAPClient()
  {
    super();
  }

  
  /**
   * to enable debugging: _debug must be set to true and _abstractMailet must be non-null.
   */
  public void SetDebug(
    boolean        debug, 
    AbstractMailet abstractMailet )
  {
    _abstractMailet = null;
    if (abstractMailet == null)
    {
      _debug = false;
    }
    else
    {
      _debug = debug;
      if (_debug == true)
      {
        _abstractMailet = abstractMailet;
      }
    }
  }


  public boolean GetDebug()
  {
    return _debug;
  }
  
  public AbstractMailet GetAbstractMailet()
  {
    return _abstractMailet;
  }
  
  @Override
  protected SOAPMessage SendMessage(
    URL    soapServiceURL,
    String messageString,
    String soapAction ) 
    throws SOAPException, PrivilegedActionException 
  {
    SOAPMessage response = super.SendMessage(soapServiceURL, messageString, soapAction);
    SOAPBody responseBody = response.getSOAPBody();
    if (responseBody.hasFault() == true)
    {
      if (_debug == true)
      {
        _abstractMailet.log(GetFaultMessage(responseBody)); 
      }
    }
    
    return response;
  }

  @Override
  public String GetFaultMessage(SOAPBody soapBody)
  {
    SOAPFault fault = soapBody.getFault();
    Name code = fault.getFaultCodeAsName();
    String string = fault.getFaultString();
    
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(Constants.CR_LF)
                 .append("  FAULT")
                 .append(Constants.CR_LF)
                 .append("  name=\"")
                 .append(code.getQualifiedName())
                 .append("\"")
                 .append(Constants.CR_LF)
                 .append("  string=\"")
                 .append(string)
                 .append("\"");
    
    return ToolBox.GetStackTrace(new Exception(stringBuilder.toString()));
  }

}
