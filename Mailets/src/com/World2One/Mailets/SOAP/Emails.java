package com.World2One.Mailets.SOAP;

import java.net.URL;
import java.text.MessageFormat;

import javax.mail.internet.InternetAddress;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPMessage;

import com.World2One.Utility.Constants;
import com.World2One.Utility.ToolBox;

/**
 * <p>Title: Emails</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One, Inc.</p>
 */
public class Emails
  extends com.World2One.Mailets.SOAP.AbstractSOAPClient
{
  
  public static final String API_EMAILS = "Emails.asmx";
  
  public static final String SOAP_ACTION_LOG_AUTHORIZED_RECIPIENT   = "http://world2one.com/LogAuthorizedRecipient"; 
  public static final String SOAP_ACTION_LOG_EMAIL                  = "http://world2one.com/LogEmail"; 

  protected static final String LOG_AUTHORIZED_RECIPIENT_SOAP_1_1_REQUEST_BODY =
    "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + Constants.CR_LF + 
    "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" + Constants.CR_LF + 
    "  <soap:Body>" + Constants.CR_LF + 
    "    <LogAuthorizedRecipient xmlns=\"http://world2one.com/\">" + Constants.CR_LF + 
    "      <icardid> {0} </icardid>" + Constants.CR_LF +    // icard id 
    "      <emailaddresses>" + Constants.CR_LF + 
    "        {1}" + Constants.CR_LF +             // comma-separated recipient addresses
    "      </emailaddresses>" + Constants.CR_LF + 
    "      <apikey>" + API_KEY + "</apikey>" + Constants.CR_LF + 
    "    </LogAuthorizedRecipient>" + Constants.CR_LF + 
    "  </soap:Body>" + Constants.CR_LF + 
    "</soap:Envelope>";    

  protected static final String LOG_EMAIL_SOAP_1_1_REQUEST_BODY =
    "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + Constants.CR_LF + 
    "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" + Constants.CR_LF + 
    "  <soap:Body>" + Constants.CR_LF + 
    "    <LogEmail xmlns=\"http://world2one.com/\">" + Constants.CR_LF + 
    "      <emailAddress> {0} </emailAddress>" + Constants.CR_LF +       // sender's email address 
    "      <rawEmailText> {1} </rawEmailText>" + Constants.CR_LF +       // string form of the entire email 
    "      <apikey>" + API_KEY + "</apikey>" + Constants.CR_LF + 
    "    </LogEmail>" + Constants.CR_LF + 
    "  </soap:Body>" + Constants.CR_LF + 
    "</soap:Envelope>";
  
  public Emails()
  {
    super();
  }

  /**
   * 
   * @param soapHost
   * @param soapPort
   * @param iCardID
   * @param emailAddress
   * @return null = iCard command completed successfully. not null = error message.
   * @throws Exception 
   */
  public String LogAuthorizedRecipients(
    String     soapProtocol,
    String     soapHost,
    String     soapPort,
    String     iCardID,
    InternetAddress[]  recipients ) 
    throws Exception
  {
    soapProtocol = soapProtocol.trim(); 
    soapHost = soapHost.trim();
    soapPort = soapPort.trim();
    iCardID = iCardID.trim();

    StringBuilder stringBuilder = new StringBuilder();
    for (InternetAddress recipient: recipients)
    {
      String recipientString = recipient.getAddress();
      stringBuilder.append(recipientString);
      stringBuilder.append(",");
    }
    stringBuilder.setLength(stringBuilder.length() - 1);  // remove trailing ","
    String recipientsAsString = stringBuilder.toString();
    
    if (_debug == true)
    {
      stringBuilder = new StringBuilder();
      
      stringBuilder.append(Constants.CR_LF)
                   .append("  soapProtocol=\"")
                   .append(soapProtocol)
                   .append("\"")
                   .append(Constants.CR_LF)
                   .append("  soapHost=\"")
                   .append(soapHost)
                   .append("\"")
                   .append(Constants.CR_LF)
                   .append("  soapPort=\"")
                   .append(soapPort)
                   .append("\"")
                   .append(Constants.CR_LF)
                   .append("  iCardID=\"")
                   .append(iCardID)
                   .append("\"")
                   .append(Constants.CR_LF)
                   .append("  recipients=\"")
                   .append(recipientsAsString)
                   .append("\"")
                   .append(Constants.CR_LF);
      
      _abstractMailet.log(stringBuilder.toString()); 
    }
    
    URL soapServiceURL = GetAPIURL(soapProtocol,soapHost,soapPort,API_EMAILS);
    String formattedBody = MessageFormat.format(LOG_AUTHORIZED_RECIPIENT_SOAP_1_1_REQUEST_BODY, iCardID, recipientsAsString);
    SOAPMessage soapMessage = SendMessage(soapServiceURL,formattedBody,SOAP_ACTION_LOG_AUTHORIZED_RECIPIENT);
    SOAPBody soapBody = soapMessage.getSOAPBody();
    
    if (soapBody.hasFault() == true)
    {
      String faultMessage = GetFaultMessage(soapBody);
      
      return faultMessage;
    }

    return null;
  }
  
  public String LogEmail(
    String     soapProtocol,
    String     soapHost,
    String     soapPort,
    String     senderAddress,
    String     emailInStringForm ) 
    throws Exception
  {
    soapProtocol = soapProtocol.trim(); 
    soapHost = soapHost.trim();
    soapPort = soapPort.trim();
    senderAddress = senderAddress.trim();

    if (_debug == true)
    {
      StringBuilder stringBuilder = new StringBuilder();
      
      stringBuilder.append(Constants.CR_LF)
                   .append("  soapProtocol=\"")
                   .append(soapProtocol)
                   .append("\"")
                   .append(Constants.CR_LF)
                   .append("  soapHost=\"")
                   .append(soapHost)
                   .append("\"")
                   .append(Constants.CR_LF)
                   .append("  soapPort=\"")
                   .append(soapPort)
                   .append("\"")
                   .append(Constants.CR_LF)
                   .append("  senderAddress=\"")
                   .append(senderAddress)
                   .append("\"")
                   .append(Constants.CR_LF);
      
      _abstractMailet.log(stringBuilder.toString()); 
    }
    
    String emailInSafeStringForm = ToolBox.Convert2SafeXMLValue(emailInStringForm);
    
    URL soapServiceURL = GetAPIURL(soapProtocol,soapHost,soapPort,API_EMAILS);
    String formattedBody = MessageFormat.format(LOG_EMAIL_SOAP_1_1_REQUEST_BODY, senderAddress, emailInSafeStringForm);
    SOAPMessage soapMessage = SendMessage(soapServiceURL,formattedBody,SOAP_ACTION_LOG_EMAIL);
    SOAPBody soapBody = soapMessage.getSOAPBody();
    
    if (soapBody.hasFault() == true)
    {
      String faultMessage = GetFaultMessage(soapBody);
      
      return faultMessage;
    }

    return null;
  }
  
}
