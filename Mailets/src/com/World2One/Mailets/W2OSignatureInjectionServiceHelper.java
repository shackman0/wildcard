package com.World2One.Mailets;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Dialog.ModalityType;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimePart;
import javax.mail.internet.ParseException;
import javax.mail.internet.PreencodedMimeBodyPart;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingWorker;

import org.apache.mailet.HostAddress;
import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;
import org.apache.mailet.MailetContext;
import org.apache.mailet.RFC2822Headers;

import com.World2One.Mailets.SOAP.Cards.MutableHTMLImageSource;
import com.World2One.Mailets.SOAP.Cards.MutablePlainTextHandling;
import com.World2One.Utility.Base64InputStream;
import com.World2One.Utility.Constants;
import com.World2One.Utility.MutableString;
import com.World2One.Utility.ToolBox;


/**
 * <p>Title: W2OSignatureInjectionServiceHelper</p>
 * <p>Description: multi-thread support for W2OSignatureInjectionService.service(Mail)</p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One, Inc.</p>
 */
public class W2OSignatureInjectionServiceHelper
  extends AbstractAddFooter
{


  protected MutableString             _iCardID;
  protected MutableString             _htmlAppendix;
  protected MutableString             _plainTextAppendix;
  protected MutableString             _imageBase64;
  protected MutablePlainTextHandling  _plainTextHandling;
  protected MutableHTMLImageSource    _htmlImageSource;

  protected String      _inlineImageContentID = null;
  protected boolean     _need2ImbedImage = false;

  
  
  public W2OSignatureInjectionServiceHelper(W2OSignatureInjectionService mailet)
  {
    super(mailet);

    _iCardID = new MutableString();
    _htmlAppendix = new MutableString();
    _plainTextAppendix = new MutableString();
    _imageBase64 = new MutableString();
    _plainTextHandling = new MutablePlainTextHandling();
    _htmlImageSource = new MutableHTMLImageSource();
  }

  
  protected static final Pattern __replyOrForwardPattern   = Pattern.compile("^[^a-zA-Z0-9]*(([Rr][Ee])|([Ff][Ww]))[^a-zA-Z0-9]+.*$");

  
  @Override
  public void service(Mail mail)
    throws MessagingException
  {
    final W2OSignatureInjectionService signatureInjectionService = (W2OSignatureInjectionService)_mailet;
    if (signatureInjectionService.IsSignatureInjectionEnabled() == true)
    {
      try
      {
        _message = mail.getMessage();
        LoadMessageHeaders();

        if (signatureInjectionService.GetLogAuthorizedRecipients() == true)
        {
          final String iCardID = _iCardID._string;
          Address[] recipients = _message.getAllRecipients();
          final InternetAddress[] recipients2 = new InternetAddress[recipients.length];
          int indx = 0;
          for (Address recipient: recipients)
          {
            recipients2[indx++] = (InternetAddress)recipient;
          }
          
          new SwingWorker()
          {
            @Override
            protected Object doInBackground()
              throws Exception
            {
              signatureInjectionService.LogAuthorizedRecipients(iCardID,recipients2);
              
              return null;
            }
          }.execute();
        }

        if (signatureInjectionService.GetInjectSignatureOnReplyOrForward() == false)
        {
          String subject = _message.getSubject();
          if (subject != null)
          {
            subject = subject.toLowerCase();
            Matcher replyOrForwardMatcher = __replyOrForwardPattern.matcher(subject);
            boolean replyOrForwardInSubject = replyOrForwardMatcher.matches();
            
            if (signatureInjectionService.GetDebug() == true)
            {
              String iCardDisposition = (replyOrForwardInSubject == true) ? " NOT appended" : " appended as normal";
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append(Constants.CR_LF)
                           .append("  Subject = \"")
                           .append(subject)
                           .append("\"")
                           .append(Constants.CR_LF)
                           .append("  contains reply or forward = ")
                           .append(replyOrForwardInSubject)
                           .append(Constants.CR_LF)
                           .append("  iCard")
                           .append(iCardDisposition);

              signatureInjectionService.log(stringBuilder.toString());
            }
            
            if (replyOrForwardInSubject == true)
            {
              return;
            }
          }
        }
        
        MailAddress senderAddress = GetSender(mail);
        if (senderAddress == null)
        {
          // sometimes this happens on return receipt requested responses
          StringBuilder logMessage = new StringBuilder();
          logMessage.append(Constants.CR_LF);
          logMessage.append("Cannot find sender address for EMail. EMail not sent.");

          Object subject = _messageHeaders.get("Subject:");
          if (subject != null)
          {
            logMessage.append(Constants.CR_LF);
            logMessage.append("Subject: ");
            logMessage.append(subject.toString());
          }

          Object messageID = _messageHeaders.get("Message-ID:");
          if (messageID != null)
          {
            logMessage.append(Constants.CR_LF);
            logMessage.append("Message ID: ");
            logMessage.append(messageID.toString());
          }
          
          Collection<MailAddress> recipients = mail.getRecipients();
          if ( (recipients != null) && 
               (recipients.size() > 0) )
          {
            logMessage.append(Constants.CR_LF);
            logMessage.append("To: ");
            for (MailAddress recipient: recipients)
            {
              logMessage.append(recipient.toString());
              logMessage.append(" ");
            }
          }
          
          Object date = _messageHeaders.get("Date:");
          if (date != null)
          {
            logMessage.append(Constants.CR_LF);
            logMessage.append("Dated: ");
            logMessage.append(date.toString());
          }
          
          _mailet.log(logMessage.toString());
          
          return;
        }
        else
        {
          String senderAddressString = senderAddress.toString();

          String errorMessage = signatureInjectionService.GetCardResourcesByEmailAddress(
                                  senderAddressString,
                                  _iCardID,
                                  _htmlAppendix,
                                  _plainTextAppendix,
                                  _imageBase64,
                                  _plainTextHandling,
                                  _htmlImageSource );
          if (errorMessage != null)
          {
            String wrappedErrorMessage = ToolBox.WrapText(senderAddressString + ": " + errorMessage, 60);
            
            if (signatureInjectionService.GetGUISupported() == true)
            {
              JOptionPane optionPane = new JOptionPane(wrappedErrorMessage,JOptionPane.ERROR_MESSAGE,JOptionPane.DEFAULT_OPTION);
              JDialog dialog = optionPane.createDialog("iCard Error Encountered");
              dialog.setModalityType(ModalityType.MODELESS);
              dialog.setVisible(true);
            }
            
            signatureInjectionService.log(wrappedErrorMessage);
            SendEmailToW2OSupport(wrappedErrorMessage);
            
            return;
          }
        }
        
        _need2ImbedImage = false;
        
        super.service(mail);

        if (_footerAttached == true) 
        {
          _message = mail.getMessage();
          LoadMessageHeaders();

          RemoveWinmail(null, _message);
          _message.saveChanges();

          if (_need2ImbedImage == true)
          {
            ConvertContentToMultipartContentIfNecessary();
            AddInlineImageBodyPart();          
          }

          if ( (signatureInjectionService.GetGUISupported() == true) &&
               (signatureInjectionService.GetDisplayMessageSentPopup() == true) )
          {
            DisplayMessageSentDialog();
          }
        }
      }
      catch (Exception ex)
      {
        signatureInjectionService.Log(ex);
        throw signatureInjectionService.GetMessagingException(ex);
      }
      finally
      {
        _message = null;
        _messageHeaders = null;
      }
    }
  }

  protected MailAddress GetSender(Mail mail)
  {
    MailAddress sender = mail.getSender();
    if (sender == null)
    {
      Object fromHeader = _messageHeaders.get("From:");
      if (fromHeader != null)
      {
        try
        {
          sender = new MailAddress(fromHeader.toString());
        }
        catch (ParseException ex)
        {
          _mailet.Log(ex);
        }
      }
    }
    
    return sender;
  }
  
  protected boolean RemoveWinmail(
    MimeMultipart ownerPart,
    MimePart      part ) 
    throws MessagingException, IOException
  {
    W2OSignatureInjectionService signatureInjectionService = (W2OSignatureInjectionService)_mailet;
    String contentType = part.getContentType().toLowerCase();
    // remove mime parts with Content-Type: application/ms-tnef; name="winmail.dat" (see ticket #1)
    if ( (contentType.contains("\"winmail.dat\"") == true) &&
         (part instanceof BodyPart) )
    {
      if (signatureInjectionService.GetDebug() == true)
      {
        signatureInjectionService.log("Removing winmail.dat");
      }
      ownerPart.removeBodyPart((BodyPart)part);
      
      return true;  
    }

    boolean winmailRemoved = false;
    Object content = part.getContent();
    if (content instanceof Multipart)
    {
      MimeMultipart multipart = (MimeMultipart)content;
      int count = multipart.getCount();
      for (int indx = 0; (indx < count) && (winmailRemoved == false); indx++)
      {
        MimeBodyPart bodypart = (MimeBodyPart)multipart.getBodyPart(indx);
        winmailRemoved = RemoveWinmail(multipart,bodypart);
      }
    }
    
    return winmailRemoved;
  }
  
  protected void SendEmailToW2OSupport(final String errorMessage) 
    throws MessagingException
  {
    final W2OSignatureInjectionService signatureInjectionService = (W2OSignatureInjectionService)_mailet;
    if (signatureInjectionService.GetW2OSupportSendEmailOnError() == true)
    {
      new SwingWorker()
      {
        @Override
        protected Object doInBackground()
          throws Exception
        {
          try
          {
            // create the properties
            Properties props = new Properties();
            props.put("mail.smtp.from", signatureInjectionService.GetW2OSupportFromEmailAddress());
            props.put("mail.smtp.host", signatureInjectionService.GetW2OSupportSMTPHost() + ":" + signatureInjectionService.GetW2OSupportPort());   //Set the host smtp address
            props.put("mail.debug", "false");  //Not needed for production environment
            props.put("mail.smtp.ehlo", "true");  // Reactivated: javamail 1.3.2 should no more have problems with "250 OK" messages (WAS "false": Prevents problems encountered with 250 OK Messages)
            props.put("mail.smtp.allow8bitmime", "false");
            props.put("mail.smtp.timeout", "30000");  //Sets timeout on going connections
            props.put("mail.smtp.connectiontimeout", "20000");
            props.put("mail.smtp.sendpartial","false");
            props.put("mail.smtp.starttls.required","false");
            props.put("mail.smtps.starttls.required","false");
            props.put("mail.smtp.starttls.enable", "false");
            props.put("mail.smtps.starttls.enable", "false");
            //Set the hostname we'll use as this server
            MailetContext mailetContext = signatureInjectionService.getMailetContext();
            if (mailetContext.getAttribute(org.apache.james.Constants.HELLO_NAME) != null) 
            {
              props.put("mail.smtp.localhost", signatureInjectionService.getMailetContext().getAttribute(org.apache.james.Constants.HELLO_NAME));
            } 
            else 
            {
              String defaultDomain = (String) mailetContext.getAttribute(org.apache.james.Constants.DEFAULT_DOMAIN);
              if (defaultDomain != null) 
              {
                props.put("mail.smtp.localhost", defaultDomain);
              }
            }

            String authUser = signatureInjectionService.GetW2OSupportAuthUser();
            String authPassword = signatureInjectionService.GetW2OSupportAuthPassword();
            boolean authenticate = ( (authUser != null) && (authUser.trim().length() > 0) );
            props.put("mail.smtp.auth",String.valueOf(authenticate));
            props.put("mail.smtps.auth",String.valueOf(authenticate));

            if (signatureInjectionService.GetDebug() == true)
            {
              StringBuilder stringBuilder = new StringBuilder();
              for (Object key: props.keySet())
              {
                stringBuilder
                  .append(key.toString())
                  .append(" = ")
                  .append(props.get(key))
                  .append(com.World2One.Utility.Constants.CR_LF);
              }
              signatureInjectionService.log(stringBuilder.toString());
            }
            
            // get the session
            SMTPAuthenticator smtpAuthenticator = new SMTPAuthenticator();
            Session session = Session.getInstance(props, smtpAuthenticator);
//            session.setDebug(true);

            // create a message
            Message message = new MimeMessage(session);

            // set the from and to address
            InternetAddress addressFrom = new InternetAddress(signatureInjectionService.GetW2OSupportFromEmailAddress());
            message.setFrom(addressFrom); 

            InternetAddress[] addressTo = new InternetAddress[] { new InternetAddress(signatureInjectionService.GetW2OSupportToEmailAddress()) }; 
            message.setRecipients(Message.RecipientType.TO, addressTo);

            // Setting the Subject and Content Type
            message.setSubject(signatureInjectionService.GetW2OSupportSubject());
            message.setContent(errorMessage, "text/plain");
            
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(signatureInjectionService.GetW2OSupportSMTPHost());
            stringBuilder.append(":");
            stringBuilder.append(signatureInjectionService.GetW2OSupportPort());
            HostAddress outgoingMailServer = new org.apache.mailet.HostAddress(stringBuilder.toString(), signatureInjectionService.GetW2OSupportSMTPProtocol() + "://" + stringBuilder.toString());
            
            if (signatureInjectionService.GetDebug() == true)
            {
              stringBuilder.setLength(0);
              
              stringBuilder.append(Constants.CR_LF)
                           .append("Sending error email")
                           .append(Constants.CR_LF)
                           .append("  via: ")
                           .append(outgoingMailServer)
                           .append(Constants.CR_LF)
                           .append("  from: ")
                           .append(signatureInjectionService.GetW2OSupportFromEmailAddress())
                           .append(Constants.CR_LF)
                           .append("  to: ")
                           .append(signatureInjectionService.GetW2OSupportToEmailAddress())
                           .append(Constants.CR_LF)
                           .append("  subject: ")
                           .append(signatureInjectionService.GetW2OSupportSubject())
                           .append(Constants.CR_LF)
                           .append("  content: ")
                           .append(errorMessage);
              signatureInjectionService.log(stringBuilder.toString());
            }
            
            Transport transport = session.getTransport(outgoingMailServer);
            if (authenticate == true)
            {
              transport.connect(signatureInjectionService.GetW2OSupportSMTPHost(), Integer.valueOf(signatureInjectionService.GetW2OSupportPort()), authUser, authPassword);
            }
            else
            {
              transport.connect();
            }
            transport.sendMessage(message, addressTo);
            transport.close();

            return null;
          }
          catch (Exception ex)
          {
            signatureInjectionService.Log(ex);
            throw ex;
          }
        }
      }.execute();
    }
 }
  
  protected void ConvertContentToMultipartContentIfNecessary()
    throws MessagingException, IOException 
  {
    W2OSignatureInjectionService signatureInjectionService = (W2OSignatureInjectionService)_mailet;
    InputStream messageInputStream = _message.getInputStream();
    if (messageInputStream != null)
    {
      messageInputStream.close();
    }
    Object content = _message.getContent();
    if (content instanceof Multipart)
    {
      // do nothing
    }
    else
    if (content instanceof String)
    {
      if (signatureInjectionService.GetDebug() == true) 
      {
        signatureInjectionService.log("Converting content from string to multi-part");
      }
      String contentString = (String)content;
      
      MimeMultipart multipartContent = new MimeMultipart("related");
      MimeBodyPart bodyPart = new MimeBodyPart();
      multipartContent.addBodyPart(bodyPart);
      String contentType = _message.getContentType();    
      bodyPart.setContent(contentString,contentType);
      
      // We have to do this last because of a bug in JavaMail (ref id 4403733)
      // http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4403733
      _message.setContent(multipartContent,"multipart/related");
    }
    else
    {
      throw new MessagingException("unsupported message content class " + content.getClass().getName());
    }
  }

  @Override
  protected void AddToText(
    MimeMultipart ownerPart,
    MimePart      part)
    throws MessagingException, IOException
  {
    switch(_plainTextHandling._plainTextHandling)
    {
      case LeaveAsPlainText:
        super.AddToText(ownerPart,part);
        break;
      case ConvertToHTML:
        if ( (EmailHasHTMLTextBodyPart() == true) &&
             (part instanceof BodyPart) )
        {
          ownerPart.removeBodyPart((BodyPart)part);
        }
        else
        {
          ConvertPlainTextToHTMLText(part);
          AddToHTML(ownerPart,part);
        }
        break;
    }
  }

  protected void ConvertPlainTextToHTMLText(MimePart part) 
    throws IOException, MessagingException
  {
    W2OSignatureInjectionService signatureInjectionService = (W2OSignatureInjectionService)_mailet;
    if (signatureInjectionService.GetDebug() == true) 
    {
      signatureInjectionService.log();
    }
    String plainText = (String)part.getContent();
    /**
     * 1. convert all "\r\n" to "<br>" (windows)
     * 2. convert all "\n" to "<br>" (*nix)
     * 3. convert all "\r" to "<br>" (Mac)
     * 4. convert all "<br>" to "<br>\r\n" (for source readability)
     */
    String convertedText = plainText.replaceAll(Constants.CR_LF, "<br>").replaceAll("\n", "<br>").replaceAll("\r", "<br>").replaceAll("<br>", "<br>" + Constants.CR_LF);
    
    String htmlText =
      "<html><head></head>" + Constants.CR_LF + 
      "<body>" + Constants.CR_LF +
      convertedText + Constants.CR_LF +
      "</body></html>";
    
    part.setContent(htmlText,"text/html");
    part.setHeader(RFC2822Headers.CONTENT_TYPE, "text/html; charset=ISO-8859-1");
  }
  
  @Override
  protected String GetFooterHTML() 
  {
    W2OSignatureInjectionService signatureInjectionService = (W2OSignatureInjectionService)_mailet;
    if (signatureInjectionService.GetDebug() == true) 
    {
      signatureInjectionService.log("Adding html footer");
    }
    switch (_htmlImageSource._htmlImageSource)
    {
      case Inline:
        ConvertHTMLAppendixToReferenceInlineImage();
        break;
      case URL:
        // do nothing
        break;
    }
    
    return _htmlAppendix.toString();
  }

  @Override
  protected String GetFooterText()
  {
    W2OSignatureInjectionService signatureInjectionService = (W2OSignatureInjectionService)_mailet;
    if (signatureInjectionService.GetDebug() == true) 
    {
      signatureInjectionService.log("Adding plain text footer");
    }
    return _plainTextAppendix.toString();
  }
  
  protected static final String JAMES_SOURCE_START_TOKEN = "James_Source_Start";
  protected static final int    JAMES_SOURCE_START_TOKEN_LENGTH = JAMES_SOURCE_START_TOKEN.length(); 
  protected static final String SRC_BEGINNING_TOKEN = "src=\"";
  protected static final int    SRC_BEGINNING_TOKEN_LENGTH = SRC_BEGINNING_TOKEN.length();
  protected static final String SRC_ENDING_TOKEN = "\"";
  protected static final int    SRC_ENDING_TOKEN_LENGTH = SRC_ENDING_TOKEN.length();
  
  protected void ConvertHTMLAppendixToReferenceInlineImage()
  {
    W2OSignatureInjectionService signatureInjectionService = (W2OSignatureInjectionService)_mailet;
    if (signatureInjectionService.GetDebug() == true) 
    {
      signatureInjectionService.log();
    }
    String htmlAppendixReferencingRemoteImage = _htmlAppendix.toString();
    int jamesSourceStartIndex = htmlAppendixReferencingRemoteImage.indexOf(JAMES_SOURCE_START_TOKEN);
    int beginIndex = htmlAppendixReferencingRemoteImage.toLowerCase().indexOf(SRC_BEGINNING_TOKEN,jamesSourceStartIndex + JAMES_SOURCE_START_TOKEN_LENGTH);
    int endIndex = htmlAppendixReferencingRemoteImage.toLowerCase().indexOf(SRC_ENDING_TOKEN,beginIndex + SRC_BEGINNING_TOKEN_LENGTH) + SRC_ENDING_TOKEN_LENGTH;
    String htmlAppendixReferencingInlineImage = 
      htmlAppendixReferencingRemoteImage.toString().substring(0,beginIndex) +
      "src=\"cid:" + GetInlineImageContentID() + "\"" +
      htmlAppendixReferencingRemoteImage.toString().substring(endIndex);
    
    _htmlAppendix._string = htmlAppendixReferencingInlineImage;
    
    _need2ImbedImage = true;
  }

  protected synchronized String GetInlineImageContentID()
  {
    if (_inlineImageContentID == null)
    {
      _inlineImageContentID = String.valueOf(System.currentTimeMillis());  // todo need a better algorithm
    }
    
    return _inlineImageContentID;
  }
  
  protected void AddInlineImageBodyPart() 
    throws MessagingException, IOException
  {
    W2OSignatureInjectionService signatureInjectionService = (W2OSignatureInjectionService)_mailet;
    if (signatureInjectionService.GetDebug() == true) 
    {
      signatureInjectionService.log();
    }
    PreencodedMimeBodyPart imageDataBodyPart = new PreencodedMimeBodyPart("base64");
    String choppedImageData = ChopImageDataIntoSeparateLines(72); 
    imageDataBodyPart.setContent(choppedImageData, "image/jpeg");
    imageDataBodyPart.setText(choppedImageData);
    imageDataBodyPart.setHeader(RFC2822Headers.CONTENT_TYPE, "image/jpeg;" + Constants.CR_LF + " name=\"w2o001.jpg\"");
    imageDataBodyPart.setHeader("Content-Transfer-Encoding", "base64");
    imageDataBodyPart.setHeader("Content-ID", "<" + GetInlineImageContentID() + ">");
    imageDataBodyPart.setHeader("Content-Disposition", "inline;" + Constants.CR_LF + " filename=\"w2o001.jpg\"");
    
    Multipart multipartContent = (Multipart)_message.getContent();
    multipartContent.addBodyPart(imageDataBodyPart);
    // We have to do this because of a bug in JavaMail (ref id 4403733)
    // http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4403733
    _message.setContent(multipartContent,"multipart/related");
    _message.saveChanges();
    MimeMultipart mimeMultipart = (MimeMultipart)_message.getContent();
    String contentType = mimeMultipart.getContentType();
    String boundary = GetBoundary(contentType);    
    _message.setHeader(RFC2822Headers.CONTENT_TYPE, "multipart/related; boundary=\"" + boundary + "\"");
  }
  
  protected static final String BOUNDARY_BEGIN = "boundary=";
  protected static final int    BOUNDARY_BEGIN_LENGTH = BOUNDARY_BEGIN.length();
  
  protected String GetBoundary(String multipartContentType) 
    throws MessagingException
  {
    String fixedUpMultipartContentType = multipartContentType.trim().replace("\"", "");
    W2OSignatureInjectionService signatureInjectionService = (W2OSignatureInjectionService)_mailet;
    int boundaryBegin = fixedUpMultipartContentType.indexOf(BOUNDARY_BEGIN);
    if (boundaryBegin == -1)
    {
      throw new MessagingException("cannot find boundary in " + multipartContentType);
    }
    boundaryBegin += BOUNDARY_BEGIN_LENGTH;
    String boundary = fixedUpMultipartContentType.substring(boundaryBegin);

    if (signatureInjectionService.GetDebug() == true) 
    {
      signatureInjectionService.log("Multi-part boundary is \"" + boundary + "\"");
    }
    
    return boundary;
  }

  protected String ChopImageDataIntoSeparateLines(int maximumLineLength)
  {
    W2OSignatureInjectionService signatureInjectionService = (W2OSignatureInjectionService)_mailet;
    if (signatureInjectionService.GetDebug() == true) 
    {
      signatureInjectionService.log("Chopping image base 64 data into " + maximumLineLength + " character lines");
    }
    String imageBase64 = _imageBase64.toString();
    int imageDataLength = imageBase64.length();
    StringBuilder choppedImageData = new StringBuilder();
    for (int beginIndex = 0; beginIndex < imageDataLength; beginIndex += maximumLineLength)
    {
      int endIndex = Math.min(beginIndex + maximumLineLength,imageDataLength);
      choppedImageData.append(imageBase64.subSequence(beginIndex, endIndex));          
      choppedImageData.append("\r\n");          
    }
    
    return choppedImageData.toString();
  }

  protected BufferedImage _image;

  protected void DisplayMessageSentDialog()
  {
    W2OSignatureInjectionService signatureInjectionService = (W2OSignatureInjectionService)_mailet;
    try
    {
      // convert base 64 into a binary byte array
      byte[] imageBase64Bytes = _imageBase64._string.getBytes();
      ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(imageBase64Bytes);
      Base64InputStream base64InputStream = new Base64InputStream(byteArrayInputStream);
      byte[] bloatedImageBytes = new byte[imageBase64Bytes.length];  // base 64 is always larger than its binary equivalent. so we know that this buffer will be large enough to hold the translated binary value.
      int numberOfBytes = base64InputStream.read(bloatedImageBytes);
      byte[] imageBytes = Arrays.copyOf(bloatedImageBytes,numberOfBytes);
      
      // convert binary byte array into an image
      byteArrayInputStream = new ByteArrayInputStream(imageBytes);
      _image = ImageIO.read(byteArrayInputStream);

      final MessageSentDialog  messageSentDialog = new MessageSentDialog();

      // size the image panel to fit the image
      int imageHeight = _image.getHeight();
      int imageWidth = _image.getWidth();
      Dimension imageDimension = new Dimension(imageWidth,imageHeight);
      messageSentDialog.GetImagePanel().setPreferredSize(imageDimension);

      messageSentDialog.pack();
      
      // position the dialog
      Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
      Dimension dialogDimension = messageSentDialog.getPreferredSize();
      int x = screenSize.width - dialogDimension.width;
      int y = screenSize.height - dialogDimension.height; 
      messageSentDialog.setLocation(x, y);
      
      // show the dialog
      if (signatureInjectionService.GetDebug() == true) 
      {
        signatureInjectionService.log();
      }
      messageSentDialog.setVisible(true);
      messageSentDialog.toFront();
      
      new SwingWorker()
      {
        
        @Override
        protected Object doInBackground()
          throws Exception
        {
          Thread.sleep(3000L);
          return null;
        }
        
        @Override
        protected void done() 
        {
          messageSentDialog.setVisible(false);
          messageSentDialog.dispose();
        }
        
      }.execute();
    }
    catch (IOException ex)
    {
      signatureInjectionService.Log(ex);
    }
  }

  protected class MessageSentDialog
    extends JDialog
  {

    protected JPanel _imagePanel;
    
    public MessageSentDialog()
    {
      super();
      
      setModalityType(ModalityType.MODELESS);
      
      _imagePanel = new JPanel() 
      { 
        @Override
        public void paint(Graphics graphics)
        {
          graphics.drawImage(_image,0,0,null);
        }
      };
      
      getContentPane().add(_imagePanel);
    }
    
    public JPanel GetImagePanel()
    {
      return _imagePanel;
    }
    
  }
  
  protected class SMTPAuthenticator 
    extends javax.mail.Authenticator
  {

    @Override
    public PasswordAuthentication getPasswordAuthentication()
    {
      W2OSignatureInjectionService signatureInjectionService = (W2OSignatureInjectionService)_mailet;
      String username = signatureInjectionService.GetW2OSupportAuthUser().trim();
      String password = signatureInjectionService.GetW2OSupportAuthPassword().trim();
          
      return new PasswordAuthentication(username, password);
    }
  }

}
