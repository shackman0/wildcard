package com.World2One.Mailets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.Enumeration;

import javax.mail.Header;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;

import com.World2One.Database.Database;


/**
 * <p>Title: ArchiverService</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One, Inc.</p>
 */
public class W2OJDBCArchiverService
  extends AbstractMailet
{


  protected Database _database;

  
  public W2OJDBCArchiverService()
  {
    super();
  }
  
  protected String GetDatabaseSoftware()
  {
    return GetInitParameter(com.World2One.Mailets.Constants.DATABASE_SOFTWARE);
  }

  protected String GetDatabaseHost()
  {
    return GetInitParameter(com.World2One.Mailets.Constants.DATABASE_HOST);
  }

  protected String GetDatabasePort()
  {
    return GetInitParameter(com.World2One.Mailets.Constants.DATABASE_PORT);
  }

  protected String GetDatabaseName()
  {
    return GetInitParameter(com.World2One.Mailets.Constants.DATABASE_NAME);
  }

  protected String GetDatabaseUser()
  {
    return GetInitParameter(com.World2One.Mailets.Constants.DATABASE_USER);
  }

  protected String GetDatabasePassword()
  {
    return GetInitParameter(com.World2One.Mailets.Constants.DATABASE_PASSWORD);
  }

  
  @Override
  public void init() 
    throws MessagingException
  {
    super.init();
    
    _database = new Database(GetDatabaseSoftware(),GetDatabaseHost(),GetDatabasePort(),GetDatabaseName(),GetDatabaseUser(),GetDatabasePassword());
    // make sure we can connect to the database
    try
    {
      _database.GetConnection();
      _database.Disconnect();
    }
    catch (SQLException ex)
    {
      Log(ex);
      throw new MessagingException(ex.getLocalizedMessage(),ex);
    }
  }
  
  @Override
  public void service(Mail mail)
    throws MessagingException
  {
    Statement statement = null;
    try
    {
      Connection connection = _database.GetConnection();
      statement = connection.createStatement();
    
      statement.execute(Database.BEGIN_TRANSACTION);
      ArchiveEMail(mail,statement);    
      statement.execute(Database.COMMIT_TRANSACTION);
      
      statement.close();
      statement = null;
    }
    catch (Exception ex)
    {
      Log(ex);
      throw new MessagingException(ex.getLocalizedMessage(),ex);
    }
    finally
    {
      try
      {
        if (statement != null)
        {
          statement.execute(Database.ROLLBACK_TRANSACTION);
          statement.close();
        }
        _database.Disconnect();
      }
      catch (SQLException ex)
      {
        Log(ex);
        throw new MessagingException(ex.getLocalizedMessage(),ex);
      }
    }
  }
  
  protected void ArchiveEMail(
    Mail      mail,
    Statement statement ) 
    throws SQLException, MessagingException, IOException
  {
    int emailID = GetNextEMailID(mail,statement);

    InsertSender(mail,statement,emailID);  
    InsertRecipients(mail,statement,emailID);
    InsertHeaders(mail,statement,emailID);
  }

  protected static final String GET_NEXT_EMAIL_ID = "select nextval('email_id_seq')";
  
  protected int GetNextEMailID(
    Mail      mail,
    Statement statement ) 
    throws SQLException
  {
    ResultSet resultSet = ExecuteQuery(statement,GET_NEXT_EMAIL_ID);
    resultSet.next();
    int nextEMailIDValue = resultSet.getInt(1);
    
    return nextEMailIDValue;
  }

  protected static final String INSERT_INTO_SENDER_TBL = "insert into sender_tbl (id, sender, subject) values ({0},''{1}'',''{2}'')";
  
  protected void InsertSender (
    Mail      mail,
    Statement statement,
    int       emailID ) 
    throws SQLException, MessagingException, IOException
  {
    MailAddress sender = mail.getSender();
    MimeMessage message = mail.getMessage();
    String subject = message.getSubject();
    
    String sqlString = MessageFormat.format(INSERT_INTO_SENDER_TBL, emailID, sender.toString(), subject);
    ExecuteUpdate(statement,sqlString);
  }
  
  protected static final String INSERT_INTO_RECIPIENT_TBL = "insert into recipient_tbl (email_id, recipient) values ({0},''{1}'')";

  protected void InsertRecipients(
    Mail      mail,
    Statement statement,
    int       emailID )
    throws SQLException
  {
    Collection<MailAddress> recipients = mail.getRecipients();
    for (MailAddress recipient: recipients)
    {
      String sqlString = MessageFormat.format(INSERT_INTO_RECIPIENT_TBL, emailID, recipient.toString());
      ExecuteUpdate(statement,sqlString);
    }
  }
  
  protected static final String INSERT_INTO_HEADER_TBL = "insert into header_tbl (email_id, name, value) values ({0},''{1}'',''{2}'')";

  protected void InsertHeaders(
    Mail      mail,
    Statement statement,
    int       emailID )
    throws SQLException, MessagingException
  {
    MimeMessage message = mail.getMessage();
    Enumeration<Header> headers = message.getAllHeaders();
    for (;headers.hasMoreElements();)
    {
      Header header = headers.nextElement();
      String sqlString = MessageFormat.format(INSERT_INTO_HEADER_TBL, emailID, header.getName(), header.getValue());
      ExecuteUpdate(statement,sqlString);
    }
  }
  
  @Override
  public void destroy()
  {
    try
    {
      _database.Disconnect();
    }
    catch (SQLException ex)
    {
      log(ex.getLocalizedMessage(),ex);
    }
    finally
    {
      _database = null;
    }
  }

  protected void ExecuteUpdate(
    Statement statement,
    String    sqlString )
    throws SQLException 
  {
    try
    {
      statement.executeUpdate(sqlString);
    }
    catch (SQLException ex)
    {
      log(sqlString);
      Log(ex);      

      throw ex;
    }
  }

  protected ResultSet ExecuteQuery(
    Statement statement,
    String    sqlString )
    throws SQLException 
  {
    ResultSet resultSet;
    try
    {
      resultSet = statement.executeQuery(sqlString);
    }
    catch (SQLException ex)
    {
      log(sqlString);
      Log(ex);      

      throw ex;
    }
    
    return resultSet;
  }
}
