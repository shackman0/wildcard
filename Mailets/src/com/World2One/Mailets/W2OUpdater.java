package com.World2One.Mailets;

import javax.mail.MessagingException;

import org.apache.mailet.Mail;

public class W2OUpdater
  extends AbstractMailet
{


  
  public W2OUpdater()
  {
    super();
  }
  
  
  @Override
  public void init() 
    throws MessagingException 
  {
    boolean updated = false;
    try
    {
      super.init();  

      log("Running WildCard Version " + com.World2One.Utility.Constants.WILDCARD_VERSION);
      
      String rmiBindAddr = getInitParameter(com.World2One.Mailets.Constants.RMI_BIND_ADDR);
      int rmiBindPort = Integer.valueOf(getInitParameter(com.World2One.Mailets.Constants.RMI_BIND_PORT));

      UpdateClient updateClient = new UpdateClient(this,rmiBindAddr,rmiBindPort);
      updated = updateClient.UpdateIfNeeded();
      System.out.println("update available = " + updated);
      updateClient.Disconnect();
      
      if (updated == true)
      {
        // todo: there has to be a better way to shut down the james server than throwing a MessagingException.
        throw new MessagingException("W2O WildCard update received. Stopping now ...");
      }
    }
    catch (Exception ex)
    {
      // if we fail, just keep on going.
      Log(ex);
    }
  }
 

  @Override
  public void service(Mail mail)
    throws MessagingException
  {
    // do nothing
  }

}
