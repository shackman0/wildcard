package com.World2One.Mailets;

import java.util.Hashtable;
import java.util.Iterator;

import javax.mail.MessagingException;

import org.apache.mailet.GenericMailet;

import com.World2One.Utility.Constants;
import com.World2One.Utility.ToolBox;

/**
 * <p>Title: AbstractMailet</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One, Inc.</p>
 */
public abstract class AbstractMailet
  extends GenericMailet
{

  protected Hashtable<String,String> _initParameters = new Hashtable();
  
  
  public AbstractMailet()
  {
    super();
  }
  
  @Override
  public void init() 
    throws MessagingException
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Runtime options:")
                 .append(Constants.CR_LF);
    for (Iterator iterator = getInitParameterNames(); iterator.hasNext();)
    {
      String initParameterName = (String)iterator.next();
      String initParameterValue = getInitParameter(initParameterName);
      stringBuilder.append(initParameterName)
                   .append(" = ")
                   .append(initParameterValue)
                   .append(Constants.CR_LF);
      _initParameters.put(initParameterName.toLowerCase(), initParameterValue);
    }
    log(stringBuilder.toString());

    super.init();
  }
  
  protected Boolean _debug = null;
  
  protected boolean GetDebug()
  {
    if (_debug == null)
    {
      _debug = GetBooleanInitParameter(com.World2One.Mailets.Constants.DEBUG,false);
    }
    
    return _debug;
  }

  protected String GetInitParameter(String parameterName)
  {
    return _initParameters.get(parameterName.toLowerCase());
  }

  protected String GetInitParameter(String parameterName, String defaultValue)
  {
    String initParameterValue = GetInitParameter(parameterName);
    if (initParameterValue == null)
    {
      initParameterValue = defaultValue;
    }
    
    return initParameterValue;
  }

  protected Boolean GetBooleanInitParameter(String parameterName)
  {
    Boolean booleanInitParameter = null;
    
    String booleanInitParameterValue = GetInitParameter(parameterName);
    if (booleanInitParameterValue != null)
    {
      booleanInitParameterValue = booleanInitParameterValue.trim();
      if (booleanInitParameterValue.length() > 0)
      {
        booleanInitParameter = Boolean.valueOf(booleanInitParameterValue);
      }
    }
    
    return booleanInitParameter;
  }

  protected boolean GetBooleanInitParameter(String parameterName, boolean defaultValue)
  {
    Boolean booleanInitParameter = GetBooleanInitParameter(parameterName);
    if (booleanInitParameter == null)
    {
      booleanInitParameter = defaultValue;
    }
    
    return booleanInitParameter;
  }

  protected Integer GetIntegerInitParameter(String parameterName)
  {
    Integer intInitParameter = null;
    
    String booleanInitParameterValue = GetInitParameter(parameterName);
    if (booleanInitParameterValue != null)
    {
      booleanInitParameterValue = booleanInitParameterValue.trim();
      if (booleanInitParameterValue.length() > 0)
      {
        intInitParameter = Integer.valueOf(booleanInitParameterValue);
      }
    }
    
    return intInitParameter;
  }

  protected int GetIntegerInitParameter(String parameterName, int defaultValue)
  {
    Integer integerInitParameter = GetIntegerInitParameter(parameterName);
    if (integerInitParameter == null)
    {
      integerInitParameter = defaultValue;
    }
    
    return integerInitParameter;
  }

  public void log()
  {
    log("");
  }
  
  @Override
  public void log(String message) 
  {
    if (message == null)
    {
      Log(new Exception("attempting to log a null message"));
    }
    else
    {
      Exception ex = new Exception();
      ex.fillInStackTrace();
      StackTraceElement[] stackTraceElements = ex.getStackTrace();

      int index = 0;
      StackTraceElement stackTraceElement;
      String methodName;
      do
      {
        index++;
        stackTraceElement = stackTraceElements[index];
        methodName = stackTraceElement.getMethodName();
      }
      while (methodName.equals("log") == true);
      
      int lineNumber = stackTraceElement.getLineNumber();
      String fileName = stackTraceElement.getFileName();
      String newMessage = methodName + "() (" + fileName + " line " + lineNumber + "): " + message;
      
      super.log(newMessage);
    }
  }
  
  protected void Log(Exception ex)
  {
    String message = ex.getMessage();
    if (message != null)
    {
      super.log(message);
    }
    
    LogException(ex);
  }

  protected void Log(String message, Exception ex)
  {
    if (message != null)
    {
      super.log(message);
    }

    LogException(ex);
  }

  protected void LogException(Exception ex)
  {
    super.log(ToolBox.GetStackTrace(ex));
  }
  
  protected MessagingException GetMessagingException(Exception ex) 
  {
    if ((ex instanceof MessagingException) == false)
    {
      ex = new MessagingException(ex.getLocalizedMessage(),ex);
    }
    
    return (MessagingException)ex;
  }
  
}
