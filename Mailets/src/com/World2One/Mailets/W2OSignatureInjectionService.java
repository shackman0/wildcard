package com.World2One.Mailets;

import java.awt.AWTException;
import java.awt.CheckboxMenuItem;
import java.awt.Menu;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

import org.apache.mailet.Mail;

import com.World2One.Mailets.SOAP.Cards;
import com.World2One.Mailets.SOAP.Emails;
import com.World2One.Mailets.SOAP.Cards.MutableHTMLImageSource;
import com.World2One.Mailets.SOAP.Cards.MutablePlainTextHandling;
import com.World2One.OS.SystemTray;
import com.World2One.Utility.MutableString;


/**
 * <p>Title: SignatureInjectionService</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One, Inc.</p>
 */
public class W2OSignatureInjectionService
  extends AbstractMailet
{


  protected Emails _soapEmails;
  protected Cards  _soapCards;

  
  public W2OSignatureInjectionService()
  {
    super();
  }
  
  protected String GetSOAPProtocol()
  {
    return GetInitParameter(com.World2One.Mailets.Constants.SOAP_PROTOCOL);
  }
  
  protected String GetSOAPHost()
  {
    return GetInitParameter(com.World2One.Mailets.Constants.SOAP_HOST);
  }
  
  protected String GetSOAPPort()
  {
    return GetInitParameter(com.World2One.Mailets.Constants.SOAP_PORT);
  }
  
  protected Boolean _injectSignatureOnReplyOrForward = null;
  
  protected boolean GetInjectSignatureOnReplyOrForward()
  {
    if (_injectSignatureOnReplyOrForward == null)
    {
      _injectSignatureOnReplyOrForward = GetBooleanInitParameter(com.World2One.Mailets.Constants.INJECT_SIGNATURE_ON_REPLY_AND_FORWARD,false);
    }
    
    return _injectSignatureOnReplyOrForward;
  }
  
  protected void SetInjectSignatureOnReplyOrForward(boolean injectSignatureOnReplyOrForward)
  {
    _injectSignatureOnReplyOrForward = injectSignatureOnReplyOrForward;
    if (GetGUISupported() == true)
    {
      if (_injectSignatureOnReplyOrForwardCheckBoxMenuItem.getState() != injectSignatureOnReplyOrForward)
      {
        _injectSignatureOnReplyOrForwardCheckBoxMenuItem.setState(injectSignatureOnReplyOrForward);
      }
    }
  }
  
  protected boolean _signatureInjectionEnabled = true;
  
  protected boolean IsSignatureInjectionEnabled()
  {
    return _signatureInjectionEnabled;
  }
  
  protected void SetSignatureInjectionEnabled(boolean signatureInjectionEnabled)
  {
    _signatureInjectionEnabled = signatureInjectionEnabled;
    if (GetGUISupported() == true)
    {
      if (_injectSignatureOnReplyOrForwardCheckBoxMenuItem.getState() != signatureInjectionEnabled)
      {
        _injectSignatureEnabledCheckBoxMenuItem.setState(signatureInjectionEnabled);
      }
      _injectSignatureOnReplyOrForwardCheckBoxMenuItem.setEnabled(signatureInjectionEnabled);
    }
  }
  
  protected boolean GetGUISupported()
  {
    return GetBooleanInitParameter(com.World2One.Mailets.Constants.GUI_SUPPORTED,false);
  }
  
  protected boolean GetLogAuthorizedRecipients()
  {
    return GetBooleanInitParameter(com.World2One.Mailets.Constants.LOG_AUTHORIZED_RECIPIENTS,false);
  }
  
  protected boolean GetW2OSupportSendEmailOnError()
  {
    return GetBooleanInitParameter(com.World2One.Mailets.Constants.W2O_SUPPORT_SEND_EMAIL_ON_ERROR,false);
  }
  
  protected String GetW2OSupportToEmailAddress()
  {
    return GetInitParameter(com.World2One.Mailets.Constants.W2O_SUPPORT_TO_EMAIL_ADDRESS);
  }
  
  protected String GetW2OSupportFromEmailAddress()
  {
    return GetInitParameter(com.World2One.Mailets.Constants.W2O_SUPPORT_FROM_EMAIL_ADDRESS);
  }
  
  protected String GetW2OSupportSMTPHost()
  {
    return GetInitParameter(com.World2One.Mailets.Constants.W2O_SUPPORT_SMTP_HOST);
  }
  
  protected String GetW2OSupportPort()
  {
    return GetInitParameter(com.World2One.Mailets.Constants.W2O_SUPPORT_PORT);
  }
  
  protected String GetW2OSupportSMTPProtocol()
  {
    return GetInitParameter(com.World2One.Mailets.Constants.W2O_SUPPORT_SMTP_PROTOCOL);
  }
  
  protected String GetW2OSupportAuthUser()
  {
    return GetInitParameter(com.World2One.Mailets.Constants.W2O_SUPPORT_AUTH_USER);
  }
  
  protected String GetW2OSupportAuthPassword()
  {
    return GetInitParameter(com.World2One.Mailets.Constants.W2O_SUPPORT_AUTH_PASSWORD);
  }
  
  protected String GetW2OSupportSubject()
  {
    return GetInitParameter(com.World2One.Mailets.Constants.W2O_SUPPORT_SUBJECT);
  }
  
  protected boolean GetDisplayMessageSentPopup()
  {
    return GetBooleanInitParameter(com.World2One.Mailets.Constants.DISPLAY_MESSAGE_SENT_POPUP,false);
  }

  protected CheckboxMenuItem  _injectSignatureEnabledCheckBoxMenuItem;
  protected CheckboxMenuItem  _injectSignatureOnReplyOrForwardCheckBoxMenuItem;

  
  
  @Override
  public void init() 
    throws MessagingException
  {
    super.init();
    
    _soapEmails = new Emails();
    _soapEmails.SetDebug(GetDebug(),this);
    
    _soapCards = new Cards();
    _soapCards.SetDebug(GetDebug(),this);
    
    if (GetGUISupported() == true)
    {
      if (GetDebug() == true)
      {
        log("adding system tray icon");
      }
      
      Menu systemTrayMenu = new Menu("Signature Injection Service");
      
      _injectSignatureEnabledCheckBoxMenuItem = new CheckboxMenuItem ("Inject Signature");
      systemTrayMenu.add(_injectSignatureEnabledCheckBoxMenuItem);
      _injectSignatureEnabledCheckBoxMenuItem.addItemListener(new InjectSignatureEnabledItemListener());
      
      _injectSignatureOnReplyOrForwardCheckBoxMenuItem = new CheckboxMenuItem ("Inject Signature on Reply or Forward");
      systemTrayMenu.add(_injectSignatureOnReplyOrForwardCheckBoxMenuItem);
      _injectSignatureOnReplyOrForwardCheckBoxMenuItem.addItemListener(new InjectSignatureOnReplyOrForwardItemListener());

      try
      {
        SystemTray.GetSingleton().AddMenu(systemTrayMenu);
      }
      catch (AWTException ex)
      {
        Log(ex);
        throw GetMessagingException(ex);
      }
    }

    SetSignatureInjectionEnabled(true);
    SetInjectSignatureOnReplyOrForward(GetInjectSignatureOnReplyOrForward());
  }
  
  @Override
  public void service(Mail mail)
    throws MessagingException
  {
    if (_signatureInjectionEnabled == true)
    {
      W2OSignatureInjectionServiceHelper signatureInjectionServiceHelper = new W2OSignatureInjectionServiceHelper(this);
      signatureInjectionServiceHelper.service(mail);
    }
  }

  protected String GetCardResourcesByEmailAddress(
    String                    senderAddress,
    MutableString             iCardID,
    MutableString             htmlAppendix,
    MutableString             plainTextAppendix,
    MutableString             imageBase64,
    MutablePlainTextHandling  plainTextHandling,
    MutableHTMLImageSource    htmlImageSource ) 
    throws Exception
  {
    String errorMessage = _soapCards.GetCardResourcesByEmailAddress(
      GetSOAPProtocol(),
      GetSOAPHost(),
      GetSOAPPort(),
      senderAddress,
      iCardID,
      htmlAppendix,
      plainTextAppendix,
      imageBase64,
      plainTextHandling,
      htmlImageSource );

    return errorMessage;
  }
  
  protected String LogAuthorizedRecipients(
    String            iCardID,
    InternetAddress[] recipients) 
    throws Exception
  {
    String errorMessage = _soapEmails.LogAuthorizedRecipients (
        GetSOAPProtocol(),
        GetSOAPHost(),
        GetSOAPPort(),
        iCardID, 
        recipients );
      
    return errorMessage;
  }
  
  protected class InjectSignatureEnabledItemListener
    implements ItemListener
  {

    public InjectSignatureEnabledItemListener()
    {
      super();
    }

    @Override
    public void itemStateChanged(ItemEvent event)
    {
      boolean newState = _injectSignatureEnabledCheckBoxMenuItem.getState();
      SetSignatureInjectionEnabled(newState);
    }
    
  }

  protected class InjectSignatureOnReplyOrForwardItemListener
    implements ItemListener
  {

    public InjectSignatureOnReplyOrForwardItemListener()
    {
      super();
    }

    @Override
    public void itemStateChanged(ItemEvent event)
    {
      boolean newState = _injectSignatureOnReplyOrForwardCheckBoxMenuItem.getState();
      SetInjectSignatureOnReplyOrForward(newState);
    }
    
  }

}
