package com.World2One.Mailets;

import javax.mail.MessagingException;

import org.apache.mailet.HostAddress;
import org.apache.mailet.MailAddress;

import com.World2One.SMTP.SMTPHost;
import com.World2One.SMTP.SMTPHost.SMTPHostsOverridesKey;
import com.World2One.SMTP.SMTPHost.SMTPHostsOverridesTable;
import com.World2One.Utility.MutableInteger;
import com.World2One.Utility.MutableString;

/**
 * <p>Title: W2ORemoteDelivery</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One, Inc.</p>
 */
public class W2ORemoteDelivery
  extends AbstractRemoteDelivery
{

  

  /**
   * we run in the top-level James bin directory. (see run.bat)
   */
  protected static final String W2O_REMOTE_DELIVERY_FILE_PATH = "../apps/james/Runtime/W2ORemoteDelivery.xml";
  
  protected SMTPHostsOverridesTable   _smtpHostsOverride = null;
  
  
  
  public W2ORemoteDelivery()
  {
    super();
  }
  
  
  @Override
  public void init() 
    throws MessagingException 
  {
    try
    {
      super.init();  

      StringBuilder stringBuilder = null;
      if (GetDebug() == true)
      {
        stringBuilder = new StringBuilder(); 
      }
      _smtpHostsOverride = SMTPHost.GetSMTPHostsOverrides(W2O_REMOTE_DELIVERY_FILE_PATH,stringBuilder);
      if (stringBuilder != null)
      {
        log("SMTP Hosts Overrides:" + stringBuilder.toString());
      }
    }
    catch (Exception ex)
    {
      _smtpHostsOverride = null;
      throw GetMessagingException(ex);
    }
  }
  
  @Override
  protected boolean IsSenderEmailInSMTPHostsOverride(String senderEmail)
  {
    return _smtpHostsOverride.ContainsKey(new SMTPHostsOverridesKey(null,senderEmail));
  }
  
  @Override
  protected HostAddress GetOutgoingMailServerAuthCredentials(
    MailAddress    sender,
    HostAddress    outgoingMailServer,
    MutableString  hostAddress,
    MutableInteger hostPort,
    MutableString  localAuthUser,
    MutableString  localAuthPass,
    MutableString  secureConnection ) 
    throws MessagingException
  {
    log("at entry outgoingMailServer = " + outgoingMailServer.toString());
    
    HostAddress newOutgoingMailServer;
    
    if (sender == null)
    {
      newOutgoingMailServer = super.GetOutgoingMailServerAuthCredentials(sender, outgoingMailServer, hostAddress, hostPort, localAuthUser, localAuthPass, secureConnection);
    }
    else
    {
      String senderEmail = sender.toString().toLowerCase();
      SMTPHost smtpHost = _smtpHostsOverride.Get(new SMTPHostsOverridesKey(null,senderEmail));
      if (smtpHost == null)
      {
        newOutgoingMailServer = super.GetOutgoingMailServerAuthCredentials(sender, outgoingMailServer, hostAddress, hostPort, localAuthUser, localAuthPass, secureConnection);
      }
      else
      {
        try
        {
          hostAddress._string = smtpHost.GetHostAddress();
          hostPort._integer = smtpHost.GetPort();
          localAuthUser._string = smtpHost.GetUser();
          authUser = localAuthUser._string;
          localAuthPass._string = smtpHost.GetPassword();
          authPass = localAuthPass._string;
          secureConnection._string = smtpHost.GetSecureConnection().name(); 
          
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(hostAddress._string);
          if (hostPort._integer != null)
          {
            stringBuilder.append(":");
            stringBuilder.append(hostPort._integer);
          }
          
          String smtpProtocol = smtpHost.GetSecureConnection().GetSMTPProtocol();
          newOutgoingMailServer = new org.apache.mailet.HostAddress(stringBuilder.toString(), smtpProtocol + "://" + stringBuilder.toString());
        }
        catch (Exception ex)
        {
          Log(ex);
          throw GetMessagingException(ex);
        }
      }
    }
    
    log("at exit outgoingMailServer = " + newOutgoingMailServer.toString());

    return newOutgoingMailServer;
  }
  
}
