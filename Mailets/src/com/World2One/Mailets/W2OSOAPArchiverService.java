package com.World2One.Mailets;

import java.io.ByteArrayOutputStream;

import javax.mail.MessagingException;
import javax.swing.SwingWorker;

import org.apache.mailet.Mail;
import org.apache.mailet.MailAddress;

import com.World2One.Mailets.SOAP.Emails;


/**
 * <p>Title: W2OSOAPArchiverService</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One, Inc.</p>
 */
public class W2OSOAPArchiverService
  extends AbstractMailet
{


  protected Emails _emails;
  
  
  public W2OSOAPArchiverService()
  {
    super();
  }
  
  protected String GetSOAPProtocol()
  {
    return GetInitParameter(com.World2One.Mailets.Constants.SOAP_PROTOCOL);
  }
  
  protected String GetSOAPHost()
  {
    return GetInitParameter(com.World2One.Mailets.Constants.SOAP_HOST);
  }
  
  protected String GetSOAPPort()
  {
    return GetInitParameter(com.World2One.Mailets.Constants.SOAP_PORT);
  }

  @Override
  public void init() 
    throws MessagingException
  {
    super.init();
    
    _emails = new Emails();
    _emails.SetDebug(GetDebug(),this);
  }

  @Override
  public void service(final Mail mail)
    throws MessagingException
  {
    new SwingWorker()
    {

      @Override
      protected Object doInBackground()
        throws Exception
      {
        try
        {
          ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
          mail.getMessage().writeTo(outputStream);
          String emailInStringForm = outputStream.toString();
          
          MailAddress senderMailAddress = mail.getSender();
          String senderAddress = senderMailAddress.toInternetAddress().getAddress();
          
          _emails.LogEmail(GetSOAPProtocol(),GetSOAPHost(), GetSOAPPort(), senderAddress, emailInStringForm);
        }
        catch (Exception ex)
        {
          Log(ex);
          throw GetMessagingException(ex);
        }

        return null;
      }
      
    }.execute();
    
  }
  
}
