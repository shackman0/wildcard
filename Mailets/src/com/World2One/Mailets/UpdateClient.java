package com.World2One.Mailets;

import java.io.IOException;
import java.rmi.RemoteException;

import com.World2One.Updater.UpdateServer;
import com.World2One.Utility.Constants;

/**
 * <p>Title: UpdateClient</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public class UpdateClient
  extends com.World2One.Updater.AbstractUpdateClient
{
  
  protected static final String WILD_CARD_UPDATE_ZIP_FILE_PATH = "../apps/james/Runtime/" + Constants.WILD_CARD_UPDATE_ZIP_FILE_NAME;

  protected W2OUpdater _w2OUpdater;

  public UpdateClient(
    W2OUpdater w2OUpdater,
    String...  args)
  {
    super(args);
    
    _w2OUpdater = w2OUpdater;
  }
  
  public UpdateClient(
    W2OUpdater w2OUpdater,
    String     rmiBindAddr, 
    int        rmiBindPort)
  {
    super(rmiBindAddr,rmiBindPort);
    
    _w2OUpdater = w2OUpdater;
  }
  
  @Override
  protected void ShutdownJVM() 
  {
    // do nothing. W2OUpdater will throw a MessagingException for me.
  }

  @Override
  protected boolean IsUpdateAvailable(UpdateServer updateServer)
    throws RemoteException
  {
    boolean wildCardUpdateIsAvailable = updateServer.IsWildCardUpdateAvailable(Constants.WILDCARD_VERSION);
    
    return wildCardUpdateIsAvailable;
  }


  @Override
  protected void ReadAndWriteUpdateZipFileAndExit(UpdateServer updateServer)
    throws RemoteException, IOException
  {
    _w2OUpdater.log("Updating WildCard to newer version");
    ReadAndWriteUpdateZipFileAndExit(updateServer, WILD_CARD_UPDATE_ZIP_FILE_PATH);
  }

  @Override
  protected byte[] GetUpdateZipFileBytes(UpdateServer updateServer, int length) 
    throws RemoteException
  {
    byte[] wildCardUpdateZipFileBytes = updateServer.GetWildCardUpdateZipFileBytes(length);
    
    return wildCardUpdateZipFileBytes;
  }
  
}
