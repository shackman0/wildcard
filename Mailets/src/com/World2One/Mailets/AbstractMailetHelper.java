package com.World2One.Mailets;

import javax.mail.MessagingException;

import org.apache.mailet.Mail;

/**
 * <p>Title: AbstractMailetHelper</p>
 * <p>Description: an abstract class to contain functionality for a mailet to ensure thread safeness of the mailet service(Mail) method.</p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public abstract class AbstractMailetHelper
{
  
  /**
   * the mailet for which this is a helper
   */
  protected AbstractMailet _mailet;
  
  public AbstractMailetHelper(AbstractMailet mailet)
  {
    super();
    
    _mailet = mailet;
  }

  public abstract void service(Mail mail)
    throws MessagingException;

}
