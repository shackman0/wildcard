/****************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one * or more contributor license agreements. See the NOTICE
 * file * distributed with this work for additional information * regarding copyright ownership. The ASF licenses this
 * file * to you under the Apache License, Version 2.0 (the * "License"); you may not use this file except in compliance
 * * with the License. You may obtain a copy of the License at * * http://www.apache.org/licenses/LICENSE-2.0 * * Unless
 * required by applicable law or agreed to in writing, * software distributed under the License is distributed on an *
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY * KIND, either express or implied. See the License for the *
 * specific language governing permissions and limitations * under the License. *
 ****************************************************************/

package com.World2One.Mailets;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Enumeration;
import java.util.HashMap;

import javax.mail.Header;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimePart;

import org.apache.mailet.Mail;
import org.apache.mailet.RFC2822Headers;

import com.World2One.Utility.Constants;


/**
 * <p>Title: AbstractAddFooter</p>
 * <p>Description: An abstract implementation of a mailet-helper that adds a Footer to an email<p>
 * Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One, Inc.</p>
 */
public abstract class AbstractAddFooter
  extends AbstractMailetHelper
{
  

  protected boolean _footerAttached = false;
  
  /**
   * the message currently being serviced
   */
  protected MimeMessage _message = null;
  protected HashMap<String,Header> _messageHeaders = null;

  
  
  public AbstractAddFooter(AbstractMailet mailet)
  {
    super(mailet);
  }

  /**
   * Takes the message and attaches a footer message to it. Right now, it only supports simple messages. Needs to have
   * additions to make it support messages with alternate content types or with attachments.
   * 
   * @param mail
   *          the mail being processed
   * 
   * @throws MessagingException
   *           if an error arises during message processing
   */
  @Override
  public void service(Mail mail)
    throws MessagingException
  {
    try
    {
      _message = mail.getMessage();
      LoadMessageHeaders();

      _footerAttached = AttachFooter(null,_message);

      if (_footerAttached == true)
      {
        _message.saveChanges();
      }
      else
      {
        _mailet.log("Unable to add footer to mail " + mail.getName());
      }
    }
    catch (UnsupportedEncodingException ex)
    {
      _mailet.log("UnsupportedEncoding Unable to add footer to mail " + mail.getName());
    }
    catch (IOException ex)
    {
      throw new MessagingException("Could not read message", ex);
    }
    finally
    {
      _message = null;
      _messageHeaders = null;
    }
    
  }

  protected void LoadMessageHeaders() 
    throws MessagingException
  {
    _messageHeaders = new HashMap();
    for (Enumeration enumeration = _message.getAllHeaders(); enumeration.hasMoreElements();)
    {
      Header header = (Header)enumeration.nextElement();
      _messageHeaders.put(header.getName(),header);
    }
  }
  
  /**
   * Prepends the content of the MimePart as text to the existing footer
   * 
   * @param part
   *          the MimePart to attach
   * 
   * @throws MessagingException
   * @throws IOException
   */
  protected void AddToText(
    MimeMultipart ownerPart,
    MimePart      part )
    throws MessagingException, IOException
  {
    _mailet.log("Trying to add footer to " + part.getContent().toString());
    String contentType = part.getContentType();
    String content = (String) part.getContent();

    if (content.endsWith("\n") == false)
    {
      content += Constants.CR_LF;
    }
    content += GetFooterText();

    part.setContent(content, contentType);
    part.setHeader(RFC2822Headers.CONTENT_TYPE, contentType);
    // log("After adding footer: " + part.getContent().toString());
  }

  /**
   * Prepends the content of the MimePart as HTML to the existing footer
   * 
   * @param part
   *          the MimePart to attach
   * 
   * @throws MessagingException
   * @throws IOException
   */
  protected void AddToHTML(
    MimeMultipart ownerPart,
    MimePart      part )
    throws MessagingException, IOException
  {
    // log("Trying to add footer to " + part.getContent().toString());
    String contentType = part.getContentType();
    String content = (String) part.getContent();

    /*
     * This HTML part may have a closing <BODY> tag. If so, we want to insert our footer immediately prior to that tag.
     */
    int index = content.toLowerCase().lastIndexOf("</body>");
    String insert = Constants.CR_LF + "<br>" + Constants.CR_LF + GetFooterHTML() + Constants.CR_LF;
    content = (index == -1) ? content + insert : content.substring(0, index) + insert + content.substring(index);

    part.setContent(content, contentType);
    part.setHeader(RFC2822Headers.CONTENT_TYPE, contentType);
    // log("After adding footer: " + part.getContent().toString());
  }

  /**
   * Attach a footer to a MimePart
   * 
   * @param part
   *          the MimePart to which the footer is to be attached
   * 
   * @return whether a footer was successfully attached
   * @throws MessagingException
   * @throws IOException
   */
  protected boolean AttachFooter(
    MimeMultipart ownerPart,
    MimePart      part )
    throws MessagingException, IOException
  {
    // log("Content type is " + part.getContentType());
    Object content = part.getContent();
    if (content instanceof String)
    {
      if (part.isMimeType("text/plain") == true)
      {
        AddToText(ownerPart,part);
        return true;
      }
      else
      if (part.isMimeType("text/html") == true)
      {
        AddToHTML(ownerPart,part);
        return true;
      }
    }
    else
    if (content instanceof Multipart)
    {
      boolean isFooterAttached = false;

      MimeMultipart multipart = (MimeMultipart) content;
      if ((part.isMimeType("multipart/mixed") == true) || (part.isMimeType("multipart/related") == true))
      {
        // Find the first body part, and determine what to do then.
        MimeBodyPart firstPart = (MimeBodyPart) multipart.getBodyPart(0);
        isFooterAttached = AttachFooter(multipart,firstPart);
      }
      else
      if (part.isMimeType("multipart/alternative") == true)
      {
        int count = multipart.getCount();
        // log("number of alternatives = " + count);
        for (int index = 0; index < count; index++)
        {
          // log("processing alternative #" + index);
          MimeBodyPart mimeBodyPart = (MimeBodyPart) multipart.getBodyPart(index);
          isFooterAttached |= AttachFooter(multipart,mimeBodyPart);
          if (multipart.getCount() < count)
          {
            index--;
            count = multipart.getCount();
          }
        }
      }

      if (isFooterAttached == true)
      {
        // We have to do this because of a bug in JavaMail (ref id 4403733)
        // http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4403733
        part.setContent(multipart);
      }

      return isFooterAttached;
    }
    else
    if (content == null)
    {
      _mailet.log("Cannot attach footer: Content is null");
    }
    else
    {
      _mailet.log("Cannot attach footer: Unrecognized content " + content.getClass().getSimpleName());
    }

    // Give up... we won't attach the footer to this MimePart
    return false;
  }
  
  /**
   * microsoft email clients appear to embed both html and plain text for html formatted email.
   * thunderbird gives you the option of including both.
   * 
   * @return
   * @throws MessagingException 
   * @throws IOException 
   */
  protected boolean EmailHasHTMLTextBodyPart() 
    throws IOException, MessagingException
  {
    Object content = _message.getContent();
    if (content instanceof Multipart)
    {
      Multipart multipart = (Multipart)content;
      int count = multipart.getCount();
      for (int indx = 0; indx < count; indx++)
      {
        MimeBodyPart mimeBodyPart = (MimeBodyPart)multipart.getBodyPart(indx);
        if (mimeBodyPart.isMimeType("text/html") == true)
        {
          return true;
        }
      }
    }
    
    return false;
  }

  /**
   * This is exposed as a method for easy subclassing to provide alternate ways to get the footer text.
   * 
   * @return the footer text
   */
  protected abstract String GetFooterText();

  /**
   * This is exposed as a method for easy subclassing to provide alternate ways to get the footer text. By default, this
   * will take the footer text, converting the linefeeds to &lt;br&gt; tags.
   * 
   * @return the HTML version of the footer text
   */
  protected abstract String GetFooterHTML();

}
