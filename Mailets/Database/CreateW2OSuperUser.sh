#! /bin/bash

# this script creates the w2o superuser

USERNAME=w2o
PASSWORD=w2o

#PGSQLPATH is the path to the top of the postgresql install directory
PGSQLPATH=/usr/local/pgsql

#BINPATH is the path to where the postgresql binaries are located
BINPATH=${PGSQLPATH}/bin

${BINPATH}/psql -d postgres -c "create role ${USERNAME} password '${PASSWORD}' superuser inherit login"



