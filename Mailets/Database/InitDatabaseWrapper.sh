#!/bin/bash

DOSFILES="InitDatabase.sh CreateW2OSuperUser.sh *.sql"
for p in $DOSFILES; do
 if test -e "$p"; then
  dos2unix $p
 fi
done

chmod a+rx CreateW2OSuperUser.sh
chmod a+rx InitDatabase.sh

./InitDatabase.sh

