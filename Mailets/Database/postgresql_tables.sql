

create sequence email_id_seq
  no cycle;

create table sender_tbl
(
  id         int       not null,
  sender     text      not null,
  subject    text      not null,
  primary key (id)
)
without oids;

alter sequence email_id_seq owned by sender_tbl.id;


create table recipient_tbl
(
  email_id    int    not null references sender_tbl(id),
  recipient   text   not null
)
without oids;


create table header_tbl
(
  email_id    int    not null references sender_tbl(id),
  name        text   not null,
  value       text   not null
)
without oids;


