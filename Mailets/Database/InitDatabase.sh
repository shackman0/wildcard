#! /bin/bash

# this script creates and initializes the World2One Archive database
# it is assumed that the ".sql" files are in the current directory.

DATE=`date +"%A %-e %b %Y @ %H:%M:%S %Z"`
echo "Started: ${DATE}"

DBNAME=w2o
echo "Target database = ${DBNAME}"

#PGSQLPATH is the path to the top of the postgresql install directory
PGSQLPATH=/usr/local/pgsql

#BINPATH is the path to where the postgresql binaries are located
BINPATH=${PGSQLPATH}/bin

#INFOLOG is the file to log the notices in
INFOLOG=./Init.log

#ERRLOG is the file to log the errors to
ERRLOG=./Init.err

#programs we'll be running
PSQL=${BINPATH}/psql
DROPDB=${BINPATH}/dropdb
CREATEDB=${BINPATH}/createdb

rm ${INFOLOG};
rm ${ERRLOG};

echo "Removing the ${DBNAME} database (if it already exists) ..."
${DROPDB} ${DBNAME} 1> ${INFOLOG} 

echo Creating the ${DBNAME} database ...
${CREATEDB} --encoding=utf8 ${DBNAME} 1>> ${INFOLOG} 2> ${ERRLOG}

if test -n "`grep failed ${ERRLOG}`"; then
  echo "There were errors during the initialization.  Check ${ERRLOG} for the full list:"
  cat ${ERRLOG}
  exit
fi


echo Creating the tables and indexes ...
${PSQL} ${DBNAME} -f ./postgresql_tables.sql 1>> ${INFOLOG} 2>> ${ERRLOG}

if test -n "`grep ERROR ${ERRLOG}`"; then
  echo "There were errors during the initialization.  Check ${ERRLOG} for the full list:"
  grep ERROR ${ERRLOG} | head -10
  exit
fi

echo Vacuuming database ${DBNAME} ...
echo "set statement_timeout to 0; vacuum full analyze; set statement_timeout to default;" | ${PSQL} ${DBNAME}

DATE=`date +"%A %-e %b %Y @ %H:%M:%S %Z"`
echo "Ended: ${DATE}"

