
for james v2.3.2

this file tells you how to modify the JAMES .\bin\run.bat file.

you must launch JAMES from the JAMES/bin directory. 
if you are running JAMES from a Command Prompt window, you should use the following command to launch JAMES:

  cd .\bin\
  .\run.bat
   

modify the JAMES\bin\run.bat file as follows:

============
1. set the CLIENT_PROXY_HOST if this client resides behind a firewall  

rem if the client is behind a firewall, uncomment CLIENT_PROXY_HOST and set the value of http.proxyHost to the external host name of the firewall.
rem replace the ? with the external host name of the firewall.

set CLIENT_PROXY_HOST=
rem set CLIENT_PROXY_HOST="-Dhttp.proxyHost=?"

============
2. echo the env vars right before invoking java:

   echo JAVA_HOME:        %JAVA_HOME%
   echo PHOENIX_JAVACMD:  %PHOENIX_JAVACMD%
   echo DEBUG:            %DEBUG%
   echo PHOENIX_HOME:     %PHOENIX_HOME%
   echo PHOENIX_JVM_OPTS: %PHOENIX_JVM_OPTS%
   echo PHOENIX_SM:       %PHOENIX_SM%
   echo PHOENIX_TMPDIR:   %PHOENIX_TMPDIR%

   rem Kicking the tires and lighting the fires!!!
   ...

============
3. include the "-D..." property declarations on the command line

   rem Kicking the tires and lighting the fires!!!
   "%PHOENIX_JAVACMD%" "-Djdbc.drivers=org.postgresql.Driver" ...

============
4. cleanup and check for an update at end:

   rem Kicking the tires and lighting the fires!!!
    ... 

   :end

   rem remove the PID file
   del %PHOENIX_HOME%\apps\james\Runtime\pid.txt
      
   cd %PHOENIX_HOME%
   
   rem check for an update. if we have an update: update w2o wildcard and restart it.
  
   set MAILETS_UPDATE_ZIP=.\apps\james\Runtime\WildCardUpdate.zip
   set JAR=./apps/james/SAR-INF/lib/W2O_Mailets.jar
  
   if exist %MAILETS_UPDATE_ZIP% (
   echo Update found. Installing now ...
   rem unzip the wildcard update file
   call .\bin\UnZip.cmd %JAR% %MAILETS_UPDATE_ZIP% .
    
   rem delete the w2o wildcard update file so we don't think we have to process this same update the next time through here
   echo Installation complete. Deleting %MAILETS_UPDATE_ZIP%. 
   del %MAILETS_UPDATE_ZIP%
  
   rem re-invoke w2o wildcard. this is a one-way call.
   cd %PHOENIX_HOME%\bin
   .\run.bat
   )

   cd %PHOENIX_HOME%\bin
    
============    