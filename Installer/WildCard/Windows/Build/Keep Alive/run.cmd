@echo off

echo Starting WildCard Keep Alive process

rem This script launches the KeepAlive application.
rem You MUST modify the values in the User Modifiable Section below when you install the
rem KeepAlive application before this script will work.


rem clear all the variables we use in this script so we don't "inherit" any from the windows environment.
rem do not modify this list.

set JAVA2SE=
set UPDATE_SERVER_ADDR=
set UPDATE_SERVER_PORT=
set RUN_JAVA=
set JVM_PARMS=
set JAR=
set WILDCARD_TOP=
set INITIAL_DELAY=
set QUERY_PERIOD=
set JAVA_DEBUG=


rem ********** BEGIN USER MODIFIABLE SECTION **********
rem ********** BEGIN USER MODIFIABLE SECTION **********
rem ********** BEGIN USER MODIFIABLE SECTION **********


rem **********
rem if you do not want to rely upon the PATH environment variable, set the JAVA2SE variable to point to the 
rem Java 2 SE runtime bin directory. Use an absolute path. be sure to end with a backslash "\"

rem set JAVA2SE=C:\Program Files\Java\jre1.6.0_07\bin\


rem **********
rem Set the UPDATE_SERVER_ADDR variable to the hostname or the IP address at which the Update Server is bound and listening.
rem Put the value in between the double quotes.
rem this is a required variable

set UPDATE_SERVER_ADDR="updater.world2one.com"


rem **********
rem Set the UPDATE_SERVER_PORT variable to the port at which the Update Server is bound and listening.
rem Put the value in between the double quotes.
rem this is a required variable

set UPDATE_SERVER_PORT="40100"


rem **********
rem Set INITIAL_DELAY to the number of seconds before Keep Alive begins to test for a running WildCard

set INITIAL_DELAY=240


rem **********
rem Set QUERY_PERIOD to the number of seconds between queries.

set QUERY_PERIOD=30


rem **********
rem set this to the top of the WildCard install directory if the configurator has been moved outside of the wildcard installation directory
rem set WILDCARD_TOP="-wildcardTop=../"


rem ********** END USER MODIFIABLE SECTION **********
rem ********** END USER MODIFIABLE SECTION **********
rem ********** END USER MODIFIABLE SECTION **********



rem ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********
rem ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********
rem ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********

if "%KEEP_ALIVE_HOME_BIN%" == "" set KEEP_ALIVE_HOME_BIN=%1

if not "%KEEP_ALIVE_HOME_BIN%" == "" goto keep_alive_home_bin

rem determine directory in which this script resides
rem %~dp0 is name of current script under NT
set KEEP_ALIVE_HOME_BIN=%~dp0*

rem : operator works similar to make : operator
set KEEP_ALIVE_HOME_BIN=%KEEP_ALIVE_HOME:\bin\*=%

if not "%KEEP_ALIVE_HOME_BIN%" == "" goto keep_alive_home_bin

echo.
echo Error: KEEP_ALIVE_HOME_BIN environment variable is not set.
echo   This needs to be set manually for Win9x as its command
echo   prompt scripting does not allow it to be set automatically;
echo   Or pass the absolute path to KeepAlive\Bin in as the first
echo   parameter to this script. 
echo.

goto end

:keep_alive_home_bin

cd %KEEP_ALIVE_HOME_BIN%

set RUN_JAVA="%JAVA2SE%java.exe"

set JAR=./Runtime/lib/W2O_KeepAlive.jar


rem set the CLASSPATH according to whether we are running this in production (.jar) or development (./bin)
rem set CLASSPATH=./bin;../Utility/bin;../Updater/bin;
set CLASSPATH=%JAR%;


rem uncomment to enable remote debugging
rem set JAVA_DEBUG=-Xdebug -Xrunjdwp:transport=dt_socket,address=8000,server=y,suspend=y


rem set the JVM_PARMS to commands you want passed to the JVM
rem put the parms in between the double quotes.

set JVM_PARMS="-Djava.security.policy=KeepAlive.policy" %JAVA_DEBUG%


rem launch the KeepAlive
%RUN_JAVA% -classpath %CLASSPATH% %JVM_PARMS% com.World2One.KeepAlive.KeepAlive -rmiBindAddr %UPDATE_SERVER_ADDR% -rmiBindPort %UPDATE_SERVER_PORT% %WILDCARD_TOP% -initialDelay %INITIAL_DELAY% -queryPeriod %QUERY_PERIOD%


rem check for an update. if we have an update: update the KeepAlive and restart it.

set KEEP_ALIVE_UPDATE_ZIP=.\Runtime\KeepAliveUpdate.zip

if exist %KEEP_ALIVE_UPDATE_ZIP% (
rem unzip the KeepAlive update file
call .\Runtime\bin\UnZip.cmd %JAR% %KEEP_ALIVE_UPDATE_ZIP% .
  
rem delete the KeepAlive update file so we don't think we have to process this same update the next time through here
del %KEEP_ALIVE_UPDATE_ZIP%

rem re-invoke the KeepAlive. this is a one-way call.
.\run.cmd %1
)

rem pause for debugging purposes
rem pause
