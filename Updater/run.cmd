@echo off

cd

rem This script launches the Update Server application.
rem You MUST modify the values in the User Modifiable Section below when you install the
rem Update Server application before this script will work.


rem clear all the variables we use in this script so we don't "inherit" any from the windows environment.
rem do not modify this list.

set JAVA2SE=
set JVM_PARMS=
set RUN_JAVA=
set CLASSPATH=
set RMI_BIND_ADDR=
set RMI_BIND_PORT=
set JAVA_DEBUG=


rem ********** BEGIN USER MODIFIABLE SECTION **********
rem ********** BEGIN USER MODIFIABLE SECTION **********
rem ********** BEGIN USER MODIFIABLE SECTION **********


rem **********
rem if you do not want to rely upon the PATH environment variable, set the JAVA2SE variable to point to the 
rem Java 2 SE runtime bin directory. Use an absolute path. be sure to end with a backslash "\"

rem set JAVA2SE=C:\Program Files\Java\jre1.6.0_07\bin\


rem **********
rem Uncomment the RUN_JAVA you want to use to launch the application
rem javaw hides the dos window. java will display a dos window.
rem this is a required variable

rem set RUN_JAVA=start "Update Server" /i /b "%JAVA2SE%javaw.exe"
set RUN_JAVA="%JAVA2SE%java.exe"


rem **********
rem Set the RMI_BIND_ADDR variable to the local hostname or the local IP address to which the Update Server will bind.
rem Put the value in between the double quotes. this is a required variable

set RMI_BIND_ADDR="192.168.3.11"


rem **********
rem Set the RMI_BIND_PORT variable to the port at which the Update Server will listen.
rem Put the value in between the double quotes.
rem this is a required variable

set RMI_BIND_PORT="40100"


rem set the CLASSPATH based upon whether we are running in development (./bin) or production (.jar)

rem set CLASSPATH=./bin;../Utility/bin;
set CLASSPATH=./Runtime/lib/*W2O_UpdateServer.jar


rem ********** END USER MODIFIABLE SECTION **********
rem ********** END USER MODIFIABLE SECTION **********
rem ********** END USER MODIFIABLE SECTION **********



rem ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********
rem ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********
rem ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********


rem uncomment to enable remote debugging
rem set JAVA_DEBUG=-Xdebug -Xrunjdwp:transport=dt_socket,address=8000,server=y,suspend=y


rem set the JVM_PARMS to commands you want passed to the JVM
rem put the values in between the double quotes.

set JVM_PARMS=-Djava.security.policy=UpdateServer.policy -Djava.rmi.server.hostname=Updater.World2One.Com %JAVA_DEBUG%


rem launch the Update Server
%RUN_JAVA% -classpath %CLASSPATH% %JVM_PARMS% com.World2One.Updater.UpdateServerImpl -rmiBindAddr %RMI_BIND_ADDR% -rmiBindPort %RMI_BIND_PORT%


rem pause if this command file doesn't work to have the DOS window remain open so you can read the error message 
rem pause
