package com.World2One.Updater;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * <p>Title: UpdateServer</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One, Inc.</p>
 */
public interface UpdateServer
  extends Remote
{

  public static final int DEFAULT_RMI_BIND_PORT     = 40100;
  public static final int RECOMMENDED_BLOCK_SIZE    = 100000;


  /**
   * @param versionNumber the version number of the WildCarder server on behalf of which the Updater is invoking this API
   * @return true if a new WildCard server version is available.
   * @throws RemoteException
   */
  public boolean IsWildCardUpdateAvailable(int versionNumber)
    throws RemoteException;


  /**
   * @param length the maximum size of the returned byte[]
   * @return keep calling this until a null (end of file) is returned. note that the returned byte[].length may be less than length (generally on the final block of data) 
   * @throws RemoteException
   */
  public byte[] GetWildCardUpdateZipFileBytes(int length)
    throws RemoteException;
  
  
  /**
   * @param versionNumber the version number of the Configurator on behalf of which the Updater is invoking this API
   * @return true if a new Configurator version is available.
   * @throws RemoteException
   */
  public boolean IsConfiguratorUpdateAvailable(int versionNumber)
    throws RemoteException;

  /**
   * @param length the maximum size of the returned byte[]
   * @return keep calling this until a null (end of file) is returned. note that the returned byte[].length may be less than length (generally on the final block of data) 
   * @throws RemoteException
   */
  public byte[] GetConfiguratorUpdateZipFileBytes(int length)
    throws RemoteException;
  
  
  /**
   * @param versionNumber the version number of the KeepAlive on behalf of which the Updater is invoking this API
   * @return true if a new KeepAlive version is available.
   * @throws RemoteException
   */
  public boolean IsKeepAliveUpdateAvailable(int versionNumber)
    throws RemoteException;

  /**
   * @param length the maximum size of the returned byte[]
   * @return keep calling this until a null (end of file) is returned. note that the returned byte[].length may be less than length (generally on the final block of data) 
   * @throws RemoteException
   */
  public byte[] GetKeepAliveUpdateZipFileBytes(int length)
    throws RemoteException;

}
