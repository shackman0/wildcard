package com.World2One.Updater;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.rmi.ConnectException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import com.World2One.Utility.RMIToolBox;

/**
 * <p>Title: UpdateClient</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public abstract class AbstractUpdateClient
{
  
  protected String    _rmiBindAddr;
  protected int       _rmiBindPort;
  protected String[]  _args = null;

  protected UpdateServer _updateServer = null;


  public AbstractUpdateClient(String... args)
  {
    super();
    
    _args = args;
  }
  
  public AbstractUpdateClient(String rmiBindAddr, int rmiBindPort)
  {
    super();
    
    _rmiBindAddr = rmiBindAddr;
    _rmiBindPort = rmiBindPort;
  }

  protected UpdateServer GetUpdateServer() 
    throws SocketException, ConnectException, RemoteException, MalformedURLException, NotBoundException
  {
    if (_updateServer == null)
    {
      RMIToolBox<UpdateServer> rmiToolBox = new RMIToolBox(UpdateServerImpl.class);
      rmiToolBox.SetDefaultRMIBindPort(UpdateServer.DEFAULT_RMI_BIND_PORT);
      if (_args != null)
      {
        rmiToolBox.ProcessRMIParms(_args);
        _updateServer = rmiToolBox.BindToServer();
      }
      else
      {
        _updateServer = rmiToolBox.BindToServer(_rmiBindAddr,_rmiBindPort);
      }
    }
    
    return _updateServer;
  }
  
  /**
   * 
   * @return true updated. false = not updated
   * @throws RemoteException
   * @throws IOException
   * @throws NotBoundException
   * @throws java.rmi.ConnectException
   */
  public boolean UpdateIfNeeded() 
    throws RemoteException, IOException, NotBoundException, java.rmi.ConnectException
  {
    boolean updateIsAvailable = IsUpdateAvailable();
    if (updateIsAvailable == true)
    {
      UpdateServer updateServer = GetUpdateServer();
      ReadAndWriteUpdateZipFileAndExit(updateServer);
    }
    
    return updateIsAvailable;
  }

  public boolean IsUpdateAvailable() 
    throws RemoteException, IOException, NotBoundException, java.rmi.ConnectException
  {
    UpdateServer updateServer = GetUpdateServer();
    
    boolean updateIsAvailable = IsUpdateAvailable(updateServer);
    
    return updateIsAvailable;
  }

  public void Disconnect()
  {
    _updateServer = null;
    System.gc();
  }
  
  protected abstract boolean IsUpdateAvailable(UpdateServer updateServer) 
    throws RemoteException;

  protected abstract void ReadAndWriteUpdateZipFileAndExit(UpdateServer updateServer)
    throws RemoteException, IOException;

  protected void ReadAndWriteUpdateZipFileAndExit(
    UpdateServer updateServer,
    String       updateZipFilePath )
    throws RemoteException, IOException
  {
    ReadAndWriteUpdateZipFile(updateServer, updateZipFilePath);
    ShutdownJVM();
  }
  
  protected void ShutdownJVM()
  {
    System.exit(0);
  }
  
  protected void ReadAndWriteUpdateZipFile(
    UpdateServer updateServer,
    String       updateZipFilePath )
    throws RemoteException
  {
    File updateZipFile = new File(updateZipFilePath);
    try
    {
      FileOutputStream fileOutputStream = new FileOutputStream(updateZipFile);
      BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
      do
      {
        byte[] updateZipFileBytes = GetUpdateZipFileBytes(updateServer, UpdateServer.RECOMMENDED_BLOCK_SIZE);
        if (updateZipFileBytes == null)
        {
          break;
        }
        bufferedOutputStream.write(updateZipFileBytes);
      }
      while (true);
      bufferedOutputStream.flush();
      bufferedOutputStream.close();
    }
    catch (IOException ex)
    {
      updateZipFile.delete();
      ex.printStackTrace();
      return;
    }
  }
  
  protected abstract byte[] GetUpdateZipFileBytes(UpdateServer updateServer, int length)
    throws RemoteException;

  
}
