package com.World2One.Updater;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Arrays;

import com.World2One.Utility.Constants;
import com.World2One.Utility.RMIToolBox;

/**
 * <p>Title: Updater</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One, Inc.</p>
 */
public class UpdateServerImpl
  extends java.rmi.server.UnicastRemoteObject
  implements UpdateServer
{


  
  public UpdateServerImpl() 
    throws RemoteException
  {
    super();
  }

  
  protected static final String WILD_CARD_UPDATE_ZIP_FILE_PATH = "./Runtime/" + Constants.WILD_CARD_UPDATE_ZIP_FILE_NAME;
  
//  protected boolean _testWildCardUpdate = true;

  @Override
  public boolean IsWildCardUpdateAvailable(int versionNumber)
    throws RemoteException
  {
    boolean wildCardUpdateIsAvailable = (Constants.WILDCARD_VERSION > versionNumber) && CheckUpdateFileExists(WILD_CARD_UPDATE_ZIP_FILE_PATH);
    
    // test
//    wildCardUpdateIsAvailable = _testWildCardUpdate;
//    _testWildCardUpdate = ! _testWildCardUpdate;

    LogClientInvocation("IsWildCardUpdateAvailable(" + versionNumber +"). Current version is " + Constants.WILDCARD_VERSION + ". Returning " + wildCardUpdateIsAvailable);

    return wildCardUpdateIsAvailable;
  }

  @Override
  public byte[] GetWildCardUpdateZipFileBytes(int length)
    throws RemoteException
  {
    try
    {
      byte[] wildCardUpdateZipFileBytes = ReadUpdateZipFileBytes(WILD_CARD_UPDATE_ZIP_FILE_PATH,length);

      String numberOfBytesReturned = (wildCardUpdateZipFileBytes == null) ? "null" : wildCardUpdateZipFileBytes.length + " bytes";
      LogClientInvocation("GetWildCardUpdateZipFileBytes(" + length +") returning " + numberOfBytesReturned);
      
      return wildCardUpdateZipFileBytes;
    }
    catch (Exception ex)
    {
      if (ex instanceof RemoteException)
      {
        throw (RemoteException)ex;
      }
      ex.printStackTrace();
      throw new RemoteException(ex.getLocalizedMessage(),ex);
    }
  }

  
  protected static final String CONFIGURATOR_UPDATE_ZIP_FILE_PATH = "./Runtime/" + Constants.CONFIGURATOR_UPDATE_ZIP_FILE_NAME;
  
//  protected boolean _testConfiguratorUpdate = true;

  @Override
  public boolean IsConfiguratorUpdateAvailable(int versionNumber)
    throws RemoteException
  {
    boolean configuratorUpdateIsAvailable = (Constants.CONFIGURATOR_VERSION > versionNumber) && CheckUpdateFileExists(CONFIGURATOR_UPDATE_ZIP_FILE_PATH);
    
    // test
//    configuratorUpdateIsAvailable = _testConfiguratorUpdate;
//    _testConfiguratorUpdate = ! _testConfiguratorUpdate;

    LogClientInvocation("IsConfiguratorUpdateAvailable(" + versionNumber +"). Current version is " + Constants.CONFIGURATOR_VERSION + ". Returning " + configuratorUpdateIsAvailable);
    
    return configuratorUpdateIsAvailable;
  }

  @Override
  public byte[] GetConfiguratorUpdateZipFileBytes(int length)
    throws RemoteException
  {
    try
    {
      byte[] configuratorUpdateZipFileBytes = ReadUpdateZipFileBytes(CONFIGURATOR_UPDATE_ZIP_FILE_PATH,length);

      String numberOfBytesReturned = (configuratorUpdateZipFileBytes == null) ? "null" : configuratorUpdateZipFileBytes.length + " bytes";
      LogClientInvocation("GetConfiguratorUpdateZipFileBytes(" + length +") returning " + numberOfBytesReturned);
      
      return configuratorUpdateZipFileBytes;
    }
    catch (Exception ex)
    {
      if (ex instanceof RemoteException)
      {
        throw (RemoteException)ex;
      }
      ex.printStackTrace();
      throw new RemoteException(ex.getLocalizedMessage(),ex);
    }
  }



  protected static final String KEEP_ALIVE_UPDATE_ZIP_FILE_PATH = "./Runtime/" + Constants.KEEP_ALIVE_UPDATE_ZIP_FILE_NAME;
  
//  protected boolean _testKeepAliveUpdate = true;
  
  @Override
  public boolean IsKeepAliveUpdateAvailable(int versionNumber)
    throws RemoteException
  {
    boolean keepAliveUpdateIsAvailable = (Constants.KEEP_ALIVE_VERSION > versionNumber) && CheckUpdateFileExists(KEEP_ALIVE_UPDATE_ZIP_FILE_PATH);
    
    // test
//    keepAliveUpdateIsAvailable = _testKeepAliveUpdate;
//    _testKeepAliveUpdate = ! _testKeepAliveUpdate;
  
    LogClientInvocation("IsKeepAliveUpdateAvailable(" + versionNumber +"). Current version is " + Constants.KEEP_ALIVE_VERSION + ". Returning " + keepAliveUpdateIsAvailable);
    
    return keepAliveUpdateIsAvailable;
  }
  
  @Override
  public byte[] GetKeepAliveUpdateZipFileBytes(int length)
    throws RemoteException
  {
    try
    {
      byte[] keepAliveUpdateZipFileBytes = ReadUpdateZipFileBytes(KEEP_ALIVE_UPDATE_ZIP_FILE_PATH,length);
  
      String numberOfBytesReturned = (keepAliveUpdateZipFileBytes == null) ? "null" : keepAliveUpdateZipFileBytes.length + " bytes";
      LogClientInvocation("GetKeepAliveUpdateZipFileBytes(" + length +") returning " + numberOfBytesReturned);
      
      return keepAliveUpdateZipFileBytes;
    }
    catch (Exception ex)
    {
      if (ex instanceof RemoteException)
      {
        throw (RemoteException)ex;
      }
      ex.printStackTrace();
      throw new RemoteException(ex.getLocalizedMessage(),ex);
    }
  }
    


  protected boolean CheckUpdateFileExists(String updateFilePath)
  {
    File updateFile = new File(updateFilePath);
    boolean updateFileExists = updateFile.exists();
    
    return updateFileExists;
  }


  protected void LogClientInvocation(String message) 
    throws RemoteException
  {
    try
    {
      String clientHost = getClientHost();
      System.out.println("From " + clientHost + ": " + message);
    }
    catch (Exception ex)
    {
      if (ex instanceof RemoteException)
      {
        throw (RemoteException)ex;
      }
      ex.printStackTrace();
      throw new RemoteException(ex.getLocalizedMessage(),ex);
    }
  }

  protected File                _updateZipFile = null;
  protected FileInputStream     _fileInputStream;
  protected BufferedInputStream _bufferedInputStream;
  
  protected byte[] ReadUpdateZipFileBytes(
    String updateZipFilePath,
    int    length ) 
    throws IOException
  {
    int bytesRead = -1;
    byte[] updateZipFileBytes = null;
    try
    {
      if (_updateZipFile == null)
      {
        _updateZipFile = new File(updateZipFilePath);
        _fileInputStream = new FileInputStream(_updateZipFile);
        _bufferedInputStream = new BufferedInputStream(_fileInputStream);
      }
      
      updateZipFileBytes = new byte[length];
      bytesRead = _bufferedInputStream.read(updateZipFileBytes);
    }
    finally
    {
      if (bytesRead == -1)
      {
        updateZipFileBytes = null;

        if (_bufferedInputStream != null)
        {
          _bufferedInputStream.close();
        }

        _bufferedInputStream = null;
        _fileInputStream = null;
        _updateZipFile = null;
      }
      else
      if (bytesRead < length)
      {
        updateZipFileBytes = Arrays.copyOf(updateZipFileBytes, bytesRead);
      }
    }
    
    return updateZipFileBytes;
  }

  

  public static void main(String... args)
  {
    RMIToolBox<UpdateServerImpl> rmiToolBox = new RMIToolBox(UpdateServerImpl.class);
    rmiToolBox.SetDefaultRMIBindPort(DEFAULT_RMI_BIND_PORT);
    rmiToolBox.LaunchServer(args);
  }



}
