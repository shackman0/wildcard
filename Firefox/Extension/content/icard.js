



const icard_prefs = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefBranch);



// say hello
function icard_hello()
{
  var stringbundle = document.getElementById("icard.properties");
  var helloWorld = stringbundle.getString("icard.hello");
  alert(helloWorld);
}

function icard_console(msg) 
{
  var consoleService = Components.classes["@mozilla.org/consoleservice;1"].getService(Components.interfaces.nsIConsoleService);

  consoleService.logStringMessage(msg);
}


//returns the value of the argument preference
function icard_getPreference(pref_name)
{
  var pref_type = icard_prefs.getPrefType(pref_name);

  if (pref_type == lug_prefs.PREF_STRING)
  {
    return icard_prefs.getCharPref(pref_name);
  }
  else 
  if (pref_type == lug_prefs.PREF_INT)
  {
    return icard_prefs.getIntPref(pref_name);
  }
  else 
  if (pref_type == lug_prefs.PREF_BOOL)
  {
    return icard_prefs.getBoolPref(pref_name);
  }
  else  // fallback on error
  {
    return lug_prefs.PREF_INVALID;
  }
  
}
