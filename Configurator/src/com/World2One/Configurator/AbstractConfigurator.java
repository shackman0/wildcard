package com.World2One.Configurator;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Dialog.ModalityType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.PrivilegedActionException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.ToolTipManager;
import javax.swing.WindowConstants;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.World2One.Configurator.MailClients.AbstractAccount;
import com.World2One.Configurator.MailClients.AbstractApplication;
import com.World2One.Configurator.MailClients.ServiceController;
import com.World2One.Configurator.MailClients.Win32.ApplicationFactory;
import com.World2One.Configurator.SOAP.Cards;
import com.World2One.GUI.MultiLineLabelUI;
import com.World2One.GUI.MultiLineToolTipUI;
import com.World2One.JAMES.Config;
import com.World2One.OS.AbstractProcess;
import com.World2One.OS.PIDFileNotFoundException;
import com.World2One.SMTP.MissingParameterException;
import com.World2One.SMTP.SMTPHost;
import com.World2One.SMTP.SMTPHost.SMTPHostsOverridesKey;
import com.World2One.SMTP.SMTPHost.SMTPHostsOverridesTable;
import com.World2One.SMTP.SMTPHost.SecureConnection;
import com.World2One.Utility.Constants;
import com.World2One.Utility.MutableBoolean;
import com.World2One.Utility.ToolBox;

/**
 * <p>Title: AbstractConfigurator</p>
 * <p>Description: Run the platform specific configurator</p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One, Inc.</p>
 */
public abstract class AbstractConfigurator
  extends JFrame
{

  protected static final String WILDCARD_TOP_PARM                              = Constants.PARM_KEYWORD_PREFIX + "wildcardTop";
  protected static final String DEBUG_PARM                                     = Constants.PARM_KEYWORD_PREFIX + "debug";
  protected static final String DO_NOT_UPDATE_PARM                             = Constants.PARM_KEYWORD_PREFIX + "doNotUpdate";
  protected static final String RESTORE_ALL_ORIGINAL_SMTP_SETTINGS_PARM        = Constants.PARM_KEYWORD_PREFIX + "restoreAllOriginalSMTPSettings";

  protected static final String W2O_SIGNATURE_INJECTION_SERVICE_ELEMENT = "W2OSignatureInjectionService";


  protected static final String TOOLTIP_TEXT_PASSWORD  = 
    "Enter the password for connecting to the SMTP server" + Constants.CR_LF +
    "for the {0} email account.";
  
  protected static final String TOOLTIP_TEXT_APPLICATION_FORMAT = 
    "These are the iCard activated accounts that were found" + Constants.CR_LF +
  	"in the {0} email application.";
  
  protected static final String TOOLTIP_TEXT_EMAIL_ACCOUNT_FORMAT = 
    "Select to activate iCard insertion for the {0}" + Constants.CR_LF +
  	"email account." + Constants.CR_LF + Constants.CR_LF +
    "Unselect to deactivate iCard insertion for this email account.";

  protected static final String TOOLTIP_TEXT_FILE_SAVE = 
    "Saves these settings to the WildCard server" + Constants.CR_LF +
    "and configures your email application(s).";
                                                
  protected static final String TOOLTIP_TEXT_FILE_EXIT = 
    "Exits the Configurator." +  Constants.CR_LF + Constants.CR_LF +
    "Be sure to save your settings before exiting.";
  
  protected static final String TOOLTIP_TEXT_INJECT_SIGNATURE_ON_REPLY_AND_FORWARD = 
    "When selected, the Signature Injection Service will" + Constants.CR_LF +
    "include your iCard when you Reply or Forward an email." + Constants.CR_LF + Constants.CR_LF +
    "When not selected, the Signature Injection Service" + Constants.CR_LF +
    "will refrain from including your iCard when you Reply" + Constants.CR_LF +
    "or Forward an email." + Constants.CR_LF + Constants.CR_LF +
    "iCards are always included with emails originating from" + Constants.CR_LF +
    "the iCard email accounts selected on the Email Accounts tab.";

  protected static final String TOOLTIP_TEXT_DISPLAY_MESSAGE_SENT_POPUP = 
    "When selected, the Signature Injection service will" + Constants.CR_LF +
    "display your iCard image in the bottom right corner" + Constants.CR_LF +
    "of your screen to indicate that it attached your iCard" + Constants.CR_LF +
    "to the email you just sent.";

  protected static final String TOOLTIP_TEXT_DEBUG =  "Additional debugging output appears in the Mailet log";

  protected static final String TOOLTIP_TEXT_LOG_AUTHORIZED_RECIPIENTS =
    "When enabled, logs authorized recipients and" + Constants.CR_LF +
    "facilitates downloading of your vCard information." + Constants.CR_LF + Constants.CR_LF +
    "If disabled, then iCard recipients may not be" + Constants.CR_LF +
    "able to download your vCard information.";

  protected static final String TOOLTIP_TEXT_SOAP_PROTOCOL = "Specify the protocol to use when accessing the iCard SOAP server.";

  protected static final String TOOLTIP_TEXT_SOAP_PROTOCOL_HTTP = "Access the iCard SOAP server using HTTP.";

  protected static final String TOOLTIP_TEXT_SOAP_PROTOCOL_HTTPS = "Access the iCard SOAP server using HTTPS.";
  
  protected static final String TOOLTIP_TEXT_SOAP_HOST = "Specify the host name of the iCard SOAP server.";

  protected static final String TOOLTIP_TEXT_SOAP_PORT = "Specify the port to use when accessing the iCard SOAP server.";
  
  protected static final String TOOLTIP_TEXT_EMAIL_ACCOUNTS_TAB = "Select the email accounts for which you want iCards attached.";
  
  protected static final String TOOLTIP_TEXT_BASIC_ICARD_TAB = "Configure basic iCard email settings.";
  
  protected static final String TOOLTIP_TEXT_ADVANCED_ICARD_TAB = "Configure advanced iCard email settings.";

  protected static final String TOOLTIP_TEXT_SOAP_ARCHIVER_TAB = "Configure the SOAP Archiver settings.";

  protected static final String TOOLTIP_TEXT_JDBC_ARCHIVER_TAB = "Configure the JDBC Archiver settings.";

  protected static final String EMAIL_ACCOUNTS_TAB_INSTRUCTIONS = "Check the box adjacent to the email accounts for which iCards are to be attached.";
  
  protected static final String SHOW_PASSWORDS_IN_PLAIN_TEXT = "Show passwords in plain text";

  protected static final String TOOLTIP_TEXT_SHOW_PASSWORDS_IN_PLAIN_TEXT = "Check this to show passwords in plain text";

  protected static final String BASIC_SIS_SETTINGS_TAB_INSTRUCTIONS = "Select from the following options to configure your iCard email settings.";

  protected static final String ADVANCED_SIS_SETTINGS_TAB_INSTRUCTIONS = "Select from the following options to configure your iCard email advanced settings.";
  
  protected static final String CONFIGURATION_SUCCESSFUL_MESSAGE =
    "Email messages that are sent from the selected email" + Constants.CR_LF +
    "accounts will automatically include your iCard";
  
  
  public static String  __wildcardTop                          = null;
  public static String  __jamesTop                             = null;
  public static String  __jamesRuntimePath                     = null;
  public static String  __w2oRemoteDeliveryFilePath            = null;
  public static String  __jamesConfigFilePath                  = null;
  public static boolean __debug                                = false;
  public static boolean __doNotUpdate                          = false;
  public static boolean __restoreAllOriginalSMTPSettings       = false;
  public static AbstractConfigurator __configurator            = null;
  
  
  protected static final String W2O_ICON_PATH = "./Runtime/images/W2O_20x23.png";
  
  protected ArrayList<UserAccountEntry>   _userAccountEntries;

  protected Config                        _config;
  protected HashMap<String,Element>       _mailetElements;
  
  
  protected JButton _next;
  protected JButton _back;
  protected JButton _finish;
  protected JButton _saveAndExit;

  protected JTabbedPane _tabbedPane;

  protected JCheckBox    _showPasswordsInPlainText;
  
  // Signature Injection Service (sis) options
  protected JCheckBox    _sisDebug;
  protected JCheckBox    _sisInjectSignatureOnReplyAndForward;
  protected JRadioButton _sisSOAPProtocolHTTP;
  protected JRadioButton _sisSOAPProtocolHTTPS;
  protected JTextField   _sisSOAPHost;
  protected JTextField   _sisSOAPPort;
  protected JCheckBox    _sisDisplayMessageSentPopup;
  protected JCheckBox    _sisLogAuthorizedRecipients;

  
  
  
  
  public AbstractConfigurator()
  {
    super();
  }
  
  public void Initialize(String[] args) 
    throws IOException, ParserConfigurationException, SAXException, MissingParameterException, SOAPException, PrivilegedActionException 
  {
    MultiLineLabelUI.initialize();
    
    MultiLineToolTipUI.initialize();
    ToolTipManager.sharedInstance().setDismissDelay(10000);

    __configurator = this;
    
    ProcessParms(args);

    if (CheckForRunningEMailPrograms() == false)
    {
      FileExit();
    }
    
    if (__doNotUpdate == false)
    {
      try
      {
        UpdateClient updateClient = new UpdateClient(args);
        updateClient.UpdateIfNeeded();
        updateClient.Disconnect();
      }
      catch (Exception ex)
      {
        // if we fail, just keep on going.
        if (__debug == true)
        {
          ex.printStackTrace();
        }
      }
    }
    
    __jamesTop = MessageFormat.format(Constants.JAMES_TOP_FORMAT, __wildcardTop);
    __jamesRuntimePath = MessageFormat.format(Constants.JAMES_RUNTIME_PATH_FORMAT, __wildcardTop);
    __w2oRemoteDeliveryFilePath = MessageFormat.format(Constants.W2O_REMOTE_DELIVERY_FILE_PATH_FORMAT, __wildcardTop);
    __jamesConfigFilePath = MessageFormat.format(Constants.JAMES_CONFIG_FILE_PATH_FORMAT, __wildcardTop);
    
    setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    addWindowListener(new LocalWindowListener());
    setTitle("World2One Configurator");
    
    _config = new Config(__jamesConfigFilePath);
    _mailetElements = _config.GetMailetElements();
    
    // load and set icon
    ByteArrayOutputStream byteArrayStream = new ByteArrayOutputStream();
    File file = new File(W2O_ICON_PATH);
    FileInputStream fileInputStream = new FileInputStream(file);
    BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
    byte[] bufferedBytes = new byte[1000];
    while (bufferedInputStream.read(bufferedBytes) != -1)
    {
      byteArrayStream.write(bufferedBytes);
    }
    bufferedInputStream.close();
    fileInputStream.close();
    byte[] byteArray = byteArrayStream.toByteArray();
    ImageIcon icon = new ImageIcon(byteArray);
    setIconImage(icon.getImage());

    // buttons
    _next = new JButton("next");
    _next.addActionListener(new NextButtonActionListener());
    
    _back = new JButton("back");
    _back.addActionListener(new BackButtonActionListener());
    
    _finish = new JButton("finish");
    _finish.addActionListener(new FinishButtonActionListener());
    
    _saveAndExit = new JButton("save & exit");
    _saveAndExit.addActionListener(new SaveAndExitButtonActionListener());

    // content ...
    JPanel contentPane = new JPanel(new BorderLayout());
    getContentPane().add(contentPane);

    AddCenterPane(contentPane);
    

    EnableSave();
    
    pack();

    Point location = ToolBox.CalculateWindowLocation(getPreferredSize());
    setLocation(location);
  }

  protected JComponent AddCenterPane(JPanel contentPane)
    throws MissingParameterException, ParserConfigurationException, SAXException, IOException, SOAPException, PrivilegedActionException
  {
    _tabbedPane = new JTabbedPane();
    contentPane.add(_tabbedPane,BorderLayout.CENTER);
    
    AddEmailAccountsTab(_tabbedPane);
    AddBasicSISTab(_tabbedPane);
    AddAdvancedSISTab(_tabbedPane);
//    AddSOAPArchiverTab(tabbedPane);
//    AddJDBCArchiverTab(tabbedPane);
    
    return _tabbedPane;
  }
  
  protected void AddEmailAccountsTab(JTabbedPane tabbedPane) 
    throws MissingParameterException, ParserConfigurationException, SAXException, IOException, SOAPException, PrivilegedActionException
  {
    JScrollPane emailAccountsScrollPane = new JScrollPane();
    tabbedPane.addTab("Email Accounts", (Icon)null, emailAccountsScrollPane, TOOLTIP_TEXT_EMAIL_ACCOUNTS_TAB);
    emailAccountsScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    emailAccountsScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

    SMTPHostsOverridesTable smtpHostsOverride = SMTPHost.GetSMTPHostsOverrides(__w2oRemoteDeliveryFilePath,null);
    
    // determine candidate user accounts    
    Cards cards = new Cards();
    MutableBoolean isW2OUser = new MutableBoolean();
    
    ApplicationFactory applicationFactory = new ApplicationFactory();
    HashSet<AbstractApplication> installedApplications = applicationFactory.GetInstalledApplications();
    if (__debug == true)
    {
      System.out.println();
      System.out.println( "The following accounts were found: ");
    }
    for (AbstractApplication installedApplication: (HashSet<AbstractApplication>)installedApplications.clone())
    {
      if (__debug == true)
      {
        System.out.println(installedApplication);
      }
      HashSet<AbstractAccount> accounts = installedApplication.GetAccounts();
      for (AbstractAccount account: (HashSet<AbstractAccount>)accounts.clone())
      {
        String errorMessage = cards.IsW2OUser(account.GetEmailAddress(), isW2OUser);
        if (errorMessage != null)
        {
          String wrappedErrorMessage = ToolBox.WrapText(errorMessage, 60);
          Object selectedValue = DisplayMessage(wrappedErrorMessage,"SOAP Error Encountered",JOptionPane.ERROR_MESSAGE,JOptionPane.OK_CANCEL_OPTION);
          if ( (selectedValue != null) &&
               ((Integer)selectedValue == JOptionPane.CANCEL_OPTION) )
          {
            System.exit(-1);
          }
          continue;
        }

        if (Boolean.FALSE.equals(isW2OUser._boolean) == true)
        {
          accounts.remove(account);
          if (accounts.size() == 0)
          {
            installedApplications.remove(installedApplication);
          }
          // if the user account is not authorized, we don't want to keep it in our smtpHostsOverride file either in case it might have been previously authorized.
          smtpHostsOverride.Remove(account.GetSMTPHostsOverridesKey());
        }
      }
    }

    if (__debug == true)
    {
      System.out.println();
      System.out.println("The following accounts have corresponding W2O accounts: ");
      for (AbstractApplication installedApplication: installedApplications)
      {
        System.out.println(installedApplication);
      }
    }

    if (__restoreAllOriginalSMTPSettings == true)
    {
      try
      {
        ShutdownKeepAlive(); 
        ShutdownWildCard();
        RestoreAllOriginalSMTPSettings(installedApplications);
      }
      catch (TransformerException ex)
      {
        if (__debug == true)
        {
          ex.printStackTrace();
        }
        else
        {
          System.out.println(ex.getLocalizedMessage());
        }
      }
      finally
      {
        FileExit();
      }
    }
    
    _userAccountEntries = new ArrayList();
    
    if (installedApplications.size() == 0)
    {
      JOptionPane optionPane = new JOptionPane("No valid email accounts were found",JOptionPane.WARNING_MESSAGE,JOptionPane.DEFAULT_OPTION);
      JDialog dialog = optionPane.createDialog("No Valid Email Accounts");
      dialog.setModalityType(ModalityType.APPLICATION_MODAL);
      dialog.setVisible(true);
    }

    JPanel emailAccountsPanel = new JPanel(new GridBagLayout());
    emailAccountsScrollPane.setViewportView(emailAccountsPanel);

    GridBagConstraints applicationPanelConstraints = new GridBagConstraints();
    applicationPanelConstraints.anchor = GridBagConstraints.NORTHWEST;
    applicationPanelConstraints.fill = GridBagConstraints.HORIZONTAL;
    applicationPanelConstraints.gridheight = 1;
    applicationPanelConstraints.gridwidth = 1;
    applicationPanelConstraints.insets = new Insets(0,0,0,0);
    applicationPanelConstraints.ipadx = 0;
    applicationPanelConstraints.ipady = 0;
    applicationPanelConstraints.weightx = 1.0d;
    applicationPanelConstraints.weighty = 0d;
    applicationPanelConstraints.gridy = 0;
    applicationPanelConstraints.gridx = 0;
    
    Component strut = Box.createVerticalStrut(15);
    emailAccountsPanel.add(strut,applicationPanelConstraints);

    applicationPanelConstraints.gridy++;
    
    Box line = Box.createHorizontalBox();
    applicationPanelConstraints.gridwidth = GridBagConstraints.REMAINDER;
    emailAccountsPanel.add(line,applicationPanelConstraints);
    strut = Box.createHorizontalStrut(5);
    line.add(strut);
    JLabel instructionsLabel = new JLabel(EMAIL_ACCOUNTS_TAB_INSTRUCTIONS);
    line.add(instructionsLabel);
    strut = Box.createHorizontalStrut(5);
    line.add(strut);
    Component glue = Box.createHorizontalGlue();
    line.add(glue);

    applicationPanelConstraints.gridy++;

    Box vertical = Box.createVerticalBox();
    emailAccountsPanel.add(vertical,applicationPanelConstraints);
    strut = Box.createVerticalStrut(5);
    vertical.add(strut);

    applicationPanelConstraints.gridy++;

    _showPasswordsInPlainText = new JCheckBox(SHOW_PASSWORDS_IN_PLAIN_TEXT);
    _showPasswordsInPlainText.setToolTipText(TOOLTIP_TEXT_SHOW_PASSWORDS_IN_PLAIN_TEXT);
    _showPasswordsInPlainText.addChangeListener(new ShowPasswordsInPlainTextChangeListener());

    line = Box.createHorizontalBox();
    applicationPanelConstraints.gridwidth = GridBagConstraints.REMAINDER;
    emailAccountsPanel.add(line,applicationPanelConstraints);
    strut = Box.createHorizontalStrut(5);
    line.add(strut);
    line.add(_showPasswordsInPlainText);
    strut = Box.createHorizontalStrut(5);
    line.add(strut);
    glue = Box.createHorizontalGlue();
    line.add(glue);

    applicationPanelConstraints.gridy++;

    strut = Box.createVerticalStrut(15);
    applicationPanelConstraints.gridwidth = 1;
    emailAccountsPanel.add(strut,applicationPanelConstraints);

    applicationPanelConstraints.gridy++;

    GridBagConstraints accountConstraints = new GridBagConstraints();
    accountConstraints.anchor = GridBagConstraints.NORTHWEST;
    accountConstraints.fill = GridBagConstraints.NONE;
    accountConstraints.gridheight = 1;
    accountConstraints.gridwidth = 1;
    accountConstraints.insets = new Insets(0,0,0,0);
    accountConstraints.ipadx = 0;
    accountConstraints.ipady = 0;
    accountConstraints.weightx = 1.0d;
    accountConstraints.weighty = 0d;
    accountConstraints.gridy = 0;
    accountConstraints.gridx = 0;
    
    // construct and place user account entries
    for (AbstractApplication installedApplication: installedApplications)
    {
      HashSet<AbstractAccount> accounts = installedApplication.GetAccounts();
      if (accounts.size() > 0)
      {
        String applicationName = installedApplication.GetApplicationName();
        
        JPanel applicationPanel = new JPanel(new GridBagLayout());
        Border border = BorderFactory.createTitledBorder(applicationName);
        applicationPanel.setBorder(border);
        applicationPanel.setToolTipText(MessageFormat.format(TOOLTIP_TEXT_APPLICATION_FORMAT, applicationName));
        emailAccountsPanel.add(applicationPanel,applicationPanelConstraints);
        applicationPanelConstraints.gridy++;

        accountConstraints.gridy = 0;
        
        for (AbstractAccount account: accounts)
        {
          UserAccountEntry userAccountEntry = new UserAccountEntry(account);
          _userAccountEntries.add(userAccountEntry);
          
          Box line1 = Box.createHorizontalBox();
          applicationPanel.add(line1,accountConstraints);
          
          strut = Box.createHorizontalStrut(20);
          line1.add(strut);
          JCheckBox senderEmail = userAccountEntry.GetSenderEmail();
          line1.add(senderEmail);
          glue = Box.createHorizontalGlue();
          line1.add(glue);

          accountConstraints.gridy++;
          
          Box line2 = Box.createHorizontalBox();
          applicationPanel.add(line2,accountConstraints);

          strut = Box.createHorizontalStrut(40);
          line2.add(strut);
          JLabel passwordLabel = new JLabel("Password:");
          passwordLabel.setToolTipText(MessageFormat.format(TOOLTIP_TEXT_PASSWORD,senderEmail.getText()));
          line2.add(passwordLabel);
          strut = Box.createHorizontalStrut(5);
          line2.add(strut);
          line2.add(userAccountEntry.GetPassword());
          glue = Box.createHorizontalGlue();
          line2.add(glue);
          
          accountConstraints.gridy++;

          SMTPHostsOverridesKey smtpHostsOverridesKey = account.GetSMTPHostsOverridesKey();
          SMTPHost smtpHost = smtpHostsOverride.Get(smtpHostsOverridesKey);
          if (smtpHost != null)
          {
            userAccountEntry.GetSenderEmail().setSelected(true);
            userAccountEntry.GetPassword().setText(smtpHost.GetPassword());
          }
        }
      }
    }

    applicationPanelConstraints.gridy++;

    applicationPanelConstraints.gridx = 0;
    applicationPanelConstraints.anchor = GridBagConstraints.EAST;
    applicationPanelConstraints.fill = GridBagConstraints.NONE;
    applicationPanelConstraints.insets = new Insets(5,5,5,5);
    emailAccountsPanel.add(_next,applicationPanelConstraints);
    
    applicationPanelConstraints.gridy++;

    glue = Box.createVerticalGlue();
    applicationPanelConstraints.fill = GridBagConstraints.BOTH;
    applicationPanelConstraints.weightx = 1.0d;
    applicationPanelConstraints.weighty = 1.0d;
    applicationPanelConstraints.insets = new Insets(0,0,0,0);
    emailAccountsPanel.add(glue,applicationPanelConstraints);
  }
    
  protected void AddBasicSISTab(JTabbedPane tabbedPane)
  {
    JScrollPane basicSISScrollPane = new JScrollPane();
    tabbedPane.addTab("iCard", (Icon)null, basicSISScrollPane, TOOLTIP_TEXT_BASIC_ICARD_TAB);
    basicSISScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    basicSISScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
    
    JPanel basicSISPanel = new JPanel(new GridBagLayout());
    basicSISScrollPane.setViewportView(basicSISPanel);
    
    GridBagConstraints sisPanelConstraints = new GridBagConstraints();
    sisPanelConstraints.anchor = GridBagConstraints.NORTHWEST;
    sisPanelConstraints.fill = GridBagConstraints.NONE;
    sisPanelConstraints.gridheight = 1;
    sisPanelConstraints.gridwidth = 1;
    sisPanelConstraints.insets = new Insets(0,0,0,0);
    sisPanelConstraints.ipadx = 0;
    sisPanelConstraints.ipady = 0;
    sisPanelConstraints.weightx = 0;
    sisPanelConstraints.weighty = 0;
    sisPanelConstraints.gridy = 0;
    sisPanelConstraints.gridx = 0;
    
    Component strut = Box.createVerticalStrut(15);
    basicSISPanel.add(strut,sisPanelConstraints);

    sisPanelConstraints.gridy++;

    Box line = Box.createHorizontalBox();
    sisPanelConstraints.gridwidth = GridBagConstraints.REMAINDER;
    basicSISPanel.add(line,sisPanelConstraints);
    strut = Box.createHorizontalStrut(5);
    line.add(strut);
    JLabel instructionsLabel = new JLabel(BASIC_SIS_SETTINGS_TAB_INSTRUCTIONS);
    line.add(instructionsLabel);
    strut = Box.createHorizontalStrut(5);
    line.add(strut);
    Component glue = Box.createHorizontalGlue();
    line.add(glue);

    sisPanelConstraints.gridy++;
    
    strut = Box.createVerticalStrut(15);
    sisPanelConstraints.gridwidth = 1;
    basicSISPanel.add(strut,sisPanelConstraints);

    sisPanelConstraints.gridy++;
    
    Element sisMailetElement = _mailetElements.get(W2O_SIGNATURE_INJECTION_SERVICE_ELEMENT);
    HashMap<String,Element> elementsByTagName = ToolBox.GetElementsByTagName(sisMailetElement);

    _sisInjectSignatureOnReplyAndForward = new JCheckBox("Inject Signature on Reply and Forward");
    sisPanelConstraints.gridx = 0;
    sisPanelConstraints.gridwidth = GridBagConstraints.REMAINDER;
    basicSISPanel.add(_sisInjectSignatureOnReplyAndForward,sisPanelConstraints);
    _sisInjectSignatureOnReplyAndForward.setToolTipText(TOOLTIP_TEXT_INJECT_SIGNATURE_ON_REPLY_AND_FORWARD);

    Element injectSignatureOnReplyAndForwardElement = elementsByTagName.get(com.World2One.Mailets.Constants.INJECT_SIGNATURE_ON_REPLY_AND_FORWARD);    
    String injectSignatureOnReplyOrForward = injectSignatureOnReplyAndForwardElement.getTextContent().trim();
    _sisInjectSignatureOnReplyAndForward.setSelected(Boolean.valueOf(injectSignatureOnReplyOrForward));

    
    sisPanelConstraints.gridy++;
    
    _sisDisplayMessageSentPopup = new JCheckBox("Display Message Sent Popup");
    sisPanelConstraints.gridx = 0;
    sisPanelConstraints.gridwidth = GridBagConstraints.REMAINDER;
    basicSISPanel.add(_sisDisplayMessageSentPopup,sisPanelConstraints);
    _sisDisplayMessageSentPopup.setToolTipText(TOOLTIP_TEXT_DISPLAY_MESSAGE_SENT_POPUP);

    Element displayMessageSentPopupElement = elementsByTagName.get(com.World2One.Mailets.Constants.DISPLAY_MESSAGE_SENT_POPUP);    
    String displayMessageSentPopup = displayMessageSentPopupElement.getTextContent().trim();
    _sisDisplayMessageSentPopup.setSelected(Boolean.valueOf(displayMessageSentPopup));

    sisPanelConstraints.gridy++;
    
    glue = Box.createVerticalGlue();
    sisPanelConstraints.fill = GridBagConstraints.BOTH;
    sisPanelConstraints.weightx = 1.0d;
    sisPanelConstraints.weighty = 1.0d;
    basicSISPanel.add(glue,sisPanelConstraints);

    sisPanelConstraints.gridy++;

    sisPanelConstraints.weightx = 0d;
    sisPanelConstraints.weighty = 0d;
    sisPanelConstraints.insets = new Insets(5,5,5,5);

    sisPanelConstraints.gridx = 0;
    sisPanelConstraints.anchor = GridBagConstraints.WEST;
    sisPanelConstraints.fill = GridBagConstraints.NONE;
    basicSISPanel.add(_back,sisPanelConstraints);

    sisPanelConstraints.gridx = 1;
    sisPanelConstraints.anchor = GridBagConstraints.EAST;
    sisPanelConstraints.fill = GridBagConstraints.NONE;
    basicSISPanel.add(_finish,sisPanelConstraints);
  }

  protected void AddAdvancedSISTab(JTabbedPane tabbedPane)
  {
    JScrollPane advancedSISScrollPane = new JScrollPane();
    tabbedPane.addTab("Advanced iCard", (Icon)null, advancedSISScrollPane, TOOLTIP_TEXT_ADVANCED_ICARD_TAB);
    advancedSISScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    advancedSISScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
    
    JPanel advancedSISPanel = new JPanel(new GridBagLayout());
    advancedSISScrollPane.setViewportView(advancedSISPanel);

    GridBagConstraints sisPanelConstraints = new GridBagConstraints();
    sisPanelConstraints.anchor = GridBagConstraints.WEST;
    sisPanelConstraints.fill = GridBagConstraints.NONE;
    sisPanelConstraints.gridheight = 1;
    sisPanelConstraints.gridwidth = 1;
    sisPanelConstraints.insets = new Insets(0,0,0,0);
    sisPanelConstraints.ipadx = 0;
    sisPanelConstraints.ipady = 0;
    sisPanelConstraints.weightx = 0;
    sisPanelConstraints.weighty = 0;
    sisPanelConstraints.gridy = 0;
    sisPanelConstraints.gridx = 0;
    
    Component strut = Box.createVerticalStrut(15);
    advancedSISPanel.add(strut,sisPanelConstraints);

    sisPanelConstraints.gridy++;

    Box line = Box.createHorizontalBox();
    sisPanelConstraints.gridwidth = GridBagConstraints.REMAINDER;
    advancedSISPanel.add(line,sisPanelConstraints);
    strut = Box.createHorizontalStrut(5);
    line.add(strut);
    JLabel instructionsLabel = new JLabel(ADVANCED_SIS_SETTINGS_TAB_INSTRUCTIONS);
    line.add(instructionsLabel);
    strut = Box.createHorizontalStrut(5);
    line.add(strut);
    Component glue = Box.createHorizontalGlue();
    line.add(glue);

    sisPanelConstraints.gridy++;
    
    strut = Box.createVerticalStrut(15);
    sisPanelConstraints.gridwidth = 1;
    advancedSISPanel.add(strut,sisPanelConstraints);

    sisPanelConstraints.gridy++;

    Element sisMailetElement = _mailetElements.get(W2O_SIGNATURE_INJECTION_SERVICE_ELEMENT);
    HashMap<String,Element> elementsByTagName = ToolBox.GetElementsByTagName(sisMailetElement);

    
    _sisDebug = new JCheckBox("Debug");
    sisPanelConstraints.gridx = 0;
    sisPanelConstraints.gridwidth = GridBagConstraints.REMAINDER;
    advancedSISPanel.add(_sisDebug,sisPanelConstraints);
    _sisDebug.setToolTipText(TOOLTIP_TEXT_DEBUG);

    Element debugElement = elementsByTagName.get(com.World2One.Mailets.Constants.DEBUG);    
    String debug = debugElement.getTextContent().trim();
    _sisDebug.setSelected(Boolean.valueOf(debug));

    sisPanelConstraints.gridy++;
    
    _sisLogAuthorizedRecipients = new JCheckBox("Log Authorized Recipients");
    sisPanelConstraints.gridx = 0;
    sisPanelConstraints.gridwidth = GridBagConstraints.REMAINDER;
    advancedSISPanel.add(_sisLogAuthorizedRecipients,sisPanelConstraints);
    _sisLogAuthorizedRecipients.setToolTipText(TOOLTIP_TEXT_LOG_AUTHORIZED_RECIPIENTS);

    Element logAuthorizedRecipientsElement = elementsByTagName.get(com.World2One.Mailets.Constants.LOG_AUTHORIZED_RECIPIENTS);    
    String logAuthorizedRecipients = logAuthorizedRecipientsElement.getTextContent().trim();
    _sisLogAuthorizedRecipients.setSelected(Boolean.valueOf(logAuthorizedRecipients));

    sisPanelConstraints.gridy++;
    
    JLabel sisSOAPProtocol = new JLabel("SOAP Protocol");
    sisPanelConstraints.gridx = 0;
    sisPanelConstraints.gridwidth = 1;
    advancedSISPanel.add(sisSOAPProtocol,sisPanelConstraints);
    sisSOAPProtocol.setToolTipText(TOOLTIP_TEXT_SOAP_PROTOCOL);
    
    _sisSOAPProtocolHTTP = new JRadioButton("HTTP");
    sisPanelConstraints.gridx = 1;
    sisPanelConstraints.gridwidth = 1;
    advancedSISPanel.add(_sisSOAPProtocolHTTP,sisPanelConstraints);
    _sisSOAPProtocolHTTP.setToolTipText(TOOLTIP_TEXT_SOAP_PROTOCOL_HTTP);
    
    _sisSOAPProtocolHTTPS = new JRadioButton("HTTPS");
    sisPanelConstraints.gridx = 2;
    sisPanelConstraints.gridwidth = 1;
    advancedSISPanel.add(_sisSOAPProtocolHTTPS,sisPanelConstraints);
    _sisSOAPProtocolHTTPS.setToolTipText(TOOLTIP_TEXT_SOAP_PROTOCOL_HTTPS);

    ButtonGroup buttonGroup = new ButtonGroup();
    buttonGroup.add(_sisSOAPProtocolHTTP);
    buttonGroup.add(_sisSOAPProtocolHTTPS);

    Element soapProtocolElement = elementsByTagName.get(com.World2One.Mailets.Constants.SOAP_PROTOCOL);    
    String soapProtocol = soapProtocolElement.getTextContent().trim();
    if (soapProtocol.equalsIgnoreCase("HTTP") == true)
    {
      _sisSOAPProtocolHTTP.setSelected(true);
      _sisSOAPProtocolHTTPS.setSelected(false);
    }
    else
    {
      _sisSOAPProtocolHTTP.setSelected(false);
      _sisSOAPProtocolHTTPS.setSelected(true);
    }

    sisPanelConstraints.gridy++;
    
    JLabel sisSOAPHost = new JLabel("SOAP Host");
    sisPanelConstraints.gridx = 0;
    sisPanelConstraints.gridwidth = 1;
    advancedSISPanel.add(sisSOAPHost,sisPanelConstraints);
    sisSOAPHost.setToolTipText(TOOLTIP_TEXT_SOAP_HOST);
    
    _sisSOAPHost = new JTextField();
    sisPanelConstraints.gridx = 1;
    sisPanelConstraints.gridwidth = GridBagConstraints.REMAINDER;
    advancedSISPanel.add(_sisSOAPHost,sisPanelConstraints);
    _sisSOAPHost.setToolTipText(TOOLTIP_TEXT_SOAP_HOST);
    _sisSOAPHost.setColumns(20);

    Element soapHostElement = elementsByTagName.get(com.World2One.Mailets.Constants.SOAP_HOST);    
    String soapHost = soapHostElement.getTextContent().trim();
    _sisSOAPHost.setText(soapHost);

    sisPanelConstraints.gridy++;
    
    JLabel sisSOAPPort = new JLabel("SOAP Port");
    sisPanelConstraints.gridx = 0;
    sisPanelConstraints.gridwidth = 1;
    advancedSISPanel.add(sisSOAPPort,sisPanelConstraints);
    sisSOAPPort.setToolTipText(TOOLTIP_TEXT_SOAP_PORT);
    
    _sisSOAPPort = new JTextField();
    sisPanelConstraints.gridx = 1;
    sisPanelConstraints.gridwidth = GridBagConstraints.REMAINDER;
    advancedSISPanel.add(_sisSOAPPort,sisPanelConstraints);
    _sisSOAPPort.setToolTipText(TOOLTIP_TEXT_SOAP_PORT);
    _sisSOAPPort.setColumns(4);

    Element soapPortElement = elementsByTagName.get(com.World2One.Mailets.Constants.SOAP_PORT);    
    String soapPort = soapPortElement.getTextContent().trim();
    _sisSOAPPort.setText(soapPort);

    sisPanelConstraints.gridy++;
    
    glue = Box.createVerticalGlue();
    sisPanelConstraints.fill = GridBagConstraints.BOTH;
    sisPanelConstraints.weightx = 1.0d;
    sisPanelConstraints.weighty = 1.0d;
    advancedSISPanel.add(glue,sisPanelConstraints);

    sisPanelConstraints.gridy++;

    sisPanelConstraints.weightx = 0d;
    sisPanelConstraints.weighty = 0d;
    sisPanelConstraints.insets = new Insets(5,5,5,5);
    sisPanelConstraints.gridx = 0;
    sisPanelConstraints.anchor = GridBagConstraints.EAST;
    sisPanelConstraints.fill = GridBagConstraints.NONE;
    advancedSISPanel.add(_saveAndExit,sisPanelConstraints);
  }

  protected void AddSOAPArchiverTab(JTabbedPane tabbedPane)
  {
    JScrollPane soapArchiverScrollPane = new JScrollPane();
    tabbedPane.addTab("SOAP Archiver", (Icon)null, soapArchiverScrollPane, TOOLTIP_TEXT_SOAP_ARCHIVER_TAB);
    soapArchiverScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    soapArchiverScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
    
    JPanel soapArchiverPanel = new JPanel(new GridBagLayout());
    soapArchiverScrollPane.setViewportView(soapArchiverPanel);
  }

  protected void AddJDBCArchiverTab(JTabbedPane tabbedPane)
  {
    JScrollPane jdbcArchiverScrollPane = new JScrollPane();
    tabbedPane.addTab("JDBC Archiver", (Icon)null, jdbcArchiverScrollPane, TOOLTIP_TEXT_JDBC_ARCHIVER_TAB);
    jdbcArchiverScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    jdbcArchiverScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
    
    JPanel jdbcArchiverPanel = new JPanel(new GridBagLayout());
    jdbcArchiverScrollPane.setViewportView(jdbcArchiverPanel);
  }

  public void ProcessParms(String[] args) 
  {
    try
    {
      for (int indx = 0; indx < args.length; indx++)
      {
        String arg = args[indx].trim();
        if (arg.equalsIgnoreCase(RESTORE_ALL_ORIGINAL_SMTP_SETTINGS_PARM) == true)
        {
          if (__restoreAllOriginalSMTPSettings == false)
          {
            __restoreAllOriginalSMTPSettings = true;
          }
          else
          {
            ToolBox.Exit(RESTORE_ALL_ORIGINAL_SMTP_SETTINGS_PARM + " was specifed more than once", ToolBox.EXIT_PARM_MULTIPLY_DEFINED_ERROR);
          }
        }
        else
        if (arg.equalsIgnoreCase(WILDCARD_TOP_PARM) == true)
        {
          if (__wildcardTop == null)
          {
            indx++;
            __wildcardTop = args[indx].trim();
          }
          else
          {
            ToolBox.Exit(WILDCARD_TOP_PARM + " was specifed more than once", ToolBox.EXIT_PARM_MULTIPLY_DEFINED_ERROR);
          }
        }
        else
        if (arg.equalsIgnoreCase(DO_NOT_UPDATE_PARM) == true)
        {
          if (__doNotUpdate == false)
          {
            __doNotUpdate = true;
          }
          else
          {
            ToolBox.Exit(DO_NOT_UPDATE_PARM + " was specifed more than once", ToolBox.EXIT_PARM_MULTIPLY_DEFINED_ERROR);
          }
        }
        else
        if (arg.equalsIgnoreCase(DEBUG_PARM) == true)
        {
          if (__debug == false)
          {
            __debug = true;
          }
          else
          {
            ToolBox.Exit(DEBUG_PARM + " was specifed more than once", ToolBox.EXIT_PARM_MULTIPLY_DEFINED_ERROR);
          }
        }
      }
    }
    catch (Exception ex)
    {
      ToolBox.Exit("A Parameter value is missing.", ToolBox.EXIT_MISSING_PARM_VALUE_ERROR);
    }
  
    // initialize missing parms
    if (__wildcardTop == null)
    {
      __wildcardTop = "../";
    }
  }

  public static Object DisplayMessage(
    String message,
    String title,
    int    messageType,
    int    optionType )
  {
    JOptionPane optionPane = new JOptionPane(message,messageType,optionType);
    JDialog dialog = optionPane.createDialog(title);
    dialog.setModalityType(ModalityType.APPLICATION_MODAL);
    dialog.setVisible(true);
    Object selectedValue = optionPane.getValue();
    
    return selectedValue;
  }

  protected static final String SHUTDOWN_RUNNING_EMAIL_PROGRAMS_TITLE = "Shutdown EMail Programs";
  protected static final String SHUTDOWN_RUNNING_EMAIL_PROGRAMS_MSG = "The email programs listed below are currently running.\n\n{0}\nAll email programs must be shutdown before the Configurator can be run.\n\nShutdown your EMail programs and press OK to continue or\npress Cancel to cancel the Configurator.\n\n";
  
  protected boolean CheckForRunningEMailPrograms()
  {
    HashSet<AbstractProcess> runningEMailPrograms;
    do
    {
      runningEMailPrograms = GetRunningEMailPrograms();
      if (runningEMailPrograms.size() > 0)
      {
        StringBuilder stringBuilder = new StringBuilder();
        for (AbstractProcess runningEMailProgram: runningEMailPrograms)
        {
          stringBuilder.append(runningEMailProgram.GetProcessName());
          stringBuilder.append("\n");
        }
    
        String message = MessageFormat.format(SHUTDOWN_RUNNING_EMAIL_PROGRAMS_MSG,stringBuilder.toString());

        Object selectedValue = DisplayMessage(message,SHUTDOWN_RUNNING_EMAIL_PROGRAMS_TITLE,JOptionPane.INFORMATION_MESSAGE,JOptionPane.OK_CANCEL_OPTION);
        if ( (selectedValue == null) ||
             ((Integer)selectedValue == JOptionPane.CANCEL_OPTION) )
        {
          return false;
        }
      }
    } while (runningEMailPrograms.size() > 0); 
    
    return true;
  }
  
  protected abstract HashSet<AbstractProcess> GetRunningEMailPrograms();
  
  protected void FileExit()
  {
    setVisible(false);
    dispose();
    Runtime.getRuntime().exit(0);
  }
  
  protected class LocalWindowListener
    implements WindowListener
  {
    
    public LocalWindowListener()
    {
      super();
    }
  
    public void windowClosing(WindowEvent event)
    {
      FileExit();
    }
    
    public void windowClosed(WindowEvent event)
    {
      FileExit();
    }

    @Override
    public void windowActivated(WindowEvent event)
    {
      // do nothing
    }

    @Override
    public void windowDeactivated(WindowEvent event)
    {
      // do nothing
    }

    @Override
    public void windowDeiconified(WindowEvent event)
    {
      // do nothing
    }

    @Override
    public void windowIconified(WindowEvent event)
    {
      // do nothing
    }

    @Override
    public void windowOpened(WindowEvent event)
    {
      // do nothing
    }

  }

  protected class UserAccountEntry
    extends KeyAdapter
    implements ItemListener
  {
    
    private AbstractAccount _account;
    
    private JLabel          _applicationName;
    private JCheckBox       _senderEmail;
    private JPasswordField  _password;
    private JTextField      _hostAddress;
    private JTextField      _hostPort;
    private JTextField      _userAccount;
    private JRadioButton    _secureConnectionNone;
    private JRadioButton    _secureConnectionSSL;
    private JRadioButton    _secureConnectionTLS;
    
    
    public UserAccountEntry(AbstractAccount account)
    {
      super();
    
      _account = account;
      
      AbstractApplication application = account.GetApplication();
      
      _applicationName = new JLabel();
      _applicationName.setText(application.GetApplicationName());
      
      _senderEmail = new JCheckBox();
      String emailAddress = account.GetEmailAddress();
      _senderEmail.setText(emailAddress);
      _senderEmail.setSelected(false);
      _senderEmail.addItemListener(this);
      _senderEmail.setToolTipText(MessageFormat.format(TOOLTIP_TEXT_EMAIL_ACCOUNT_FORMAT,emailAddress));
      
      _hostAddress = new JTextField();
      _hostAddress.setText(account.GetHostAddress());
      _hostAddress.setColumns(20);
      
      _hostPort = new JTextField();
      _hostPort.setText(String.valueOf(account.GetPort()));
      _hostPort.setColumns(5);
      
      _userAccount = new JTextField();
      _userAccount.setText(account.GetUserName());
      _userAccount.setColumns(20);
      
      _password = new JPasswordField();
      _password.setText(null);
      _password.setColumns(12);
      _password.setEnabled(false);
      _password.addKeyListener(this);
      _password.setToolTipText(MessageFormat.format(TOOLTIP_TEXT_PASSWORD,emailAddress));
      
      _secureConnectionNone = new JRadioButton(SecureConnection.None.name());
      _secureConnectionSSL = new JRadioButton(SecureConnection.SSL.name());
      _secureConnectionTLS = new JRadioButton(SecureConnection.TLS.name());

      JRadioButton radioButton = new JRadioButton();
      radioButton.add(_secureConnectionNone);
      radioButton.add(_secureConnectionSSL);
      radioButton.add(_secureConnectionTLS);
      
      SecureConnection connectionType = account.GetConnectionType();
      switch (connectionType)
      {
        case None:
          _secureConnectionNone.setSelected(true);
          break;
        case SSL:
          _secureConnectionSSL.setSelected(true);
          break;
        case TLS:
          _secureConnectionTLS.setSelected(true);
          break;
      }
    }

    @Override
    public void itemStateChanged(ItemEvent event)
    {
      boolean isSelected = _senderEmail.isSelected();
      _password.setEnabled(isSelected);
      if (isSelected == true)
      {
        _password.requestFocusInWindow();
      }
      
      EnableSave();
    }

    @Override
    public void keyTyped(KeyEvent event)
    {
      EnableSave();
    }
    
    public String GetSecureConnection()
    {
      String secureConnection = SecureConnection.None.name();
      if (_secureConnectionSSL.isSelected() == true)
      {
        secureConnection = SecureConnection.SSL.name();
      }
      else
      if (_secureConnectionTLS.isSelected() == true)
      {
        secureConnection = SecureConnection.TLS.name();
      }
      
      return secureConnection;
    }

    public AbstractAccount GetAccount()
    {
      return _account;
    }

    public void SetAccount(AbstractAccount account)
    {
      _account = account;
    }

    public JLabel GetApplicationName()
    {
      return _applicationName;
    }

    public void SetApplicationName(JLabel applicationName)
    {
      _applicationName = applicationName;
    }

    public JCheckBox GetSenderEmail()
    {
      return _senderEmail;
    }

    public void SetSenderEmail(JCheckBox senderEmail)
    {
      _senderEmail = senderEmail;
    }

    public JPasswordField GetPassword()
    {
      return _password;
    }

    public void SetPassword(JPasswordField password)
    {
      _password = password;
    }

    public JTextField GetHostAddress()
    {
      return _hostAddress;
    }

    public void SetHostAddress(JTextField hostAddress)
    {
      _hostAddress = hostAddress;
    }

    public JTextField GetHostPort()
    {
      return _hostPort;
    }

    public void SetHostPort(JTextField hostPort)
    {
      _hostPort = hostPort;
    }

    public JTextField GetUserAccount()
    {
      return _userAccount;
    }

    public void SetUserAccount(JTextField userAccount)
    {
      _userAccount = userAccount;
    }

    public JRadioButton GetSecureConnectionNone()
    {
      return _secureConnectionNone;
    }

    public void SetSecureConnectionNone(JRadioButton secureConnectionNone)
    {
      _secureConnectionNone = secureConnectionNone;
    }

    public JRadioButton GetSecureConnectionSSL()
    {
      return _secureConnectionSSL;
    }

    public void SetSecureConnectionSSL(JRadioButton secureConnectionSSL)
    {
      _secureConnectionSSL = secureConnectionSSL;
    }

    public JRadioButton GetSecureConnectionTLS()
    {
      return _secureConnectionTLS;
    }

    public void SetSecureConnectionTLS(JRadioButton secureConnectionTLS)
    {
      _secureConnectionTLS = secureConnectionTLS;
    }

    public SMTPHostsOverridesKey GetSMTPHostsOverridesKey() 
      throws MissingParameterException
    {
      return new SMTPHostsOverridesKey(_applicationName.getText(),_senderEmail.getText());
    }
    
    public SMTPHost CreateSMTPHost()
    {
      return new SMTPHost(
        _applicationName.getText(), 
        _senderEmail.getText(), 
        _hostAddress.getText(), 
        _hostPort.getText(), 
        _userAccount.getText(), 
        String.valueOf(_password.getPassword()), 
        SecureConnection.valueOf(GetSecureConnection()));      
    }
  }

  protected void EnableSave()
  {
    boolean enabled = true;
    
    for (UserAccountEntry userAccountEntry: _userAccountEntries)
    {
      if (userAccountEntry.GetSenderEmail().isSelected() == true)
      {
        char[] passwordCharArray = userAccountEntry.GetPassword().getPassword();
        if (passwordCharArray == null)
        {
          String password = String.valueOf(passwordCharArray).trim();
          if (password.trim().length() == 0)
          {
            enabled = false;
            break;
          }
        }
      }
    }
    
    _finish.setEnabled(enabled);
  }
  
  protected void RestoreAllOriginalSMTPSettings(HashSet<AbstractApplication> installedApplications) 
    throws ParserConfigurationException, SAXException, IOException, MissingParameterException, TransformerException
  {
    SMTPHostsOverridesTable smtpHostsOverride = SMTPHost.GetSMTPHostsOverrides(__w2oRemoteDeliveryFilePath,null);
    for (AbstractApplication installedApplication: installedApplications)
    {
      HashSet<AbstractAccount> accounts = installedApplication.GetAccounts();
      for (AbstractAccount account: accounts)
      {
        SMTPHost smtpHost = smtpHostsOverride.Get(account.GetSMTPHostsOverridesKey());
        if (smtpHost != null)
        {
          account.SetSMTPSettings(account.GetAccountName(),smtpHost.GetSenderEmail(),smtpHost.GetHostAddress(),smtpHost.GetPort(),smtpHost.GetUser(),smtpHost.GetPassword(),smtpHost.GetSecureConnection());
        }
      }
    }
    
    SMTPHost.ClearSMTPHostsOverrides(__w2oRemoteDeliveryFilePath,null);
    
    JOptionPane.showMessageDialog(null,"Your original SMTP settings have been restored.","Original SMTP Settings Restored",JOptionPane.INFORMATION_MESSAGE);
  }

  protected void ShutdownWildCard() 
    throws IOException
  {
    try
    {
      ServiceController.ShutdownWildCard(__wildcardTop);
    }
    catch (PIDFileNotFoundException ex)
    {
      if (__debug == true)
      {
        ex.printStackTrace();
      }
      else
      {
        System.out.println(ex.getLocalizedMessage());
      }
    }
  }

  protected void ShutdownKeepAlive() 
    throws IOException
  {
    try
    {
      ServiceController.ShutdownKeepAlive(__wildcardTop);
    }
    catch (PIDFileNotFoundException ex)
    {
      if (__debug == true)
      {
        ex.printStackTrace();
      }
      else
      {
        System.out.println(ex.getLocalizedMessage());
      }
    }
  }
  
  protected void FileSave() 
    throws IOException, ParserConfigurationException, SAXException, MissingParameterException, TransformerException 
  {
    ShutdownWildCard();

    /*
     * W2ORemoteDelivery.xml
     */
    SMTPHostsOverridesTable smtpHostsOverride = SMTPHost.GetSMTPHostsOverrides(__w2oRemoteDeliveryFilePath,null);
    for (UserAccountEntry userAccountEntry: _userAccountEntries)
    {
      SMTPHost smtpHost = smtpHostsOverride.Get(userAccountEntry.GetSMTPHostsOverridesKey());

      if (smtpHost != null)
      {
        smtpHost.RemoveFromSMTPHostsOverrideDocument();
      }

      AbstractAccount account = userAccountEntry.GetAccount();
      
      if (userAccountEntry.GetSenderEmail().isSelected() == true)
      {
        // add selected account
        smtpHost = userAccountEntry.CreateSMTPHost();
        smtpHost.AddToSMTPHostsOverrideDocument();
        account.SetSMTPSettings2ReferenceLocalWildCardServer();
      }
      else
      {
        if (smtpHost == null)
        {
          if (account.GetPassword() == null)
          {
            String password = String.valueOf(userAccountEntry.GetPassword().getPassword());
            if (password.length() > 0)
            {
              account.SetPassword(password);
            }
          }
          userAccountEntry.GetAccount().SetSMTPSettings2InitialValues();
        }
        else
        {
          userAccountEntry.GetAccount().SetSMTPSettings(smtpHost);
        }
      }
    }

    // all remote delivery smtp host entries are in place. write the file out.
    SMTPHost.WriteW2ORemoteDelivery(__w2oRemoteDeliveryFilePath);
    
    /**
     * JAMES config.xml
     */
    // W2OSignatureInjectionService ...
    Element sisMailetElement = _mailetElements.get(W2O_SIGNATURE_INJECTION_SERVICE_ELEMENT);
    HashMap<String,Element> elementsByTagName = ToolBox.GetElementsByTagName(sisMailetElement);
    
    Element debugElement = elementsByTagName.get(com.World2One.Mailets.Constants.DEBUG);    
    debugElement.setTextContent(String.valueOf(_sisDebug.isSelected()));
    
    Element injectSignatureOnReplyOrForwardElement = elementsByTagName.get(com.World2One.Mailets.Constants.INJECT_SIGNATURE_ON_REPLY_AND_FORWARD);    
    injectSignatureOnReplyOrForwardElement.setTextContent(String.valueOf(_sisInjectSignatureOnReplyAndForward.isSelected()));
    
    Element displayMessageSentPopupElement = elementsByTagName.get(com.World2One.Mailets.Constants.DISPLAY_MESSAGE_SENT_POPUP);    
    displayMessageSentPopupElement.setTextContent(String.valueOf(_sisDisplayMessageSentPopup.isSelected()));
    
    Element logAuthorizedRecipientsElement = elementsByTagName.get(com.World2One.Mailets.Constants.LOG_AUTHORIZED_RECIPIENTS);    
    logAuthorizedRecipientsElement.setTextContent(String.valueOf(_sisLogAuthorizedRecipients.isSelected()));
    
    Element soapProtocolElement = elementsByTagName.get(com.World2One.Mailets.Constants.SOAP_PROTOCOL);
    String soapProtocolText = _sisSOAPProtocolHTTP.isSelected() == true ? "HTTP" : "HTTPS";
    soapProtocolElement.setTextContent(soapProtocolText);
    
    Element soapHostElement = elementsByTagName.get(com.World2One.Mailets.Constants.SOAP_HOST);    
    soapHostElement.setTextContent(_sisSOAPHost.getText());
    
    Element soapPortElement = elementsByTagName.get(com.World2One.Mailets.Constants.SOAP_PORT);    
    soapPortElement.setTextContent(_sisSOAPPort.getText());
    
    // W2OJDBCArchiverService ...

    // todo
    
    // W2OSOAPArchiverService ...

    // todo

    // done with james config file modifications. write it out to disk.
    _config.Write(__jamesConfigFilePath);
    
    ServiceController.LaunchWildCard(__wildcardTop);
    
    JOptionPane.showMessageDialog(null,CONFIGURATION_SUCCESSFUL_MESSAGE,"Configuration Successful",JOptionPane.INFORMATION_MESSAGE);
  }
  
  protected class FileSaveActionListener
    implements ActionListener
  {
    public FileSaveActionListener()
    {
      super();
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      try
      {
        FileSave();
      }
      catch (Exception ex)
      {
        JOptionPane.showMessageDialog(null,ex.getLocalizedMessage(),ex.getClass().getSimpleName(),JOptionPane.ERROR_MESSAGE);
        ex.printStackTrace();
      }
    }
  }
  
  protected class FileExitActionListener
    implements ActionListener
  {
    public FileExitActionListener()
    {
      super();
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      FileExit();
    }
  }

  protected class NextButtonActionListener
    implements ActionListener
  {
    public NextButtonActionListener()
    {
      super();
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      _tabbedPane.setSelectedIndex(1);
    }
  }

  protected class BackButtonActionListener
    implements ActionListener
  {
    public BackButtonActionListener()
    {
      super();
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      _tabbedPane.setSelectedIndex(0);
    }
  }

  protected class FinishButtonActionListener
    implements ActionListener
  {
    public FinishButtonActionListener()
    {
      super();
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      try
      {
        FileSave();
        FileExit();
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
    }
  }

  protected class SaveAndExitButtonActionListener
    implements ActionListener
  {
    public SaveAndExitButtonActionListener()
    {
      super();
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      try
      {
        FileSave();
        FileExit();
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
    }
  }

  protected class ShowPasswordsInPlainTextChangeListener
    implements ChangeListener
  {
    public ShowPasswordsInPlainTextChangeListener()
    {
      super();
    }

    @Override
    public void stateChanged(ChangeEvent event)
    {
      char echoChar = (_showPasswordsInPlainText.isSelected() == true) ? 0 : '*';
      for (UserAccountEntry userAccountEntry: _userAccountEntries)
      {
        userAccountEntry.GetPassword().setEchoChar(echoChar);
      }
    }
    
  }
}
