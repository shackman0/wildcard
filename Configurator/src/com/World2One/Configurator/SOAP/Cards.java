package com.World2One.Configurator.SOAP;

import java.net.MalformedURLException;
import java.net.URL;
import java.security.PrivilegedActionException;
import java.text.MessageFormat;
import java.util.HashMap;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import com.World2One.SOAP.AbstractSOAPClient;
import com.World2One.Utility.Constants;
import com.World2One.Utility.MutableBoolean;

/**
 * <p>Title: Cards</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One, Inc.</p>
 */
public class Cards
  extends AbstractSOAPClient
{
  
  public static final String API_CARDS  = "Cards.asmx";
  
  public static final String SOAP_ACTION_IS_W2O_USER   = "http://world2one.com/IsW2OUser"; 

  protected static final String IS_W2O_USER_SOAP_1_1_REQUEST_BODY =
    "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + Constants.CR_LF + 
    "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" + Constants.CR_LF + 
    "  <soap:Body>" + Constants.CR_LF + 
    "    <IsW2OUser xmlns=\"http://world2one.com/\">" + Constants.CR_LF + 
    "      <emailaddress> {0} </emailaddress>" + Constants.CR_LF +     // email address
    "      <apikey>" + API_KEY + "</apikey>" + Constants.CR_LF + 
    "    </IsW2OUser>" + Constants.CR_LF + 
    "  </soap:Body>" + Constants.CR_LF + 
    "</soap:Envelope>";
  

  protected static final String SOAP_PROTOCOL = "http";
  protected static final String SOAP_HOST     = "www.world2one.com";
  protected static final String SOAP_PORT     = "80";
  
  
 
  
  public Cards()
  {
    super();
  }
  
  /**
   * 
   * @param soapHost  in
   * @param soapPort  in
   * @param emailAddress  in
   * @param isW2OUser  out
   * @return null = no errors. not null = error message.
   * @throws MalformedURLException 
   * @throws PrivilegedActionException 
   * @throws SOAPException 
   * @throws Exception 
   */
  public String IsW2OUser(
    String                   emailAddress,
    MutableBoolean           isW2OUser ) 
    throws MalformedURLException, SOAPException, PrivilegedActionException 
  {
    URL soapServiceURL = GetAPIURL(SOAP_PROTOCOL,SOAP_HOST,SOAP_PORT,API_CARDS);
    String formattedBody = MessageFormat.format(IS_W2O_USER_SOAP_1_1_REQUEST_BODY, emailAddress);
    SOAPMessage soapMessage = SendMessage(soapServiceURL,formattedBody,SOAP_ACTION_IS_W2O_USER);
    SOAPBody soapBody = soapMessage.getSOAPBody();
    
    if (soapBody.hasFault() == true)
    {
      String faultMessage = GetFaultMessage(soapBody);
      
      return faultMessage;
    }
    
    HashMap<String,SOAPBodyElement> elementsByTagName = GetElementsByTagName(soapBody);    
    
    SOAPBodyElement isW2OUserBodyElement = elementsByTagName.get("IsW2OUserResult");
    if (isW2OUserBodyElement == null)
    {
      return ("IsW2OUserResult is missing");
    }
    String isW2OUserValue = isW2OUserBodyElement.getTextContent();
    if (isW2OUserValue.trim().length() == 0)
    {
      return ("IsW2OUserResult is blank");
    }
    isW2OUser._boolean = Boolean.valueOf(isW2OUserValue);

    return null;
  }
  
}
