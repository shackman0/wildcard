package com.World2One.Configurator.MailClients;

import java.util.HashMap;
import java.util.HashSet;
import java.util.regex.Pattern;

import sun.security.pkcs11.wrapper.Constants;



/**
 * <p>Title: AbstractApplication</p>
 * <p>Description: abstract base class for all Email Applications</p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public abstract class AbstractApplication
{

  /**
   * used by sub classes to register themselves for use by the Create method.
   */
  protected static HashMap<Pattern /* APPLICATION_NAME_PATTERN */,Class<? extends AbstractApplication>>  __applicationNamePattern2ClassRegistry = new HashMap();
  
  
  public static AbstractApplication Create(Pattern applicationNamePattern) 
  {
    try
    {
      Class<? extends AbstractApplication> applicationClass = __applicationNamePattern2ClassRegistry.get(applicationNamePattern);
      AbstractApplication application = applicationClass.newInstance();
      
      return application;
    }
    catch (IllegalAccessException ex)
    {
      throw new RuntimeException(ex);
    }
    catch (InstantiationException ex)
    {
      throw new RuntimeException(ex);
    }
  }
  
  

  protected HashSet<AbstractAccount>   _accounts = null;
  
  
  public AbstractApplication()
  {
    super();
  }
  
  public abstract String GetApplicationName();
  
  public HashSet<AbstractAccount> GetAccounts()
  {
    if (_accounts == null)
    {
      _accounts = new HashSet();
      PopulateAccounts();
    }
    
    return _accounts;
  }

  protected abstract void PopulateAccounts();

  @Override
  public String toString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    
    stringBuilder.append(getClass().getSimpleName());
    for (AbstractAccount account: GetAccounts())
    {
      stringBuilder.append(Constants.NEWLINE);
      stringBuilder.append("  ");
      stringBuilder.append(account.toString());
    }
    
    return stringBuilder.toString();
  }

}
