package com.World2One.Configurator.MailClients.Win32;

import java.util.HashSet;

import javax.swing.JOptionPane;

import com.World2One.Configurator.AbstractConfigurator;
import com.World2One.OS.AbstractProcess;
import com.World2One.OS.AbstractProcessManager;


public class Configurator
  extends AbstractConfigurator
{

  
  
  public Configurator()
  {
    super();
  }
  
  protected static final String EMAIL_PROGRAM_IMAGE_NAME_THUNDERBIRD = "thunderbird.*";
  protected static final String EMAIL_PROGRAM_IMAGE_NAME_OUTLOOK     = "outlook.*";
  
  
  @Override
  protected HashSet<AbstractProcess> GetRunningEMailPrograms()
  {
    HashSet<AbstractProcess> runningEMailPrograms = new HashSet();
    AbstractProcessManager.GetSingleton().FindProcesses(EMAIL_PROGRAM_IMAGE_NAME_THUNDERBIRD,runningEMailPrograms);
    AbstractProcessManager.GetSingleton().FindProcesses(EMAIL_PROGRAM_IMAGE_NAME_OUTLOOK,runningEMailPrograms);
    
    return runningEMailPrograms;
  }

  
  public static void main(String... args)
  {
    Configurator configurator = new Configurator();
    try
    {
      configurator.Initialize(args);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      JOptionPane.showMessageDialog(null,ex.getLocalizedMessage(), "Configurator Initialization Error",JOptionPane.ERROR_MESSAGE);
      System.exit(-1);
    }
    configurator.setVisible(true);
    configurator.toFront();
  }
  
}
