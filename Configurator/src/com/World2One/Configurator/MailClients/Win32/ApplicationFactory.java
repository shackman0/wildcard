package com.World2One.Configurator.MailClients.Win32;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.regex.Pattern;

import org.sf.feeling.swt.win32.extension.registry.RegistryException;
import org.sf.feeling.swt.win32.extension.registry.RegistryKey;
import org.sf.feeling.swt.win32.extension.registry.RegistryValue;
import org.sf.feeling.swt.win32.extension.registry.RootKey;

import com.World2One.Configurator.MailClients.AbstractApplication;
import com.World2One.OS.OperatingSystemInfo;
import com.World2One.Utility.ToolBox;



/**
 * <p>Title: ApplicationFactory</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public class ApplicationFactory
{


  protected static HashSet<Pattern> __prospectiveApplications;
  {
    __prospectiveApplications = new HashSet();
    __prospectiveApplications.add(Thunderbird.APPLICATION_NAME_PATTERN);
    __prospectiveApplications.add(WindowsMail.APPLICATION_NAME_PATTERN);
    __prospectiveApplications.add(Outlook2000.APPLICATION_NAME_PATTERN);
    __prospectiveApplications.add(Outlook2003.APPLICATION_NAME_PATTERN);
    __prospectiveApplications.add(Outlook2007.APPLICATION_NAME_PATTERN);
    __prospectiveApplications.add(OutlookExpress.APPLICATION_NAME_PATTERN);
  }

  protected static final String DISPLAY_NAME = "DisplayName";
  
  
  public ApplicationFactory()
  {
    super();
  }

  public HashSet<AbstractApplication> GetInstalledApplications()
  {
    HashMap<String,AbstractApplication> installedApplications = new HashMap();   // key = AbstractApplication.GetApplicationName() 
    
    HashSet<Pattern> registeredApplications = new HashSet(__prospectiveApplications);

    RegistryKey topKey = new RegistryKey(RootKey.HKEY_LOCAL_MACHINE, "Software\\Microsoft\\Windows\\CurrentVersion\\Uninstall");
    Iterator<RegistryKey> subKeys = topKey.subkeys();
    while (subKeys.hasNext() == true)
    {
      RegistryKey subKey = subKeys.next();
      RegistryValue subKeyValue;
      try
      {
        subKeyValue = subKey.getValue(DISPLAY_NAME);
      }
      catch (RegistryException ex)
      {
        // subKey does not have a DISPLAY_NAME value
        continue;
      }
      String displayName = (String)subKeyValue.getData();
      Pattern matchingApplicationPattern = ToolBox.Matches(displayName,registeredApplications);
      if (matchingApplicationPattern != null)
      {
        if (installedApplications.containsKey(matchingApplicationPattern) == false)
        {
          AbstractApplication application = AbstractApplication.Create(matchingApplicationPattern);
          String applicationName = application.GetApplicationName();
          installedApplications.put(applicationName,application);
        }
      }
    }

    int majorVersion = OperatingSystemInfo.GetMajorVersion();
    if (majorVersion < 6)
    {
      AbstractApplication application = AbstractApplication.Create(OutlookExpress.APPLICATION_NAME_PATTERN);
      installedApplications.put(application.GetApplicationName(),application);
    }
    else
    {
      AbstractApplication application = AbstractApplication.Create(WindowsMail.APPLICATION_NAME_PATTERN);
      installedApplications.put(application.GetApplicationName(),application);
    }
    
    return new HashSet(installedApplications.values());
  }


  /**
   * unit test
   * @param args
   */
//  public static void main(String... args)
//  {
//    ApplicationFactory applicationFactory = new ApplicationFactory();
//    HashSet<AbstractApplication> installedApplications = applicationFactory.GetInstalledApplications();
//    for (AbstractApplication installedApplication: installedApplications)
//    {
//      System.out.println(installedApplication);
//      System.out.println();
//    }
//  }

}
