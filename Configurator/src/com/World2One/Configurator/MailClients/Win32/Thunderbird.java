package com.World2One.Configurator.MailClients.Win32;

import java.io.File;
import java.text.MessageFormat;
import java.util.regex.Pattern;

import com.World2One.Configurator.MailClients.AbstractApplication;
import com.World2One.Configurator.MailClients.AbstractThunderbird;

/**
 * <p>Title: Thunderbird</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public class Thunderbird
  extends AbstractThunderbird
{


  public static final String APPLICATION_NAME_REGEX      = "^Mozilla Thunderbird.*$";
  public static final Pattern APPLICATION_NAME_PATTERN;
  static
  {
    APPLICATION_NAME_PATTERN = Pattern.compile(APPLICATION_NAME_REGEX);
    AbstractApplication.__applicationNamePattern2ClassRegistry.put(APPLICATION_NAME_PATTERN, Thunderbird.class);
  }

  /**
   * 0 = user home
   */
  protected static final String PROFILES_INI_FULL_PATH = "{0}/AppData/Roaming/Thunderbird/profiles.ini";

  
  public Thunderbird()
  {
    super();
  }
  
  @Override
  protected File GetProfilesIniFile()
  {
    String userHome = System.getProperty("user.home");
    String profilesIniFullPath = MessageFormat.format(PROFILES_INI_FULL_PATH,userHome);
    File profilesIniFile = new File(profilesIniFullPath);
    
    return profilesIniFile;
  }

}
