package com.World2One.Configurator.MailClients.Win32;

import java.util.Vector;
import java.util.regex.Pattern;

import org.sf.feeling.swt.win32.extension.registry.RegistryException;
import org.sf.feeling.swt.win32.extension.registry.RegistryKey;
import org.sf.feeling.swt.win32.extension.registry.RootKey;

import com.World2One.Configurator.AbstractConfigurator;
import com.World2One.Configurator.MailClients.AbstractApplication;
import com.World2One.Configurator.MailClients.Win32.Registry.RegistryKeyValidator;


/**
 * <p>Title: OutlookExpress</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 */
public class OutlookExpress
  extends AbstractApplication
{
  
  protected static final String OUTLOOKEXPRESS_REGISTRY_KEY_ACCOUNT_NAME              = "Account Name";
  protected static final String OUTLOOKEXPRESS_REGISTRY_KEY_POP3_PASSWORD             = "POP3 Password2";
  protected static final String OUTLOOKEXPRESS_REGISTRY_KEY_POP3_USER                 = "POP3 User Name";
  protected static final String OUTLOOKEXPRESS_REGISTRY_KEY_SMTP_EMAIL_ADDRESS        = "SMTP Email Address";
  protected static final String OUTLOOKEXPRESS_REGISTRY_KEY_SMTP_SECURE_CONNECTION    = "SMTP Secure Connection";
  protected static final String OUTLOOKEXPRESS_REGISTRY_KEY_SMTP_SERVER               = "SMTP Server";
  protected static final String OUTLOOKEXPRESS_REGISTRY_KEY_SMTP_PASSWORD             = "SMTP Password2";
  protected static final String OUTLOOKEXPRESS_REGISTRY_KEY_SMTP_PORT                 = "SMTP Port";
  protected static final String OUTLOOKEXPRESS_REGISTRY_KEY_SMTP_USER                 = "SMTP User Name";
  
  protected static final String OUTLOOKEXPRESS_PATH                                   = "Software\\Microsoft\\Internet Account Manager\\Accounts\\";
  
  public static final String APPLICATION_NAME            = "Outlook Express";
  public static final String APPLICATION_NAME_REGEX      = "^Outlook Express$";
  public static final Pattern APPLICATION_NAME_PATTERN;
  static
  {
    APPLICATION_NAME_PATTERN = Pattern.compile(APPLICATION_NAME_REGEX);
    AbstractApplication.__applicationNamePattern2ClassRegistry.put(APPLICATION_NAME_PATTERN, OutlookExpress.class);
  }
 
  
  
  public OutlookExpress()
  {
    super();
  }
  
  
  @Override
  public String GetApplicationName()
  {
    return APPLICATION_NAME;
  }

  @Override
  protected void PopulateAccounts()
  {
    Vector<RegistryKey> registryKeysHavingSMTPUser = Registry.FindRegistryKeys(
                                                       RootKey.HKEY_CURRENT_USER, 
                                                       OUTLOOKEXPRESS_PATH, 
                                                       new OutlookExpressRegistryKeyValidator() );
    
    // Process Paths
    for (RegistryKey registryKeyHavingSMTPUser : registryKeysHavingSMTPUser)
    {
      try
      {
        OutlookExpressAccount outlookExpressAccount = new OutlookExpressAccount(this,registryKeyHavingSMTPUser);
        outlookExpressAccount.Initialize();
        _accounts.add(outlookExpressAccount);
      }
      catch (RegistryException ex)
      {
        //Not a valid account
        if (AbstractConfigurator.__debug == true)
        {
          ex.printStackTrace();
        }
        continue;
      }
    }
  }
  
  protected static class OutlookExpressRegistryKeyValidator
    extends RegistryKeyValidator
  {
    public OutlookExpressRegistryKeyValidator()
    {
      super();
    }

    @Override
    public boolean IsValid(RegistryKey registryKey)
    {
      try
      {
        if ( (registryKey.hasValue(OUTLOOKEXPRESS_REGISTRY_KEY_ACCOUNT_NAME) == false) ||
             (registryKey.hasValue(OUTLOOKEXPRESS_REGISTRY_KEY_SMTP_EMAIL_ADDRESS) == false) ||
             (registryKey.hasValue(OUTLOOKEXPRESS_REGISTRY_KEY_SMTP_SERVER) == false) ||
             ( (registryKey.hasValue(OUTLOOKEXPRESS_REGISTRY_KEY_POP3_USER) == false) &&
               (registryKey.hasValue(OUTLOOKEXPRESS_REGISTRY_KEY_SMTP_USER) == false) ) )
        {
          return false;
        }
       
       return true;
      }
      catch (RegistryException ex)
      {
        return false;
      }
    }
    
    
  }

}
