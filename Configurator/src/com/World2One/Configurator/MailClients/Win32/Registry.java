package com.World2One.Configurator.MailClients.Win32;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Vector;

import org.sf.feeling.swt.win32.extension.registry.RegistryException;
import org.sf.feeling.swt.win32.extension.registry.RegistryKey;
import org.sf.feeling.swt.win32.extension.registry.RootKey;

/**
 * <p>Title: SearchRegistry</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 */
public class Registry
{
  
  
  private Registry()
  {
    super();
  }

  public static Vector<RegistryKey> FindRegistryKeys(RootKey rootKey, String startPath, RegistryKeyValidator registryKeyValidator)
  {
    Vector<RegistryKey> validRegistryKeys = new Vector<RegistryKey>();
    
    FindRegistryKeys(rootKey, startPath, registryKeyValidator, validRegistryKeys);
    
    return validRegistryKeys;
  }

  protected static void FindRegistryKeys(
    RootKey               rootKey, 
    String                startPath, 
    RegistryKeyValidator  registryKeyValidator,
    Vector<RegistryKey>   validRegistryKeys )
  {
    RegistryKey registryKey = new RegistryKey(rootKey, startPath);

    if (registryKeyValidator.IsValid(registryKey) == true)
    {
      validRegistryKeys.add(registryKey);
    }

    FindSubKeys(registryKey,registryKeyValidator,validRegistryKeys);
  }

  protected static RegistryKey GetNextRegistryKey(Iterator<RegistryKey> registryKeys)
  {
    RegistryKey registryKey = null; 

    while (true)
    {
      try
      {
        boolean hasNext = registryKeys.hasNext();
        if (hasNext == true)
        {
          registryKey = registryKeys.next();
        }
        break;
      }
      catch (RegistryException ex1)
      {
        try
        {
          registryKeys.next();
        }
        catch (RegistryException ex2)
        {
          // do nothing
        }
        catch (NoSuchElementException ex2)
        {
          break;
        }
      }
    }

    return registryKey;
  }
  
  protected static void FindSubKeys(
    RegistryKey           registryKey,
    RegistryKeyValidator  registryKeyValidator,
    Vector<RegistryKey>   validRegistryKeys )
  {
    Iterator<RegistryKey> subkeys = registryKey.subkeys();
    while (true)
    {
      RegistryKey subKey = GetNextRegistryKey(subkeys);
      if (subKey == null)
      {
        break;
      }
      
      if (registryKeyValidator.IsValid(subKey) == true)
      {
        validRegistryKeys.add(subKey);
      }

      if (subKey.hasSubkeys() == true)
      {
        FindSubKeys(subKey, registryKeyValidator, validRegistryKeys);
      }
    }
  }
  
  
  public abstract static class RegistryKeyValidator
  {
    public RegistryKeyValidator()
    {
      super();
    }
    
    public abstract boolean IsValid(RegistryKey registryKey);
  }

}
