package com.World2One.Configurator.MailClients.Win32;

import org.sf.feeling.swt.win32.extension.registry.RegistryException;
import org.sf.feeling.swt.win32.extension.registry.RegistryKey;
import org.sf.feeling.swt.win32.extension.registry.RegistryValue;
import org.sf.feeling.swt.win32.extension.registry.ValueType;

import com.World2One.Configurator.MailClients.AbstractApplication;
import com.World2One.SMTP.SMTPHost.SecureConnection;
import com.World2One.Utility.RegistryValueToolBox;

/**
 * <p>Title: OutlookExpressAccount</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public class OutlookExpressAccount
  extends AbstractRegistryAccount
{
  
  
  public OutlookExpressAccount(
    AbstractApplication application,
    RegistryKey         registryKey )
  {
    super(application,registryKey);
  }

  @Override
  protected void GetSMTPSettings()
  {
    _emailAddress = RegistryValueToolBox.GetDataAsString(_registryKey.getValue(OutlookExpress.OUTLOOKEXPRESS_REGISTRY_KEY_SMTP_EMAIL_ADDRESS));
    _accountName = RegistryValueToolBox.GetDataAsString(_registryKey.getValue(OutlookExpress.OUTLOOKEXPRESS_REGISTRY_KEY_ACCOUNT_NAME));
    _hostAddress = RegistryValueToolBox.GetDataAsString(_registryKey.getValue(OutlookExpress.OUTLOOKEXPRESS_REGISTRY_KEY_SMTP_SERVER));
    
    String outlookExpressRegistryKeyUser = (_registryKey.hasSubkey(OutlookExpress.OUTLOOKEXPRESS_REGISTRY_KEY_SMTP_USER) == true) ? OutlookExpress.OUTLOOKEXPRESS_REGISTRY_KEY_SMTP_USER : OutlookExpress.OUTLOOKEXPRESS_REGISTRY_KEY_POP3_USER;
    _userName = RegistryValueToolBox.GetDataAsString(_registryKey.getValue(outlookExpressRegistryKeyUser));

    _password = null;
    
    _connectionType = null;
    try
    {
      RegistryValue smtpSecureConnection = _registryKey.getValue(OutlookExpress.OUTLOOKEXPRESS_REGISTRY_KEY_SMTP_SECURE_CONNECTION);  // throws RegistryException
      _connectionType = SecureConnection.values()[RegistryValueToolBox.GetDataAsInteger(smtpSecureConnection)];
    }
    catch (RegistryException ex) 
    {
      // value OutlookExpress.OUTLOOKEXPRESS_REGISTRY_KEY_SMTP_SECURE_CONNECTION not found. do nothing.
    }
    if (_connectionType == null)
    {
      _connectionType = SecureConnection.None;
    }

    _port = -1;
    try
    {
      RegistryValue smtpPort = _registryKey.getValue(OutlookExpress.OUTLOOKEXPRESS_REGISTRY_KEY_SMTP_PORT);  // throws RegistryException
      _port = RegistryValueToolBox.GetDataAsInteger(smtpPort);
    }
    catch (RegistryException ex) 
    {
      // value OutlookExpress.OUTLOOKEXPRESS_REGISTRY_KEY_SMTP_PORT not found. do nothing. 
    }
    if (_port == -1)
    {
      _port = _connectionType.GetDefaultPort();
    }
  }


  @Override
  public void SetSMTPSettingsHelper(
    String           accountName, 
    String           emailAddress, 
    String           hostAddress, 
    int              port, 
    String           userName, 
    String           password, 
    SecureConnection connectionType )
  {
    RegistryValue registryValueEmailAddress = new RegistryValue(OutlookExpress.OUTLOOKEXPRESS_REGISTRY_KEY_SMTP_EMAIL_ADDRESS,emailAddress);
    RegistryValue registryValueSMTPServer = new RegistryValue(OutlookExpress.OUTLOOKEXPRESS_REGISTRY_KEY_SMTP_SERVER,hostAddress);
    RegistryValue registryValueSMTPPort = new RegistryValue(OutlookExpress.OUTLOOKEXPRESS_REGISTRY_KEY_SMTP_PORT,port);
    RegistryValue registryValueSMTPUser = new RegistryValue(OutlookExpress.OUTLOOKEXPRESS_REGISTRY_KEY_SMTP_USER,userName);
    RegistryValue registryValueSMTPPassword;
    if (password == null)
    {
      registryValueSMTPPassword = new RegistryValue(OutlookExpress.OUTLOOKEXPRESS_REGISTRY_KEY_SMTP_PASSWORD,ValueType.REG_SZ,"");
    }
    else
    {
      registryValueSMTPPassword = new RegistryValue(OutlookExpress.OUTLOOKEXPRESS_REGISTRY_KEY_SMTP_PASSWORD,ValueType.REG_SZ,password);
    }
    RegistryValue registryValuesecureConnection = new RegistryValue(OutlookExpress.OUTLOOKEXPRESS_REGISTRY_KEY_SMTP_SECURE_CONNECTION,connectionType.ordinal());
    
    _registryKey.setValue(registryValueEmailAddress);
    _registryKey.setValue(registryValueSMTPServer);
    _registryKey.setValue(registryValuesecureConnection);
    _registryKey.setValue(registryValueSMTPUser);
    _registryKey.setValue(registryValueSMTPPassword);
    _registryKey.setValue(registryValueSMTPPort);
  }

}
