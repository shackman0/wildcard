package com.World2One.Configurator.MailClients.Win32;

import java.util.regex.Pattern;

import com.World2One.Configurator.MailClients.AbstractApplication;

/**
 * <p>Title: Outlook2007</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public class Outlook2007
  extends AbstractOutlook
{
  

  /*
   * because Outlook 2003 and Outlook 2007 use the same account registry path, we can only have one instance of Outlook in
   * ApplicationFactory.GetInstalledApplications(). therefore, this APPLICATION_NAME is the same value as Outlook 2003's APPLICATION_NAME.
   */
  public static final String APPLICATION_NAME            = "Microsoft Outlook";

  protected static final String OUTLOOK_REGISTRY_KEY_EMAIL                     = "Email";
  protected static final String OUTLOOK_REGISTRY_KEY_ACCOUNT_NAME              = "Account Name";
  protected static final String OUTLOOK_REGISTRY_KEY_SMTP_SERVER               = "SMTP Server";
  protected static final String OUTLOOK_REGISTRY_KEY_SMTP_SECURE_CONNECTION    = "SMTP Secure Connection";
  protected static final String OUTLOOK_REGISTRY_KEY_SMTP_USER                 = "SMTP User";
  protected static final String OUTLOOK_REGISTRY_KEY_SMTP_USE_AUTH             = "SMTP Use Auth";
  protected static final String OUTLOOK_REGISTRY_KEY_SMTP_PASSWORD             = "SMTP Password";
  protected static final String OUTLOOK_REGISTRY_KEY_SMTP_PORT                 = "SMTP Port";
  
  protected static final String OUTLOOK_REGISTRY_KEY_POP3_USER                 = "POP3 User";
  protected static final String OUTLOOK_REGISTRY_KEY_POP3_PASSWORD             = "POP3 Password";
  
  protected static final String OUTLOOK_REGISTRY_TOP_PATH                      = "Software\\Microsoft\\Windows NT\\CurrentVersion\\Windows Messaging Subsystem\\Profiles";
  
  public static final String APPLICATION_NAME_REGEX      = "^.* Office Outlook .* 2007$";
  public static final Pattern APPLICATION_NAME_PATTERN;
  static
  {
    APPLICATION_NAME_PATTERN = Pattern.compile(APPLICATION_NAME_REGEX);
    AbstractApplication.__applicationNamePattern2ClassRegistry.put(APPLICATION_NAME_PATTERN, Outlook2007.class);
  }

  
  public Outlook2007()
  {
    super();
  }
  
  @Override
  public String GetApplicationName()
  {
    return APPLICATION_NAME;
  }

  @Override
  protected Pattern GetApplicationNamePattern()
  {
    return APPLICATION_NAME_PATTERN;
  }

  @Override
  protected String GetApplicationNameRegex()
  {
    return APPLICATION_NAME_REGEX;
  }

  @Override
  protected String GetOutlookRegistryKeyAccountName()
  {
    return OUTLOOK_REGISTRY_KEY_ACCOUNT_NAME;
  }

  @Override
  protected String GetOutlookRegistryKeySMTPEmailAddress()
  {
    return OUTLOOK_REGISTRY_KEY_EMAIL;
  }

  @Override
  protected String GetOutlookRegistryKeyPop3Password()
  {
    return OUTLOOK_REGISTRY_KEY_POP3_PASSWORD;
  }

  @Override
  protected String GetOutlookRegistryKeyPop3User()
  {
    return OUTLOOK_REGISTRY_KEY_POP3_USER;
  }

  @Override
  protected String GetOutlookRegistryKeySMTPPassword()
  {
    return OUTLOOK_REGISTRY_KEY_SMTP_PASSWORD;
  }

  @Override
  protected String GetOutlookRegistryKeySMTPPort()
  {
    return OUTLOOK_REGISTRY_KEY_SMTP_PORT;
  }

  @Override
  protected String GetOutlookRegistryKeySMTPSecureConnection()
  {
    return OUTLOOK_REGISTRY_KEY_SMTP_SECURE_CONNECTION;
  }

  @Override
  protected String GetOutlookRegistryKeySMTPServer()
  {
    return OUTLOOK_REGISTRY_KEY_SMTP_SERVER;
  }

  @Override
  protected String GetOutlookRegistryKeySMTPUser()
  {
    return OUTLOOK_REGISTRY_KEY_SMTP_USER;
  }

  @Override
  protected String GetOutlookRegistryKeySMTPUseAuth()
  {
    return OUTLOOK_REGISTRY_KEY_SMTP_USE_AUTH;
  }

  @Override
  protected String GetOutlookRegistryTopPath()
  {
    return OUTLOOK_REGISTRY_TOP_PATH;
  }
  

}
