package com.World2One.Configurator.MailClients.Win32;

import java.io.File;
import java.io.FilenameFilter;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Set;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;

import com.World2One.Configurator.AbstractConfigurator;
import com.World2One.Configurator.MailClients.AbstractApplication;
import com.World2One.SMTP.SMTPHost.SecureConnection;

/**
 * <p>Title: WindowsMailActions</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public class WindowsMail
  extends AbstractApplication
{

  public static final String  APPLICATION_NAME            = "Windows Mail";
  public static final String  APPLICATION_NAME_REGEX      = "^Windows Mail$";
  public static final Pattern APPLICATION_NAME_PATTERN;
  static
  {
    APPLICATION_NAME_PATTERN = Pattern.compile(APPLICATION_NAME_REGEX);
    AbstractApplication.__applicationNamePattern2ClassRegistry.put(APPLICATION_NAME_PATTERN, WindowsMail.class);
  }

  /**
   * 0 = user home
   */
  protected static final String WINDOWS_MAIL_ACCOUNT_DIRECTORY = "{0}/AppData/Local/Microsoft/Windows Mail/Local Folders/";

  protected static final String DOT_WINDOWS_MAIL_ACCOUNT = ".oeaccount";
  
  protected static final String WINDOWS_MAIL_ACCOUNT_ELEMENT_POP3_USER_NAME           = "POP3_User_Name";
  protected static final String WINDOWS_MAIL_ACCOUNT_ELEMENT_SMTP_USER_NAME           = "SMTP_User_Name";
  protected static final String WINDOWS_MAIL_ACCOUNT_ELEMENT_SMTP_SERVER              = "SMTP_Server";
  protected static final String WINDOWS_MAIL_ACCOUNT_ELEMENT_SMTP_EMAIL_ADDRESS       = "SMTP_Email_Address";
  protected static final String WINDOWS_MAIL_ACCOUNT_ELEMENT_SMTP_DISPLAY_NAME        = "SMTP_Display_Name";
  protected static final String WINDOWS_MAIL_ACCOUNT_ELEMENT_SMTP_PORT                = "SMTP_Port";
  protected static final String WINDOWS_MAIL_ACCOUNT_ELEMENT_SMTP_SECURE_CONNECTION   = "SMTP_Secure_Connection";

  protected static final int WINDOWS_MAIL_ACCOUNT_DEFAULT_SMTP_PORT                   = 25;
  protected static final SecureConnection WINDOWS_MAIL_ACCOUNT_DEFAULT_SMTP_SECURE_CONNECTION      = SecureConnection.None;
  
  
  public WindowsMail()
  {
    super();
  }
  

  @Override
  public String GetApplicationName()
  {
    return APPLICATION_NAME;
  }
  
  @Override
  protected void PopulateAccounts()
  {
    Document[] windowsMailAccountDocuments = GetWindowsMailAccountDocuments();
    for (Document windowsMailAccountDocument: windowsMailAccountDocuments)
    {
      WindowsMailAccount windowsMailAccount = new WindowsMailAccount(this,windowsMailAccountDocument);
      windowsMailAccount.Initialize();
      _accounts.add(windowsMailAccount);
    }
  }


  protected HashMap<Document,File> _windowsMailAccountDocuments2FilesMap = null;

  protected HashMap<Document,File> GetWindowsMailAccountDocuments2FilesMap()
  {
    if (_windowsMailAccountDocuments2FilesMap == null)
    {
      _windowsMailAccountDocuments2FilesMap = new HashMap();
      
      File[] windowsMailAccountFiles = GetWindowsMailAccountFiles();
      if (windowsMailAccountFiles != null)
      {
        for (File windowsMailAccountFile: windowsMailAccountFiles)
        {
          Document windowsMailAccountDocument = GetWindowsMailAccountDocument(windowsMailAccountFile);
          _windowsMailAccountDocuments2FilesMap.put(windowsMailAccountDocument, windowsMailAccountFile);
        }
      }
    }
    
    return _windowsMailAccountDocuments2FilesMap;
  }
  
  protected File[] GetWindowsMailAccountFiles()
  {
    String userHome = System.getProperty("user.home");
    String windowsMailAccountDirectoryString = MessageFormat.format(WINDOWS_MAIL_ACCOUNT_DIRECTORY,userHome);
    File windowsMailAccountDirectory = new File(windowsMailAccountDirectoryString);
    File[] windowsMailAccountFiles = null;
    if (windowsMailAccountDirectory.exists() == true)
    {
      FilenameFilter filter = new FilenameFilter() 
      {
        @Override
        public boolean accept(File dir, String name)
        {
          boolean accept = name.toLowerCase().endsWith(DOT_WINDOWS_MAIL_ACCOUNT);
          return accept;
        } 
      };
      windowsMailAccountFiles = windowsMailAccountDirectory.listFiles(filter);
    }
    
    return windowsMailAccountFiles;
  }
  
  protected Document GetWindowsMailAccountDocument(File windowsMailAccountFile)
  {
    Document windowsMailAccountDocument = null;
    
    try
    {
      DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
      windowsMailAccountDocument = docBuilder.parse(windowsMailAccountFile);
      windowsMailAccountDocument.getDocumentElement().normalize();
    }
    catch (Exception ex)
    {
      if (AbstractConfigurator.__debug == true)
      {
        ex.printStackTrace();
      }
      JOptionPane.showMessageDialog(null,ex.getLocalizedMessage(), "Error With Windows Mail Account Document",JOptionPane.ERROR_MESSAGE);
    }
    
    return windowsMailAccountDocument;
  }
  
  public Document[] GetWindowsMailAccountDocuments()
  {
    Set<Document> documentSet = GetWindowsMailAccountDocuments2FilesMap().keySet();
    Document[] windowsMailAccountDocuments = new Document[documentSet.size()];
    documentSet.toArray(windowsMailAccountDocuments);
    
    return windowsMailAccountDocuments;
  }
  
}
