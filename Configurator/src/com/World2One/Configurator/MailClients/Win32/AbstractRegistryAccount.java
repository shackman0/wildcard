package com.World2One.Configurator.MailClients.Win32;

import java.text.MessageFormat;

import javax.swing.JOptionPane;

import org.sf.feeling.swt.win32.extension.registry.RegistryException;
import org.sf.feeling.swt.win32.extension.registry.RegistryKey;

import com.World2One.Configurator.MailClients.AbstractAccount;
import com.World2One.Configurator.MailClients.AbstractApplication;
import com.World2One.SMTP.SMTPHost.SecureConnection;
import com.World2One.Utility.Constants;

/**
 * <p>Title: AbstractRegistryApplication</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public abstract class AbstractRegistryAccount
  extends AbstractAccount
{

  protected static final String REGISTRY_EXCEPTION_MSG = 
    "The following Registry Exception has occurred:"  + Constants.CR_LF +
    "    \"{0}\"" + Constants.CR_LF +
    "for registry entry having key:"  + Constants.CR_LF +
    "    \"{1} {2}\"" + Constants.CR_LF +
    "The registry entry remains unmodified." + Constants.CR_LF +
    "Please ensure that you have appropriate authorization to modify the registry.";
  
  protected RegistryKey _registryKey;

  
  protected AbstractRegistryAccount(
    AbstractApplication application,
    RegistryKey         registryKey )
  {
    super(application);

    _registryKey = registryKey;
  }

  @Override
  public void SetSMTPSettings(
    String           accountName, 
    String           emailAddress, 
    String           hostAddress, 
    int              port, 
    String           userName, 
    String           password, 
    SecureConnection connectionType )
  {
    try
    {
      SetSMTPSettingsHelper(accountName,emailAddress,hostAddress,port,userName,password,connectionType);
    }
    catch (RegistryException ex)
    {
      String registryExceptionMessage = MessageFormat.format(REGISTRY_EXCEPTION_MSG, ex.getLocalizedMessage(), _registryKey.getRootKey(), _registryKey.getPath());
      JOptionPane.showMessageDialog(null,registryExceptionMessage,"Registry Exception",JOptionPane.INFORMATION_MESSAGE);
    }
  }

  protected abstract void SetSMTPSettingsHelper(
    String           accountName, 
    String           emailAddress, 
    String           hostAddress, 
    int              port, 
    String           userName, 
    String           password, 
    SecureConnection connectionType )
    throws RegistryException;
    
}
