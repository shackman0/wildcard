package com.World2One.Configurator.MailClients.Win32;

import java.util.Vector;
import java.util.regex.Pattern;

import org.sf.feeling.swt.win32.extension.registry.RegistryException;
import org.sf.feeling.swt.win32.extension.registry.RegistryKey;
import org.sf.feeling.swt.win32.extension.registry.RootKey;

import com.World2One.Configurator.AbstractConfigurator;
import com.World2One.Configurator.MailClients.AbstractApplication;
import com.World2One.Configurator.MailClients.Win32.Registry.RegistryKeyValidator;




/**
 * <p>Title: Outlook</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 */
public abstract class AbstractOutlook
  extends AbstractApplication
{

  
  public AbstractOutlook()
  {
    super();
  }
  
  
  @Override
  protected void PopulateAccounts()
  {
    Vector<RegistryKey> validRegistryKeys = Registry.FindRegistryKeys(
                                              RootKey.HKEY_CURRENT_USER, 
                                              GetOutlookRegistryTopPath(), 
                                              new OutlookRegistryKeyValidator() );
    
    // Process Paths
    for (RegistryKey validRegistryKey : validRegistryKeys)
    {
      try
      {
        OutlookAccount outlookAccount = new OutlookAccount(this,validRegistryKey);
        outlookAccount.Initialize();
        _accounts.add(outlookAccount);
      }
      catch (RegistryException ex)
      {
        //Not a valid account
        if (AbstractConfigurator.__debug == true)
        {
          ex.printStackTrace();
        }
        continue;
      }
    }
  }

  protected abstract Pattern GetApplicationNamePattern();

  protected abstract String GetApplicationNameRegex();
  
  protected abstract String GetOutlookRegistryKeyAccountName();

  protected abstract String GetOutlookRegistryKeySMTPEmailAddress();

  protected abstract String GetOutlookRegistryKeyPop3Password();

  protected abstract String GetOutlookRegistryKeyPop3User();

  protected abstract String GetOutlookRegistryKeySMTPPassword();

  protected abstract String GetOutlookRegistryKeySMTPPort();

  protected abstract String GetOutlookRegistryKeySMTPSecureConnection();

  protected abstract String GetOutlookRegistryKeySMTPServer();

  protected abstract String GetOutlookRegistryKeySMTPUser();

  protected abstract String GetOutlookRegistryKeySMTPUseAuth();

  protected abstract String GetOutlookRegistryTopPath();
  
  
  protected class OutlookRegistryKeyValidator
    extends RegistryKeyValidator
  {
    public OutlookRegistryKeyValidator()
    {
      super();
    }

    @Override
    public boolean IsValid(RegistryKey registryKey)
    {
      try
      {
        if ( (registryKey.hasValue(GetOutlookRegistryKeyAccountName()) == false) ||
             (registryKey.hasValue(GetOutlookRegistryKeySMTPEmailAddress()) == false) ||
             (registryKey.hasValue(GetOutlookRegistryKeySMTPServer()) == false) )
        {
          return false;
        }
         
        if ( (registryKey.hasValue(GetOutlookRegistryKeyPop3User()) == false) &&
             (registryKey.hasValue(GetOutlookRegistryKeySMTPUser()) == false) )
        {
          return false;
        }
         
        if ( (registryKey.hasValue(GetOutlookRegistryKeyPop3Password()) == false) &&
             (registryKey.hasValue(GetOutlookRegistryKeySMTPPassword()) == false) )
        {
          return false;
        }
         
        return true;
      }
      catch (RegistryException ex)
      {
        return false;
      }
    }
    
    
  }
  
}
