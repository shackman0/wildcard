package com.World2One.Configurator.MailClients.Win32;

import java.util.regex.Pattern;

import com.World2One.Configurator.MailClients.AbstractApplication;

/**
 * <p>Title: Outlook2000</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public class Outlook2000
  extends AbstractOutlook
{

  public static final String APPLICATION_NAME            = "Microsoft Outlook 2000";

  protected static final String OUTLOOK_REGISTRY_KEY_ACCOUNT_NAME              = "Account Name";
  protected static final String OUTLOOK_REGISTRY_KEY_SMTP_EMAIL_ADDRESS        = "SMTP Email Address";
  protected static final String OUTLOOK_REGISTRY_KEY_POP3_PASSWORD2            = "POP3 Password2";
  protected static final String OUTLOOK_REGISTRY_KEY_POP3_USER_NAME            = "POP3 User Name";
  protected static final String OUTLOOK_REGISTRY_KEY_SMTP_PASSWORD2            = "SMTP Password2";
  protected static final String OUTLOOK_REGISTRY_KEY_SMTP_PORT                 = "SMTP Port";
  protected static final String OUTLOOK_REGISTRY_KEY_SMTP_SECURE_CONNECTION    = "SMTP Secure Connection";
  protected static final String OUTLOOK_REGISTRY_KEY_SMTP_SERVER               = "SMTP Server";
  protected static final String OUTLOOK_REGISTRY_KEY_SMTP_USER_NAME            = "SMTP User Name";
  protected static final String OUTLOOK_REGISTRY_KEY_SMTP_USE_AUTH             = "SMTP Use Auth";
  
  protected static final String OUTLOOK_REGISTRY_TOP_PATH                      = "Software\\Microsoft\\Office\\Outlook\\OMI Account Manager\\Accounts";
  
  public static final String APPLICATION_NAME_REGEX      = "^.* Office 2000 .*$";
  public static final Pattern APPLICATION_NAME_PATTERN;
  static
  {
    APPLICATION_NAME_PATTERN = Pattern.compile(APPLICATION_NAME_REGEX);
    AbstractApplication.__applicationNamePattern2ClassRegistry.put(APPLICATION_NAME_PATTERN, Outlook2000.class);
  }

  
  public Outlook2000()
  {
    super();
  }
  
  @Override
  public String GetApplicationName()
  {
    return APPLICATION_NAME;
  }

  @Override
  protected Pattern GetApplicationNamePattern()
  {
    return APPLICATION_NAME_PATTERN;
  }

  @Override
  protected String GetApplicationNameRegex()
  {
    return APPLICATION_NAME_REGEX;
  }

  @Override
  protected String GetOutlookRegistryKeyAccountName()
  {
    return OUTLOOK_REGISTRY_KEY_ACCOUNT_NAME;
  }

  @Override
  protected String GetOutlookRegistryKeySMTPEmailAddress()
  {
    return OUTLOOK_REGISTRY_KEY_SMTP_EMAIL_ADDRESS;
  }

  @Override
  protected String GetOutlookRegistryKeyPop3Password()
  {
    return OUTLOOK_REGISTRY_KEY_POP3_PASSWORD2;
  }

  @Override
  protected String GetOutlookRegistryKeyPop3User()
  {
    return OUTLOOK_REGISTRY_KEY_POP3_USER_NAME;
  }

  @Override
  protected String GetOutlookRegistryKeySMTPPassword()
  {
    return OUTLOOK_REGISTRY_KEY_SMTP_PASSWORD2;
  }

  @Override
  protected String GetOutlookRegistryKeySMTPPort()
  {
    return OUTLOOK_REGISTRY_KEY_SMTP_PORT;
  }

  @Override
  protected String GetOutlookRegistryKeySMTPSecureConnection()
  {
    return OUTLOOK_REGISTRY_KEY_SMTP_SECURE_CONNECTION;
  }

  @Override
  protected String GetOutlookRegistryKeySMTPServer()
  {
    return OUTLOOK_REGISTRY_KEY_SMTP_SERVER;
  }

  @Override
  protected String GetOutlookRegistryKeySMTPUser()
  {
    return OUTLOOK_REGISTRY_KEY_SMTP_USER_NAME;
  }

  @Override
  protected String GetOutlookRegistryKeySMTPUseAuth()
  {
    return OUTLOOK_REGISTRY_KEY_SMTP_USE_AUTH;
  }

  @Override
  protected String GetOutlookRegistryTopPath()
  {
    return OUTLOOK_REGISTRY_TOP_PATH;
  }
  

}
