package com.World2One.Configurator.MailClients.Win32;

import org.sf.feeling.swt.win32.extension.registry.RegistryException;
import org.sf.feeling.swt.win32.extension.registry.RegistryKey;
import org.sf.feeling.swt.win32.extension.registry.RegistryValue;
import org.sf.feeling.swt.win32.extension.registry.ValueType;

import com.World2One.Configurator.MailClients.AbstractApplication;
import com.World2One.SMTP.SMTPHost.SecureConnection;
import com.World2One.Utility.RegistryValueToolBox;

/**
 * <p>Title: OutlookAccount</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public class OutlookAccount
  extends AbstractRegistryAccount
{
  
  
  public OutlookAccount(
    AbstractApplication application,
    RegistryKey         registryKey )
  {
    super(application,registryKey);
  }

  @Override
  protected void GetSMTPSettings()
  {
    AbstractOutlook application = (AbstractOutlook)_application;
    
    _emailAddress = RegistryValueToolBox.GetDataAsString(_registryKey.getValue(application.GetOutlookRegistryKeySMTPEmailAddress()));
    
    _accountName = RegistryValueToolBox.GetDataAsString(_registryKey.getValue(application.GetOutlookRegistryKeyAccountName()));
    
    _hostAddress = RegistryValueToolBox.GetDataAsString(_registryKey.getValue(application.GetOutlookRegistryKeySMTPServer()));
    
    String outlookRegistryKeyUser = (_registryKey.hasSubkey(application.GetOutlookRegistryKeySMTPUser()) == true) ? application.GetOutlookRegistryKeySMTPUser() : application.GetOutlookRegistryKeyPop3User();
    _userName = RegistryValueToolBox.GetDataAsString(_registryKey.getValue(outlookRegistryKeyUser));
    
    _password = null;
    
    _connectionType = null;
    try
    {
      RegistryValue smtpSecureConnection = _registryKey.getValue(application.GetOutlookRegistryKeySMTPSecureConnection());  // throws RegistryException
      _connectionType = SecureConnection.values()[RegistryValueToolBox.GetDataAsInteger(smtpSecureConnection)];
    }
    catch (RegistryException ex) 
    {
      // value for application.GetOutlookRegistryKeySMTPSecureConnection() not found. do nothing.
    }
    if (_connectionType == null)
    {
      _connectionType = SecureConnection.None;
    }

    _port = -1;
    try
    {
      RegistryValue smtpPort = _registryKey.getValue(application.GetOutlookRegistryKeySMTPPort());  // throws RegistryException
      _port = RegistryValueToolBox.GetDataAsInteger(smtpPort);
    }
    catch (RegistryException ex) 
    {
      // value for application.GetOutlookRegistryKeySMTPPort() not found. do nothing. 
    }
    if (_port == -1)
    {
      _port = _connectionType.GetDefaultPort();
    }
  }

  @Override
  public void SetSMTPSettingsHelper(
    String            accountName,
    String            emailAddress,
    String            hostAddress,
    int               port,
    String            userName,
    String            password,
    SecureConnection  connectionType )
  {
    AbstractOutlook application = (AbstractOutlook)_application;
    
    RegistryValue registryValueEmailAddress = new RegistryValue(application.GetOutlookRegistryKeySMTPEmailAddress(),ValueType.REG_SZ,emailAddress);
    RegistryValue registryValueSMTPServer = new RegistryValue(application.GetOutlookRegistryKeySMTPServer(),ValueType.REG_SZ,hostAddress);
    RegistryValue registryValueSMTPPort = new RegistryValue(application.GetOutlookRegistryKeySMTPPort(),ValueType.REG_DWORD,port);
    RegistryValue registryValueSMTPUser = new RegistryValue(application.GetOutlookRegistryKeySMTPUser(),ValueType.REG_SZ,userName);
    RegistryValue registryValueSMTPPassword;
    if (password == null)
    {
      registryValueSMTPPassword = new RegistryValue(application.GetOutlookRegistryKeySMTPPassword(),ValueType.REG_SZ,"");
    }
    else
    {
      registryValueSMTPPassword = new RegistryValue(application.GetOutlookRegistryKeySMTPPassword(),ValueType.REG_SZ,password);
    }
    RegistryValue secureConnection = new RegistryValue(application.GetOutlookRegistryKeySMTPSecureConnection(),ValueType.REG_DWORD,connectionType.ordinal());
    
    _registryKey.setValue(registryValueEmailAddress);
    _registryKey.setValue(registryValueSMTPServer);
    _registryKey.setValue(registryValueSMTPPort);
    _registryKey.setValue(registryValueSMTPUser);
    _registryKey.setValue(registryValueSMTPPassword);
    _registryKey.setValue(secureConnection);
    String outlookRegistryKeySMTPUseAuth = application.GetOutlookRegistryKeySMTPUseAuth();
    if (connectionType.UseAuth() == true)
    {
      RegistryValue registryValueUseAuth = new RegistryValue(outlookRegistryKeySMTPUseAuth,ValueType.REG_DWORD,1);
      _registryKey.setValue(registryValueUseAuth);
    }
    else
    if (_registryKey.hasValue(outlookRegistryKeySMTPUseAuth) == true)
    {
      _registryKey.deleteValue(outlookRegistryKeySMTPUseAuth);
    }
  }

}
