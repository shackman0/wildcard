package com.World2One.Configurator.MailClients.Win32;

import java.io.File;
import java.util.HashMap;

import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.World2One.Configurator.MailClients.AbstractAccount;
import com.World2One.OS.AbstractProcess;
import com.World2One.OS.AbstractProcessManager;
import com.World2One.SMTP.SMTPHost.SecureConnection;
import com.World2One.Utility.ToolBox;
import com.World2One.Utility.ToolBox.TrimPosition;

/**
 * <p>Title: WindowsMailAccount</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public class WindowsMailAccount
  extends AbstractAccount
{

  
  protected Document _windowsMailAccountDocument;

  
  public WindowsMailAccount(
    WindowsMail application,
    Document    windowsMailAccountDocument )
  {
    super(application);
    
    _windowsMailAccountDocument = windowsMailAccountDocument;
  }
  

  @Override
  protected void GetSMTPSettings()
  {
    Element documentElement = _windowsMailAccountDocument.getDocumentElement();
    HashMap<String,Element> elementsByTagName = ToolBox.GetElementsByTagName(documentElement);
    
    Element windowsMailAccountUserNameElement = elementsByTagName.get(WindowsMail.WINDOWS_MAIL_ACCOUNT_ELEMENT_SMTP_USER_NAME);
    if (windowsMailAccountUserNameElement == null)
    {
      windowsMailAccountUserNameElement = elementsByTagName.get(WindowsMail.WINDOWS_MAIL_ACCOUNT_ELEMENT_POP3_USER_NAME);
    }
    String windowsMailAccountUserName = windowsMailAccountUserNameElement.getTextContent();
    _userName = windowsMailAccountUserName;
    
    Element windowsMailAccountSMTPServerElement = elementsByTagName.get(WindowsMail.WINDOWS_MAIL_ACCOUNT_ELEMENT_SMTP_SERVER);
    String windowsMailAccountSMTPServer = windowsMailAccountSMTPServerElement.getTextContent();
    _hostAddress = windowsMailAccountSMTPServer;
    
    Element windowsMailAccountSMTPEmailAddressElement = elementsByTagName.get(WindowsMail.WINDOWS_MAIL_ACCOUNT_ELEMENT_SMTP_EMAIL_ADDRESS);
    String windowsMailAccountSMTPEmailAddress = windowsMailAccountSMTPEmailAddressElement.getTextContent();
    _emailAddress = windowsMailAccountSMTPEmailAddress;
    
    Element windowsMailAccountSMTPDisplayNameElement = elementsByTagName.get(WindowsMail.WINDOWS_MAIL_ACCOUNT_ELEMENT_SMTP_DISPLAY_NAME);
    String windowsMailAccountSMTPDisplayName = windowsMailAccountSMTPDisplayNameElement.getTextContent();
    _accountName = windowsMailAccountSMTPDisplayName;
    
    Element windowsMailAccountSMTPPortElement = elementsByTagName.get(WindowsMail.WINDOWS_MAIL_ACCOUNT_ELEMENT_SMTP_PORT);
    if (windowsMailAccountSMTPPortElement == null)
    {
      _port = WindowsMail.WINDOWS_MAIL_ACCOUNT_DEFAULT_SMTP_PORT;
    }
    else
    {
      String smtpPort = windowsMailAccountSMTPPortElement.getTextContent().trim();
      smtpPort = ToolBox.Trim("0", smtpPort, TrimPosition.Leading);
      _port = Integer.parseInt(smtpPort,16);
    }
    
    Element windowsMailAccountSMTPSecureConnectionElement = elementsByTagName.get(WindowsMail.WINDOWS_MAIL_ACCOUNT_ELEMENT_SMTP_SECURE_CONNECTION);
    if (windowsMailAccountSMTPSecureConnectionElement == null)
    {
      _connectionType = WindowsMail.WINDOWS_MAIL_ACCOUNT_DEFAULT_SMTP_SECURE_CONNECTION;
    }
    else
    {
      String windowsMailAccountSMTPSecureConnection = windowsMailAccountSMTPSecureConnectionElement.getTextContent().trim();
      SecureConnection connectionType = SecureConnection.values()[Integer.valueOf(windowsMailAccountSMTPSecureConnection)];
      _connectionType = connectionType;
    }
  }

  @Override
  public void SetSMTPSettings(
    String           accountName, 
    String           emailAddress, 
    String           hostAddress, 
    int              port, 
    String           userName, 
    String           password, 
    SecureConnection connectionType )
  {
    HashMap<String,Element> elementsByTagName = ToolBox.GetElementsByTagName(_windowsMailAccountDocument.getDocumentElement());

    Element windowsMailAccountUserNameElement = elementsByTagName.get(WindowsMail.WINDOWS_MAIL_ACCOUNT_ELEMENT_SMTP_USER_NAME);
    if (windowsMailAccountUserNameElement == null)
    {
      Element messageAccount = (Element) _windowsMailAccountDocument.getElementsByTagName("MessageAccount").item(0);
      windowsMailAccountUserNameElement = _windowsMailAccountDocument.createElement(WindowsMail.WINDOWS_MAIL_ACCOUNT_ELEMENT_SMTP_USER_NAME);
      windowsMailAccountUserNameElement.setAttribute("type", "SZ");
      messageAccount.appendChild(windowsMailAccountUserNameElement);
      elementsByTagName.put(WindowsMail.WINDOWS_MAIL_ACCOUNT_ELEMENT_SMTP_USER_NAME, windowsMailAccountUserNameElement);
    }
    windowsMailAccountUserNameElement.setTextContent(userName);
    
    Element windowsMailAccountSMTPServerElement = elementsByTagName.get(WindowsMail.WINDOWS_MAIL_ACCOUNT_ELEMENT_SMTP_SERVER);
    windowsMailAccountSMTPServerElement.setTextContent(hostAddress);
    
    Element windowsMailAccountSMTPPortElement = elementsByTagName.get(WindowsMail.WINDOWS_MAIL_ACCOUNT_ELEMENT_SMTP_PORT);
    if (windowsMailAccountSMTPPortElement != null)
    {
      String portDWord = ToolBox.Prepend("0", 8, Integer.toHexString(port));
      windowsMailAccountSMTPPortElement.setTextContent(portDWord);
    }
    
    Element windowsMailAccountSMTPSecureConnectionElement = elementsByTagName.get(WindowsMail.WINDOWS_MAIL_ACCOUNT_ELEMENT_SMTP_SECURE_CONNECTION);
    if (windowsMailAccountSMTPSecureConnectionElement != null)
    {
      String secureConnectionDWord = ToolBox.Prepend("0", 8, String.valueOf(connectionType.ordinal()));
      windowsMailAccountSMTPSecureConnectionElement.setTextContent(secureConnectionDWord);
    }
   
    File oeAccountFile = ((WindowsMail)_application).GetWindowsMailAccountDocuments2FilesMap().get(_windowsMailAccountDocument);
    StreamResult oeAccountStreamResult = new StreamResult(oeAccountFile);
    try
    {
      ToolBox.WriteXML(_windowsMailAccountDocument, oeAccountStreamResult);
    }
    catch (TransformerException ex)
    {
      ex.printStackTrace();
    }
    
    for (AbstractProcess process: AbstractProcessManager.GetSingleton().FindProcesses("winmail.exe"))
    {
      process.Kill();
    }
  }
  
}
