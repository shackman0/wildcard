package com.World2One.Configurator.MailClients;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;

import com.World2One.Utility.MutableObject;
import com.World2One.Utility.MutableString;

/**
 * <p>Title: Thunderbird</p>
 * <p>Description: Mozilla Thunderbird Email Application</p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public abstract class AbstractThunderbird
  extends AbstractApplication
{

  public static final String APPLICATION_NAME            = "Mozilla Thunderbird";

  protected static final String PROFILES_INI_PATH_KEY   = "Path=";
  protected static final String PREFS_JS_FILE_NAME      = "/prefs.js";

  protected static final String PREFS_JS_KEY_MAIL_ACCOUNTMANAGER_ACCOUNTS = "mail.accountmanager.accounts";
  protected static final String PREFS_JS_KEY_MAIL_ACCOUNT_X_IDENTITIES    = "mail.account.{0}.identities";
  
  protected static final String PREFS_JS_KEY_MAIL_IDENTITY_X_FULL_NAME    = "mail.identity.{0}.fullName";
  protected static final String PREFS_JS_KEY_MAIL_IDENTITY_X_USER_EMAIL   = "mail.identity.{0}.useremail";
  protected static final String PREFS_JS_KEY_MAIL_IDENTITY_X_SMTP_SERVER  = "mail.identity.{0}.smtpServer";
  
  protected static final String PREFS_JS_KEY_MAIL_SMTP_SERVER_X_HOST_NAME    = "mail.smtpserver.{0}.hostname";
  protected static final String PREFS_JS_KEY_MAIL_SMTP_SERVER_X_PORT         = "mail.smtpserver.{0}.port";
  protected static final String PREFS_JS_KEY_MAIL_SMTP_SERVER_X_USER_NAME    = "mail.smtpserver.{0}.username";
  protected static final String PREFS_JS_KEY_MAIL_SMTP_SERVER_X_AUTH_METHOD  = "mail.smtpserver.{0}.auth_method";
  protected static final String PREFS_JS_KEY_MAIL_SMTP_SERVER_X_TRY_SSL      = "mail.smtpserver.{0}.try_ssl";


  
  protected PrefsJSProperties _prefsJSProperties;
  
  
  public AbstractThunderbird()
  {
    super();
  }

  @Override
  public String GetApplicationName()
  {
    return APPLICATION_NAME;
  }
  
  @Override
  protected void PopulateAccounts()
  {
    try
    {
      File profilesIniFile = GetProfilesIniFile();
      if (profilesIniFile.exists() == true)
      {
        FileReader fileReader = new FileReader(profilesIniFile);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String readerLine;
        while ((readerLine = bufferedReader.readLine()) != null)
        {
          if (readerLine.startsWith(PROFILES_INI_PATH_KEY) == true)
          {
            int index = readerLine.indexOf("=") + 1;
            String path = readerLine.substring(index);
            String prefsJsFilePath = path + PREFS_JS_FILE_NAME;
            File prefsJsFile = new File(prefsJsFilePath);
            _prefsJSProperties = new PrefsJSProperties(prefsJsFile);
            String mailAccountManagerAccountsPropertyValue = (String)_prefsJSProperties.get(PREFS_JS_KEY_MAIL_ACCOUNTMANAGER_ACCOUNTS);
            String[] mailAccountManagerAccounts = mailAccountManagerAccountsPropertyValue.split(",");
            for (String mailAccountManagerAccount: mailAccountManagerAccounts)
            {
              String mailAccountIdentityKey = MessageFormat.format(PREFS_JS_KEY_MAIL_ACCOUNT_X_IDENTITIES, mailAccountManagerAccount);
              String mailAccountIdentity = (String)_prefsJSProperties.get(mailAccountIdentityKey);
              if (mailAccountIdentity != null)
              {
                ThunderbirdAccount thunderbirdAccount = new ThunderbirdAccount(this,mailAccountIdentity);
                try
                {
                  thunderbirdAccount.Initialize();
                  _accounts.add(thunderbirdAccount);
                }
                catch (NotAValidAccountException ex)
                {
                  // ignore this mailAccountIdentity
                  thunderbirdAccount = null;
                }
              }
            }
          }
        }
      }
    }
    catch (IOException ex)
    {
      // TODO Auto-generated catch block
      ex.printStackTrace();
    }
  }

  protected abstract File GetProfilesIniFile();
  
  
  protected static final String USER_PREF                                 = "user_pref";
  protected static final String USER_PREF_PARENTHESIS_DOUBLE_QUOTE        = USER_PREF + "(\"";
  protected static final int    USER_PREF_PARENTHESIS_DOUBLE_QUOTE_LENGTH = USER_PREF_PARENTHESIS_DOUBLE_QUOTE.length();
  
  
  /**
   * wraps prefs.js with keyword=value access
   */
  protected static class PrefsJSProperties
    extends HashMap<String,Object>
  {
    
    protected ArrayList<String>  _preamble;
    protected File               _file;

    
    public PrefsJSProperties(File file) 
      throws IOException 
    {
      super();
      
      _file = file;

      Load();
    }

    public ArrayList<String> GetPreamble()
    {
      return _preamble;
    }
    
    public void Load() 
      throws IOException 
    {
      FileReader fileReader = new FileReader(_file);
      BufferedReader bufferedReader = new BufferedReader(fileReader);

      _preamble = new ArrayList();

      String readerLine;
      while ( ((readerLine = bufferedReader.readLine()) != null) &&
              (readerLine.startsWith(USER_PREF) == false) )
      {
        _preamble.add(readerLine);
      }

      if (readerLine != null)
      {
        MutableString userPrefKey = new MutableString();
        MutableObject userPrefValue = new MutableObject();
        do
        {
          ParseUserPref(readerLine,userPrefKey,userPrefValue);
          put(userPrefKey._string,userPrefValue._object);
        }
        while ((readerLine = bufferedReader.readLine()) != null);
      }
      
    }

    /**
     * keys are always String
     * values can be either a String, a boolean, or an Integer
     * 
     * example entries: 
     *   user_pref("fee", "fum");
     *   user_pref("fie", 123);
     *   user_pref("foe", true);
     * 
     * @param userPref
     * @param userPrefKey
     * @param userPrefValue
     */
    protected void ParseUserPref(
      String        userPref,
      MutableString userPrefKey,
      MutableObject userPrefValue )
    {
      int startKeyIndex = userPref.indexOf(USER_PREF_PARENTHESIS_DOUBLE_QUOTE) + USER_PREF_PARENTHESIS_DOUBLE_QUOTE_LENGTH;
      int endKeyIndex = userPref.indexOf("\"", startKeyIndex);
      userPrefKey._string = userPref.substring(startKeyIndex,endKeyIndex);
      
      int startValueIndex = endKeyIndex + 3;   // skip the "\", "
      int endValueIndex = userPref.length() - 2;  // truncate the ");"
      String stringValue = userPref.substring(startValueIndex,endValueIndex);
      stringValue = stringValue.trim();
      if (stringValue.contains("\"") == true)
      {
        // value is a string
        userPrefValue._object = stringValue.replaceAll("\"", "");
      }
      else
      if ( (stringValue.equalsIgnoreCase("true") == true) ||
           (stringValue.equalsIgnoreCase("false") == true) )
      {
        userPrefValue._object = Boolean.valueOf(stringValue);
      }
      else  // it must be an integer
      {
        userPrefValue._object = Integer.valueOf(stringValue);
      }
    }

    protected static final String USER_PREF_FORMAT = "user_pref(\"{0}\", {1});";
    
    public void Save() 
      throws IOException
    {
//      StringWriter fileWriter = new StringWriter();
      
      FileWriter fileWriter = new FileWriter(_file);
      BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

      for (String line: _preamble)
      {
        bufferedWriter.write(line);
        bufferedWriter.newLine();
      }
      
      Set keySet = keySet();
      String[] keys = new String[keySet.size()];
      keySet.toArray(keys);
      Arrays.sort(keys);
      
      for (String key: keys)
      {
        Object value = get(key);
        if (value instanceof String)
        {
          value = "\"" + value + "\"";
        }
        String line = MessageFormat.format(USER_PREF_FORMAT, key, value.toString());
        bufferedWriter.write(line);
        bufferedWriter.newLine();
      }

      bufferedWriter.flush();
      bufferedWriter.close();
      
//      System.out.println(fileWriter);
    }
  }
  
}
