package com.World2One.Configurator.MailClients;

import java.io.IOException;
import java.text.MessageFormat;

import com.World2One.OS.AbstractProcess;
import com.World2One.OS.AbstractProcessManager;
import com.World2One.OS.PIDFileNotFoundException;
import com.World2One.Utility.Constants;

/**
 * <p>Title: ServiceController</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public class ServiceController
{

  
  
  private ServiceController()
  {
    super();
  }
  
  public static Process LaunchWildCard(String wildCardTop) 
    throws IOException
  {
    String jamesRunFile = MessageFormat.format(Constants.JAMES_RUN_FILE_PATH_FORMAT, wildCardTop);
    Process process = Runtime.getRuntime().exec(jamesRunFile);
    return process;
  }

  public static void ShutdownWildCard(String wildCardTop) 
    throws PIDFileNotFoundException, IOException
  {
    String jamesPIDFilePathName = MessageFormat.format(Constants.JAMES_PID_FILE_PATH_NAME_FORMAT, wildCardTop);
    AbstractProcess process = AbstractProcessManager.CreateProcess(jamesPIDFilePathName, "WildCard");
    process.Kill();
  }

  public static void ShutdownKeepAlive(String wildCardTop) 
    throws PIDFileNotFoundException, IOException
  {
    String keepAlivePIDFilePathName = MessageFormat.format(Constants.KEEP_ALIVE_PID_FILE_PATH_NAME_FORMAT, wildCardTop);
    AbstractProcess process = AbstractProcessManager.CreateProcess(keepAlivePIDFilePathName, "Keep Alive");
    process.Kill();
  }
  
}
