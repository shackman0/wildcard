package com.World2One.Configurator.MailClients;

import java.util.HashSet;

/**
 * <p>Title: AbstractApplicationFactory</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public abstract class AbstractApplicationFactory
{
  
  
  protected AbstractApplicationFactory()
  {
    super();
  }
  
  
  public abstract HashSet<AbstractApplication> GetInstalledApplications();
 

}
