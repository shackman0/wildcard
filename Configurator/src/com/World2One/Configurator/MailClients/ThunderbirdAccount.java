package com.World2One.Configurator.MailClients;

import java.io.IOException;
import java.text.MessageFormat;

import javax.swing.JOptionPane;

import com.World2One.Configurator.AbstractConfigurator;
import com.World2One.Configurator.MailClients.Win32.Thunderbird;
import com.World2One.SMTP.SMTPHost.SecureConnection;


/**
 * <p>Title: ThunderbirdAccount</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public class ThunderbirdAccount
  extends AbstractAccount
{
  
  /**
   * the mail account identity for this account in the Thunderbird prefs.js file
   */
  protected String                 _mailAccountIdentity;
  
  
  
  public ThunderbirdAccount(
    AbstractThunderbird application,
    String              mailAccountIdentity )
  {
    super(application);
    
    _mailAccountIdentity = mailAccountIdentity;
  }

  @Override
  protected void GetSMTPSettings()
  {
    Thunderbird application = (Thunderbird)_application;
    
    String mailIdentityFullNameKey = MessageFormat.format(AbstractThunderbird.PREFS_JS_KEY_MAIL_IDENTITY_X_FULL_NAME, _mailAccountIdentity);
    String mailIdentityFullName = (String)application._prefsJSProperties.get(mailIdentityFullNameKey);
    _accountName = mailIdentityFullName;
    
    if (mailIdentityFullName == null)
    {
      // _mailAccountIdentity is to an "alias" account and not a true account
      throw new NotAValidAccountException(_mailAccountIdentity);
    }
    
    String mailIdentityUserEmailKey = MessageFormat.format(AbstractThunderbird.PREFS_JS_KEY_MAIL_IDENTITY_X_USER_EMAIL, _mailAccountIdentity);
    String mailIdentityUserEmail = (String)application._prefsJSProperties.get(mailIdentityUserEmailKey);
    _emailAddress = mailIdentityUserEmail;
    
    String mailIdentitySMTPServerKey = MessageFormat.format(AbstractThunderbird.PREFS_JS_KEY_MAIL_IDENTITY_X_SMTP_SERVER, _mailAccountIdentity);
    String mailIdentitySMTPServer = (String)application._prefsJSProperties.get(mailIdentitySMTPServerKey);
    
    String mailSMTPServerHostNameKey = MessageFormat.format(AbstractThunderbird.PREFS_JS_KEY_MAIL_SMTP_SERVER_X_HOST_NAME, mailIdentitySMTPServer);
    String mailSMTPServerHostName = (String)application._prefsJSProperties.get(mailSMTPServerHostNameKey);
    _hostAddress = mailSMTPServerHostName;
    
    String mailSMTPServerPortKey = MessageFormat.format(AbstractThunderbird.PREFS_JS_KEY_MAIL_SMTP_SERVER_X_PORT, mailIdentitySMTPServer);
    Integer mailSMTPServerPort = (Integer)application._prefsJSProperties.get(mailSMTPServerPortKey);
    if (mailSMTPServerPort == null)
    {
      mailSMTPServerPort = 25;
    }
    _port = mailSMTPServerPort;
    
    String mailSMTPServerUserNameKey = MessageFormat.format(AbstractThunderbird.PREFS_JS_KEY_MAIL_SMTP_SERVER_X_USER_NAME, mailIdentitySMTPServer);
    String mailSMTPServerUserName = (String)application._prefsJSProperties.get(mailSMTPServerUserNameKey);
    _userName = mailSMTPServerUserName;

    _password = null;

    String mailSMTPServerTrySSLKey = MessageFormat.format(AbstractThunderbird.PREFS_JS_KEY_MAIL_SMTP_SERVER_X_TRY_SSL, mailIdentitySMTPServer);
    Integer mailSMTPServerTrySSL = (Integer)application._prefsJSProperties.get(mailSMTPServerTrySSLKey);
    if (mailSMTPServerTrySSL == null)
    {
      mailSMTPServerTrySSL = 0;
    }
    SecureConnection connectionType;
    if (mailSMTPServerTrySSL == 0)
    {
      connectionType = SecureConnection.None;
    }
    else
    {
      connectionType = SecureConnection.SSL;
    }
    _connectionType = connectionType;
  }




  @Override
  public void SetSMTPSettings(
    String           accountName, 
    String           emailAddress, 
    String           hostAddress, 
    int              port, 
    String           userName, 
    String           password, 
    SecureConnection connectionType )
  {
    AbstractThunderbird thunderbird = (AbstractThunderbird)_application;
    
    String mailIdentityUserEmailKey = MessageFormat.format(AbstractThunderbird.PREFS_JS_KEY_MAIL_IDENTITY_X_USER_EMAIL, _mailAccountIdentity);
    thunderbird._prefsJSProperties.put(mailIdentityUserEmailKey,emailAddress);
    
    String mailIdentitySMTPServerKey = MessageFormat.format(AbstractThunderbird.PREFS_JS_KEY_MAIL_IDENTITY_X_SMTP_SERVER, _mailAccountIdentity);
    String mailIdentitySMTPServer = (String)thunderbird._prefsJSProperties.get(mailIdentitySMTPServerKey);
        
    String mailSMTPServerHostNameKey = MessageFormat.format(AbstractThunderbird.PREFS_JS_KEY_MAIL_SMTP_SERVER_X_HOST_NAME, mailIdentitySMTPServer);
    thunderbird._prefsJSProperties.put(mailSMTPServerHostNameKey,hostAddress);
    
    String mailSMTPServerPortKey = MessageFormat.format(AbstractThunderbird.PREFS_JS_KEY_MAIL_SMTP_SERVER_X_PORT, mailIdentitySMTPServer);
    thunderbird._prefsJSProperties.put(mailSMTPServerPortKey,port);
    
    String mailSMTPServerUserNameKey = MessageFormat.format(AbstractThunderbird.PREFS_JS_KEY_MAIL_SMTP_SERVER_X_USER_NAME, mailIdentitySMTPServer);
    thunderbird._prefsJSProperties.put(mailSMTPServerUserNameKey,userName);
    
    String mailSMTPServerTrySSLKey = MessageFormat.format(AbstractThunderbird.PREFS_JS_KEY_MAIL_SMTP_SERVER_X_TRY_SSL, mailIdentitySMTPServer);
    thunderbird._prefsJSProperties.put(mailSMTPServerTrySSLKey,connectionType.ordinal());
    
    try
    {
      thunderbird._prefsJSProperties.Save();
    }
    catch (IOException ex)
    {
      JOptionPane.showMessageDialog(null,ex.getLocalizedMessage(),"Error during save of " + thunderbird._prefsJSProperties._file.getName(),JOptionPane.ERROR_MESSAGE);
      if (AbstractConfigurator.__debug == true)
      {
        ex.printStackTrace();
      }
    }
  }

}
