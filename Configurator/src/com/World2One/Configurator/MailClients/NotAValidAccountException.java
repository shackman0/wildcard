package com.World2One.Configurator.MailClients;

/**
 * <p>Title: NotAValidAccountException</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public class NotAValidAccountException
  extends RuntimeException
{
  
  public NotAValidAccountException(String message)
  {
    super(message);
  }

  
}
