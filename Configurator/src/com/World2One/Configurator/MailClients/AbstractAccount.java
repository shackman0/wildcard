package com.World2One.Configurator.MailClients;

import com.World2One.SMTP.MissingParameterException;
import com.World2One.SMTP.SMTPHost;
import com.World2One.SMTP.SMTPHost.SMTPHostsOverridesKey;
import com.World2One.SMTP.SMTPHost.SecureConnection;

/**
 * <p>Title: Account</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public abstract class AbstractAccount
{

  protected static final String LOCAL_WILDCARD_HOST_NAME = "WildCard.World2One.Com";
  protected static final int    LOCAL_WILDCARD_PORT      = 25;
  
  protected AbstractApplication    _application;

  protected String            _accountName;
  protected String            _emailAddress;
  protected String            _hostAddress;
  protected String            _userName;
  protected String            _password;
  protected SecureConnection  _connectionType;
  protected int               _port;

  
  
  protected AbstractAccount(AbstractApplication application)
  {
    super();
    
    _application = application;
  }

  public void Initialize()
  {
    GetSMTPSettings();
  }
  
  public void SetSMTPSettings2InitialValues()
  {
    SetSMTPSettings(_accountName,_emailAddress,_hostAddress,_port,_userName,_password,_connectionType);
  }

  public void SetSMTPSettings2ReferenceLocalWildCardServer()
  {
    SetSMTPSettings(_accountName,_emailAddress,LOCAL_WILDCARD_HOST_NAME,LOCAL_WILDCARD_PORT,_userName,_password,SecureConnection.None);
  }

  protected abstract void GetSMTPSettings();
  
  public void SetSMTPSettings(SMTPHost smtpHost) 
    throws MissingParameterException
  {
    SetSMTPSettings(smtpHost.GetUser(),smtpHost.GetSenderEmail(),smtpHost.GetHostAddress(),smtpHost.GetPort(),smtpHost.GetUser(),smtpHost.GetPassword(),smtpHost.GetSecureConnection());
  }
  
  public abstract void SetSMTPSettings(
    String            accountName,
    String            emailAddress,
    String            hostAddress,
    int               port,
    String            userName,
    String            password,
    SecureConnection  connectionType );
  
  
  
  public AbstractApplication GetApplication()
  {
    return _application;
  }


  public void SetApplication(AbstractApplication application)
  {
    _application = application;
  }


  public String GetAccountName()
  {
    return _accountName;
  }


  public void SetAccountName(String accountName)
  {
    _accountName = accountName;
  }


  public String GetEmailAddress()
  {
    return _emailAddress;
  }


  public void SetEmailAddress(String emailAddress)
  {
    _emailAddress = emailAddress;
  }


  public String GetHostAddress()
  {
    return _hostAddress;
  }


  public void SetHostAddress(String hostAddress)
  {
    _hostAddress = hostAddress;
  }


  public String GetUserName()
  {
    return _userName;
  }


  public void SetUserName(String userName)
  {
    _userName = userName;
  }


  public String GetPassword()
  {
    return _password;
  }


  public void SetPassword(String password)
  {
    _password = password;
  }


  public SecureConnection GetConnectionType()
  {
    return _connectionType;
  }


  public void SetConnectionType(SecureConnection connectionType)
  {
    _connectionType = connectionType;
  }


  public int GetPort()
  {
    return _port;
  }


  public void SetPort(int port)
  {
    _port = port;
  }

  public SMTPHostsOverridesKey GetSMTPHostsOverridesKey()
  {
    SMTPHostsOverridesKey smtpHostsOverridesKey = new SMTPHostsOverridesKey(_application.GetApplicationName(),_emailAddress);
    
    return smtpHostsOverridesKey;
  }
  
  @Override
  public String toString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    
    stringBuilder.append("Account Name=\"");
    stringBuilder.append(_accountName);
    stringBuilder.append("\" ");
    
    stringBuilder.append("Email Address=\"");
    stringBuilder.append(_emailAddress);
    stringBuilder.append("\" ");
    
    stringBuilder.append("Host Address=\"");
    stringBuilder.append(_hostAddress);
    stringBuilder.append("\" ");
    
    stringBuilder.append("Port=\"");
    stringBuilder.append(_port);
    stringBuilder.append("\" ");
    
    stringBuilder.append("User Name=\"");
    stringBuilder.append(_userName);
    stringBuilder.append("\" ");
    
    stringBuilder.append("Password=\"");
    stringBuilder.append(_password);
    stringBuilder.append("\" ");
    
    stringBuilder.append("Connection Type=\"");
    stringBuilder.append(_connectionType.name());
    stringBuilder.append("\" ");
    
    return stringBuilder.toString();
  }
  
}
