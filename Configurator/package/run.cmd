@echo off

cd

rem This script launches the Configurator application.
rem You MUST modify the values in the User Modifiable Section below when you install the
rem Configurator application before this script will work.


rem clear all the variables we use in this script so we don't "inherit" any from the windows environment.
rem do not modify this list.

set JAVA2SE=
set UPDATE_SERVER_ADDR=
set UPDATE_SERVER_PORT=
set RUN_JAVA=
set JVM_PARMS=
set JAR=
set CLIENT_PROXY_HOST=
set WILDCARD_TOP=
set DEBUG=
set JAVA_DEBUG=


rem ********** BEGIN USER MODIFIABLE SECTION **********
rem ********** BEGIN USER MODIFIABLE SECTION **********
rem ********** BEGIN USER MODIFIABLE SECTION **********


rem **********
rem uncomment DEBUG to cause debugging output to be written to std out
set DEBUG=-debug


rem **********
rem if you do not want to rely upon the PATH environment variable, set the JAVA2SE variable to point to the 
rem Java 2 SE runtime bin directory. Use an absolute path. be sure to end with a backslash "\"

rem set JAVA2SE=C:\Program Files\Java\jre1.6.0_07\bin\


rem **********
rem Set the UPDATE_SERVER_ADDR variable to the hostname or the IP address at which the Update Server is bound and listening.
rem Put the value in between the double quotes.
rem this is a required variable

set UPDATE_SERVER_ADDR="updater.world2one.com"


rem **********
rem Set the UPDATE_SERVER_PORT variable to the port at which the Update Server is bound and listening.
rem Put the value in between the double quotes.
rem this is a required variable

set UPDATE_SERVER_PORT="40100"


rem if the client is behind a firewall, uncomment "set CLIENT_PROXY_HOST" following and replace the ???'s with the host name of the firewall.

rem set CLIENT_PROXY_HOST=-Dhttp.proxyHost=???


rem set this to the top of the WildCard install directory if the configurator has been moved outside of the wildcard installation directory
rem set WILDCARD_TOP="-wildcardTop=../"


rem ********** END USER MODIFIABLE SECTION **********
rem ********** END USER MODIFIABLE SECTION **********
rem ********** END USER MODIFIABLE SECTION **********



rem ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********
rem ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********
rem ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********


set RUN_JAVA="%JAVA2SE%java.exe"

set JAR=./Runtime/lib/W2O_Configurator_Win32.jar


rem uncomment to enable remote debugging
rem set JAVA_DEBUG=-Xdebug -Xrunjdwp:transport=dt_socket,address=8000,server=y,suspend=y


rem set the JVM_PARMS to commands you want passed to the JVM
rem put the parms in between the double quotes.

set JVM_PARMS=-Djava.security.policy=Configurator.policy -Djava.library.path=./Runtime/lib/; %CLIENT_PROXY_HOST% %JAVA_DEBUG%

rem set the CLASSPATH according to whether we are running this in production (.jar) or development (./bin)
rem set CLASSPATH=./bin;../Utility/bin;../Updater/bin;
set CLASSPATH=%JAR%;./Runtime/lib/org.eclipse.swt.win32.jar;./Runtime/lib/org.sf.feeling.swt.win32.extension.jar;


rem launch the Configurator
%RUN_JAVA% -classpath %CLASSPATH% %JVM_PARMS% com.World2One.Configurator.MailClients.Win32.Configurator -doNotUpdate -rmiBindAddr %UPDATE_SERVER_ADDR% -rmiBindPort %UPDATE_SERVER_PORT% %WILDCARD_TOP% %DEBUG% %1 %2 %3


rem check for an update. if we have an update: update the configurator and restart it.

set CONFIGURATOR_UPDATE_FILE=.\Runtime\ConfiguratorUpdate.zip

if exist %CONFIGURATOR_UPDATE_FILE% (
rem unzip the configurator update file
call .\Runtime\bin\UnZip.cmd %JAR% %CONFIGURATOR_UPDATE_FILE% .
  
rem delete the configurator update file so we don't think we have to process this same update the next time through here
del %CONFIGURATOR_UPDATE_FILE%

rem re-invoke the Configurator. this is a one-way call.
.\run.cmd %1 %2 %3
)

rem pause for debugging purposes
rem pause
