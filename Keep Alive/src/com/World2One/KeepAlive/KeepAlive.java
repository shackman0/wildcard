package com.World2One.KeepAlive;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.TimerTask;

import com.World2One.OS.AbstractProcess;
import com.World2One.OS.AbstractProcessManager;
import com.World2One.OS.PIDFileNotFoundException;
import com.World2One.Utility.Constants;
import com.World2One.Utility.MutableInteger;
import com.World2One.Utility.ToolBox;



/**
 * <p>Title: KeepAlive</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public class KeepAlive
{

  protected java.util.Timer    _timer;
  protected KeepAliveTimerTask _keepAliveTimerTask;
  protected String             _wildcardTop;
  protected long               _queryPeriodMilliseconds;
  protected long               _initialDelayMilliseconds;
  
  protected String _runJamesCommandLine;
  protected String _jamesPIDFilePathName;

  protected static final String PARM_KEYWORD_WILDCARD_TOP = Constants.PARM_KEYWORD_PREFIX + "wildcardTop";
  protected static final String DEFAULT_WILDCARD_TOP = "../";

  protected static final String PARM_KEYWORD_INITIAL_DELAY   = Constants.PARM_KEYWORD_PREFIX + "initialDelay";
  protected static final String DEFAULT_INITIAL_DELAY_SECONDS = "180";

  protected static final String PARM_KEYWORD_QUERY_PERIOD   = Constants.PARM_KEYWORD_PREFIX + "queryPeriod";
  protected static final String DEFAULT_QUERY_PERIOD_SECONDS = "30";
  

  
  public KeepAlive()
  {
    super();
  }

  public void Initialize(String[] args) 
  {
    // process command-line parms
    HashMap<String,String> parms = ToolBox.ProcessParms(Constants.PARM_KEYWORD_PREFIX, true, args);

    _wildcardTop = parms.get(PARM_KEYWORD_WILDCARD_TOP);
    if (_wildcardTop == null)
    {
      _wildcardTop = DEFAULT_WILDCARD_TOP;
    }

    String initialDelaySeconds = parms.get(PARM_KEYWORD_INITIAL_DELAY);
    if (initialDelaySeconds == null)
    {
      initialDelaySeconds = DEFAULT_INITIAL_DELAY_SECONDS;
    }
    _initialDelayMilliseconds = Long.valueOf(initialDelaySeconds.trim()) * 1000L;

    String queryPeriodSeconds = parms.get(PARM_KEYWORD_QUERY_PERIOD);
    if (queryPeriodSeconds == null)
    {
      queryPeriodSeconds = DEFAULT_QUERY_PERIOD_SECONDS;
    }
    _queryPeriodMilliseconds = Long.valueOf(queryPeriodSeconds.trim()) * 1000L;

    String keepAlivePIDFilePathName = MessageFormat.format(com.World2One.Utility.Constants.KEEP_ALIVE_PID_FILE_PATH_NAME_FORMAT, _wildcardTop);

    // is KeepAlive already running?
    try
    {
      MutableInteger processID = new MutableInteger();
      if (AbstractProcessManager.IsRunning(keepAlivePIDFilePathName,processID) == true)
      {
        throw new RuntimeException("KeepAlive is already running with PID " + processID._integer);
      }
    }
    catch (IOException ex)
    {
      // do nothing. probably couldn't find the pid file.
    }

    // do we need to update?
    try
    {
      UpdateClient updateClient = new UpdateClient(args);
      updateClient.UpdateIfNeeded();
      updateClient.Disconnect();
    }
    catch (Exception ex)
    {
      // if we fail, just keep on going.
      ex.printStackTrace();
    }
    
    // write my PID file
    try
    {
      AbstractProcessManager.WriteMyPIDToPIDFile(keepAlivePIDFilePathName);
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
      System.exit(-1);
    }

    // continue initialization ...
    _timer = new java.util.Timer();
    
    _runJamesCommandLine = MessageFormat.format(Constants.JAMES_RUN_FILE_PATH_FORMAT, _wildcardTop);
    _jamesPIDFilePathName = MessageFormat.format(Constants.JAMES_PID_FILE_PATH_NAME_FORMAT, _wildcardTop);
  }

  public void Run()
  {
    _keepAliveTimerTask = new KeepAliveTimerTask();
    _timer.scheduleAtFixedRate(_keepAliveTimerTask, _initialDelayMilliseconds, _queryPeriodMilliseconds);
  }

  protected class KeepAliveTimerTask
    extends TimerTask
  {
    
    public KeepAliveTimerTask()
    {
      super();
    }

    @Override
    public void run()
    {
      AbstractProcess process = null;
      try
      {
        process = AbstractProcessManager.CreateProcess(_jamesPIDFilePathName,"wildcard");
      }
      catch (PIDFileNotFoundException ex)
      {
        // do nothing
      }
      catch (IOException ex)
      {
        ex.printStackTrace();
        System.exit(-1);
      }
      
      if ( (process != null) &&
           (process.IsRunning() == true) )
      {
        System.out.println(ToolBox.GetDTS() + ": WildCard is running as PID " + process.GetProcessID());
      }
      else
      {
        try
        {
          Runtime.getRuntime().exec(_runJamesCommandLine);
          System.out.println(ToolBox.GetDTS() + ": WildCard launched");
        }
        catch (IOException ex)
        {
          // do nothing?
          ex.printStackTrace();
        }
      }
    }
    
  }

  public void Stop()
  {
    _timer.cancel();
  }
  
  
  
  protected static KeepAlive __keepAlive;
  
  public static void main(String... args)
    throws Exception
  {
    __keepAlive = new KeepAlive();
    __keepAlive.Initialize(args);
    __keepAlive.Run();
  }

  public static void stop()
  {
    __keepAlive.Stop();
  }
  
}
