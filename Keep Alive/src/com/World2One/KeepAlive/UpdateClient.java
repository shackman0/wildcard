package com.World2One.KeepAlive;

import java.io.IOException;
import java.rmi.RemoteException;

import com.World2One.Updater.UpdateServer;
import com.World2One.Utility.Constants;

/**
 * <p>Title: UpdateClient</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: World2One.com</p>
 * @author Floyd Shackelford
 */
public class UpdateClient
  extends com.World2One.Updater.AbstractUpdateClient
{
  
  protected static final String KEEP_ALIVE_UPDATE_ZIP_FILE_PATH = "./Runtime/" + Constants.KEEP_ALIVE_UPDATE_ZIP_FILE_NAME;


  public UpdateClient(String... args)
  {
    super(args);
  }
  
  public UpdateClient(String rmiBindAddr, int rmiBindPort)
  {
    super(rmiBindAddr,rmiBindPort);
  }

  @Override
  protected boolean IsUpdateAvailable(UpdateServer updateServer)
    throws RemoteException
  {
    boolean keepAliveUpdateIsAvailable = updateServer.IsKeepAliveUpdateAvailable(Constants.KEEP_ALIVE_VERSION);
    System.out.println(getClass().getSimpleName() + ".IsUpdateAvailable(): " + keepAliveUpdateIsAvailable);
    
    return keepAliveUpdateIsAvailable;
  }


  @Override
  protected void ReadAndWriteUpdateZipFileAndExit(UpdateServer updateServer)
    throws RemoteException, IOException
  {
    ReadAndWriteUpdateZipFileAndExit(updateServer, KEEP_ALIVE_UPDATE_ZIP_FILE_PATH);
  }

  @Override
  protected byte[] GetUpdateZipFileBytes(UpdateServer updateServer, int length) 
    throws RemoteException
  {
    byte[] keepAliveUpdateZipFileBytes = updateServer.GetKeepAliveUpdateZipFileBytes(length);
    
    return keepAliveUpdateZipFileBytes;
  }

}
