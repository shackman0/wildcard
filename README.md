this software belongs to Floyd Shackelford. 

you are allowed to scrutinize this software for the purposes of evaluating my skills as a software engineer. 
any other use requires advance permission.

this is a spec (speculative) product i developed for a friend of mine that ran a company known
as World2One (which is no longer in business).

basically it:

1. intercepts all outgoing emails and appends the user's
"business card", as defined on the W2O iCard server, to the bottom of the email
below the sender's signature before forwarding it on to the user's smtp server.
2. then archives the email on the W2O email archive server (sort of like google does with your gmail)

start with the requirements in the /docs/ folder.